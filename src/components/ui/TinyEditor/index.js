import React, { Component } from 'react'
import { Editor } from '@tinymce/tinymce-react'

class TinyEditor extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedTab: 0
    }
  }

  setBriefTab = index => {
    if (index !== this.state.selectedTab) {
      this.setState({
        selectedTab: index
      })
    }
  }

  handleEditorChange = content => {
    const { handleChange, isEdit } = this.props
    if(isEdit) {
      handleChange(content)
    }
  }

  instanceCb(editor) {
    editor.getBody() && editor.getBody().setAttribute('contenteditable',false)
    editor.on('click', function (e) {
      if (e.target.nodeName === 'SPAN') {
        const rng = document.createRange()
        rng.selectNode(e.target)
        const sel = editor.contentDocument.getSelection()
        sel.removeAllRanges()
        sel.addRange(rng)
      }
    })
  }

  render() {
    const { data } = this.props
    const initOptions = {
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime nonbreaking save table contextmenu directionality',
          'template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],        
        branding: false,
        height: 500,  
        menubar:false,
        statusbar: false, 
        toolbar: false,
        removed_menuitems: 'undo, redo',
        selection_toolbar: false,
        readonly : 1, 
        init_instance_callback: this.instanceCb
      }

    return (
      <div className="custom-size-page">
        <Editor
          id="editor"
          onEditorChange={this.handleEditorChange}
          init={initOptions}
          value={data.value}
        />
      </div>
    )
  }
}

export default TinyEditor