import React, { Component } from 'react'

class Radiobutton extends Component {

  getValue = () => {
    const {
      checked
    } = this.props 

    return checked
  }

  render() {
    const {
      name,
      checked,
      required,
      onCheck,
      onChange
    } = this.props 
    
    return (
      <div className="radio pl-20">
        <div className="choice">
          <span className={checked ? "checked": ""}>
            <input 
              type="radio" 
              onClick={e => {
                e.stopPropagation()          
                onCheck && onCheck(name)
              }}
              checked={checked}
              onChange={e => onChange && onChange(e)}         
              required={required === undefined ? false : true}
            />
          </span>
        </div>
      </div>
    )
  }
}

export default Radiobutton
