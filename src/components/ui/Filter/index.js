import React from 'react'
import propTypes from 'prop-types'

const Filter = (props) => {
  const {
    setRef,
    searchItem,
    onChangeHandler,
    defaultValue,
    resetFilters,
    customClasses
  } = props

  const handleReset = () => {
    inputValue.current.value = ''
    resetFilters()
  }

  const inputValue = setRef || React.createRef()
  return (
    <div className={`dataTables_filter ${customClasses}`}>
      <label>
        <span>Search {searchItem}:</span>
        <input
          ref={inputValue}
          type="search"
          placeholder="Type to filter..."
          aria-controls="DataTables_Table_1"
          onChange={e => onChangeHandler(e)}
          defaultValue={defaultValue}
          autoFocus={true}
        />
      </label>
      <span>
        <a
          className="ml-20"
          onClick={handleReset}>
          <i className="icon-reset mr-5"></i>
            Reset Filters
        </a>
      </span>
    </div>

  )
}

Filter.defaultProps = {
  resetFilters: () => null
}

Filter.propTypes = {
  searchItem: propTypes.string.isRequired,
  onChangeHandler: propTypes.func.isRequired,
  defaultValue: propTypes.string,
  resetFilters: propTypes.func.isRequired,
  customClasses: propTypes.string
}

export default Filter