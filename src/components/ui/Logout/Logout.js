import React, { Component } from 'react'

class Logout extends Component {

  render() {

    let { authActions } = this.props

    return (
      <a onClick={authActions.signOut}>
        <i className="icon-switch2"></i>
        Logout
      </a>
    )

  }

}

export default Logout
