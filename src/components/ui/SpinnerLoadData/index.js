import React from 'react'
import classNames from 'classnames'

import './SpinnerLoadData.css'

class SpinnerLoadData extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShow: false
    }
  }

  componentDidMount() {
    const { delay } = this.props
    this.timer = setTimeout(() => {
      this.show()
    }, delay)
  }

  show = () => {
    this.setState({ isShow: true })
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  render() {
    const { isAbsolute } = this.props
    const classNamesSpinner = classNames(
      'theme_corners theme_corners_with_text', 
      {
        'spiner-absolute': isAbsolute,
        'spiner-relative': !isAbsolute
      }
    )
    return this.state.isShow ? (
      <div className={classNamesSpinner}>
        <div className="pace_progress" data-progress-text="60%" data-progress="60">
        </div>
        <div className="pace_activity" style={{ backgroundColor: '#777' }}>
        </div>
      </div>
    ) : null
  }
}

SpinnerLoadData.defaultProps = {
  isAbsolute: true,
  delay: 2000,
}
export default SpinnerLoadData
