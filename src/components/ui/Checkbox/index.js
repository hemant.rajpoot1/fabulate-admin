import React, { Component } from 'react'

import $ from 'utils/jquery'

class Checkbox extends Component {

  assign() {

    $(this.refs.checkbox).uniform()

  }

  destroy() {

    $.uniform.restore(this.refs.checkbox)

  }

  componentWillUnmount() {

    this.destroy()

  }

  componentDidMount() {

    this.assign()

  }

  componentWillUpdate() {

    // this.destroy()

  }

  componentDidUpdate() {

    this.assign()

  }

  // shouldComponentUpdate() {
  //   const check = $(this.refs.checkbox)[0].offsetParent.className === 'checked' 
  //   return this.props.checked !== check 
  // }

  getValue = () => {
    let check = false

    if($(this.refs.checkbox)
        && $(this.refs.checkbox)[0]
        && $(this.refs.checkbox)[0].offsetParent
    ) {
      check = $(this.refs.checkbox)[0].offsetParent.className === 'checked' 
    }
    return check
  }

  render() {
    const {
      name,
      checked,
      required,
      onCheck,
      onChange
    } = this.props 
    
    return (
      <input 
        type="checkbox" 
        ref="checkbox" 
        onClick={e => {
          e.stopPropagation()          
          onCheck && onCheck(name)
        }}
        checked={checked}
        onChange={e => onChange && onChange(e)}         
        required={required === undefined ? false : true}
      />
    )
  }
}

export default Checkbox
