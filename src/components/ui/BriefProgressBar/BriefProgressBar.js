import React, { Component } from 'react'
import _ from 'lodash'

class BriefProgressBar extends Component {
  renderItem = item => {
    const { changePage, current, stepNames } = this.props
    const status = item <= current 
      ? (item === current)
        ? 'current'
        : 'done'
      : 'disable'
       
    const stepName = _.size(stepNames) && stepNames[ item - 1] 
      ? stepNames[ item - 1]
      : undefined 
    return (
      <li 
        key={`progress_item_${item}`} 
        className={`${status} first`}
      >
        <a id="steps-uid-0-t-0">
          <span className="current-info"></span>
          <span className="number" onClick={() => changePage && changePage(item)}>
            {item}
          </span>
          {stepName && (
            <div>{stepName}</div>
          )}
        </a>
      </li>
    )
  }
  
  renderItems = () => {
    const { itemCount } = this.props

    const items = []
  
    for (let i = 1; i <= itemCount; i++) {
      items.push(this.renderItem(i))
    }

    return items
  }

  render() {
    return (
      <div className="progress_bar">
        <div className="wizard">
          <div className="steps clearfix">
            <ul>
              {this.renderItems()}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}
export default BriefProgressBar