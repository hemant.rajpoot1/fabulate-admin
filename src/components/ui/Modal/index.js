import React, { PureComponent } from 'react'

import $ from 'utils/jquery'

class Modal extends PureComponent {

  static close() {

    $('.modal').modal('hide')

  }

  static showModal(modalId) {

    $('#' + modalId).modal({
      backdrop: 'static',
      keyboard: false,
    })

  }

  render() {
    return (
      <div id={this.props.idModal} className="modal fade" tabIndex="-1">
        <div className="modal-dialog modal-lg">
          { this.props.children }
        </div>
      </div>
    )
  }
}

export default Modal
