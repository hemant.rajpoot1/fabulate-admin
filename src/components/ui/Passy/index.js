import React, { Component } from 'react'

import $ from 'utils/jquery'

class Passy extends Component {

  assign() {

    // Requirements a inserted password should meet
    $.passy.requirements = {

      // Character types a password should contain
      characters: [
        $.passy.character.DIGIT,
        $.passy.character.LOWERCASE,
        $.passy.character.UPPERCASE,
        $.passy.character.PUNCTUATION
      ],

      // A minimum and maximum length
      length: {
        min: 3,
        max: Infinity
      }

    }

    // Thresholds are numeric values representing
    // the estimated amount of days for a single computer
    // to brute force the password, assuming it processes
    // about 1,000,000 passwords per second

    $.passy.threshold = {
      medium: 365,
      high: Math.pow( 365, 2 ),
      extreme: Math.pow( 365, 5 )
    }

    // Strength meter
    const feedback = [
      {color: '#D55757', text: 'Weak', textColor: '#fff'},
      {color: '#EB7F5E', text: 'Normal', textColor: '#fff'},
      {color: '#3BA4CE', text: 'Good', textColor: '#fff'},
      {color: '#40B381', text: 'Strong', textColor: '#fff'},
    ]

    //
    // Setup strength meter
    //

    // Absolute positioned label
    $(this.refs.input).passy((strength) => {

      $(this.refs.label).text(feedback[strength].text)

      $(this.refs.label).css(
        'background-color',
        feedback[strength].color
      ).css(
        'color',
        feedback[strength].textColor
      )

    })

  }

  generatePassword() {

    $(this.refs.input).passy( 'generate', 12 )

  }

  togglePassword() {

    this.refs.input.type = this.refs.input.type === 'password' ?
      'text' :
      'password'

    this.refs.eye.classList.toggle('icon-eye-blocked')

  }

  destroy() {

    // no destroy method =(

  }

  componentWillUnmount() {

    this.destroy()

  }

  componentDidMount() {

    this.assign()

  }

  componentWillUpdate() {

    this.destroy()

  }

  componentDidUpdate() {

    this.assign()

  }

  getValue () {
    return this.refs.input.value
  }

  render() {
    const {
      children,
      placeholder,
      onChange,
      onBlur, 
      nameInput
    } = this.props
    return (
      <div className="label-indicator-absolute input-group">
        {children}
        <input
          ref="input"
          type="password"
          className="form-control"
          placeholder={placeholder}
          onBlur={e => onBlur && onBlur(e)}
          onChange={e => onChange && onChange(e)}
          name={nameInput}
        />
        <div
          className="input-group-addon"
        >
          <span
            title="Generate password"
            onClick={e => {
              this.generatePassword()
              onChange(e, this.refs.input.value)
            }}
            ref="label"
            className="label cursor-pointer"
          >
            <i className="icon-dice text-default"></i>
          </span>
        </div>
        <span
          onClick={() => this.togglePassword()}
          title="Toggle password"
          className="input-group-addon cursor-pointer"
        >
          <i ref="eye" className="icon-eye"></i>
        </span>
      </div>
    )
  }
}

export default Passy
