/* eslint-disable jsx-a11y/alt-text */
import React, { Component, Fragment } from 'react'

import classNames from 'classnames'

import './file.css'

class FileComponent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      filePreview: this.props.file.preview || null
    }
  }

  componentDidMount() {
    const { file, uploadFile } = this.props
    const addFilePreview = (filePreview) => {
      this.setState({ filePreview })
    }

    if (file instanceof Blob) {
      const fileReader = new FileReader()
      fileReader.onload = function (event) {
        if (this.result) {
          uploadFile(this.result, file.preview)
          addFilePreview(file.preview)
        }
      }

      fileReader.readAsDataURL(file)
    }
  }

  render() {
    const { file, removeAttach, isImagesShow, isRemoveShow } = this.props
    const { filePreview } = this.state
    return (
      <div onClick={e => e.stopPropagation()} className={`file-image-wrapper token${file.isError ? ' bg-danger' : ''}`}>
        {!isImagesShow && <span className="token-label" style={{ maxWidth: 550 }}>{file.name || file.mediaName}</span>}
        {isImagesShow && filePreview && <img className='ui-file-img-preview' src={filePreview} />}
        {file.isFetching ? (
          <a className="icon-spinner2 spinner spinner-position"><Fragment> </Fragment></a>
        ) : isRemoveShow
          ? (
            <a className={classNames('close ', { 'ui-file-close-images': isImagesShow })} onClick={() => removeAttach(file)}>×</a>
          ) : null}
      </div>
    )
  }
}

FileComponent.defaultProps = {
  isImagesShow: false,
  isRemoveShow: true,
}

export default FileComponent
