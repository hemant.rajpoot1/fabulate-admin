import React, { PureComponent } from 'react'
import _ from 'lodash'
import './select.css'

class Select extends PureComponent {
  render() {
    const {
      items,
      value,
      placeholder,
      isSearch,
      cantReset,
      onSelect,
      onSearch,
      keyValue,
      readOnly,
      isBlock,
      isInput,
      inputChange,
      inputName,
      selectFieldName
    } = this.props
    return (
      <div className="select-container">
        <div
          className="full-width"
          aria-expanded="false"
          data-toggle={!readOnly && isBlock !== true ? 'dropdown' : ''}
        >
          {isInput ? (
            <input
              className="form-control"
              type="text"
              name={inputName}
              onChange={e => inputChange && inputChange(e)}
              value={value || ''}
            />
          ) : (
            <span className="select2 select2-container select2-container--default select2-container--below full-width">
              <span
                className={`select2-selection select2-selection--single ${
                  readOnly ? 'border_bottom_none' : ''
                }`}
              >
                <span
                  className={`select2-selection__rendered ${
                    value ? '' : 'select2-selection__placeholder'
                  }`}
                >
                  {value || placeholder}
                </span>
                {!readOnly && (
                  <span className="select2-selection__arrow">
                    <b role="presentation" />
                  </span>
                )}
              </span>
              <span className="dropdown-wrapper" aria-hidden="true" />
            </span>
          )}
        </div>

        {!readOnly && !cantReset && value && (
          <span
            className="select2-selection__clear cursor-pointer"
            auto-close="nonInput"
            aria-haspopup="true"
            aria-expanded="false"
            onClick={e => {
              onSelect(null)
            }}
          >
            ×
          </span>
        )}

        {(_.size(items) || isSearch) ? (
          <span className="dropdown-menu select2-container--open full-width">
            <span
              className="select2-dropdown select2-dropdown--above select-list-container_wrapper"
              dir="ltr"
            >
              <span>
                {isSearch && (
                  <span className="select2-search select2-search--dropdown">
                    <input
                      className="select2-search__field"
                      type="search"
                      onChange={onSearch}
                      onClick={e => e.stopPropagation()}
                      autoFocus
                    />
                  </span>
                )}
                <span className="select2-results">
                  <ul
                    className="select2-results__options"
                    role="tree"
                    id="select2-state-search-results"
                    aria-expanded="true"
                    aria-hidden="false"
                  >
                    {_.map(items, (item, i) => (
                      <li
                        key={`select_${keyValue}_${item.id || item.name || i}`}
                        className="select2-results__option"
                        role="treeitem"
                        onClick={e => {
                          onSelect(item)
                        }}
                      >
                        {item[selectFieldName || 'name']}
                      </li>
                    ))}
                  </ul>
                </span>
              </span>
            </span>
          </span>
        ) : null}
      </div>
    )
  }
}

export default Select
