import React, { Component } from 'react'

import _ from 'lodash'

import './TagField.css'

class TagField extends Component {
  addTag(e) {
    const { setSkills, data } = this.props

    const value = e.target.value ? e.target.value.trim() : null

    this.refs.tagContRef.className = 'tag-field-container'
    this.refs.helpBlock.className = 'hidden-block'

    if (e.keyCode === 13 && value && value !== '') {
      const tag = value.toLowerCase()
      e.target.value = ''
      const nextData = data ? data.slice() : []

      if (nextData.indexOf(tag) === -1) {
        nextData.push(tag)
        setSkills(nextData)
      } else {
        this.refs.tagContRef.className += ' has-warning'
        this.refs.helpBlock.className = 'help-block'
      }
    }
  }

  delTag(e, key) {
    const { setSkills, data } = this.props
    const nextData = data ? data.slice() : []
    nextData.splice(key, 1)
    setSkills(nextData)
  }

  onRemove = (key) => {
    const { onRemove } = this.props
    onRemove(key)
  }

  render() {
    const {
      data,
      onlyTags,
      tagObj,
      onRemove,
      isStatic,
    } = this.props
    const labelKey  = this.props.labelKey || 'name'
    return (
      <div className="tag-field-container" ref="tagContRef">
        <div className="tokenfield">
          {!!_.size(data) && _.map(data, (item, i) => (
            <div className="token" key={i}>
              <span className={`token-label ${isStatic ? 'static-token' : ''}`}>
                {tagObj && tagObj[item] ? tagObj[item][labelKey] : item}
              </span>
              {!isStatic && (<span
                className="close"
                onClick={(e) => onRemove
                  ? onRemove(item)
                  : this.delTag(e, item)
                }
              >
                ×
              </span>)}
            </div>
          )
          )}
        </div>

        {!onlyTags && (
          <input
            type="text"
            className="form-control token-field"
            placeholder="Enter your skills"
            onKeyDown={(e) => this.addTag(e)}
          />
        )}
        <span ref="helpBlock" className="hidden-block">such skill has already been added</span>
      </div>
    )
  }
}

export default TagField