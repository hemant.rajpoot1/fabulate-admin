import React, { Component, Fragment } from 'react'
import classNames from 'classnames'
import propTypes from 'prop-types'

import './CollapseButton.css'
class CollapseButton extends Component {
    state = {
      collapsed: false
    }
    render() {
      const {
        title,
        isBlockEdit,
        resetModel,
        isEdit,
        currentTab
      } = this.props
      const anchorClass= classNames({
        'ml-20': true,
        [`icon-arrow-${this.state.collapsed ? 'down' : 'up'}32`]: true
      })
      return (
        <Fragment>
          <p className="text-semibold">{title}
            <a
              href={`#collapse-link-${currentTab}`}
              className={anchorClass}
              data-toggle="collapse"
              onClick={() =>
                this.setState({ collapsed: !this.state.collapsed })
              }
            >
              <Fragment> </Fragment>
            </a>
            {!isBlockEdit && (
              <small>
                <a
                  className="w-56 ml-10 pull-right label bg-success text-lowercase badge-col-green border-none"
                  onClick={() => {
                    resetModel()
                  }}
                >
                  {isEdit ? 'Close edit' : 'Edit'}
                </a>
              </small>)}
          </p>
        </Fragment>
      )
    }
} 

CollapseButton.propTypes = {
  title: propTypes.string.isRequired,
  isBlockEdit: propTypes.bool,
  resetModel: propTypes.func,
  isEdit: propTypes.bool,
  currentTab: propTypes.string.isRequired
}

export default CollapseButton