import React, { PureComponent } from 'react'
import Dropzone from 'react-dropzone'
import Cropper from 'react-cropper'

import avatar from 'assets/images/placeholder.jpg'

import './UploadPhoto.css'

const roles = {
  2: 'Byuer',
  3: 'Contractor'
}

class UploadPhoto extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      cropURI: null,
      img: null
    }
  }
 
  save = () => {
    const { cropped, toggleModal } = this.props
    if (this.state.cropURI) {
      cropped(this.state.cropURI)
    }

    toggleModal()
    this.setState({
      img: null
    })
  }

  _crop() {
    // image in dataUrl
    if(!this.state.cropURI) {
      const cropURI = this.cropper.getCroppedCanvas().toDataURL('image/jpeg', 0.3)
      this.setState({ cropURI })
    }
  }

  drop = image => {
    this.setState({
      img: image[0],
    })
  }

  cropend = data => {
    const cropURI = this.cropper.getCroppedCanvas().toDataURL('image/jpeg', 0.3)
    this.setState({ cropURI })
  }

  render() {
    // Your photo will look like this
    const { img, cropURI } = this.state
    const { currentUser, isShow, toggleModal } = this.props

    return (
      <div onClick={() => toggleModal()} id="modal_default" className=" modal-background modal fade in " style={{ display: isShow ? 'block' : 'none' }}>
        <div className="modal-dialog" onClick={ e => e.stopPropagation()}>
          <div className="modal-content">
            <div className="modal-header">
              <button onClick={() => toggleModal()} type="button" className="close" data-dismiss="modal">×</button>
              <h5 className="modal-title">Your profile portrait</h5>
            </div>
            {isShow && (
              <div className="modal-body row">
                <div className="col-md-6">
                  <div style={{ margin: '0 auto', width: 250, height: 320 }}>
                    <Dropzone
                      multiple={false}
                      accept='image/*'
                      onDrop={this.drop}
                      disableClick={img ? true : false}
                      className="dropzone dz-clickable mb-12"
                      onClick={e => e.preventDefault()}
                    >
                      {img ? (
                        <Cropper
                          onClick={e => e.preventDefault()}
                          ref={ref => { this.cropper = ref }}
                          src={img.preview}
                          style={{ height: 320, width: '100%', overflow: 'hidden' }}
                          aspectRatio={16 / 16}
                          guides={false}
                          crop={this._crop.bind(this)}
                          cropend={this.cropend}
                        />
                      ) : (
                        <div className="dz-default dz-message">
                          <span>Drop or click here your photo</span>
                        </div>
                      )}
                    </Dropzone>
                  </div>
                </div>
                <div className="col-md-6">
                  <h6 className="modal-title text-center">Your photo will look like this</h6>
                  <div className="thumbnail">
                    <div className="thumb thumb-rounded thumb-slide example-user-photo">
                      <img className="example-user-photo" src={cropURI || currentUser.image || avatar} alt="" />
                    </div>

                    <div className="caption text-center">
                      <h6 className="text-semibold no-margin">{currentUser.userName} <small className="display-block">{roles[currentUser.permissions]}</small></h6>
                      <ul className="icons-list mt-15">
                        <li><a data-popup="tooltip" title="Google Drive"><i className="icon-google-drive"></i></a></li>
                        <li><a data-popup="tooltip" title="Twitter"><i className="icon-twitter"></i></a></li>
                        <li><a data-popup="tooltip" title="Github"><i className="icon-github"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            )}

            <div className="modal-footer">
              <button
                onClick={() => toggleModal()}
                type="button"
                className="btn btn-link legitRipple"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                onClick={() => this.save()}
                type="button"
                className="btn btn-primary legitRipple"
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UploadPhoto
