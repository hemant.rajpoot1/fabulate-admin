import React, { Component } from 'react'
import { formattingQuery } from 'helpers/tools'
import Select from 'components/ui/Select'
import { amplifyRoles } from 'helpers/params'

import './index.css'

const roles = [
  {
    id: 0,
    name: 'Super admin'
  },
  {
    id: 1,
    name: 'Admin'
  },
  {
    id: 2,
    name: 'Buyer'
  },
  {
    id: 3,
    name: 'Contractor'
  },      
  {
    id: 4,
    name: 'Editor'
  },
  // id: 5 , name: 'Guest'
  {
    id: 6,
    name: 'Amplify',
  },
]

class Create extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openSelect: false
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.windowClick)
  }

  windowClick = () => {
    if(this.state.openSelect) {
      this.setState({openSelect: false})
    }
  }
  

  onSubmit = e => {

    const { userActions, validation, currentUser, selected } = this.props
    const formData = formattingQuery({
      userName: validation.userName.value,
      email: validation.email.value,
      phoneNumber: validation.phoneNumber.value,
      password: validation.password.value,
      commission: validation.commission.value,
      xeroId: validation.xeroId.value,
      isInvited: true,
      isTerms: true,
    })

    if (formData.email
      && validation.email.isValid
      && formData.userName
      && validation.userName.isValid
      && formData.password
      && validation.password.isValid
    ) {

      if(currentUser.permissions === 0) {
        formData.permissions = selected.roles.id
        if (selected.roles.id === 6) {
          formData.amplifyRoles = selected.amplifyRoles.value
        }
      }
      e.preventDefault()
      userActions.create(formData)
    }

  }

  changeInput = (type, value) => {
    const { validation, userActions, validRules } = this.props
    userActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  render() {

    let { validation, translate, userActions, selected, isCreateFetching } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">New user</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">First and Last Name</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.nameInput = ref : null }
                  type="text"
                  className="form-control"
                  name="name"
                  onBlur={e => (this.nameInput.value !== '' && validation.userName.isValid === undefined) && this.changeInput('userName', e.target.value)}
                  onChange={e => (validation.userName.isValid !== undefined) && this.changeInput('userName', e.target.value)}
                />
                {!validation.userName.isValid && validation.userName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`userError.${validation.userName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Email</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.emailInput = ref : null }
                  type="email"
                  className="form-control"
                  name="email"
                  onBlur={e => (this.emailInput.value !== '' && validation.email.isValid === undefined) && this.changeInput('email', e.target.value)}
                  onChange={e => (validation.email.isValid !== undefined) && this.changeInput('email', e.target.value)}
                  required
                />
                {!validation.email.isValid && validation.email.message && (
                  <label
                    id="email-error"
                    className="validation-error-label"
                    forhtml="email"
                  >
                    {translate(`userError.${validation.email.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Password</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.passwordInput = ref : null }
                  type="password"
                  className="form-control"
                  name="password"
                  onBlur={e => (this.passwordInput.value !== '' && validation.password.isValid === undefined) && this.changeInput('password', e.target.value)}
                  onChange={e => (validation.password.isValid !== undefined) && this.changeInput('password', e.target.value)}
                  required
                />
                {!validation.password.isValid && validation.password.message && (
                  <label
                    id="password-error"
                    className="validation-error-label"
                    forhtml="password"
                  >
                    {translate(`userError.${validation.password.message}`)}
                  </label>
                )}
              </div>
            </div>            

            <div className="form-group">
              <label className="col-lg-3 control-label">Phone number</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.phoneInput = ref : null }
                  type="text"
                  className="form-control"
                  name="phone"
                  onBlur={e => (this.phoneInput.value !== '' && validation.phoneNumber.isValid === undefined) && this.changeInput('phoneNumber', e.target.value)}
                  onChange={e => (validation.phoneNumber.isValid !== undefined) && this.changeInput('phoneNumber', e.target.value)}
                />
                {!validation.phoneNumber.isValid && validation.phoneNumber.message && (
                  <label
                    id="phone-error"
                    className="validation-error-label"
                    forhtml="phone"
                  >
                    {translate(`userError.${validation.phoneNumber.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Commission</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.commissionInput = ref : null }
                  type="text"
                  className="form-control"
                  name="commision"
                  onBlur={e => (this.commissionInput.value !== '' && validation.commission.isValid === undefined) && this.changeInput('commission', e.target.value)}
                  onChange={e => (validation.commission.isValid !== undefined) && this.changeInput('commission', e.target.value)}
                  // value={validation.commission.value}
                />
                {!validation.commission.isValid && validation.commission.message && (
                  <label
                    id="phone-error"
                    className="validation-error-label"
                    forhtml="phone"
                  >
                    {translate(`userError.${validation.commission.message}`)}
                  </label>
                )}
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Xero Id</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.xeroIdInput = ref : null }
                  type="text"
                  className="form-control"
                  name="xeroId"
                  onBlur={e => (this.xeroIdInput.value !== '' && validation.xeroId.isValid === undefined) && this.changeInput('xeroId', e.target.value)}
                  onChange={e => (validation.xeroId.isValid !== undefined) && this.changeInput('xeroId', e.target.value)}
                />
                {!validation.xeroId.isValid && validation.xeroId.message && (
                  <label
                    id="phone-error"
                    className="validation-error-label"
                    forhtml="phone"
                  >
                    {translate(`userError.${validation.xeroId.message}`)}
                  </label>
                )}
              </div>
            </div>                       

            <div className="form-group">
              <label className="col-lg-3 control-label">Roles</label>
              <div className="col-lg-9">
                <Select
                  onClick={e => {
                    e.stopPropagation()
                    this.setState({ openSelect: !this.state.openSelect })
                  }}
                  isOpen={this.state.openSelect}
                  isSearch={false}
                  onSelect={value => {
                    userActions.select({
                      roles: value
                    })
                  }}
                  items={roles}
                  value={selected.roles.name}
                />
              </div>
            </div>

            { selected.roles.id === 6 ? (
              <div className="form-group">
                <label className="col-lg-3 control-label">Amplify Role</label>
                <div className="col-lg-9">
                  <Select
                    onClick={e => {
                      e.stopPropagation()
                      this.setState({ openSelect: !this.state.openSelect })
                    }}
                    isOpen={this.state.openSelect}
                    isSearch={false}
                    onSelect={value => {
                      userActions.select({
                        amplifyRoles: value
                      })
                    }}
                    items={amplifyRoles}
                    value={selected.amplifyRoles.name}
                  />
                </div>
              </div>
            ) : (
              null
            )}

            <div className="text-right">
              <button
                type="button"
                className="btn btn-primary"
                onClick={e => this.onSubmit(e)}
                disabled={isCreateFetching}
              >
                  Create
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Create
