import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as userActions from 'store/actions/user'
import { getField } from 'store/selectors/modelSelectors'


class CreateContainer extends Component {
  componentDidMount() {
    const { userActions } = this.props

    userActions.resetSelect()
  }

  render() {
    return (
      <Create { ...this.props } />
    )
  }
}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.user.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : null

  return {
    translate: getTranslate(state.locale),
    validation: state.user.validation,
    selected: state.user.selected,
    validRules,
    currentUser,
    isCreateFetching: state.user.isCreateFetching
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
