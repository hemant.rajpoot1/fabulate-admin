import React, { Component, Fragment } from 'react'

import Input from './Input'
import _ from 'lodash'
import moment from 'moment'

import { settings } from 'helpers/params'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Select from 'components/ui/Select'
import { amplifyRoles } from 'helpers/params'

const roles = [
  {
    id: 0,
    name: 'Super admin'
  },
  {
    id: 1,
    name: 'Admin'
  },
  {
    id: 2,
    name: 'Buyer'
  },
  {
    id: 3,
    name: 'Contractor'
  },   
  {
    id: 4,
    name: 'Editor'
  },
  {
    id: 6,
    name: 'Amplify',
  },
]

const amplifyRole = {
  0: 'Owner',
  1: 'Editor',
  2: 'Viewer',
}

class Edit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      select: undefined,
      workspaceEdit: null,
      isLegalTerms: false
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.changeOpenSelect)
  }  

  changeOpenSelect = name => {
    const { select } = this.state
    let nextSelect = undefined
    if(typeof name === 'string') {
      nextSelect = name
    }
    if(nextSelect !== select) {
      this.setState({
        select: nextSelect
      })
    }
  }  

  handleChange({ target }) {
    const { userActions } = this.props
    userActions.edit({ [target.name]: target.value })
  }

  handleOnSelect = (field, item) => {
    const { userActions } = this.props
    if (field !== 'workspace') {
      userActions.edit({ [field]: item ? item.value : undefined })    
    } else {
      this.setState({
        workspaceEdit: item.id
      })
    }
  }

  handleSearch = (e) => {
    const { value } = e.target
    const { workspaceActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      workspaceActions.get({
        name: value
      })
    }, 800)
  }

  onSubmit() {
    const { userActions, workspaceUserActions, user, selected } = this.props
    const { workspaceEdit } = this.state
    const { publicationHistory = [] } = user
    if (!_.isEqual(selected.roles.id, 6)) {
      user.amplifyRoles = null
    }
    const publicationHistoryTransferData = _.map(publicationHistory, item => item.id)
    userActions.update({
      ...user,
      publicationHistory: publicationHistoryTransferData,
      email: undefined
    })
    if (workspaceEdit && (!user.workspace || user.workspace.id !== workspaceEdit)) {
      workspaceUserActions.create({
        userId: user.id,
        workspaceId: workspaceEdit
      })
    }
  }

  onClickCancel = () => {
    const { id, userActions } = this.props

    userActions.setIsEdit(false)
    userActions.getId(id, true)
  }

  renderButtons = () => {
    const { isEdit, userActions } = this.props
    return isEdit ?
      (
        <div>
          <button
            type="button"
            className="btn btn-primary cancel-button"
            onClick={this.onClickCancel}
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btn-primary"
            onClick={() => this.onSubmit()}
          >
            Update
          </button>
        </div>
      ) : (
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => userActions.setIsEdit(true)}
        >
          Edit
        </button>
      )
  }

  render() {

    const { user, userActions, selected, workspaceList, isEdit, isGetFetching} = this.props
    const { select, workspaceEdit, isLegalTerms } = this.state
    const { workspace = {} } = user
    const editedValue = _.get(workspaceList, `[${workspaceEdit}].name`)
    const isAmplify = user.permissions && _.isEqual(user.permissions.id, 6)
    const isSelectedAmplify = _.isEqual(selected.roles.id, 6)

    if (isGetFetching) {
      return (<SpinnerLoadData />)
    }

    const legalUrl = _.get(user, 'legalUrl', [])

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">{isEdit ? 'Edit' : 'View'}</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">First and Last Name</label>
              <div className="col-lg-9">
                <Input
                  isEdit={isEdit}
                  name="userName"
                  value={ user.userName || ''}
                  onChange={ e => this.handleChange(e) }
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Email</label>
              <div className="col-lg-9">
                <Input
                  name="email"
                  value={ user.email || ''}
                  onChange={ e => this.handleChange(e) }
                  readOnly="readonly"
                  isEdit={isEdit}
                />
              </div>
            </div>  

            {(user.permissions === 2 || user.permissions === 3) && (
              <div className="form-group">
                <label className="col-lg-3 control-label">Organization</label>
                <div className="col-lg-9">
                  <Select
                    isSearch={true}
                    cantReset={true}
                    placeholder="Select user's organization"
                    onSearch={e => this.handleSearch(e, 'cause')}
                    onSelect={value => this.handleOnSelect('workspace', value)}
                    items={workspaceList}
                    value={editedValue || workspace.name}
                    readOnly={!isEdit}
                  />
                </div>
              </div>
            )}

            <div className="form-group">
              <label className="col-lg-3 control-label">Phone number</label>
              <div className="col-lg-9">
                <Input 
                  name="phoneNumber"
                  value={ user.phoneNumber || ''}
                  onChange={ e => this.handleChange(e) }
                  isEdit={isEdit}
                />
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Commission</label>
              <div className="col-lg-9">
                <Input
                  name="commission"
                  value={ user.commission || ''}
                  onChange={ e => this.handleChange(e) }
                  isEdit={isEdit}
                />
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Xero Id</label>
              <div className="col-lg-9">
                <Input 
                  name="xeroId"
                  value={ user.xeroId || ''}
                  onChange={ e => this.handleChange(e) }
                  isEdit={isEdit}
                />
              </div>
            </div>                           

            {user.permissions === 3 && [
              <div className="form-group">
                <label className="col-lg-3 control-label">Rate</label>
                <div className="col-lg-9">
                  <Input
                    name="rateContractor"
                    value={ user.rateContractor || ''}
                    onChange={ e => this.handleChange(e) }
                    isEdit={isEdit}
                  />
                </div>
              </div>,
              <div className="form-group">
                <label className="col-lg-3 control-label">Specialist</label>
                <div className="col-lg-9">
                  <Select
                    isSearch={false}
                    placeholder={settings['typeContractor'][0].name}
                    onClick={e => {
                      e.stopPropagation()
                      this.changeOpenSelect('typeContractor')
                    }}
                    isOpen={select === 'typeContractor'}
                    onSelect={value => {
                      this.handleOnSelect('typeContractor', value)
                    }}
                    items={settings['typeContractor']}
                    value={settings['typeContractor'][user.typeContractor]
                      ? settings['typeContractor'][user.typeContractor].name
                      : undefined
                    }
                    readOnly={!isEdit}
                  />
                </div>
              </div>,
              <div className="form-group">
                <label className="col-lg-3 control-label">Experience</label>
                <div className="col-lg-9">
                  <Select
                    readOnly={!isEdit}
                    isSearch={false}
                    placeholder={settings['experienceContractor'][0].name}
                    onClick={e => {
                      e.stopPropagation()
                      this.changeOpenSelect('experienceContractor')
                    }}
                    isOpen={select === 'experienceContractor'}
                    onSelect={value => {
                      this.handleOnSelect('experienceContractor', value)
                    }}
                    items={settings['experienceContractor']}
                    value={settings['experienceContractor'][user.experienceContractor]
                      ? settings['experienceContractor'][user.experienceContractor].name
                      : undefined
                    }
                  />
                </div>
              </div>                                             
            ]} 


            {user.permissions >= 2 && (
              <div className="form-group">
                <label className="col-lg-3 control-label">Bio</label>
                <div className="col-lg-9">
                  <textarea 
                    type="text" 
                    className="form-control" 
                    name="bio"
                    rows="5" 
                    value={ user.bio || ''}
                    onChange={ e => this.handleChange(e) }
                    disabled={!isEdit}
                  />
                </div>
              </div>  
            )}
            <div className="form-group">
              <label className="col-lg-3 control-label">Roles</label>
              <div className="col-lg-9">
                <Select
                  onClick={e => {
                    e.stopPropagation()
                    this.changeOpenSelect('roles')
                  }}
                  isOpen={select === 'roles'}                  
                  isSearch={false}
                  onSelect={value => {
                    userActions.select({
                      roles: value
                    })
                    userActions.edit({ 'permissions': value.id })
                  }}
                  items={roles}
                  value={selected.roles.name}
                  readOnly={!isEdit}
                />
              </div>
            </div>
            <div className="panel-heading">
              <h5 className="panel-title">Legal terms information</h5>
              <div className="heading-elements">
                <ul className="icons-list">
                  <li>
                    <a data-action="collapse" onClick={() => this.setState({ isLegalTerms: !this.state.isLegalTerms})}>&nbsp;</a>
                  </li>
                </ul>
              </div>
            </div>
            {isLegalTerms &&  
              <table className="table datatable-select-basic">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Date of agreement</th>
                    <th>Version</th>
                  </tr>
                </thead>
                <tbody>
                  {_.map(legalUrl, (item, i) =>{
                    const date = _.get(item , 'createdAt', null)

                    return (
                      <tr key={item.id + i}>
                        <td>{_.get(item, 'legalUrl.id', 'n/a')}</td>
                        <td>{date ? moment(date).format('MMM D, YYYY') : 'n/a'}</td>
                        <td>{_.get(item, 'legalUrl.version', 'n/a')}</td>         
                      </tr>
                    )})}
                </tbody>
              </table>
            }

            { isAmplify || isSelectedAmplify ? (
              <div className="form-group">
                <label className="col-lg-3 control-label">Amplify Role</label>
                <div className="col-lg-9">
                  <Select
                    onClick={e => {
                      e.stopPropagation()
                      this.changeOpenSelect('amplifyRoles')
                    }}
                    isOpen={select === 'amplifyRoles'}                  
                    isSearch={false}
                    onSelect={value => {
                      userActions.select({
                        amplifyRoles: value
                      })
                      userActions.edit({ 'amplifyRoles': value.value })
                    }}
                    items={amplifyRoles}
                    value={ _.isNil(selected.amplifyRoles) ? amplifyRole[user.amplifyRoles] : selected.amplifyRoles.name }
                    readOnly={!isEdit}
                  />
                </div>
              </div>
            ) : (
              null
            )}
  
            <div className="text-right">
              {this.renderButtons()}
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Edit
