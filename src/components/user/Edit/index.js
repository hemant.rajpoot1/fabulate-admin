import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as userActions from 'store/actions/user'
import * as workspaceActions from 'store/actions/workspace'
import * as workspaceUserActions from 'store/actions/workspaceUser'
import { getField } from 'store/selectors/modelSelectors'

class EditContainer extends Component {

  componentDidMount() {
    const { userActions, workspaceActions, id } = this.props
    
    userActions.getId(id)
    workspaceActions.get()
  }

  render() {
    return (
      <Edit { ...this.props } />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.user.validation))
  return {
    user: state.user.edit,
    selected: state.user.selected,     
    translate: getTranslate(state.locale),
    workspaceList: state.workspace.origin,
    validRules,
    isEdit: state.user.edit.isEdit,
    id: props.match.params.id,
    isGetFetching: state.user.isGetFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    workspaceUserActions: bindActionCreators(workspaceUserActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
