import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import Pagination from 'react-js-pagination'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'

import './List.css'

const roles = {
  0: 'Super admin',
  1: 'Admin',
  2: 'Buyer',
  3: 'Contractor',
  4: 'Editor',
  5: 'Guest',
  6: 'Amplify',
}

class UsersList extends Component {
  constructor(props) {
    super(props)
    this.responsiveTableRef = React.createRef()
    this.changeResponsiveTableChange = this.changeResponsiveTableChange.bind(this)
  }

  onBlock = ({ id, isBlocked }) => {
    const { userActions } = this.props

    userActions.update({
      id: id,
      isBlocked: !isBlocked
    })
  }

  onRemove = (id, type) => {
    const {
      translate,
      filter,
      userActions,
      workspaceUserActions
    } = this.props

    const textParam = type === 'user' ? 'userRemoveConfirm' : 'userRemoveConfirmWorkspace'

    const notice = new PNotify({
      title: translate(`${textParam}.title`),
      text: translate(`${textParam}.text`),
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      if (type === 'user') {
        userActions.remove(id)
      } else {
        workspaceUserActions.remove({
          id,
          filter
        })
      }
    })
  }

  loginAsUser(userId) {
    const { userActions } = this.props

    userActions.loginAsAnotherUser(userId)
  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { userActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { userSearchData: value !== '' ? value : undefined }
      userActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { userActions } = this.props
    userActions.resetFilter()
  }

  changeResponsiveTableChange = () => {
    const responsiveTableNode = this.responsiveTableRef.current

    if(getComputedStyle(responsiveTableNode)['overflow'] === 'auto') {
      responsiveTableNode.style.overflow = 'inherit'
    } else {
      responsiveTableNode.style.overflow = 'auto'
    }
  }

  renderUsersSearch = () => (
    <div>
      <Filter
        searchItem="user"
        onChangeHandler={this.setSearchFilter}
        defaultValue={this.props.filter.userSearchData || ''}
        resetFilters={this.resetFilters}
        customClasses="float-none m-10p-0"
      />
    </div>
  )

  render() {
    const {
      users,
      currentUser,
      page,
      count,
      fetchList,
      userActions,
      isGetFetching,
    } = this.props

    if (isGetFetching) {
      return (<SpinnerLoadData />)
    }

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Users List</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>

        <div className="users-table">
          {this.renderUsersSearch()}
          <div className="table-responsive" ref={this.responsiveTableRef}>
            <table className="table datatable-select-basic">
              <thead>
                <tr>
                  <th>#</th>
                  <th className="text-center-info">Name</th>
                  <th>Email</th>
                  <th>Role</th>
                  <th>Organization</th>
                  <th className="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                {_.map(users, user => {
                  return (
                    <tr key={user.id}>
                      <td>{user.id}</td>
                      <td className="text-center-info">
                        <Link to={`/user/${user.id}`} onClick={() => userActions.setIsEdit(false)}>
                          {user.userName}
                        </Link>
                      </td>
                      <td>{user.email}</td>
                      <td>{roles[user.permissions]}</td>
                      <td>{_.map(user.workspaces, workspace => workspace ? workspace.name : '-')}</td>
                      <td className="text-center">
                        {currentUser.permissions === 0 ? (
                          <ul className="icons-list">
                            <li className="dropdown" onClick={() => this.changeResponsiveTableChange()}>
                              <a className="dropdown-toggle" data-toggle="dropdown">
                                <i className="icon-menu9"></i>
                              </a>
                              <ul className="dropdown-menu dropdown-menu-right">
                                <li>
                                  <Link to={`/user/${user.id}/edit`} onClick={() => userActions.setIsEdit(true)}>
                                    <i className="icon-pencil"></i>
                                    Edit
                                  </Link>
                                </li>
                                <li onClick={() => this.onRemove(user.id, 'user')}>
                                  <a>
                                    <i className="icon-cross"></i>
                                    Delete
                                  </a>
                                </li>
                                <li onClick={() => this.onBlock(user)}>
                                  <a><i className={user.isBlocked ? ' icon-user' : 'icon-user-block'}></i>{user.isBlocked ? 'Unblock user' : 'Block user'}</a>
                                </li>

                                {!_.includes([0, 1], user.permissions) && (
                                  <li onClick={() => this.loginAsUser(user.id)}>
                                    <a>
                                      <i className="icon-enter" />
                                      Login as
                                    </a>
                                  </li>
                                )}
                              </ul>
                            </li>
                          </ul>
                        ) : (
                          <a onClick={() => this.onBlock(user)}>
                            <i className={user.isBlocked ? ' icon-user' : 'icon-user-block'}></i>
                              &nbsp;&nbsp;{user.isBlocked ? 'Unblock user' : 'Block user'}
                          </a>
                        )}
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
        {count > 20 &&
          <div className="pager p-20">
            <Pagination
              hideNavigation
              pageRangeDisplayed={3}
              activePage={page}
              itemsCountPerPage={20}
              totalItemsCount={count}
              onChange={(page) => fetchList(page)}
            />
          </div>
        }
      </div>
    )
  }
}

export default UsersList
