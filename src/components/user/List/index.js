import List from './List'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import * as userActions from 'store/actions/user'
import * as workspaceActions from 'store/actions/workspace'
import * as workspaceUserActions from 'store/actions/workspaceUser'

class ListContainer extends Component {
  componentDidMount() {
    const { filter } = this.props
    this.fetch(filter)
  }

  componentDidUpdate(prevProps) {
    const { filter, userActions } = prevProps

    if (!_.isEqual(this.props.filter, filter)) {
      this.fetch(this.props.filter)
    }

    if (this.props.location.key !== prevProps.location.key) {
      userActions.resetFilter()
    }
  }

  fetch = filter => {
    let { userActions, workspaceActions } = this.props
    userActions.get(filter)

    if (filter && filter.workspaceId) {
      workspaceActions.getId(filter.workspaceId)
    }
  }

  fetchList = (page) => {
    const { userActions } = this.props
    userActions.get({}, page)
  }

  render() {
    return (
      <List
        {...this.props}
        fetchList={this.fetchList}
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const currentWorkspace = state.workspace && state.workspace.edit
    ? state.workspace.edit
    : null
  const currentUser = state.auth && state.auth.user ? state.auth.user : null

  return {
    users: state.user.list.origin,
    page: state.user.list.page,
    count: state.user.list.count,
    filter: state.user.filter,
    translate: getTranslate(state.locale),
    isGetFetching: state.user.isGetFetching,
    currentWorkspace,
    currentUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    workspaceUserActions: bindActionCreators(workspaceUserActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
