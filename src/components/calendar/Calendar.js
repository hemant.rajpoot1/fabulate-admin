import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'

import mobiscroll from '@mobiscroll/react'
import '@mobiscroll/react/dist/css/mobiscroll.react.min.css'
import _ from 'lodash'
import moment from 'moment'

import Select from 'components/ui/Select'
import Filter from 'components/ui/Filter'

import './Calendar.css'

const items = [
  {
    name: 'All',
    value: undefined
  },
  {
    name: 'Draft',
    value: 0
  },
  {
    name: 'Active',
    value: 1
  },
  {
    name: 'In progress',
    value: 2
  },
  {
    name: 'Completed',
    value: 3
  },
]

const getCalendarDateRangeFrom = (date) => moment(date).subtract(14, 'days').format('MM/DD/YYYY')
const getCalendarDateRangeTo = (date) => moment(date).add(14, 'days').format('MM/DD/YYYY')

class Calendar extends React.Component {
  state = {
    selectedFilters: {
      briefStatus: items[0],
      buyer: { name: null },
      organisation: { name: null }
    },
    selectedCause: null,
    dateFilter: {
      dateFilter: 'calendar',
      dateFrom: moment().startOf('month').format('MM/DD/YYYY'),
      dateTo: moment().endOf('month').format('MM/DD/YYYY'),
      disablePagination: true
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.filter !== this.props.filter) {
      const { selectedFilters, dateFilter } = this.state
      const { causeActions, filter } = this.props
      causeActions.get({
        ...filter,
        ...dateFilter,
        causeStatus: selectedFilters.briefStatus.value,
        ownerId: this.state.selectedFilters.buyer.value,
        workspaceId: this.state.selectedFilters.organisation.value
      })
    }
  }

  onPageChange = (event) => {
    const { firstDay } = event
    const { causeActions, filter } = this.props
    const { selectedFilters } = this.state
    const lastDay = new Date(firstDay.getFullYear(), firstDay.getMonth() + 1, 0)
    this.setState({
      dateFilter: {
        ...this.state.dateFilter,
        dateFrom: getCalendarDateRangeFrom(firstDay),
        dateTo: getCalendarDateRangeTo(lastDay),
      }
    }, () => causeActions.get({
      ...filter,
      causeStatus: selectedFilters.briefStatus.value,
      ...this.state.dateFilter,
      ownerId: this.state.selectedFilters.buyer.value,
      workspaceId: this.state.selectedFilters.organisation.value
    }))
  }

  renderColorView = () => {

    const filteringItems = _.filter(items, item => !_.isNil(item.value))

    return (
      <div className="calendar__color-view">
        {_.map(filteringItems, (item, index) => {
          const backgroundColor = this.getColor(item.value)
          return (
            <div key={item.name + index} className="calendar__color-view__item">
              <span style={{ backgroundColor }} className="calendar__color-view__color"/>
              {item.name}
            </div>
          )
        })}
      </div>
    )}

  getColor = causeStatus => {
    switch(causeStatus) {
    case 0: {
      return '#90a4ae'
    }
    case 1: {
      return '#4caf50'
    }
    case 2: {
      return '#009688'
    }
    case 3: {
      return '#4dd0e1'
    }
    default: {
      return '#90a4ae'
    }
    }
  }

  modifyCauseData = () => {
    const { causes } = this.props
    return _.map(causes, cause => {
      let startDate = cause.startDate
        ? new Date(cause.startDate)
        : new Date()
      const endDate = new Date(cause.causeDueDate)
      if (startDate > endDate) {
        startDate = endDate
      }
      return {
        id: cause.id,
        start: startDate,
        end: endDate,
        text: cause.causeName,
        color: this.getColor(cause.causeStatus),
        causeData: cause
      }
    })
  }

  handleSearch = ({ value }, type) => {
    const { workspaceActions, userActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    switch(type) {
    case 'workspace': {
      this.timer = setTimeout(() => {
        workspaceActions.get({
          name: value
        })
      }, 800)
      return
    }
    case 'user': {
      this.timer = setTimeout(() => {
        userActions.get({
          userName: value,
          permissions: 2
        })
      }, 800)
      return
    }
    default: {
      return null
    }
    }
  }

  handleSelect = (value, name, filterName) => {
    const { workspaceUser, causeActions, filter } = this.props
    const { dateFilter, isOrgBriefs, selectedFilters } = this.state

    this.setState({
      selectedFilters: {
        ...selectedFilters,
        [filterName]: {
          value,
          name
        }
      }
    }, () => {
      const applyCheckbox = isOrgBriefs && { workspaceId: workspaceUser.id }

      const newFilter = {
        ...filter,
        ...applyCheckbox,
        causeStatus: this.state.selectedFilters.briefStatus.value,
        ...dateFilter,
        ownerId: this.state.selectedFilters.buyer.value,
        workspaceId: this.state.selectedFilters.organisation.value
      }

      causeActions.get({
        ...newFilter
      })
    })
  }

  searchCause = ({ target }) => {
    const { value } = target
    const { causeActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { causeName: value !== '' ? value : undefined }
      causeActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { causeActions } = this.props
    causeActions.resetFilters()
  }

  handleEventSelect = ({ event }) => {
    const { workroomActions } = this.props
    if (event.id === _.get(this, 'state.selectedCause.id')) {
      this.setState({ selectedCause: null })
    } else {
      this.setState({ selectedCause: event.causeData })
      if (event.causeData.causeStatus > 1) {
        workroomActions.getId({
          causeId: event.id
        })
      }
    }
  }

  getProductData = (selectedCause) => {
    const { contentCategories } = this.props
    return _.map(contentCategories, contentCategoryItem => {
      const filteredData = _.filter(selectedCause.products,
        productItem => productItem && productItem.contentCategoryId === contentCategoryItem.id)
      return {
        contentCategoryName: contentCategoryItem.contentCategoryName,
        contentCategoryId: contentCategoryItem.id,
        filteredData
      }
    })
  }

  generateProduct = product => {
    return _.map(product, productItem => {
      if (!_.size(productItem.filteredData)) {
        return null
      }
      return (
        <React.Fragment key={`product_${productItem.contentCategoryId}`}>
          <span className="active border-double mr-10">
            <span className="m-0 text-semibold">
              {productItem.contentCategoryName}&nbsp;
            </span>
            {
              _.map(productItem.filteredData, (filterItem, index) => (
                <React.Fragment key={filterItem.id}>
                  <span>
                    {`${filterItem.count}x ${filterItem.productDisplayName}`}
                  </span>
                  {productItem.filteredData.length - 1 !== index && <span>,&nbsp;</span>}
                </React.Fragment>
              ))
            }
          </span>
        </React.Fragment>
      )
    })
  }

  getArticlesData = () => {
    const { selectedCause } = this.state
    const { workroom, workroomCauseId } = this.props
    if (!workroom || workroomCauseId !== selectedCause.id) {
      const articlesData = _.map(selectedCause.products, ({ productDisplayName, count }) => ({
        productDisplayName,
        count
      }))
      return articlesData
    }

    const articlesData = _.map(workroom, workroomItem => {
      const { productTemplate, workItems, productDisplayName } = workroomItem
      const productTemplateLength = productTemplate.templateTask.length
      return _.map(workItems, workItem => ({
        productDisplayName,
        isFinish: workItem.milestoneStatus === productTemplateLength
      }))
    })
    const flatData = _.flatten(articlesData)
    return flatData
  }

  renderPitchData = () => {
    const { currentUser: { id } } = this.props
    const { selectedCause: { causeUserCause } } = this.state
    const currentUserPitch = _.find(causeUserCause, { contractorId: id })
    return (
      <div>
        <div>
          <span className="text-semibold">Tagline:&nbsp;</span>
          <span>{currentUserPitch.tagline}</span>
        </div>
        <div>
          <span className="text-semibold">Outline:&nbsp;</span>
          <span>{currentUserPitch.outline}</span>
        </div>
        <div>
          <span className="text-semibold">Brand integration:&nbsp;</span>
          <span>{currentUserPitch.brandIntegration}</span>
        </div>
      </div>
    )
  }

  renderEditorSelectors = () => {
    const { buyerList, workspaceList } = this.props
    const { selectedFilters } = this.state

    return (
      <Fragment>
        <Select
          placeholder="Choose organisation..."
          isSearch={true}
          items={workspaceList}
          value={selectedFilters.organisation.name}
          onSelect={org => this.handleSelect(
            _.get(org, 'id', null),
            _.get(org, 'name', null),
            'organisation'
          )}
          onSearch={e => this.handleSearch(e.target, 'workspace')}
        />

        <Select
          placeholder="Choose buyer..."
          isSearch={true}
          items={buyerList}
          selectFieldName="userName"
          value={selectedFilters.buyer.name}
          onSelect={user => this.handleSelect(
            _.get(user, 'id', null),
            _.get(user, 'userName', null),
            'buyer'
          )}
          onSearch={e => this.handleSearch(e.target, 'user')}
        />
      </Fragment>
    )
  }

  render() {
    const { causes, workroomCauseId, filter } = this.props
    const {
      selectedFilters,
      selectedCause,
    } = this.state
    const calendarData = this.modifyCauseData(causes)
    const articlesData = selectedCause && this.getArticlesData()

    return (
      <Fragment>
        {this.renderEditorSelectors()}
        <Select
          items={items}
          value={selectedFilters.briefStatus.name}
          onSelect={({ value, name }) => this.handleSelect( value, name, 'briefStatus')}
          cantReset
        />
        <Filter
          searchItem="briefs"
          onChangeHandler={this.searchCause}
          defaultValue={filter.causeName || ''}
          resetFilters={this.resetFilters}
          customClasses="float-none m-10p-0"
        />

        {this.renderColorView()}

        <div className="row">
          <div className={`col-lg-${selectedCause ? '9' : '12'}`}>
            <mobiscroll.Eventcalendar
              display="inline"
              data={calendarData}
              calendarHeight={614}
              view={{
                calendar: {
                  labels: true
                }
              }}
              theme="ios"
              onEventSelect={this.handleEventSelect}
              onSetDate={this.onSetDate}
              onPageChange={this.onPageChange}
            />
          </div>
          {selectedCause && (
            <div className="panel pb-20 col-lg-3">
              <h3>
                <Link to={`/cause/${selectedCause.id}`}>{selectedCause.causeName}</Link>
              </h3>
              <div>
                <span className="text-semibold">Deadline:&nbsp;</span>
                <span>{moment(selectedCause.causeDueDate).format('MMM DD, YYYY')}</span>
              </div>
              {(
                <Fragment>
                  <span className="text-semibold">Articles:&nbsp;</span>
                  <ul>
                    {articlesData.map((article, i) => {
                      const isInProgressBrief = selectedCause.id === workroomCauseId
                      return isInProgressBrief ?
                        (
                          <li key={`atricle_${i}`}>
                            {article.productDisplayName}: {article.isFinish ? 'Finished': 'Not finished'}
                          </li>
                        ) : (
                          <li key={`atricle_${i}`}>
                            {article.productDisplayName} x {article.count}
                          </li>
                        )
                    })}
                  </ul>
                </Fragment>
              )}
            </div>
          )}
        </div>
      </Fragment>
    )
  }
}

export default Calendar
