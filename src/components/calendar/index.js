import Calendar from './Calendar'
import React, { Component } from 'react'
import _ from 'lodash'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as causeActions from 'store/actions/cause'
import * as contentCategoryActions from 'store/actions/contentCategory'
import * as workroomActions from 'store/actions/workroom'
import * as userActions from 'store/actions/user'
import * as workspaceActions from 'store/actions/workspace'
import moment from 'moment'

const currentMouthBriefs = {
  dateFilter: 'calendar',
  dateFrom: moment().startOf('month').subtract(14, 'days').format('MM/DD/YYYY'),
  dateTo: moment().endOf('month').add(14, 'days').format('MM/DD/YYYY'),
  disablePagination: true
}

class CalendarContainer extends Component {
  componentDidMount() {
    const {
      causeActions,
      filter,
      contentCategoryActions,
      contentCategories,
      userActions,
      workspaceActions
    } = this.props
    if(_.isEmpty(contentCategories)) {
      contentCategoryActions.get({})
    }
    userActions.get({
      permissions: 2
    })
    workspaceActions.get({})
    causeActions.get({
      ...filter,
      ...currentMouthBriefs,
    })
  }

  render() {
    return (
      <Calendar
        {...this.props}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const workroom = _.get(state, 'workroom.validationEdit.workroomItems.value')
  const workroomCauseId = _.get(state, 'workroom.validationEdit.causeId.value') 
  return {
    causes: state.cause.list.origin,
    filter: state.cause.filter,
    currentUser: _.get(state, 'auth.user', {}),
    workspaceUser: _.get(state, 'auth.workspace', {}),
    contentCategories: state.contentCategory.origin,
    workroom,
    workroomCauseId,
    buyerList: _.get(state, 'user.list.origin', {}),
    workspaceList: _.get(state, 'workspace.origin', {})
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
    workroomActions: bindActionCreators(workroomActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CalendarContainer))
