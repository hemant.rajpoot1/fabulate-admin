import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as assetActions from 'store/actions/asset'
import { getField } from 'store/selectors/modelSelectors'

class EditContainer extends Component {
  
  componentDidMount() {
    const { assetActions, match } = this.props
    let { id } = match.params
    assetActions.getId(id)
  }  

  render() {

    return (
      <Edit { ...this.props } />
    )

  }

}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.asset.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}

  return {
    translate: getTranslate(state.locale),
    validation: state.asset.validation,  
    asset: state.asset.edit,
    search: state.asset.search, 
    attachments: state.asset.attachments,       
    currentUser,
    validRules
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    assetActions: bindActionCreators(assetActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
