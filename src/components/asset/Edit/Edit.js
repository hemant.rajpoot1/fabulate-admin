import React, { Component } from 'react'
import Dropzone from 'react-dropzone'
import { formattingQuery } from 'helpers/tools'
import _ from 'lodash'

import FileLoad from 'components/ui/File'
import Select from 'components/ui/Select'


import './edit-asset.css'

class Edit extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openSelect: false,
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.windowClick)
  }

  windowClick = () => {
    if (this.state.openSelect) {
      this.setState({ openSelect: false })
    }
  }

  onSubmit = e => {

    const {
      assetActions,
      // currentUser,
      validation,
      search,
      asset,
      attachments
    } = this.props
    const formData = formattingQuery({
      id: asset.id,
      causeId: search.selected.cause.id,
      assetName: validation.assetName.value,
      attachments: attachments.filter(item => item.storeId)
    })

    if (formData.causeId && formData.assetName && validation.assetName.isValid) {
      assetActions.update(formData)
    } else {
      this.changeInput('causeName', search.selected.cause.name || '')
      this.changeInput('assetName', validation.assetName.value)
    }

  }


  handleChange({ target }) {
    const { assetActions } = this.props

    assetActions.edit({ 'assetName': target.value })
  }

  changeInput = (type, value) => {
    const { validation, assetActions, validRules } = this.props
    assetActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { assetActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        this.setState({
          isSearch: true
        })
        assetActions.search(type, { q: value })
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  drop = (files) => {
    const { assetActions } = this.props

    if (files && files[0]) {
      assetActions.attachFile(files)
    }
  }

  removeAttach = file => {
    const { assetActions } = this.props
    if(file.id) {
      assetActions.editAttach(file.id)
    } else {
      assetActions.removeAttach(file.preview)
    }
  }

  uploadFile = (uri, preview) => {
    const { assetActions } = this.props
    assetActions.uploadFile({ uri, preview })
  }

  render() {

    let { translate, validation, search, assetActions, attachments, asset } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Edit</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">
            <div className="form-group">
              <label className="col-lg-3 control-label">Cause name</label>
              <div className="col-lg-9">
                <Select
                  onClick={e => {
                    e.stopPropagation()
                    this.setState({ openSelect: !this.state.openSelect })
                  }}
                  isSearch={true}
                  isOpen={this.state.openSelect}
                  onSearch={e => this.onChangeSelectInput(e, 'cause')}
                  onSelect={value => {
                    assetActions.select({
                      cause: value
                    })
                  }}
                  items={search.cause}
                  value={search.selected.cause.name}
                />
                {!validation.causeName.isValid && validation.causeName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="causeName"
                  >
                    {translate(`causeError.${validation.causeName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Description documents of cause</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.assetName = ref : null}
                  type="text"
                  className="form-control"
                  name="assetName"
                  value={asset.assetName || ''}
                  onBlur={e => (this.assetName.value !== '' && validation.assetName.isValid === undefined) && this.changeInput('assetName', e.target.value)}
                  onChange={e => {
                    (validation.causeName.isValid !== undefined) && this.changeInput('assetName', e.target.value)
                    this.handleChange(e)
                  }}
                />
                {!validation.assetName.isValid && validation.assetName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="assetName"
                  >
                    {translate(`assetError.${validation.assetName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label"></label>
              <div className="col-lg-9">
                <Dropzone
                  onDrop={this.drop}
                  className="dropzone-asset"
                >
                  {attachments.length > 0 ? (
                    <div className="tokenfield">
                      {_.map(attachments, (item, i) => (
                        <FileLoad
                          id={item.id}
                          key={item.id || item.preview}
                          file={item}
                          uploadFile={this.uploadFile}
                          removeAttach={this.removeAttach}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="drop-text">
                      Click or drop files
                    </div>
                  )}
                </Dropzone>

              </div>
            </div>


            <div className="text-right">
              <button
                type="button"
                className="btn btn-primary"
                onClick={e => this.onSubmit(e)}
              >
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Edit
