import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as assetActions from 'store/actions/asset'
import { getField } from 'store/selectors/modelSelectors'

class CreateContainer extends Component {
  componentWillMount() {
    const { assetActions } = this.props
  
    assetActions.resetSelect()
  }

  render() {

    return (
      <Create { ...this.props } />
    )

  }

}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.asset.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}

  return {
    translate: getTranslate(state.locale),
    validation: state.asset.validation,   
    search: state.asset.search, 
    attachments: state.asset.attachments,       
    currentUser,
    validRules
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    assetActions: bindActionCreators(assetActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
