import List from './List'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import * as assetActions from 'store/actions/asset'

class ListContainer extends Component {

  componentDidMount() {
    const { filter } = this.props

    this.fetch(filter)
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props

    if(!_.isEqual(prevProps.filter, filter)) {
      this.fetch(this.props.filter)
    }
  }

  fetch = filter => {
    let { assetActions, workspaceActions } = this.props
    assetActions.get(filter)
    if(filter && filter.workspaceId) {
      workspaceActions.getId(filter.workspaceId)
    }    
  }

  render() {

    return (
      <List { ...this.props }/>
    )

  }

}

let mapStateToProps = (state, props) => {
  const currentWorkspace = state.workspace && state.workspace.edit
    ? state.workspace.edit
    : null
  const currentUser = state.auth && state.auth.asset ? state.auth.asset : null

  return {
    assets: state.asset.origin,
    filter: state.asset.filter,
    translate: getTranslate(state.locale),
    currentWorkspace,
    currentUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    assetActions: bindActionCreators(assetActions, dispatch),
    // workspaceActions: bindActionCreators(workspaceActions, dispatch),
    // workspaceUserActions: bindActionCreators(workspaceUserActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
