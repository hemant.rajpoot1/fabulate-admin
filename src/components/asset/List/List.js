import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

import _ from 'lodash'

import Filter from 'components/ui/Filter'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

import './List.css'

class AssetList extends Component {

  download = id => {
    const { assetActions } = this.props
    assetActions.downloadFile(id, this.assetDownload)
  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { assetActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { assetSearch: value !== '' ? value : undefined }
      assetActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { assetActions } = this.props
    assetActions.resetFilter()
  }

  renderSearchFilter = () => (
    <Filter
      searchItem="assets"
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.assetSearch || ''}
      resetFilters={this.resetFilters}
      customClasses="float-none m-10p-0"
    />
  )

  onRemove = (id, type) => {

    const {
      filter,
      assetActions,
      workspaceUserActions
    } = this.props

    let notice = new PNotify({
      title: 'Remove asset',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      if (type === 'asset') {
        assetActions.remove(id)
      } else {
        workspaceUserActions.remove({
          id,
          filter
        })
      }
    })

  }

  render() {

    const { assets } = this.props

    return (
      <div className="panel panel-flat">
        <a
          ref={(ref) => {
            this.assetDownload = ref
          }}
          className="downloadLink"
        >
          <Fragment> </Fragment>
        </a>
        <div className="panel-heading">

          <h5 className="panel-title">Asset List</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>

        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Description</th>
                <th>Brief name</th>
                <th>Attachments</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>

            <tbody>
              {
                _.map(assets, asset => (
                  <tr key={asset.id}>
                    <td>{asset.id}</td>
                    <td>{asset.assetName}</td>
                    <td>{asset.causeAsset ? asset.causeAsset.causeName : '-'}</td>
                    <td>
                      <ul className="document-wrapper">
                        {_.map(asset.mediaAsset, attach => (
                          <li key={attach.id} onClick={() => this.download(attach.storeId)}>
                            <a>{attach.mediaName}</a>
                          </li>
                        ))}
                      </ul>
                    </td>
                    <td className="text-center">
                      <ul className="icons-list">
                        <li className="dropdown">
                          <a className="dropdown-toggle" data-toggle="dropdown">
                            <i className="icon-menu9"></i>
                          </a>
                          <ul className="dropdown-menu dropdown-menu-right">
                            <li><Link to={`/asset/${asset.id}`}><i className="icon-pencil"></i>Edit</Link></li>

                            <li onClick={() => this.onRemove(asset.id, 'asset')}>
                              <a><i className="icon-cross"></i>Delete</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )

  }

}

export default AssetList
