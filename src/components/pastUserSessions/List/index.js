import React, { useEffect } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as pastUserSessionsActions from 'store/actions/pastUserSessions'
import { PastUserSessionsList } from './PastUserSessionsList'

import _ from 'lodash'

const PastUserSessions = (props) => {

  useEffect(() => {
    const { filter, page, pastUserSessionsActions } = props
    pastUserSessionsActions.get(filter, page)
  }, [props.filter, props.page])

  return (
    <PastUserSessionsList {...props} />
  )
}

const mapStateToProps = (state) => {
  return {
    sessions: state.pastUserSessions.origin,
    filter: _.get(state, 'pastUserSessions.filter', {}),
    sort: _.get(state, 'pastUserSessions.sort', {}),
    page: _.get(state, 'pastUserSessions.page', 1),
    count: _.get(state, 'pastUserSessions.count', 0),
    isGetFetching: _.get(state, 'pastUserSessions.isGetFetching')
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pastUserSessionsActions: bindActionCreators(pastUserSessionsActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PastUserSessions))