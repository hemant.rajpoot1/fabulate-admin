import React, { useState } from 'react'

import TimePicker from 'react-time-picker'
import DatePicker from 'react-datepicker'
import Pagination from 'react-js-pagination'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'

import _ from 'lodash'
import moment from 'moment'

import './pastSessions.css'

export const PastUserSessionsList = (props) => {

  const { sessions, filter, count, page } = props

  const [timer, setTimer] = useState()

  const setSearchFilter = ({ target }) => {
    const { value } = target
    const { pastUserSessionsActions, filter } = props
    if (timer) {
      clearTimeout(timer)
      setTimer(null)
    }
    setTimer(setTimeout(() => {
      pastUserSessionsActions.setFilter({
        ...filter,
        userName: value
      })
    }, 800))
  }

  const setFilter = (value, type) => {
    const { pastUserSessionsActions, filter } = props

    pastUserSessionsActions.setFilter({
      ...filter,
      [type]: value
    })
  }

  const fetchList = (page) => {
    const { pastUserSessionsActions } = props
    pastUserSessionsActions.setPage(page)
  }

  const resetFilter = () => {
    const { pastUserSessionsActions } = props
    pastUserSessionsActions.resetFilter()
  }

  const renderSearchFilter = () => (
    <div className='d-flex past-user-sessions'>
      <Filter
        searchItem="active users sessions"
        onChangeHandler={setSearchFilter}
        defaultValue={filter.userName || ''}
        resetFilters={resetFilter}
        customClasses="float-none mb-10"
      />
      <div className='date-time-wrapper mb-10'>
        <div className='login-date-time-wrapper'>
          <span className='date-time-item'>Login date/time:</span>
          <div className='d-flex'>
            <DatePicker
              selected={filter.loginDate}
              placeholderText='- -/- -/- - - -'
              onChange={(value) => setFilter(value, 'loginDate')}
              dateFormat="MMMM D, YYYY"
            />
            <TimePicker
              value={filter.loginTime}
              hourPlaceholder='- - '
              minutePlaceholder=' - -'
              onChange={(value) => setFilter(value, 'loginTime')}
              disableClock={true}
              clearIcon={false}
            />
          </div>
        </div>
        <div className='logout-date-time-wrapper'>
          <span className='date-time-item'>Logout date/time:</span>
          <div className='d-flex'>
            <DatePicker
              selected={filter.logoutDate}
              placeholderText='- -/- -/- - - -'
              onChange={(value) => setFilter(value, 'logoutDate')}
              dateFormat="MMMM D, YYYY"
            />
            <TimePicker
              value={filter.logoutTime}
              hourPlaceholder='- - '
              minutePlaceholder=' - -'
              onChange={(value) => setFilter(value, 'logoutTime')}
              disableClock={true}
              clearIcon={false}
            />
          </div>
        </div>
      </div>
    </div>
  )

  const renderTableBody = () => {
    const { isGetFetching } = props

    return isGetFetching
      ? (
        <tr>
          <td colspan='7'>
            <SpinnerLoadData isAbsolute={false} delay="0" />
          </td>
        </tr>
      )
      : (
        _.map(sessions, item => {
          const { id, userName, userMail, createdAt, logoutAt } = item

          return (
            <tr className="text-center-info" key={id}>
              <td>{userName}</td>
              <td>{userMail}</td>
              <td>{moment(createdAt).format('D MMMM, YYYY, HH:mm')}</td>
              <td>{moment(logoutAt).format('D MMMM, YYYY, HH:mm')}</td>
              <td>{moment(createdAt).utc().format('D MMMM, YYYY, HH:mm')}</td>
              <td>{moment(logoutAt).utc().format('D MMMM, YYYY, HH:mm')}</td>
              <td>{moment.utc(moment(logoutAt) - moment(createdAt)).format('HH : mm : ss')}</td>
            </tr>
          )
        })
      )
  }

  return <div className="panel panel-flat">
    <div className="panel-heading">

      <h5 className="panel-title">Past User Sessions</h5>
      <div className="heading-elements">
        <ul className="icons-list">
          <li>
            <a data-action="collapse">&nbsp;</a>
          </li>
        </ul>
      </div>
    </div>
    {renderSearchFilter()}
    <div>
      <table className="table datatable-select-basic">
        <thead>
          <tr>
            <th>User name</th>
            <th>User mail</th>
            <th>Login date/time</th>
            <th>Logout date/time</th>
            <th>Login date/time (UTC)</th>
            <th>Logout date/time (UTC)</th>
            <th>Session duration</th>
          </tr>
        </thead>
        <tbody>
          {renderTableBody()}
        </tbody>
      </table>
      {count > 20 &&
        <div className="pager p-20">
          <Pagination
            hideNavigation
            pageRangeDisplayed={5}
            activePage={page}
            itemsCountPerPage={20}
            totalItemsCount={count}
            onChange={(page) => fetchList(page)}
          />
        </div>
      }
    </div>
  </div>
}