import PublicationHistoryCreate from './PublicationHistoryCreate'

import React, { useEffect } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'

import * as publicationHistoryActions from 'store/actions/publicationHistory'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

const PublicationHistoryCreateContainer = (props) => {
  const change = (type, value) => {
    const { validation, publicationHistoryActions, validRules } = props

    publicationHistoryActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }

  useEffect(() => {
    return () => {
      change('publicationName', '')
      change('image', '')
    }
  }, [])

  const onSubmit = () => {
    const { publicationHistoryActions, validation } = props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }
    if (formData.publicationName) {
      publicationHistoryActions.create(formData)
    } else {
      change('publicationName', validation.publicationName.value)
    }
  }

  return (
    <PublicationHistoryCreate
      {...props}
      change={change}
      onSubmit={onSubmit}
    />
  )
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.publicationHistory.validation))
  const currentUser = _.get(state, 'auth.user', {})
  return {
    validation: state.publicationHistory.validation,
    currentUser,
    validRules,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    publicationHistoryActions: bindActionCreators(publicationHistoryActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PublicationHistoryCreateContainer))
