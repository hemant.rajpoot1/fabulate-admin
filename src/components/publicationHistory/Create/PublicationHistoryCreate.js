import React from 'react'

const PublicationHistoryCreate = (props) => {

  const { change, onSubmit, validation } = props
  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Create</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li><a data-action="collapse">&nbsp;</a></li>
          </ul>
        </div>
      </div>

      <div className="panel-body">
        <div className="form-horizontal">
          <div className="form-group">
            <label className="col-lg-3 control-label">Publication name</label>
            <div className="col-lg-9">
              <input
                className="form-control"
                type="text"
                name="publicationName"
                value={validation.publicationName.value}
                onChange={e => change('publicationName', e.target.value)}
              />
              {!validation.publicationName.isValid && validation.publicationName.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="name"
                >
                    Publication name can not be null
                </label>
              )}
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">Publication image</label>
            <div className="col-lg-9">
              <input
                className="form-control"
                type="text"
                name="image"
                value={validation.image.value}
                onChange={e => change('image', e.target.value)}
              />
              {!validation.image.isValid && validation.image.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="name"
                >
                  {validation.image.message}
                </label>
              )}
            </div>
          </div>
        </div>
        <div className="text-right">
          <button className="btn btn-primary" onClick={onSubmit} >create publication</button>
        </div>
      </div>
    </div>
  )
}

export default PublicationHistoryCreate
