import PublicationHistory from './PublicationHistory'

import _ from 'lodash'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as publicationHistoryActions from 'store/actions/publicationHistory'

class PublicationHistoryContainer extends Component {

  componentDidMount() {
    const { filter } = this.props
    
    this.fetch(filter)
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props

    if (!_.isEqual(prevProps.filter.publicationName, filter.publicationName)) {
      this.fetch(filter)
    }
  }

  fetch = filter => {
    let { publicationHistoryActions } = this.props

    publicationHistoryActions.get(filter)
  }

  fetchList = (page) => {
    const { publicationHistoryActions } = this.props
    publicationHistoryActions.get({
      page
    })
  }

  render() {
    return (
      <PublicationHistory
        {...this.props }
        fetchList={this.fetchList}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    publicationHistoryList: state.publicationHistory.list,
    isGetFetching: state.publicationHistory.isGetFetching,
    count: state.publicationHistory.count,
    page: state.publicationHistory.page,
    translate: getTranslate(state.locale),
    filter: state.publicationHistory.filter,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    publicationHistoryActions: bindActionCreators(publicationHistoryActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PublicationHistoryContainer))
