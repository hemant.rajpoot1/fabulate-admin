/* eslint-disable indent */
import React, { useRef } from 'react'
import { Link } from 'react-router-dom'

import _ from 'lodash'
import Pagination from 'react-js-pagination'

import Filter from 'components/ui/Filter'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

const PublicationHistory = props => {

  const timer = useRef()

  const onDelete = id => {
    const {
      publicationHistoryActions
    } = props

    const notice = new PNotify({
      title: 'Remove publication',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      publicationHistoryActions.remove(id)
    })
  }

  const setSearchFilter = ({ target: { value } }) => {
    const { publicationHistoryActions } = props

    timer.current && clearTimeout(timer.current)

    timer.current = setTimeout(() => {
      const query = { publicationName: value !== '' ? value : undefined }
      publicationHistoryActions.setFilter(query)
    }, 800)
  }

  const resetFilters = () => {
    const { publicationHistoryActions } = props
    publicationHistoryActions.resetFilter()
  }

  const renderSearchFilter = () => (
    <Filter
      searchItem="publications"
      onChangeHandler={setSearchFilter}
      defaultValue={props.filter.publicationName || ''}
      resetFilters={resetFilters}
      customClasses="float-none m-10p-0"
    />
  )

  const renderTable = () => {
    const {
      isGetFetching,
      publicationHistoryList,
    } = props

    if (isGetFetching) {
      return <SpinnerLoadData isAbsolute={false} delay="0" />
    }
    return (
      _.map(publicationHistoryList, publication =>
        (
          <tr key={publication.id}>
            <td>{publication.id}</td>
            <td>{publication.publicationName}</td>
            <td>
              <div>
                {publication.image
                  ? <a href={publication.image} target="_blank">{publication.image}</a>
                  : null}
              </div>
            </td>
            <td className="text-center">
              <ul className="icons-list">
                <li className="dropdown">
                  <a className="dropdown-toggle" data-toggle="dropdown">
                    <i className="icon-menu9"></i>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li><Link to={`/publication-history/${publication.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                    <li onClick={() => onDelete(publication.id)}>
                      <a><i className="icon-cross"></i>Delete</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </td>
          </tr>
        )
      )
    )
  }

  const renderPagination = () => {
    const {
      count,
      page,
      fetchList,
      isGetFetching,
    } = props

    return (
      isGetFetching
      ? null
      : count > 20 && (
        <div className="pager p-20">
          <Pagination
            hideNavigation
            pageRangeDisplayed={3}
            activePage={page}
            itemsCountPerPage={20}
            totalItemsCount={count}
            onChange={(page) => fetchList(page)}
          />
        </div>
      )
    )
  }

  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Publication history list</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li>
              <a data-action="collapse">&nbsp;</a>
            </li>
          </ul>
        </div>
      </div>

      <div>
        {renderSearchFilter()}
        <table className="table datatable-select-basic">
          <thead>
            <tr>
              <th>#</th>
              <th>Publication name</th>
              <th>Image</th>
              <th className="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            {renderTable()}
          </tbody>
        </table>
        {renderPagination()}
      </div>
    </div>
  )
}

export default PublicationHistory