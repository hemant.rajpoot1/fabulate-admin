import Edit from './PublicationHistoryEdit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as publicationHisrotyActions from 'store/actions/publicationHistory'

class EditContainer extends Component {

  componentDidMount() {
    const { publicationHisrotyActions } = this.props
    const { id } = this.props.match.params
    publicationHisrotyActions.getId(id)
  }

  render() {
    return (
      <Edit { ...this.props } />
    )
  }
}

let mapStateToProps = (state, props) => {
  return {
    publicationHistory: state.publicationHistory.edit,
    isGetIdFetching: state.publicationHistory.isGetIdFetching,
    translate: getTranslate(state.locale),
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    publicationHisrotyActions: bindActionCreators(publicationHisrotyActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
