import React from 'react'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

const Edit = (props) => {

  const handleChange = ({ target }) => {
    const { publicationHisrotyActions } = props
    publicationHisrotyActions.edit({ [target.name]: target.value })
  }

  const onSubmit = () => {
    const { publicationHisrotyActions, publicationHistory } = props
    const request = {
      id: publicationHistory.id,
      publicationName: publicationHistory.publicationName,
      image: publicationHistory.image
    }
    publicationHisrotyActions.update(request)
  }

  const { publicationHistory, isGetIdFetching } = props
  if (isGetIdFetching) {
    return <SpinnerLoadData />
  }
  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Edit</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li>
              <a data-action="collapse">&nbsp;</a>
            </li>
          </ul>
        </div>
      </div>

      <div className="panel-body">
        <form className="form-horizontal">
          <div className="form-group">
            <label className="col-lg-3 control-label">Name</label>
            <div className="col-lg-9">
              <input
                type="text"
                className="form-control"
                name="publicationName"
                value={publicationHistory.publicationName || ''}
                onChange={e => handleChange(e)}
              />
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">Publication image</label>
            <div className="col-lg-9">
              <input
                className="form-control"
                type="text"
                name="image"
                value={publicationHistory.image}
                onChange={e => handleChange(e)}
              />
            </div>
          </div>

          <div className="text-right">
            <button
              type="button"
              className="btn btn-primary"
              onClick={onSubmit}
            >
              Update
            </button>
          </div>
        </form>
      </div>
    </div>
  )

}

export default Edit
