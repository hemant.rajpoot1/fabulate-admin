import React, { useMemo } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import Pagination from 'react-js-pagination'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'

import { selectValueByField } from 'helpers/tools'

const ItemList = props => {
  const {
    id,
    amplifyProductId,
    buyerPrice,
    amplifyPrice,
    fabulateCommission,
    onDelete,
    amplifyProducts,
    workspace
  } = props

  const currentAmplifyProductName = useMemo(() => {
    const currentAmplifyProduct = selectValueByField(amplifyProducts, amplifyProductId, 'id')
    return _.get(currentAmplifyProduct, 'name', '-')
  }, [amplifyProducts, amplifyProductId])

  return (
    <tr key={id} className="amplify-workspace-product-list-item">
      <td>{id}</td>
      <td>{workspace.name}</td>
      <td>{currentAmplifyProductName}</td>
      <td>{buyerPrice}</td>
      <td>{amplifyPrice}</td>
      <td>{fabulateCommission}</td>
      <td className="text-center analytics-project-list-item-actions">
        <ul className="icons-list">
          <li className="dropdown">
            <a className="dropdown-toggle" data-toggle="dropdown">
              <i className="icon-menu9"></i>
            </a>
            <ul className="dropdown-menu dropdown-menu-right">
              <li>
                <Link
                  to={`/amplify-workspace-product/${id}`}
                >
                  <i className="icon-pencil"></i>
                  Edit
                </Link>
              </li>
              <li
                onClick={() => onDelete(id)}
              >
                <a>
                  <i className="icon-cross"></i>Delete
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </td>
    </tr>
  )
}

const List = props => {
  const { 
    amplifyWorkspaceProducts,
    amplifyProducts,
    count,
    page,
    onDelete,
    fetchList,
    isGetFetching,
    amplifyWorkspaceProductActions
  } = props

  const renderedItemsList = useMemo(() => {
    return _.map(amplifyWorkspaceProducts,
      product => {
        return <ItemList key={product.id} amplifyProducts={amplifyProducts} {...product} onDelete={onDelete} />
      }
    )
  }, [amplifyWorkspaceProducts, amplifyProducts])
  
  const setSearchFilter = ({ target }) => {
    const { value } = target
    
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { amplifyProductSearch: value !== '' ? value : undefined }
      amplifyWorkspaceProductActions.setFilter(query)
    }, 800)
  }

  const resetFilters = () => {
    amplifyWorkspaceProductActions.resetFilter()
  }

  return isGetFetching ? <SpinnerLoadData/> : (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Amplify workspace products list</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li>
              <a data-action="collapse">&nbsp;</a>
            </li>
          </ul>
        </div>
      </div>

      <div className="panel-body">
        <Filter
          searchItem="amplify workspace product"
          onChangeHandler={setSearchFilter}
          defaultValue={props.filter.amplifyProductSearch || ''}
          resetFilters={resetFilters}
          customClasses="float-none m-10p-0"
        />
        <table className="table datatable-select-basic">
          <thead>
            <tr>
              <th>#</th>
              <th>Workspace</th>
              <th>Amplify Product</th>
              <th>Buyer Price</th>
              <th>Amplify Price</th>
              <th>Fabulate Commission</th>
              <th className="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            {renderedItemsList}
          </tbody>
        </table>

        {count > 20 && (
          <div className="pager p-20">
            <Pagination
              hideNavigation
              pageRangeDisplayed={3}
              activePage={page}
              itemsCountPerPage={20}
              totalItemsCount={count}
              onChange={(page) => fetchList(page)}
            />
          </div>
        )}
      </div>
    </div>
  )
}

export default List