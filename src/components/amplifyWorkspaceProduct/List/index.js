import List from './List'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import _ from 'lodash'

import * as amplifyProductActions from 'store/actions/amplifyProduct'
import * as amplifyWorkspaceProductActions from 'store/actions/amplifyWorkspaceProduct'

class ListContainer extends Component {

  componentDidMount() {
    const { 
      amplifyWorkspaceProductActions,
      amplifyProductActions,
      page,
    } = this.props

    amplifyWorkspaceProductActions.get({ page })
    amplifyProductActions.get()
  }
  componentDidUpdate(prevProps){
    const { filter } = this.props

    if(!_.isEqual(prevProps.filter, filter)){
      this.fetch(filter)
    }
  }

  fetch = filter => {
    const { amplifyWorkspaceProductActions } = this.props

    amplifyWorkspaceProductActions.get(filter)
  }


  fetchList = (page) => {
    const { amplifyWorkspaceProductActions } = this.props
    amplifyWorkspaceProductActions.get({ page })
  }

  onDelete = id => {
    const { amplifyWorkspaceProductActions, amplifyWorkspaceProducts, page } = this.props

    const notice = new PNotify({
      title: 'Remove amplify workspace product',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    const size = _.size(amplifyWorkspaceProducts)
    
    const data = { id, size, page }

    notice.get().on('pnotify.confirm', () => {
      amplifyWorkspaceProductActions.remove(data)
    })
  }

  render() {
    return (
      <List 
        {...this.props}
        onDelete={this.onDelete}
        fetchList={this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    amplifyWorkspaceProducts: state.amplifyWorkspaceProduct.origin,
    amplifyProducts: state.amplifyProduct.origin,
    count: state.amplifyWorkspaceProduct.count,
    page: state.amplifyWorkspaceProduct.page,
    isGetFetching: state.amplifyWorkspaceProduct.isGetFetching,
    filter: _.get(state, 'amplifyWorkspaceProduct.filter', {})
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    amplifyWorkspaceProductActions: bindActionCreators(amplifyWorkspaceProductActions, dispatch),
    amplifyProductActions: bindActionCreators(amplifyProductActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
