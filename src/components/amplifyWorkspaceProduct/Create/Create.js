import React, { useMemo } from 'react'
import _ from 'lodash'

import Select from 'components/ui/Select'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

import { selectValueByField } from 'helpers/tools'

this.timer = null
const Create = props => {
  const { 
    validation,
    workspaces,
    amplifyProducts,
    change,
    onSubmit,
    isGetIdFetching,
    amplifyProductActions,
    workspaceActions
  } = props
  
  const amplifyProductId = _.get(validation, 'amplifyProductId')
  const workspaceId = _.get(validation, 'workspaceId')

  const currentWorkspace = useMemo(() => {
    return selectValueByField(workspaces, workspaceId.value, 'id')
  }, [workspaces, workspaceId])

  const currentAmplifyProduct = useMemo(() => {
    return selectValueByField(amplifyProducts, amplifyProductId.value, 'id')
  }, [amplifyProducts, amplifyProductId])

  const onChangeAmplifyProductSelectInput = e => {
    const { value } = e.target

    clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      amplifyProductActions.get({ name: value })
    }, 300)
  }

  const onChangeWorkspaceSelectInput = e => {
    const { value } = e.target

    clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      workspaceActions.get({ name: value })
    }, 300)
  }

  return isGetIdFetching ? <SpinnerLoadData /> : (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Add amplify product to workspace</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li><a data-action="collapse">&nbsp;</a></li>
          </ul>
        </div>
      </div>

      <div className="panel-body">
        <div className="form-horizontal">

          <div className="form-group">
            <label className="col-lg-3 control-label">
              Workspace
            </label>
            <div className="col-lg-9">
              <Select
                isSearch={true}
                onSearch={onChangeWorkspaceSelectInput}
                items={workspaces}
                value={_.get(currentWorkspace, 'name', null)}
                inputName={'workspaceId'}
                onSelect={item => change('workspaceId', item.id)}
                cantReset
              />
              {!validation.workspaceId.isValid && validation.workspaceId.message && (
                <label
                  id="workspaceId-error"
                  className="validation-error-label"
                  forhtml="workspaceId"
                >
                  {validation.workspaceId.message}
                </label>
              )}
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">
              Amplify product
            </label>
            <div className="col-lg-9">
              <Select
                isSearch={true}
                onSearch={onChangeAmplifyProductSelectInput}
                items={amplifyProducts}
                value={_.get(currentAmplifyProduct, 'name', null)}
                inputName={'amplifyProductId'}
                onSelect={item => change('amplifyProductId', item.id)}
                cantReset
              />
              {!validation.amplifyProductId.isValid && validation.amplifyProductId.message && (
                <label
                  id="amplifyProductId-error"
                  className="validation-error-label"
                  forhtml="amplifyProductId"
                >
                  {validation.amplifyProductId.message}
                </label>
              )}
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">Fabulate commission</label>
            <div className="col-lg-9">
              <input
                className="form-control"
                type="number"
                name="fabulateCommission"
                value={validation.fabulateCommission.value}
                onChange={({target}) => change('fabulateCommission', Number(target.value))}
              />
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">Buyer price</label>
            <div className="col-lg-9">
              <input
                className="form-control"
                type="number"
                name="buyerPrice"
                value={validation.buyerPrice.value || ''}
                onChange={({target}) => change('buyerPrice', Number(target.value))}
              />
            </div>
          </div> 

          <div className="form-group">
            <label className="col-lg-3 control-label">Amplify price</label>
            <div className="col-lg-9">
              <input
                className="form-control"
                type="number"
                name="amplifyPrice"
                value={validation.amplifyPrice.value || ''}
                onChange={({target}) => change('amplifyPrice', Number(target.value))}
              />
            </div>
          </div>

        </div>

        <div className="text-right">
          <button className="btn btn-primary" onClick={onSubmit}>add Amplify product to workspace</button>
        </div>
      </div>
    </div>
  )
}

export default Create