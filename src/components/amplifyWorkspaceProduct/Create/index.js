import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import PNotify from 'pnotify'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

import * as amplifyWorkspaceProductActions from 'store/actions/amplifyWorkspaceProduct'
import * as amplifyProductActions from 'store/actions/amplifyProduct'
import * as workspaceActions from 'store/actions/workspace'

class CreateContainer extends Component {
  componentDidMount() {
    const { 
      workspaceActions,
      amplifyProductActions,
      amplifyProducts,
    } = this.props

    const workspaceFilter = {
      withoutLimit: true
    }

    workspaceActions.get(workspaceFilter)
    !_.size(amplifyProducts) && amplifyProductActions.get()
  }

  change = (name, value) => {
    const { validation, amplifyWorkspaceProductActions, validRules } = this.props
    amplifyWorkspaceProductActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      true
    )
  }

  onSubmit = () => {
    const { amplifyWorkspaceProductActions, validation } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }

    const isFieldsFilled = _.every(formData, element => !!element)
    if (isFieldsFilled) {
      amplifyWorkspaceProductActions.create(formData)
    } else {
      return new PNotify({
        type: 'error',
        title: 'Error',
        text: 'Please, correctly fill all fields'
      })  
    }
  }

  render() {
    return (
      <Create 
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.amplifyWorkspaceProduct.validation))

  return {
    validation: state.amplifyWorkspaceProduct.validation,
    workspaces: state.workspace.origin,
    amplifyProducts: state.amplifyProduct.origin,
    validRules,
    isGetIdFetching: state.amplifyWorkspaceProduct.isGetIdFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    amplifyWorkspaceProductActions: bindActionCreators(amplifyWorkspaceProductActions, dispatch),
    amplifyProductActions: bindActionCreators(amplifyProductActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
