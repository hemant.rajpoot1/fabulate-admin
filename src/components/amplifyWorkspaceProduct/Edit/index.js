import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import PNotify from 'pnotify'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

import * as amplifyWorkspaceProductActions from 'store/actions/amplifyWorkspaceProduct'
import * as workspaceActions from 'store/actions/workspace'
import * as amplifyProductActions from 'store/actions/amplifyProduct'

class CreateContainer extends Component {

  componentDidMount() {
    const { 
      workspaceActions,
      amplifyWorkspaceProductActions,
      amplifyProductActions,
      amplifyProducts
    } = this.props

    const  { id } = this.props.match.params

    const workspaceFilter = {
      withoutLimit: true
    }
    
    workspaceActions.get(workspaceFilter)
    !_.size(amplifyProducts) && amplifyProductActions.get()

    amplifyWorkspaceProductActions.getId(id)
  }

  change = (name, value) => {
    const { validation, amplifyWorkspaceProductActions, validRules } = this.props
    amplifyWorkspaceProductActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      false
    )
  }

  onSubmit = () => {
    const { amplifyWorkspaceProductActions, validation } = this.props
    const  { id } = this.props.match.params
    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
      id
    }

    const isFieldsFilled = _.every(formData, element => !!element)
    if (isFieldsFilled) {
      amplifyWorkspaceProductActions.update(formData)
    } else {
      return new PNotify({
        type: 'error',
        title: 'Error',
        text: 'Please, correctly fill all fields'
      })  
    }
  }

  render() {
    return (
      <Edit 
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = state => {
  const validRules = getField(state, Object.keys(state.amplifyWorkspaceProduct.validation))
  return {
    validation: state.amplifyWorkspaceProduct.validation,
    workspaces: state.workspace.origin,
    amplifyProducts: state.amplifyProduct.origin,
    validRules,
    isGetIdFetching: state.amplifyWorkspaceProduct.isGetIdFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    amplifyWorkspaceProductActions: bindActionCreators(amplifyWorkspaceProductActions, dispatch),
    amplifyProductActions: bindActionCreators(amplifyProductActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
