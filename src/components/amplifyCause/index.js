import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'

import _ from 'lodash'

import * as amplifyCauseActions from 'store/actions/amplifyCause'

import { List } from './List'

const AmplifyCauseContainer = (props) => {

  const { amplifyCauseActions, filter } = props

  useEffect(() => {
    amplifyCauseActions.get()
  }, [])

  useEffect(() => {
    amplifyCauseActions.get({...filter})  
  }, [filter])

  const fetchList = page => {    
    const filter = {
      page,
    }
    amplifyCauseActions.get({...filter})
  }

  return <List {...props} fetchList={fetchList} />

}

const mapStateToProps = (state) => ({
  origin: _.get(state, 'amplifyCause.origin', {}),
  filter: _.get(state, 'amplifyCause.filter', {}),
  isGetFetching: _.get(state, 'amplifyCause.isGetFetching')
})
    
const mapDispatchToProps = (dispatch) => ({
  amplifyCauseActions: bindActionCreators(amplifyCauseActions, dispatch),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AmplifyCauseContainer))
