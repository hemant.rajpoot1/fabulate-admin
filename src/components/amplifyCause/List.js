import React, { useRef } from 'react'
import { Link } from 'react-router-dom'
import Pagination from 'react-js-pagination'

import _ from 'lodash'
import moment from 'moment'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'

export const List = (props) => {
  const { origin, fetchList, filter, isGetFetching } = props

  const page = useRef(1)

 const setSearchFilter = ({ target }) => {    
    const { value } = target
    const { amplifyCauseActions } = props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { amplifyCauseName: value !== '' ? value : undefined }
      amplifyCauseActions.setFilter(query)
    }, 800)
  }

const  resetFilters = () => {
    const { amplifyCauseActions } = props
    amplifyCauseActions.resetFilter()
  }

  if(isGetFetching) {
    return <SpinnerLoadData />
  }

  return <div className="panel panel-flat">
    <div className="panel-heading">

      <h5 className="panel-title">Amplify Briefs List</h5>

      <div className="heading-elements">
        <ul className="icons-list">
          <li>
            <a data-action="collapse">&nbsp;</a>
          </li>
        </ul>
      </div>
    </div>

    <Filter
      searchItem="Amplify Brief"
      onChangeHandler={setSearchFilter}
      defaultValue={filter.amplifyCauseName || ''}
      resetFilters={resetFilters}
      customClasses="float-none m-10p-0"
    />

    <div >
      <table className="table datatable-select-basic" >
        <thead>
          <tr>
            <th>Brief id</th>
            <th>Amplify Brief id</th>
            <th>Name</th>
            <th>Hired contractor</th>
            <th>Owner</th>
            <th>Xero invoice id</th>
            <th>Xero bill Id</th>
            <th>Xero invoice status</th>
            <th>Xero bill status</th>
            <th>Due date for invoice</th>
            <th>Due date for bill</th>
            <th>Organization</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>

        <tbody>
          {origin.count > 0 && _.map(origin.rows, (item, index) => {
            const { amplifyCauseInfo, amplifyBuyerCause, amplifyWorkspaceCause } = item
            const { causeContractor } = amplifyCauseInfo

            return <tr key={item.id + index}>
              <td>{amplifyCauseInfo.id || '-'}</td>
              <td>{item.id || '-'}</td>
              <td>{item.amplifyCauseName || '-'}</td>
              <td>{causeContractor.userName || '-'}</td>
              <td>{amplifyBuyerCause.userName || '-'}</td>
              <td>{item.xeroInvoicesId || '-'}</td>
              <td>{item.xeroBillId || '-'}</td>
              <td>{item.status || '-'}</td>
              <td>{item.xeroBillStatus || '-'}</td>
              <td>{moment(item.campaignStart).format('MMMM Do YYYY, h:mm:ss a')}</td>
              <td>{moment(item.campaignDeadline).format('MMMM Do YYYY, h:mm:ss a')}</td>
              <td>{amplifyWorkspaceCause.name || '-'}</td>
              <td className="text-center">
                <ul className="icons-list">
                  <li className="dropdown">
                    <a className="dropdown-toggle" data-toggle="dropdown">
                      <i className="icon-menu9"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-right">
                      <li>
                        <Link to={`/amplify-cause/${item.id}`}><i className="icon-pencil"></i>Edit</Link>
                      </li>
                    </ul>
                  </li>
                </ul>
              </td>
            </tr>
          })}
        </tbody>
      </table>
    </div>
    {origin.count > 20 &&
      <div className="pager p-20">
        <Pagination
          hideNavigation
          pageRangeDisplayed={3}
          activePage={page.current}
          itemsCountPerPage={20}
          totalItemsCount={origin.count}
          onChange={valuePage => {
            page.current = valuePage
            fetchList(valuePage) 
          }}
        />
      </div>
    }
  </div>
}
