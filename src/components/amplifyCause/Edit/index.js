import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'

import _ from 'lodash'

import * as amplifyCauseActions from 'store/actions/amplifyCause'

import { Edit } from './Edit'

const AmplifyCauseContainer = (props) => {

  const { amplifyCauseActions, match, validationEdit } = props
  const id = match.params.id
  
  useEffect(() => {
    amplifyCauseActions.getId({id})
  }, [])

  const change = (type, value) => {
    amplifyCauseActions.change({
      ...validationEdit[type], value , isValid: value ? true : false, type
    })
  }

  const onUpdate = () => {
    const campaignDeadline = validationEdit.campaignDeadline.value 
    const campaignStart = validationEdit.campaignStart.value
    if(campaignDeadline && campaignStart) {
      amplifyCauseActions.update(id, {
        campaignDeadline,
        campaignStart
      })  
    }
}

  return <Edit {...props} onUpdate={onUpdate} change={change} />

}

const mapStateToProps = (state) => ({
  validationEdit: _.get(state, 'amplifyCause.validationEdit', null)
})
    
const mapDispatchToProps = (dispatch) => ({
  amplifyCauseActions: bindActionCreators(amplifyCauseActions, dispatch),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AmplifyCauseContainer))
