import React, { useState } from 'react'
import DatePicker from 'react-datepicker'
import { Link } from 'react-router-dom'

import _ from 'lodash'
import moment from 'moment'

import 'react-datepicker/dist/react-datepicker.css'

const amplifyCauseStatus = [ 
  'Active',
  'Awaiting Content',
  'Content Ready',
  'Completed',
  'Archived',
  'Declined',
]

export const Edit = (props) => {
  const [newCampaignDeadline, setNewCampaignDeadline] = useState()
  const [isChangeCampaignDeadline, setIsChangeCampaignDeadline] = useState(false)

  const [newCampaignStart, setNewCampaignStart] = useState()
  const [isChangeCampaignStart, setIsChangeCampaignStart] = useState(false)

  const { onUpdate, change, validationEdit: { status, owner, products, campaignDeadline, campaignStart, createdAt, updatedAt } } = props  

  const onCampaignDeadline = (date) => {
    setNewCampaignDeadline(date)
    setIsChangeCampaignDeadline(false)
    change('campaignDeadline', date)
  }

  const onCampaignStart = (date) => {
    setNewCampaignStart(date)
    setIsChangeCampaignStart(false)
    change('campaignStart', date)
  }

  return  <div className="panel panel-flat">
    <div className="panel-heading">
      <h5 className="panel-title">Edit</h5>
      <div className="heading-elements">
        <ul className="icons-list">
          <li><a data-action="collapse">&nbsp;</a></li>
        </ul>
      </div>
    </div>

    <div className="panel-body">
      <div className="form-horizontal">

        <div className="form-group">
          <label className="col-lg-3 control-label">Status</label>
          <div className="col-lg-9">
            <span className="form-control">{amplifyCauseStatus[status.value]}</span>
          </div>
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">Campaign Start</label>
          <div className="col-lg-9">
            {isChangeCampaignStart ? 
              <DatePicker
                selected={newCampaignStart}
                onChange={date => onCampaignStart(date)}
                dateFormat="MMMM D, YYYY"
              /> : 
              <span className="form-control" onClick={() => setIsChangeCampaignStart(true)}>
                {moment(campaignStart.value).format('MMMM Do YYYY')}
              </span>
            } 
          </div>
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">Campaign Deadline</label>
          <div className="col-lg-9">
            {isChangeCampaignDeadline ? 
              <DatePicker
                selected={newCampaignDeadline}
                onChange={date => onCampaignDeadline(date)}
                dateFormat="MMMM D, YYYY"
              /> : 
              <span className="form-control" onClick={() => setIsChangeCampaignDeadline(true)}>
                {moment(campaignDeadline.value).format('MMMM Do YYYY')}
              </span>
            }   
          </div>
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">CreatedAt</label>
          <div className="col-lg-9">
            <span className="form-control">{moment(createdAt.value).format('MMMM Do YYYY, h:mm:ss a')}</span>
          </div>
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">UpdatedAt</label>
          <div className="col-lg-9">
            <span className="form-control">{moment(updatedAt.value).format('MMMM Do YYYY, h:mm:ss a')}</span>
          </div>
        </div>

        <div className="form-group">
          <label className="col-lg-3 control-label">Owner</label>
          <div className="col-lg-9">
            <span className="form-control"><Link to={`/user/${owner.valueId}`}>{owner.value}</Link></span>
          </div>
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">Products</label>
          <div className="col-lg-9">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">№</th>
                  <th scope="col">#</th>
                  <th scope="col">Count</th>
                  <th scope="col">Product Display Name</th>
                  <th scope="col">createdAt</th>
                  <th scope="col">updatedAt</th>
                </tr>
              </thead>
              <tbody>
                {products.value && _.map(products.value, (item, index) => {
                    
                  return <tr key={item.id}>
                    <th>{index + 1}</th>  
                    <th>{item.id}</th>
                    <td>{item.count}</td>
                    <td><Link to={`/product-card/${item.id}`}>{item.productDisplayName}</Link></td>
                    <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm:ss a')}</td>
                    <td>{moment(item.updatedAt).format('MMMM Do YYYY, h:mm:ss a')}</td>
                  </tr>
                })}                 
              </tbody>
            </table>
          </div>
        </div>
        <div className="text-right">
          <button
            type="button"
            className="btn btn-primary"
            onClick={onUpdate}
          >
              Update
          </button>
        </div>  
      </div>
    </div>
</div>
}
