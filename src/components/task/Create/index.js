import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as taskActions from 'store/actions/task'
import { getField } from 'store/selectors/modelSelectors'

class CreateContainer extends Component {
  componentDidMount() {
    const { taskActions } = this.props

    taskActions.resetSelect()
  }

  render() {

    return (
      <Create { ...this.props } />
    )

  }

}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.task.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}

  return {
    translate: getTranslate(state.locale),
    validation: state.task.validation,  
    search: state.task.search,         
    currentUser,
    validRules
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    taskActions: bindActionCreators(taskActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
