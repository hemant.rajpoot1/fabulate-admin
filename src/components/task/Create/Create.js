import React, { Component } from 'react'
import { formattingQuery } from 'helpers/tools'

import Select from 'components/ui/Select'

class Create extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openSelect: undefined
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.windowClick)
  }

  windowClick = () => {
    if(this.state.openSelect) {
      this.setState({openSelect: undefined})
    }
  }

  onSubmit = e => {

    const {
      taskActions,
      validation,
      search
    } = this.props
    const formData = formattingQuery({
      taskName: validation.taskName.value,
      causeId: search.selected.cause.id,
      userId: search.selected.user.id
    })

    if (formData.taskName && validation.taskName.isValid
      && formData.causeId
      && formData.userId
    ) {
      taskActions.create(formData)
    } else {
      this.changeInput('taskName', this.taskName.value)
      this.changeInput('causeName', search.selected.cause.name || '')
      this.changeInput('userName', search.selected.user.name || '')
    }

  }

  changeInput = (type, value) => {
    const { validation, taskActions, validRules } = this.props
    taskActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { taskActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        this.setState({
          isSearch: true
        })
        taskActions.search(type, { q: value })
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  setOpenSelect = type => {
    let nextOpenSelect = this.state.openSelect

    if(nextOpenSelect === type) {
      nextOpenSelect = undefined
    } else {
      nextOpenSelect = type
    }

    this.setState({openSelect: nextOpenSelect})
  }

  render() {

    let { translate, validation, taskActions, search } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Create</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Task name</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.taskName = ref : null}
                  type="text"
                  className="form-control"
                  name="taskName"
                  onBlur={e => (this.taskName.value !== '' && validation.taskName.isValid === undefined) && this.changeInput('taskName', e.target.value)}
                  onChange={e => (validation.taskName.isValid !== undefined) && this.changeInput('taskName', e.target.value)}
                />
                {!validation.taskName.isValid && validation.taskName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="taskName"
                  >
                    {translate(`taskError.${validation.taskName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Brief name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={(e) => {
                    e.stopPropagation()
                    this.setOpenSelect('cause')
                  }}
                  onSearch={e => this.onChangeSelectInput(e, 'cause')}
                  onSelect={value => {
                    taskActions.select({
                      cause: value
                    })
                  }}
                  isOpen={this.state.openSelect === 'cause'}
                  items={search.cause}
                  value={search.selected.cause.name}
                />
                {!validation.causeName.isValid && validation.causeName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="causeName"
                  >
                    {translate(`causeError.${validation.causeName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">User name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={e => {
                    e.stopPropagation()
                    this.setOpenSelect('user')
                  }}
                  onSearch={e => this.onChangeSelectInput(e, 'user')}
                  onSelect={value => {
                    taskActions.select({
                      user: value
                    })
                  }}
                  isOpen={this.state.openSelect === 'user'}
                  items={search.user}
                  value={search.selected.user.name}
                />
                {!validation.userName.isValid && validation.userName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="userName"
                  >
                    {translate(`userError.${validation.userName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="text-right">
              <button
                type="button"
                className="btn btn-primary"
                onClick={e => this.onSubmit(e)}
              >
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Create
