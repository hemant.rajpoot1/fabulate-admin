import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as taskActions from 'store/actions/task'
import { getField } from 'store/selectors/modelSelectors'

class EditContainer extends Component {

  componentDidMount() {

    let { taskActions } = this.props

    let { id } = this.props.match.params

    taskActions.getId(id)

  }

  render() {

    return (
      <Edit { ...this.props } />
    )

  }

}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.task.validation))
  
  return {
    task: state.task.edit,
    translate: getTranslate(state.locale),
    search: state.task.search,   
    validation: state.task.validation,  
    validRules     
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    taskActions: bindActionCreators(taskActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
