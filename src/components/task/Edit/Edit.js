import React, { Component } from 'react'
import { formattingQuery } from 'helpers/tools'

import Select from 'components/ui/Select'

class Edit extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openSelect: undefined
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.windowClick)
  }

  windowClick = () => {
    if (this.state.openSelect) {
      this.setState({ openSelect: undefined })
    }
  }

  handleChange({ target }) {

    const { taskActions } = this.props

    taskActions.edit({ [target.name]: target.value })

  }

  onSubmit() {

    const { taskActions, task, validation, search } = this.props
    const formData = formattingQuery({
      id: task.id,
      taskName: task.taskName,
      causeId: search.selected.cause.id,
      userId: search.selected.user.id
    })

    if (formData.taskName
      && (validation.taskName.isValid || validation.taskName.isValid === undefined)
      && formData.causeId && formData.userId
    ) {
      taskActions.update({
        ...formData
      })
    }
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { taskActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        this.setState({
          isSearch: true
        })
        taskActions.search(type, { q: value })
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  changeInput = (type, value) => {
    const { validation, taskActions, validRules } = this.props
    taskActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  setOpenSelect = type => {
    let nextOpenSelect = this.state.openSelect

    if(nextOpenSelect === type) {
      nextOpenSelect = undefined
    } else {
      nextOpenSelect = type
    }

    this.setState({openSelect: nextOpenSelect})
  }

  render() {
    const { task, translate, taskActions, search, validation } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Edit</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Task name</label>
              <div className="col-lg-9">
                <input type="text" className="form-control" name="taskName"
                  ref={ref => ref ? this.taskName = ref : null}
                  value={task.taskName || ''}
                  onBlur={e => (this.taskName.value !== '' && validation.taskName.isValid === undefined) && this.changeInput('taskName', e.target.value)}
                  onChange={e => {
                    (validation.taskName.isValid !== undefined) && this.changeInput('taskName', e.target.value)
                    this.handleChange(e)
                  }}
                // onChange={ e => this.handleChange(e) }
                />
                {!validation.taskName.isValid && validation.taskName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="taskName"
                  >
                    {translate(`causeError.${validation.taskName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Cause name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={e => {
                    e.stopPropagation()
                    this.setOpenSelect('cause')
                  }}
                  isOpen={this.state.openSelect === 'cause'}
                  onSearch={e => this.onChangeSelectInput(e, 'cause')}
                  onSelect={value => {
                    taskActions.select({
                      cause: value
                    })
                  }}
                  items={search.cause}
                  value={search.selected.cause.name}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">User name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={e => {
                    e.stopPropagation()
                    this.setOpenSelect('user')
                  }}
                  isOpen={this.state.openSelect === 'user'}
                  onSearch={e => this.onChangeSelectInput(e, 'user')}
                  onSelect={value => {
                    taskActions.select({
                      user: value
                    })
                  }}
                  items={search.user}
                  value={search.selected.user.name}
                />
              </div>
            </div>

            <div className="text-right">
              <button type="button" className="btn btn-primary"
                onClick={() => this.onSubmit()}
              >
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Edit
