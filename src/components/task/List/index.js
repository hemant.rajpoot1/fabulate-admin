import List from './List'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as taskActions from 'store/actions/task'
import * as userActions from 'store/actions/user'

class ListContainer extends Component {

  componentDidMount() {
    const { taskActions } = this.props
    taskActions.get()
  }

  render() {
    return (
      <List { ...this.props }/>
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    tasks: state.task.origin,
    translate: getTranslate(state.locale),
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    taskActions: bindActionCreators(taskActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
