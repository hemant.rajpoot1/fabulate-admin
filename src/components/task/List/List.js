import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

class List extends Component {

  onDelete(id) {
    let {
      taskActions
    } = this.props

    let notice = new PNotify({
      title: 'Remove task',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      taskActions.remove(id)
    })

  }

  render() {

    const { tasks } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">

          <h5 className="panel-title">Tasks List</h5>

          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>

        <div>

          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Brief name</th>
                <th>User name</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>

            <tbody>
              {
                _.map(tasks, (task, i) =>
                  (
                    <tr key={ `${task.id}_index_${i}` }>
                      <td>{ task.id }</td>
                      <td>{task.taskName}</td>
                      <td>{task.causeTask ? task.causeTask.causeName : '-'}</td>
                      <td>{task.userTask ? task.userTask.userName : '-'}</td>
                      <td className="text-center">
                        <ul className="icons-list">
                          <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown">
                              <i className="icon-menu9"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-menu-right">
                              <li><Link to={`/task/${task.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                              <li onClick={ () => this.onDelete(task.id) }>
                                <a><i className="icon-cross"></i>Delete</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </td>
                    </tr>
                  )
                )
              }
            </tbody>
          </table>

        </div>
      </div>
    )

  }

}

export default withRouter(List)
