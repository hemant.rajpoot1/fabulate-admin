import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Select from 'components/ui/Select'

import _ from 'lodash'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import Input from './Input'

import Pagination from 'react-js-pagination'

import { notFound } from 'helpers/tools.js'

const sortBy = [
  {
    name: 'Product card name',
    value: 'productCardName'
  },
  // {
  //   name: 'Genre',
  //   value: 'genre'
  // },
  {
    name: 'Size',
    value: 'sizeLabel'
  }
]

class WorkspaceProductCardList extends Component {

  setSearchFilter = value => {
    const { workspaceProductCardActions } = this.props
    const query = {
      workspaceId: _.isNil(value) ? undefined : value.id,
    }
    workspaceProductCardActions.setFilter(query)
  }

  resetFilters = () => {
    const { workspaceProductCardActions } = this.props
    workspaceProductCardActions.resetFilter()
    workspaceProductCardActions.setIsEdit()
  }

  choiceItems = () => {
    const {
      products,
      workspaces,
      sort
    } = this.props

    switch (sort.value) {
    case 'productCardName':
      return products
    default:
      return workspaces
    }
  }

  selectFieldName = () => {
    const { sort } = this.props
    switch (sort.value) {
    case 'productCardName':
      return 'productCardName'
    default:
      return 'name'
    }
  }

  renderTools = () => {
    const {
      filter,
      workspaceProductCardActions,
      sort
    } = this.props
    return (
      <div className="filter-selector center-flex">
        <span className="mr-15">Select the organization: </span>
        <div className="container-selector col-md-3">
          <Select
            isSearch={true}
            onSelect={value => { this.setSearchFilter(value) }}
            onSearch={e => this.onChangeSelectInput(e.target, sort.value === 'productCardName' ? 'product' : 'workspace')}
            items={this.choiceItems()}
            value={this.getName('workspace', filter.workspaceId)}
            defaultValue=''
            placeholder="Click to filter..."
            selectFieldName={this.selectFieldName()}
          />
        </div>
        <span>
          <a
            className="ml-20"
            onClick={this.resetFilters}
          >
            <i className="icon-reset mr-5"></i>
            Reset Filters
          </a>
        </span>
        <div className="ml-20 sort-item-size">
          <Select
            items={sortBy}
            placeholder="Set sorting by"
            onSelect={workspaceProductCardActions.setSort}
            value={_.get(sort, 'name', '')}
          />
        </div>
      </div>
    )
  }


  handleChange({ target }) {
    const { workspaceProductCardActions } = this.props
    workspaceProductCardActions.edit({ [target.name]: target.value })
  }

  getName = (type, id) => {
    const { workspaces, products, genre } = this.props
    switch (type) {
    case 'workspace': {
      const workspace = _.find(workspaces, { id })
      return _.get(workspace, 'name')
    }
    case 'productDisplayName': {
      const productCard = _.find(products, { id })
      return _.get(productCard, 'productDisplayName')
    }
    case 'productCardName': {
      const productCard = _.find(products, { id })
      return _.get(productCard, 'productCardName')
    }
    case 'sizeLabel': {
      const productCard = _.find(products, { id })
      return _.get(productCard, 'sizeLabel')
    }
    case 'genreName': {
      const productCard = _.find(products, { id })
      const productCardGenres = _.get(productCard, 'genre')
      return _.chain(genre)
        .filter(genreItem => _.includes(productCardGenres, genreItem.id))
        .map(genreItem => genreItem.genreName)
        .join(', ')
        .value()
    }
    default: {
      return null
    }
    }
  }

  onChangeSelectInput = ({ value }, type) => {
    const { workspaceActions, productActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    switch (type) {
    case 'workspace': {
      this.timer = setTimeout(() => {
        workspaceActions.get({
          name: value
        })
      }, 800)
      return
    }
    case 'product': {
      this.timer = setTimeout(() => {
        productActions.get({
          productCardName: value
        })
      }, 800)
      return
    }
    default: {
      return null
    }
    }
  }

  onDelete(id) {
    const {
      workspaceProductCardActions
    } = this.props
    const notice = new PNotify({
      title: 'Remove organization product card',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })
    notice.get().on('pnotify.confirm', () => {
      workspaceProductCardActions.remove(id)
    })

  }

  renderTable = (workspaceProductCards) => {
    const {
      isEdit,
      workspaces,
      change,
      products,
      validation,
      isGetFetching,
    } = this.props

    if (isGetFetching) {
      return (
        <tr>
          <td colspan='10'>
            <SpinnerLoadData isAbsolute={false} delay={0} />
          </td>
        </tr>
      )
    }

    return _.map(workspaceProductCards, (workspaceProductCard, i) => {
      const productCardValid = validation[workspaceProductCard.id]
      const workspaceId = _.get(productCardValid, 'workspaceId.value') || _.get(workspaceProductCard, 'workspaceId', '')
      const productCardId = _.get(productCardValid, 'productCardId.value') || _.get(workspaceProductCard, 'productCardId', '')

      return (
        <tr key={`${workspaceProductCard.id}`}>
          <td>{workspaceProductCard.id}</td>
          <td>
            <Select
              readOnly={!isEdit}
              isSearch={true}
              onSelect={value => {
                change('workspaceId', value && value.id, workspaceProductCard.id)
              }}
              onSearch={e => this.onChangeSelectInput(e.target, 'workspace')}
              items={workspaces}
              value={this.getName('workspace', workspaceId)}
            />
          </td>
          <td>
            <Select
              readOnly={!isEdit}
              isSearch={true}
              onSelect={value => {
                change('productCardId', value && value.id, workspaceProductCard.id)
              }}
              items={products}
              onSearch={e => this.onChangeSelectInput(e.target, 'product')}
              selectFieldName={'productCardName'}
              value={this.getName('productCardName', productCardId)}
            />
          </td>
          <td>
            {this.getName('productDisplayName', productCardId)}
          </td>
          <td>
            {this.getName('sizeLabel', productCardId)}
          </td>
          <td>
            {this.getName('genreName', productCardId)}
          </td>
          <td>
            <Input
              name="fabulateCommission"
              value={_.get(productCardValid, 'fabulateCommission.value', workspaceProductCard.fabulateCommission) || '0'}
              isEdit={isEdit}
              onChange={({ target }) => change('fabulateCommission', target.value, workspaceProductCard.id)}

            />
          </td>
          <td>
            <Input
              name="buyerPrice"
              value={_.get(productCardValid, 'buyerPrice.value', workspaceProductCard.buyerPrice) || '0'}
              isEdit={isEdit}
              onChange={({ target }) => change('buyerPrice', target.value, workspaceProductCard.id)}
            />
          </td>
          <td>
            <Input
              name="contractorPrice"
              value={_.get(productCardValid, 'contractorPrice.value', workspaceProductCard.contractorPrice) || '0'}
              isEdit={isEdit}
              onChange={({ target }) => change('contractorPrice', target.value, workspaceProductCard.id)}
            />
          </td>
          <td>
            <Input
              name="unlimitedPrice"
              value={_.get(productCardValid, 'unlimitedPrice.value', workspaceProductCard.unlimitedPrice) || '0'}
              isEdit={isEdit}
              onChange={({ target }) => change('unlimitedPrice', target.value, workspaceProductCard.id)}
            />
          </td>
          {isEdit ?
            (
              <td className="button-actions" />
            ) : (
              <td className="button-actions">
                <div onClick={() => this.onDelete(workspaceProductCard.id)}>
                  <a><i className="icon-cross"></i>Delete</a>
                </div>
              </td>
            )
          }
        </tr>
      )
    })
  }



  onSubmit() {
    const { workspaceProductCardActions, workspaceProductCard, isGetFetching } = this.props
    workspaceProductCardActions.update(workspaceProductCard)
    workspaceProductCardActions.setIsEdit(false)
    if (isGetFetching) {
      return (<SpinnerLoadData isAbsolute={false} delay={0} />)
    }

  }

  onClickCancel = () => {
    const { workspaceProductCardActions, filter } = this.props

    workspaceProductCardActions.setIsEdit(false)
    workspaceProductCardActions.get(filter)

  }

  renderButtons = () => {
    const { isEdit, workspaceProductCardActions, onSubmit } = this.props
    return isEdit ?
      (
        <div>
          <button
            type="button"
            className="btn btn-primary cancel-button"
            onClick={this.onClickCancel}
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btn-primary"
            onClick={onSubmit}
          >
            Update
          </button>
        </div>
      ) : (
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => workspaceProductCardActions.setIsEdit(true)}
        >
          Edit
        </button>
      )
  }

  renderTableHead = () => {
    const { workspaceProductCards } = this.props

    return (
      <table className="table datatable-select-basic">
        <thead>
          <tr>
            <th>#</th>
            <th>Organization name</th>
            <th>Product card name</th>
            <th>Product display name</th>
            <th>Size label</th>
            <th>Genre name</th>
            <th>Fabulate commission</th>
            <th>Buyer price</th>
            <th>Contractor price</th>
            <th>Unlimited price</th>
            <th className="text-center" />
          </tr>
        </thead>
        <tbody>
          {
            this.renderTable(workspaceProductCards)
          }
        </tbody>
      </table>
    )
  }

  render() {
    const {
      filter,
      isGetFetching,
      fetchList,
      count,
      page
    } = this.props
    const isFullProductCards = !_.isEmpty(this.props.workspaceProductCards)
    return (
      <div className="panel panel-flat">
        <div className="panel-heading" style={{ height: '151px' }}>
          <h5 className="panel-title">Organization's product cards</h5>

          <div className="text-right">
            {isFullProductCards && this.renderButtons()}
          </div>
          {this.renderTools()}
        </div>
        <div className="overflow-auto mh-480">
          {filter.workspaceId && ((isFullProductCards || isGetFetching) ? this.renderTableHead() : notFound())}
          {(count > 20 && filter.workspaceId && !isGetFetching) && (
            <div className="pager p-20">
              <Pagination
                hideNavigation
                pageRangeDisplayed={5}
                activePage={page}
                itemsCountPerPage={20}
                totalItemsCount={count}
                onChange={(page) => fetchList(page)}
              />
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default withRouter(WorkspaceProductCardList)
