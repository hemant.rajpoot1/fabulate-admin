import WorkspaceProductCardList from './workspaceProductCardList'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import PNotify from 'pnotify'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

import * as workspaceProductCardActions from 'store/actions/workspaceProductCard'
import * as workspaceActions from 'store/actions/workspace'
import * as productActions from 'store/actions/product'
import * as genreActions from 'store/actions/genre'

class WorkspaceProductCardListContainer extends Component {

  componentDidMount() {
    const {
      workspaceProductCardActions,
      workspaces,
      workspaceActions,
      products,
      productActions,
      filter,
      genre,
      genreActions
    } = this.props

    if (_.isEmpty(workspaces)) {
      workspaceActions.get()
    }

    if (_.isEmpty(products)) {
      productActions.get()
    }

    if (!_.isNil(filter.workspaceId)) {
      workspaceProductCardActions.get(filter)
    }

    if (_.isEmpty(genre)) {
      genreActions.get()
    }
  }

  componentDidUpdate(prevProps) {
    const { filter, sort } = this.props
    if ((!_.isEqual(prevProps.filter, filter)) && (filter.workspaceId)) {
      this.fetch()
    }
    if (!_.isEqual(prevProps.sort, sort) && filter.workspaceId) {
      this.fetch()
    }
  }

  fetch = () => {
    const {
      workspaceProductCardActions,
      filter,
      sort,
      page
    } = this.props
    workspaceProductCardActions.get({
      ...filter,
      sort: sort.value,
      page
    })
  }

  fetchList = (page) => {
    const {
      workspaceProductCardActions,
      filter,
      sort
    } = this.props
    workspaceProductCardActions.get({
      ...filter,
      sort: sort.value,
      page
    })
  }
  

  change = (type, value, productCardId) => {
    const { validation, workspaceProductCardActions, validRules } = this.props
    workspaceProductCardActions.setValidProduct(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      productCardId,
    )
  }

  onSubmit = () => {
    const { workspaceProductCardActions, validation, filter } = this.props

    let lastFormData

    _.forEach(validation, (product, id) => {
      const validObj = formattingValidObj(product)

      const formData = {
        ...validObj,
        id
      }

      const isFieldsFilled = _.every(formData, element => !!element)
        
      if (isFieldsFilled) {
        workspaceProductCardActions.update({
          formData,
          filter
        })
        workspaceProductCardActions.setIsEdit(false)
      } else {
        return new PNotify({
          type: 'error',
          title: 'Error',
          text: 'Please, correctly fill all fields'
        })  
      }

      lastFormData = formData
    })
    
    if(_.isNil(lastFormData)) {
      lastFormData = Object.values(this.props.workspaceProductCards)[0]
    }

    workspaceProductCardActions.download(lastFormData)
  }

  select = value => {
    const { workspaceProductCardActions } = this.props
    workspaceProductCardActions.select(value ? value.id : null, false)
  }

  render() {
    return (
      <WorkspaceProductCardList
        { ...this.props }
        change={this.change}
        onSubmit={this.onSubmit}
        fetchList={this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.product.validationEdit))
  return {
    workspaceProductCards: state.workspaceProductCard.origin,
    translate: getTranslate(state.locale),
    isEdit: state.workspaceProductCard.edit.isEdit,
    workspaces: state.workspace.origin,
    products: state.product.origin,
    genre: state.genre.list,
    validation: state.workspaceProductCard.validationEdit,
    validRules,
    filter: state.workspaceProductCard.filter,
    sort: state.workspaceProductCard.sort,
    page: state.workspaceProductCard.page,
    count: state.workspaceProductCard.count,
    isGetFetching: state.workspaceProductCard.isGetFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    genreActions: bindActionCreators(genreActions, dispatch),
    workspaceProductCardActions: bindActionCreators(workspaceProductCardActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkspaceProductCardListContainer))
