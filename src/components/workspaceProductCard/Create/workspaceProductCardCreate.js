import React, { Component } from 'react'
import Select from 'components/ui/Select'
import _ from 'lodash'

class WorkspaceProductCardCreate extends Component {

  getName = (type, id) => {
    const { workspaces, products } = this.props
    switch (type) {
    case 'workspace': {
      const workspace = _.find(workspaces, { id })
      return _.get(workspace, 'name')
    }
    case 'productCard': {
      const productCard = _.find(products, { id })
      return _.get(productCard, 'productCardName')
    }
    default: {
      return null
    }
    }
  }

  onChangeSelectInput = ({ value }, type) => {
    const { workspaceActions, productActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    switch(type) {
    case 'workspace': {
      this.timer = setTimeout(() => {
        workspaceActions.get({
          name: value
        })
      }, 800)
      return
    }
    case 'product': {
      this.timer = setTimeout(() => {
        productActions.get({
          productCardName: value
        })
      }, 800)
      return
    }
    default: {
      return null
    }
    }
  }

  render() {
    const {
      change,
      onSubmit,
      validation,
      workspaces,
      products
    } = this.props
    const workspaceId = _.get(validation, 'workspaceId.value')
    const productCardId = _.get(validation, 'productCardId.value')
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Create</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Organization name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onSelect={value => {
                    change('workspaceId', value && value.id)
                  }}
                  onSearch={e => this.onChangeSelectInput(e.target, 'workspace')}
                  items={workspaces}
                  value={this.getName('workspace', workspaceId)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Product card name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onSelect={value => {
                    change('productCardId', value && value.id)
                  }}
                  onSearch={e => this.onChangeSelectInput(e.target, 'product')}
                  items={products}
                  selectFieldName={'productCardName'}
                  value={this.getName('productCard', productCardId)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Fabulate commission</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="fabulateCommission"
                  value={validation.fabulateCommission.value}
                  onChange={({target}) => change('fabulateCommission', target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Buyer price</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="buyerPrice"
                  value={validation.buyerPrice.value || ''}
                  onChange={({target}) => change('buyerPrice', target.value)}
                />
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Contractor price</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="contractorPrice"
                  value={validation.contractorPrice.value || ''}
                  onChange={({target}) => change('contractorPrice', target.value)}
                />
              </div>
            </div>     
            
            <div className="form-group">
              <label className="col-lg-3 control-label">Unlimited price</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="unlimitedPrice"
                  value={validation.unlimitedPrice.value || ''}
                  onChange={({target}) => change('unlimitedPrice', target.value)}
                />
              </div>
            </div>     
          </div>

          <div className="text-right">
            <button className="btn btn-primary" onClick={onSubmit}>add product card to organization</button>
          </div>
        </div>

      </div>
    )
  }
}

export default WorkspaceProductCardCreate
