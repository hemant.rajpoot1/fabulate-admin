import WorkspaceProductCardCreate from './workspaceProductCardCreate'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as workspaceProductCardActions from 'store/actions/workspaceProductCard'
import * as workspaceActions from 'store/actions/workspace'
import * as productActions from 'store/actions/product'

import _ from 'lodash'
import PNotify from 'pnotify'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

class WorkspaceProductCardContainer extends Component {

  componentDidMount() {
    const {
      workspaces,
      workspaceActions,
      products,
      productActions
    } = this.props
    if (_.isEmpty(workspaces)) {
      workspaceActions.get()
    }
    if (_.isEmpty(products)) {
      productActions.get()
    }
  }

  change = (type, value) => {
    const { validation, workspaceProductCardActions, validRules } = this.props
    workspaceProductCardActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }  

  onSubmit = () => {
    const { workspaceProductCardActions, validation } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }
    const isFieldsFilled = _.every(formData, element => !!element)
    if (isFieldsFilled) {
      workspaceProductCardActions.create(formData)
    } else {
      return new PNotify({
        type: 'error',
        title: 'Error',
        text: 'Please, correctly fill all fields'
      })  
    }
  }


  render() {
    return (
      <WorkspaceProductCardCreate
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.product.validationCreate))
  return {
    products: state.product.origin,
    workspaces: state.workspace.origin,
    validation: state.workspaceProductCard.validationCreate,
    validRules,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    workspaceProductCardActions: bindActionCreators(workspaceProductCardActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkspaceProductCardContainer))
