import WorkspaceProductCardEdit from './WorkspaceProductCardEdit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as workspaceProductCardActions from 'store/actions/workspaceProductCard'
import * as workspaceActions from 'store/actions/workspace'
import * as productActions from 'store/actions/product'

import _ from 'lodash'
import PNotify from 'pnotify'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

class ContentCategoryEditContainer extends Component {

  componentDidMount() {
    const {
      workspaceProductCardActions,
      id,
      workspaces,
      workspaceActions,
      products,
      productActions
    } = this.props
    if (_.isEmpty(workspaces)) {
      workspaceActions.get()
    }
    if (_.isEmpty(products)) {
      productActions.get()
    }
    workspaceProductCardActions.getId(id)
  }

  change = (type, value) => {
    const { validation, workspaceProductCardActions, validRules } = this.props
    workspaceProductCardActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }  

  onSubmit = () => {
    const { workspaceProductCardActions, validation, id } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
      id
    }

    const isFieldsFilled = _.every(formData, element => !!element)
    if (isFieldsFilled) {
      workspaceProductCardActions.update(formData)
    } else {
      return new PNotify({
        type: 'error',
        title: 'Error',
        text: 'Please, correctly fill all fields'
      })  
    }
  }

  select = value => {
    const { workspaceProductCardActions } = this.props
    workspaceProductCardActions.select(value ? value.id : null, false)
  }

  render() {
    return (
      <WorkspaceProductCardEdit
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.product.validationEdit))
  return {
    products: state.product.origin,
    workspaces: state.workspace.origin,
    validation: state.workspaceProductCard.validationEdit,
    validRules,
    id: props.match.params.id
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    workspaceProductCardActions: bindActionCreators(workspaceProductCardActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentCategoryEditContainer))