import React, { Component } from 'react'

class PasswordForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      password: null,
      newPassword: null,
      repeatPassword: null
    }
  }

  handleChange = ({ target }) => {
   
    this.setState({[target.name]: target.value})
  }  

  updatePassword = () => {
    const { updatePassword, currentUser } = this.props
    const { password, newPassword, repeatPassword } = this.state
    if(repeatPassword === newPassword) {
      updatePassword({
        id: currentUser.id,
        password,
        newPassword
      })
    }
  }

  render() {
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h6 className="panel-title">Account settings</h6>
        </div>

        <div className="panel-body">
          <div>
            <div className="form-group">
              <div className="row">
                <div className="col-md-4">
                  <label>Current password</label>
                  <input name="password" onChange={this.handleChange} type="password" className="form-control" />
                </div>              
                <div className="col-md-4">
                  <label>New password</label>
                  <input name="newPassword" onChange={this.handleChange} type="password" placeholder="Enter new password" className="form-control" />
                </div>

                <div className="col-md-4">
                  <label>Repeat password</label>
                  <input name="repeatPassword" onChange={this.handleChange} type="password" placeholder="Repeat new password" className="form-control" />
                </div>
              </div>
            </div>
          </div>

          <div className="text-right">
            <button onClick={() => this.updatePassword()} className="btn btn-primary">Save <i className="icon-arrow-right14 position-right"></i></button>
          </div>
        </div>        
      </div>
    )

  }

}

export default PasswordForm
