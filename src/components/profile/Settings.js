import React, { Component } from 'react'

import SettingAsAdmin from './SettingAsAdmin'


class Settings extends Component {
  constructor(props) {
    super(props)

    this.state = {
      select: undefined
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.changeOpenSelect)
  }  

  changeOpenSelect = name => {
    const { select } = this.state
    let nextSelect = undefined
    if(typeof name === 'string') {
      nextSelect = name
    }

    if(nextSelect !== select) {
      this.setState({
        select: nextSelect
      })
    }
  }  

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : undefined)
  }

  render() {
    return (
      <SettingAsAdmin 
        {...this.props} 
        handleOnSelect={this.handleOnSelect} 
        changeOpenSelect={this.changeOpenSelect} 
      />
    )
  }

}

export default Settings
