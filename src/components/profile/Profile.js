import React, { Component } from 'react'
import UserCard from './UserCard'
import Settings from './Settings'


class Profile extends Component {

  render() {
    const { currentUser, change, handleChange, edit, updateUserInfo, cropped, updatePassword } = this.props

    return (
      <div className="page-container">
        <div className="page-content">
          <div className="content-wrapper">

            <div className="row">
              <div className="col-lg-9">
                <Settings
                  edit={edit}
                  change={change}
                  handleChange={handleChange}
                  updateUserInfo={updateUserInfo}
                  updatePassword={updatePassword}
                />
              </div>

              <div className="col-lg-3">
                <UserCard
                  currentUser={currentUser}
                  cropped={cropped}
                  updateUserInfo={updateUserInfo}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )


  }

}

export default Profile
