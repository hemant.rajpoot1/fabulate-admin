import React, { Component } from 'react'
import avatar from 'assets/images/placeholder.jpg'
import UploadPhoto from 'components/ui/UploadPhoto'

import './userCard.css'

const roles = {
  0: 'Super admin',
  1: 'Admin'
}

class UserCard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openModal: false
    }
  }

  toggleModal = (e) => {
    this.setState({
      openModal: !this.state.openModal
    })
  }  

  render() {
    const { currentUser, cropped, updateUserInfo } = this.props

    return (
      <div>
        <UploadPhoto 
          isShow={this.state.openModal} 
          cropped={cropped} 
          currentUser={currentUser} 
          toggleModal={this.toggleModal}
        />
        <div className="thumbnail">
          <div className="thumb thumb-rounded thumb-slide profile-image">
            <img className="profile-image" src={currentUser.image || avatar} alt="" />
            <div className="caption">
              <span>
                <a onClick={(e) => this.toggleModal(e)} className="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i className="icon-plus2"></i></a>
                <a onClick={() => {
                  updateUserInfo({
                    ...currentUser,
                    image: null
                  })
                }} className="btn bg-success-400 btn-icon btn-xs"><i className="transform-plus-icon icon-plus2"></i></a>
              </span>
            </div>
          </div>

          <div className="caption text-center">
            <h6 className="text-semibold no-margin">{currentUser.userName} <small className="display-block">{roles[currentUser.permissions]}</small></h6>
          </div>
        </div>

        
        {/*



        <div className="panel panel-flat">
          <div className="panel-heading">
            <h6 className="panel-title">Share your thoughts</h6>
            <div className="heading-elements">
              <ul className="icons-list">
                <li><a data-action="close"></a></li>
              </ul>
            </div>
          </div>

          <div className="panel-body">
            <form action="#">
              <div className="form-group">
                <textarea name="enter-message" className="form-control mb-15" rows="3" cols="1" placeholder="What's on your mind?"></textarea>
              </div>

              <div className="row">
                <div className="col-xs-6">
                  <ul className="icons-list icons-list-extended mt-10">
                    <li><a data-popup="tooltip" title="Add photo" data-container="body"><i className="icon-images2"></i></a></li>
                    <li><a data-popup="tooltip" title="Add video" data-container="body"><i className="icon-film2"></i></a></li>
                    <li><a data-popup="tooltip" title="Add event" data-container="body"><i className="icon-calendar2"></i></a></li>
                  </ul>
                </div>

                <div className="col-xs-6 text-right">
                  <button type="button" className="btn btn-primary btn-labeled btn-labeled-right">Share <b><i className="icon-circle-right2"></i></b></button>
                </div>
              </div>
            </form>
          </div>
        </div>


        <div className="panel panel-flat">
          <div className="panel-heading">
            <h6 className="panel-title">Balance changes</h6>
            <div className="heading-elements">
              <span className="heading-text"><i className="icon-arrow-down22 text-danger"></i> <span className="text-semibold">- 29.4%</span></span>
            </div>
          </div>

          <div className="panel-body">
            <div className="chart-container">
              <div className="chart" id="visits" style={{height: 300}}></div>
            </div>
          </div>
        </div>


        <div className="panel panel-flat">
          <div className="panel-heading">
            <h6 className="panel-title">Latest connections</h6>
            <div className="heading-elements">
              <span className="badge badge-success heading-text">+32</span>
            </div>
          </div>

          <ul className="media-list media-list-linked pb-5">
            <li className="media-header">Office staff</li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <span className="media-heading text-semibold">James Alexander</span>
                  <span className="media-annotation">UI/UX expert</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-success"></span>
                </div>
              </a>
            </li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <span className="media-heading text-semibold">Jeremy Victorino</span>
                  <span className="media-annotation">Senior designer</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-danger"></span>
                </div>
              </a>
            </li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <div className="media-heading"><span className="text-semibold">Jordana Mills</span></div>
                  <span className="text-muted">Sales consultant</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-grey-300"></span>
                </div>
              </a>
            </li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <div className="media-heading"><span className="text-semibold">William Miles</span></div>
                  <span className="text-muted">SEO expert</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-success"></span>
                </div>
              </a>
            </li>

            <li className="media-header">Partners</li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <span className="media-heading text-semibold">Margo Baker</span>
                  <span className="media-annotation">Google</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-success"></span>
                </div>
              </a>
            </li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <span className="media-heading text-semibold">Beatrix Diaz</span>
                  <span className="media-annotation">Facebook</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-warning-400"></span>
                </div>
              </a>
            </li>

            <li className="media">
              <a className="media-link">
                <div className="media-left"><img src="assets/images/placeholder.jpg" className="img-circle img-md" alt="" /></div>
                <div className="media-body">
                  <span className="media-heading text-semibold">Richard Vango</span>
                  <span className="media-annotation">Microsoft</span>
                </div>
                <div className="media-right media-middle">
                  <span className="status-mark bg-grey-300"></span>
                </div>
              </a>
            </li>
          </ul>
        </div>
        */}
      </div>
    )


  }

}

export default UserCard
