import React, { Component } from 'react'
import PasswordForm from './PasswordForm'

class SettingAsAdmin extends Component {
  render() {
    const { edit, handleChange, updateUserInfo, updatePassword } = this.props
    return (
      <div className="tab-pane" id="settings">

        <div className="panel panel-flat">
          <div className="panel-heading">
            <h6 className="panel-title">Profile information</h6>
          </div>

          <div className="panel-body">
            <div>
              <div className="form-group">
                <div className="row">
                  <div className="col-md-4">
                    <label>Username</label>
                    <input onChange={e => handleChange(e)} type="text" name="userName" value={edit.userName} className="form-control" />
                  </div>
                  <div className="col-md-4">
                    <label>Email</label>
                    <input onChange={e => handleChange(e)} type="text" name="email" value={edit.email} readOnly="readonly" className="form-control" />
                  </div>
                  <div className="col-md-4">
                    <label>Phone</label>
                    <input onChange={e => handleChange(e)} type="text" name="phoneNumber" value={edit.phoneNumber} className="form-control" />
                    <span className="help-block">+99-99-9999-9999</span>
                  </div>                  
                </div>
              </div>              

              <div className="text-right">
                <button onClick={() => updateUserInfo(edit)} className="btn btn-primary">Save <i className="icon-arrow-right14 position-right"></i></button>
              </div>
            </div>
          </div>
        </div>

        <PasswordForm currentUser={edit} updatePassword={updatePassword}/>
      </div>
    )


  }

}

export default SettingAsAdmin
