import Profile from './Profile'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as authActions from '../../store/actions/auth'

class ProfileContainer extends Component {

  componentDidMount() {
    const { authActions } = this.props
    authActions.initEdit()
  }

  change = (type, value) => {
    const { authActions } = this.props    
   
    authActions.edit({[type]: value})
  }  

  handleChange = ({ target }) => {
    const { authActions } = this.props    
    authActions.edit({[target.name]: target.value})
  }  

  updateUserInfo = (edit) => {
    const { authActions } = this.props 
    authActions.updateUserInfo(edit)
  }  
  
  updatePassword = (data) => {
    const { authActions } = this.props    
    authActions.updateUserPassword(data)
  }    

  cropped = url => {
    const { authActions, currentUser } = this.props    
    authActions.uploadUserPhoto(url, currentUser.id)
  }

  render() {
    return (
      <Profile 
        {...this.props} 
        change={this.change}
        handleChange={this.handleChange}
        changePage={this.changePage}
        updateUserInfo={this.updateUserInfo}
        cropped={this.cropped}
        updatePassword={this.updatePassword}
      />
    )
  }
}



let mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : null
  return {
    currentUser,
    edit: state.auth.edit
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    authActions: bindActionCreators(authActions, dispatch)    
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfileContainer))
