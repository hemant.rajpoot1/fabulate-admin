import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as contentCategoryActions from 'store/actions/contentCategory'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

import ContentCategoryEdit from './ContentCategoryEdit'

class ContentCategoryEditContainer extends Component {

  componentDidMount() {
    const { contentCategoryActions, id } = this.props

    contentCategoryActions.getId(id)
  }

  change = (type, value) => {
    const { validation, contentCategoryActions, validRules } = this.props
    contentCategoryActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }  

  onSubmit = () => {
    const { contentCategoryActions, validation, id } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
      id
    }

    if (formData.contentCategoryName) {
      contentCategoryActions.update(formData)
    } else {
      this.change('contentCategoryName', validation.contentCategoryName.value)  
    }
  }

  select = value => {
    const { contentCategoryActions } = this.props
    contentCategoryActions.select(value ? value.id : null, false)
  }

  render() {
    return (
      <ContentCategoryEdit
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.contentCategory.validationEdit))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  let { id } = props.match.params

  return {
    validation: state.contentCategory.validationEdit,
    currentUser,
    validRules,
    id
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentCategoryEditContainer))