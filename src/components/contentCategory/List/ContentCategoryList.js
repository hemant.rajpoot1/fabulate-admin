import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

import { workroomTypes } from 'helpers/params'

class ContentCategoryList extends Component {

  onDelete(id) {
    const {
      contentCategoryActions
    } = this.props

    const notice = new PNotify({
      title: 'Remove content category',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      contentCategoryActions.remove(id)
    })

  }

  renderItemList = (contentCategory, i) =>{
    const currentWorkroomTypeName = _.get(_.find(workroomTypes, item => _.isEqual(item.value, contentCategory.workroomType)), 'name', null)
    return (
      <tr key={`${contentCategory.id}`}>
        <td>{contentCategory.id}</td>
        <td>{contentCategory.contentCategorySequence}</td>
        <td>{contentCategory.contentCategoryName}</td>
        <td>{currentWorkroomTypeName}</td>
        <td className="text-center">
          <ul className="icons-list">
            <li className="dropdown">
              <a className="dropdown-toggle" data-toggle="dropdown">
                <i className="icon-menu9"></i>
              </a>
              <ul className="dropdown-menu dropdown-menu-right">
                <li><Link to={`/content-category/${contentCategory.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                <li onClick={() => this.onDelete(contentCategory.id)}>
                  <a><i className="icon-cross"></i>Delete</a>
                </li>
              </ul>
            </li>
          </ul>
        </td>
      </tr>
    )}

  render() {
    const { contentCategories } = this.props
    
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Content categories</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>
        <div>

          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Sequence</th>
                <th>Content category name</th>
                <th>Workroom type</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              {
                _.map(contentCategories, this.renderItemList)
              }
            </tbody>
          </table>

        </div>
      </div>
    )
  }
}

export default withRouter(ContentCategoryList)
