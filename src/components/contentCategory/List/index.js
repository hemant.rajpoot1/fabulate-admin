import ContentCategoryList from './ContentCategoryList'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as contentCategoryActions from 'store/actions/contentCategory'

class ContentCategoryListContainer extends Component {

  componentDidMount() {
    const { contentCategoryActions } = this.props
    contentCategoryActions.get()
  }

  render() {
    return (
      <ContentCategoryList { ...this.props }/>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    contentCategories: state.contentCategory.origin,
    translate: getTranslate(state.locale),
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentCategoryListContainer))
