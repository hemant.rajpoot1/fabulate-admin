import React, { Component } from 'react'

import _ from 'lodash'

import Select from 'components/ui/Select'
import { workroomTypes } from 'helpers/params'

import './ContentCategoryCreate.css'

class ContentCategoryCreate extends Component {

  handleOnSelect = (field, value) => {
    const { change } = this.props
    change(field, value)
  }

  render() {
    const { change, onSubmit, validation } = this.props

    const currentWorkroomTypeName = _.get(_.find(workroomTypes, item => _.isEqual(item.value, validation.workroomType.value)), 'name', undefined)

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Create</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Content category name</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="contentCategoryName"
                  value={validation.contentCategoryName.value}
                  onChange={e => change('contentCategoryName', e.target.value)}
                />
                {!validation.contentCategoryName.isValid && validation.contentCategoryName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    Content category name can not be null
                  </label>
                )}                
              </div>
              <div className="workroom-type-wrapper">
                <label className="col-lg-3 control-label">
                  Workroom type
                </label>
                <div className="col-lg-9">
                  <Select
                    isSearch={false}
                    items={workroomTypes}
                    value={currentWorkroomTypeName}
                    inputName={'workroomType'}
                    onSelect={item => this.handleOnSelect('workroomType', _.get(item, 'value', item))}
                    cantReset
                  />
                  {!validation.workroomType.isValid && validation.workroomType.message && (
                    <label
                      id="name-error"
                      className="validation-error-label"
                      forhtml="workroomType"
                    >
                      {validation.workroomType.message}
                    </label>
                  )}
                </div>
              </div>
              <label className="col-lg-3 control-label">Content category sequence</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="contentCategorySequence"
                  value={validation.contentCategorySequence.value || ''}
                  onChange={e => change('contentCategorySequence', e.target.value)}
                />
              </div>
            </div>                                     
          </div>

          <div className="text-right">
            <button className="btn btn-primary" onClick={() => onSubmit()}>create content category</button>
          </div>
        </div>

      </div>
    )
  }
}

export default ContentCategoryCreate
