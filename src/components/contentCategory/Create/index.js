
import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as contentCategoryActions from 'store/actions/contentCategory'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

import ContentCategoryCreate from './ContentCategoryCreate'

class ContentCategoryCreateContainer extends Component {

  change = (type, value) => {
    const { validation, contentCategoryActions, validRules } = this.props
    contentCategoryActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }  

  onSubmit = () => {
    const { contentCategoryActions, validation } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }
    if (formData.contentCategoryName) {
      contentCategoryActions.create(formData)
    } else {
      this.change('contentCategoryName', validation.contentCategoryName.value)  
    }
  }


  render() {
    return (
      <ContentCategoryCreate
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.contentCategory.validationCreate))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  return {
    validation: state.contentCategory.validationCreate,
    currentUser,
    validRules,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentCategoryCreateContainer))
