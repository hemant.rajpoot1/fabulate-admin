import React, { Fragment } from 'react'

import { Link } from 'react-router-dom'

import _ from 'lodash'
import moment from 'moment'

export const LegalPages = (props) => {

  const { origin, remove } = props

  // const { id, version, learnMoreLink, privacyLink, termsLink, updatedAt, createdAt } = origin

  return <div className="panel panel-flat">
    <div className="panel-heading">

      <h5 className="panel-title">Require Term & Condition Update</h5>
      <div className="heading-elements">
        <ul className="icons-list">
          <li>
            <a data-action="collapse">&nbsp;</a>
          </li>
        </ul>
      </div>
    </div>

    <div>
      <table className="table datatable-select-basic">
        <thead>
          <tr>
            <th>#</th>
            <th>Version</th>
            <th>Datetime</th>
            <th>Terms Link</th>
            <th>Privacy Link</th>
            <th>Learn More Link</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>

        {origin.length > 0 && _.map(origin, item => {
          const { id, version, createdAt, updatedAt, activeDate, termsLink, privacyLink, learnMoreLink, forceUpdate } = item 

          return <tbody key={id}>
            <tr className="text-center-info">
              <td>{id}</td>
              <th>
                <Link to={`/legal-pages/${id}`} >{version}{forceUpdate ? ' - FORCED' : ''}</Link> 
              </th>
              <td>created: {moment(createdAt).format('MMMM D, YYYY')} <br />
                  updated: {moment(updatedAt).format('MMMM D, YYYY')} <br />
                  active date: {moment(activeDate).format('MMMM D, YYYY')}
              </td>
              <td><a rel="noopener noreferrer" target='_blank' href={termsLink} title="Terms &amp;Conditions">{termsLink}</a></td>
              <td><a rel="noopener noreferrer" target='_blank' href={privacyLink} title="Terms &amp;Conditions">{privacyLink}</a></td>
              <td><a rel="noopener noreferrer" target='_blank' href={learnMoreLink} title="Terms &amp;Conditions">{learnMoreLink}</a></td>
              <td className="text-center">
                <ul className="icons-list">
                  <li className="dropdown">
                    <a className="dropdown-toggle" data-toggle="dropdown">
                      <i className="icon-menu9"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-right">
                      <li><Link to={`/legal-pages/${id}`}><i className="icon-pencil"></i>Edit</Link></li>

                      <li>
                        <a onClick={() => remove(id)}><i className="icon-cross" ></i>Delete</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </td>
            </tr>

          </tbody>
      
        })}
      </table>
    </div>
  </div>
}
