import React, {useState, useEffect} from 'react'
import DatePicker from 'react-datepicker'

import moment from 'moment'

import 'react-datepicker/dist/react-datepicker.css'

export const Edit = (props) => {
  const { validation, change, onUpdate } = props
  const { termLink, privacyLink, version, forceUpdate, learnMoreLink, activeDate } = validation
  const [isDateEditMode, setIsDateEditMode ] = useState(false)
  const [isTerms, setIsTerms] = useState(false)
  const [isPrivacy, setIsPrivacy] = useState(false)
  const [isVersion, setIsVersion] = useState(false)
  const [isLearnMoreLink, setIsLearnMoreLink] = useState(false)
  const [editDate, setEditDate] = useState(null)

  useEffect(() => {
    if(termLink.value) {
      setIsTerms(true)
    }
    if(privacyLink.value) {
      setIsPrivacy(true)
    }
    if(version.value) {
      setIsVersion(true)
    }
    if(learnMoreLink.value) {
      setIsLearnMoreLink(true)
    }
  }, [termLink, privacyLink, version, forceUpdate, learnMoreLink])

  const onSetDate = (date) => {
    const currentDate = moment.utc(moment(date).format('LL')).format()
    setEditDate(date)
    change('activeDate',  currentDate)
  }

  return <div className="panel panel-flat">
    <div className="panel-heading">
      <h5 className="panel-title">Edit</h5>
      <div className="heading-elements">
        <ul className="icons-list">
          <li><a data-action="collapse">&nbsp;</a></li>
        </ul>
      </div>
    </div>

    <div className="panel-body">
      <div className="form-horizontal">
        
        <div className="form-group">
          <label className="col-lg-3 control-label">Version</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="Version"
              value={validation.version.value}
              onChange={e => change('version', e.target.value)}
            />
            {!validation.version.isValid && isVersion && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="name"
              >
                Version can not be null
              </label>
            )}
          </div>
        </div>
        
        <div className="form-group">
          <label className="col-lg-3 control-label">Force Update</label>
          <div className="col-lg-9">
            <input
              className=""
              type="checkbox"
              name="Force Update"
              checked={validation.forceUpdate.value}
              onChange={e => change('forceUpdate', e.target.checked)}
            />
          </div>
        </div>

        <div className="form-group">
          <label className="col-lg-3 control-label">Term Link</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="termLink"
              value={validation.termLink.value}
              onChange={e => change('termLink', e.target.value)}
            />
            {!validation.termLink.isValid && isTerms && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="name"
              >
                Term Link can not be null
              </label>
            )}                  
           
          </div>
          
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">Privacy Link</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="privacyLink"
              value={validation.privacyLink.value}
              onChange={e => change('privacyLink', e.target.value)}
            />
            {!validation.privacyLink.isValid && isPrivacy && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="name"
              >
                Privacy Link can not be null
              </label>
            )}                  
          </div>
        </div> 
        <div className="form-group">
          <label className="col-lg-3 control-label">Learn More Link</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="learnMoreLink"
              value={validation.learnMoreLink.value}
              onChange={e => change('learnMoreLink', e.target.value)}
            />
            {!validation.learnMoreLink.isValid && isLearnMoreLink && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="name"
              >
               Learn More Link can not be null
              </label>
            )}                  
          </div>
        </div>
        <div className="form-group">
          <label className="col-lg-3 control-label">Active Date</label>
          <div className="col-lg-9">
            {isDateEditMode ? <DatePicker
              selected={editDate}
              onChange={date => onSetDate(date)}
              dateFormat="MMMM D, YYYY"
            /> : <span onClick={() => setIsDateEditMode(!isDateEditMode)}>{moment(activeDate.value).format('MMMM D, YYYY')}</span>}
          </div>
        </div>
        <div className="text-right">
          <button
            type="button"
            className="btn btn-primary"
            onClick={onUpdate}
          >
            Edit
          </button>
        </div> 
      </div>
    </div>
  </div>
}