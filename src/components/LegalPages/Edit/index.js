import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _ from 'lodash'
import PNotify from 'pnotify'

import * as legalPagesActions from 'store/actions/legalPages'

import { Edit } from './Edit'

const EditLegalPages = (props) => {
  const { legalPagesActions, validation, match: { params: { id }}, editOrigin } = props

  useEffect(() => {
    legalPagesActions.getId(id)
  }, [])

  const change = (type, value) => {
    legalPagesActions.change({
      ...validation[type], value , isValid: value ? true : false, type
    })
  }

  const onUpdate = () => {
    const version = validation.version.value 
    const forceUpdate = validation.forceUpdate.value
    const termsLink = validation.termLink.value
    const privacyLink = validation.privacyLink.value
    const learnMoreLink = validation.learnMoreLink.value
    const activeDate = validation.activeDate.value

    if(version && termsLink && privacyLink && learnMoreLink && activeDate) {
      legalPagesActions.update(editOrigin.id ,{
        version,
        forceUpdate,
        termsLink,
        privacyLink,
        learnMoreLink,
        activeDate
      })
    } else {
      new PNotify({
        addclass: 'bg-danger',
        title: 'Error',
        text: 'All fields must be filled'
      })
    }
  }
  return <Edit {...props} change={change} onUpdate={onUpdate} />
}

const mapStateToProps = (state) => ({
  editOrigin: _.get(state, 'legalPages.editOrigin', {}),
  validation: _.get(state, 'legalPages.validation', {}),
})
  
const mapDispatchToProps = (dispatch) => ({
  legalPagesActions: bindActionCreators(legalPagesActions, dispatch),
})
  
export default connect(mapStateToProps, mapDispatchToProps)(EditLegalPages)