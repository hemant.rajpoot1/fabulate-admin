import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'

import _ from 'lodash'

import * as legalPagesActions from 'store/actions/legalPages'

import {LegalPages} from './LegalPages'

const LegalPagesContainer = (props) => {

  const { legalPagesActions } = props

  useEffect(() => {
    legalPagesActions.get()
  }, [])

  const remove = (id) => {
    legalPagesActions.remove(id)
  }

  return <LegalPages {...props} remove={remove} />
}

const mapStateToProps = (state) => ({
  origin: _.get(state, 'legalPages.origin', [])
})
  
const mapDispatchToProps = (dispatch) => {
  return {
    legalPagesActions: bindActionCreators(legalPagesActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LegalPagesContainer))