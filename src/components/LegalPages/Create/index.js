import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _ from 'lodash'
import PNotify from 'pnotify'

import * as legalPagesActions from 'store/actions/legalPages'
import { Create } from './Create'

const CreateLegalPages = (props) => {
  const { legalPagesActions, validation } = props

  useEffect(() => {
    legalPagesActions.clearValidation()
  }, [])

  const change = (type, value) => {
    legalPagesActions.change({
      ...validation[type], value , isValid: value ? true : false, type
    })
  }

  const onSubmit = (e) => {
    const version = validation.version.value
    const forceUpdate = validation.forceUpdate.value
    const termsLink = validation.termLink.value
    const privacyLink = validation.privacyLink.value
    const learnMoreLink = validation.learnMoreLink.value
    const activeDate = validation.activeDate.value

    if(version && termsLink && privacyLink && learnMoreLink && activeDate) {
      legalPagesActions.create({
        version,
        forceUpdate,
        termsLink,
        privacyLink,
        learnMoreLink,
        activeDate
      })
    } else {
      new PNotify({
        addclass: 'bg-danger',
        title: 'Error',
        text: 'All fields must be filled'
      })
    }
  }

  return <Create {...props} change={change} onSubmit={onSubmit} />
}

const mapStateToProps = (state) => ({
  origin: _.get(state, 'legalPages.origin', []),
  validation: _.get(state, 'legalPages.validation', {}),
})

const mapDispatchToProps = (dispatch) => ({
  legalPagesActions: bindActionCreators(legalPagesActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateLegalPages)