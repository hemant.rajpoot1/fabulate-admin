import React, { useEffect, useMemo } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as activeUserSessionsActions from 'store/actions/activeUserSessions'
import { ActiveUserSessionsList } from './ActiveUserSessionsList'

import _ from 'lodash'

const ActiveUserSessions = (props) => {

  useEffect(() => {
    const { filter, page, activeUserSessionsActions } = props
    activeUserSessionsActions.get(filter, page)
  }, [props.filter, props.page])

  return (
    <ActiveUserSessionsList {...props} />
  )
}

const mapStateToProps = (state) => {
  return {
    sessions: _.get(state, 'activeUserSessions.origin', []),
    filter: _.get(state, 'activeUserSessions.filter', {}),
    sort: _.get(state, 'activeUserSessions.sort', {}),
    page: _.get(state, 'activeUserSessions.page', 1),
    count: _.get(state, 'activeUserSessions.count', 0),
    isGetFetching: _.get(state, 'activeUserSessions.isGetFetching')
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    activeUserSessionsActions: bindActionCreators(activeUserSessionsActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ActiveUserSessions))
