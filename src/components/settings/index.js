import Settings from './Settings'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import * as settingsActions from 'store/actions/settings'
import { getField } from 'store/selectors/modelSelectors'

import { validation } from 'helpers/validation'
class SettingsContainer extends Component {

  componentDidMount() {
    const { settingsActions, settings } = this.props
    !_.size(settings) && settingsActions.get()
  }

  handleChange = (type, value) => {
    const { settingsActions, validRules } = this.props
    settingsActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  onSubmit = () => {
    const { settingsActions, validation } = this.props
    settingsActions.update({
      consumerKey: validation.consumerKey.value,
      consumerSecret: validation.consumerSecret.value
    },'xeroSettings')
  }

  render() {
    return (
      <Settings
        { ...this.props }
        handleChange={this.handleChange}
        onSubmit={this.onSubmit}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.settings.validationEdit))
  return {
    validRules: validRules,
    validation: state.settings.validationEdit,
    settings: state.settings.origin,
    translate: getTranslate(state.locale),
    isGetFetching: state.settings.isGetFetching
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    settingsActions: bindActionCreators(settingsActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SettingsContainer))
