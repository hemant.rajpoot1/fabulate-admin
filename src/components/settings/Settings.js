import React, { Component, Fragment } from 'react'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

import _ from 'lodash'

class Settings extends Component {

  render() {
    const {
      handleChange,
      validation,
      settings: { consumerKey, consumerSecret },
      onSubmit,
      isGetFetching
    } = this.props
    const changedConsumerKey = _.get(validation, 'consumerKey.value', '')
    const changedConsumerSecret = _.get(validation, 'consumerSecret.value', '')

    if(isGetFetching) {
      return (
        <SpinnerLoadData />
      )
    }
    return (
      <Fragment>
        <div className="panel panel-flat">
          <div className="panel-heading">
            <h5 className="panel-title">Settings</h5>
          </div>
        </div>

        <div className="panel panel-flat">
          <div className="panel-heading">
            <h6 className="panel-title">Xero settings</h6>
          </div>
          <div className="panel-body">
            <div className="form-group">
              <label className="col-lg-3 control-label mt-10">
                Xero consumer key
              </label>
              <div className="col-lg-9">
                <input
                  type="text"
                  className="form-control"
                  name="consumerKey"
                  value={changedConsumerKey || ''}
                  onChange={({ target }) => handleChange(target.name, target.value)}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-lg-3 control-label mt-10">
                Xero consumer secret
              </label>
              <div className="col-lg-9">
                <input
                  type="text"
                  className="form-control"
                  name="consumerSecret"
                  value={changedConsumerSecret || ''}
                  onChange={({ target }) => handleChange(target.name, target.value)}
                />
              </div>
            </div>
            <div className="text-right">
              <button
                type="button"
                className="btn btn-primary mt-20"
                onClick={onSubmit}
                disabled={
                  !changedConsumerKey || !changedConsumerSecret ||
                  changedConsumerKey === consumerKey || 
                  changedConsumerSecret === consumerSecret 
                }
              >
                Update
              </button>
            </div>
          </div>
        </div>
      </Fragment>
    )

  }

}

export default Settings