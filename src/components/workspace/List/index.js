import List from './List'

import React, { Component } from 'react'

import _ from 'lodash'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as workspaceActions from 'store/actions/workspace'
import * as userActions from 'store/actions/user'

class ListContainer extends Component {
  componentDidMount() {
    const { workspaceActions, filter } = this.props
    workspaceActions.get(filter)
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props

    if (!_.isEqual(prevProps.filter, filter)) {
      this.fetch(this.props.filter)
    }
  }

  fetch = filter => {
    const { workspaceActions } = this.props

    workspaceActions.get(filter)
  }

  fetchList = (page) => {
    const { workspaceActions } = this.props
    workspaceActions.get({ page })
  }

  render() {
    return (
      <List
        {...this.props}
        fetchList={this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    workspaces: state.workspace.origin,
    count: state.workspace.count,
    page: state.workspace.page,
    translate: getTranslate(state.locale),
    filter: state.workspace.filter
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
