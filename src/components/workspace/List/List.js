import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import Filter from 'components/ui/Filter'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import Pagination from 'react-js-pagination'

class List extends Component {
  onDelete(id, page, workspacesLength) {
    let { workspaceActions } = this.props

    let notice = new PNotify({
      title: 'Remove organization',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })
    
    const data = {
      id,
      page,
      workspacesLength
    }

    notice.get().on('pnotify.confirm', () => {
      workspaceActions.remove(data)
    })
  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { workspaceActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { workspaceSearch: value !== '' ? value : undefined }
      workspaceActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { workspaceActions } = this.props
    workspaceActions.resetFilter()
  }

  openUsers = id => {
    const { userActions, history } = this.props
    userActions.setFilter({ workspaceId: id, userSearchData: '' })
    history.push('/user')
  }

  renderSearchFilter = () => (
    <Filter
      searchItem="organizations"
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.workspaceSearch || ''}
      resetFilters={this.resetFilters}
      customClasses="float-none m-10p-0"
    />
  )

  render() {
    const { workspaces, count, page, fetchList } = this.props
    const workspacesLength = Object.keys(workspaces).length

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Organizations List</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              {_.map(workspaces, workspace => (
                <tr key={workspace.id}>
                  <td>{workspace.id}</td>
                  <td>
                    <Link to={`/workspace/${workspace.id}`} onClick={() => this.props.workspaceActions.setIsEdit(false)}>{workspace.name}</Link>
                  </td>
                  <td className="text-center">
                    <ul className="icons-list">
                      <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown">
                          <i className="icon-menu9"></i>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                          <li><Link to={`/workspace/${workspace.id}`} onClick={() => this.props.workspaceActions.setIsEdit(true)}><i className="icon-pencil"></i>Edit</Link></li>
                          <li onClick={() => this.onDelete(workspace.id, page, workspacesLength)}>
                            <a><i className="icon-cross"></i>Delete</a>
                          </li>
                          <li>
                            <a onClick={() => this.openUsers(workspace.id)}>
                              <i className="icon-users4"></i>
                              Watch users of organization
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </td>
                </tr>
              )
              )}
            </tbody>
          </table>
          {count > 20 && (
            <div className="pager p-20">
              <Pagination
                hideNavigation
                pageRangeDisplayed={3}
                activePage={page}
                itemsCountPerPage={20}
                totalItemsCount={count}
                onChange={(page) => fetchList(page)}
              />
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default withRouter(List)
