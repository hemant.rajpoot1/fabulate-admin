import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import * as workspaceActions from 'store/actions/workspace'
import * as xeroContactActions from 'store/actions/xeroContact'
import * as analyticsProjectActions from 'store/actions/analyticsProject'

import { getField } from 'store/selectors/modelSelectors'

class CreateContainer extends Component {
  
  componentDidMount() {
    const { analyticsProjectActions, workspaceActions } = this.props

    this.getXeroContacts()
    analyticsProjectActions.get()
    workspaceActions.resetAttachments()
  }

  getXeroContacts = () => {
    const { xeroContactActions } = this.props
    xeroContactActions.getXeroContact()
  }

  setFilter = (filter) => {
    const { xeroContactActions } = this.props
    xeroContactActions.getXeroContact(filter)
  }

  render() {
    return (
      <Create 
        { ...this.props } 
        getXeroContacts={this.getXeroContacts}
        setFilter={this.setFilter}
      />
    )
  }
}

const mapStateToProps = (state) => {
  const validRules = getField(state, Object.keys(state.workspace.validation))
  const currentUser = state.auth && _.get(state, 'auth.user', {})
  
  return {
    translate: getTranslate(state.locale),
    validation: state.workspace.validation,
    xeroContacts: state.xeroContact.contacts,
    isGetFetching: state.workspace.isGetFetching,
    analyticsProjects: state.analyticsProject.origin,
    search: state.workspace.search,
    currentUser,
    validRules,
    attachments: _.get(state, 'workspace.attachments', [])
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    xeroContactActions: bindActionCreators(xeroContactActions, dispatch),
    analyticsProjectActions: bindActionCreators(analyticsProjectActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
