import React, { Component } from 'react'
import Dropzone from 'react-dropzone'

import _ from 'lodash'

import { formattingQuery } from 'helpers/tools'
import { workspaceTypes, workspaceTypesValues } from 'helpers/params'

import Checkbox from 'components/ui/Checkbox'
import Select from 'components/ui/Select'
import FileLoad from 'components/ui/File'

class Create extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: false,
      isOpenAmplify: false,
      currentAnalyticsProject: null,
    }
  }

  onChangeSelectInput = (e, type, amplifyProduct) => {
    const { value } = e.target
    const { workspaceActions } = this.props
  
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        workspaceActions.search(type, { q: value }, amplifyProduct)
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  onSubmit = e => {
    const {
      workspaceActions,
      currentUser,
      validation
    } = this.props

    const formData = formattingQuery({
      name: validation.name.value,
      commission: validation.commission.value,
      icon: validation.icon.value,
      shortDescription: validation.shortDescription.value,
      longDescription: validation.longDescription.value,
      xeroId: validation.xeroId.value,
      accountCode: validation.accountCode.value,
      paymentTerms: validation.paymentTerms.value,
      bankNumber: validation.bankNumber.value,
      gst: validation.gst.value,
      abn: validation.abn.value,
      ownerId: currentUser.id,
      analyticsProjectId: _.get(validation, 'analyticsProjectId.value.id'),
      isUnlimited: validation.isUnlimited.value,
      copyProductCardOrganization: _.get(validation, 'copyProductCardOrganization.value.id', null),
      createAmplifyProductsOrganisation: _.get(validation, 'createAmplifyProductsOrganisation.value.id', null),
      workspaceType: validation.workspaceType.value
    })

    if (formData.name && validation.name.isValid) {
      e.preventDefault()
      workspaceActions.create(formData)
    }

  }

  changeInput = (type, value) => {
    const { validation, workspaceActions, validRules } = this.props
    workspaceActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  drop = (files) => {
    const { workspaceActions } = this.props

    if (files && files[0]) {
      workspaceActions.attachFile(files)
    }
  }

  uploadFile = (uri, preview) => {
    const { workspaceActions, validation } = this.props

    workspaceActions.uploadFile({ uri, preview })

    this.changeInput('icon', uri)
    
  }

  removeAttach = file => {
    const { workspaceActions } = this.props
    workspaceActions.removeAttach(file.preview)
  }


  render() {
    const {
      translate,
      validation,
      xeroContacts,
      getXeroContacts,
      setFilter,
      isGetFetching,
      search,
      analyticsProjects,
      attachments
    } = this.props

    const { currentAnalyticsProject } = this.state

    const currentAnalyticsProjects = _.size(search.analyticsProjects) ? search.analyticsProjects : analyticsProjects
    const isDefaultWorkspace = validation.workspaceType.value === workspaceTypesValues.default
    const isBuyerWorkspace = validation.workspaceType.value === workspaceTypesValues.buyer
    const isCreatorWorkspace = validation.workspaceType.value === workspaceTypesValues.creator

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">New organization</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>
        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Name</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.nameInput = ref : null}
                  type="text"
                  className="form-control"
                  name="name"
                  onBlur={e => (this.nameInput.value !== '' && validation.name.isValid === undefined) && this.changeInput('name', e.target.value)}
                  onChange={e => (validation.name.isValid !== undefined) && this.changeInput('name', e.target.value)}
                />
                {!validation.name.isValid && validation.name.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`workspaceError.${validation.name.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Workspace Type</label>
              <div className="col-lg-9">
                <Select
                  cantReset={true}
                  isSearch={false}
                  onSelect={item => {
                    this.changeInput('workspaceType', item.value)
                  }}
                  items={workspaceTypes}
                  value={_.get(workspaceTypes[validation.workspaceType.value], 'name')}
                />
              </div>
            </div>

            {(isDefaultWorkspace || isBuyerWorkspace) &&
              <div className="form-group">
                <label className="col-lg-3 control-label">Unlimited</label>
                <div className="checkbox col-lg-9">
                  <label className="check__label">
                    <Checkbox
                      name="isUnlimited"
                      checked={validation.isUnlimited.value}
                      onCheck={name => {
                        this.changeInput('isUnlimited', !validation.isUnlimited.value)
                      }}
                    />
                  </label>
                </div>
              </div>}

            <div className="form-group">
              <label className="col-lg-3 control-label">Commission</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.commissionInput = ref : null}
                  type="number"
                  step="0.01"
                  className="form-control"
                  name="commission"
                  onBlur={e => (this.commissionInput.value !== '' && validation.commission.isValid === undefined) && this.changeInput('commission', e.target.value)}
                  onChange={e => (validation.commission.isValid !== undefined) && this.changeInput('commission', e.target.value)}
                />
                {!validation.commission.isValid && validation.commission.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {`workspaceError.${validation.commission.message}`}
                  </label>
                )}
              </div>
            </div>
            {!isBuyerWorkspace && !isDefaultWorkspace && !isCreatorWorkspace && (
              <div className="form-group">
                <label className="col-lg-3 control-label">Icon</label>
                <div className="col-lg-9">
                  <Dropzone
                    onDrop={this.drop}
                    className="dropzone-asset"
                  >
                    {attachments.length > 0 ? (
                      <div className="tokenfield">
                        {_.map(attachments, (item, i) => (
                          <FileLoad
                            key={item.preview}
                            file={item}
                            uploadFile={this.uploadFile}
                            removeAttach={this.removeAttach}
                            isImagesShow={true}
                          />
                        ))}
                      </div>
                    ) : (
                      <div className="drop-text">
                      Click or drop files
                      </div>
                    )}
                  </Dropzone>
                </div>
              </div>
            )}
            <div className="form-group">
              <label className="col-lg-3 control-label">Short description</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.shortDescriptionInput = ref : null}
                  type="text"
                  className="form-control"
                  name="shortDescription"
                  onBlur={e => (this.shortDescriptionInput.value !== '' && validation.shortDescription.isValid === undefined) && this.changeInput('shortDescription', e.target.value)}
                  onChange={e => (validation.shortDescription.isValid !== undefined) && this.changeInput('shortDescription', e.target.value)}
                />
                {!validation.shortDescription.isValid && validation.shortDescription.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {validation.shortDescription.message}
                  </label>
                )}
              </div>
            </div>
            
            <div className="form-group">
              <label className="col-lg-3 control-label">Long description</label>
              <div className="col-lg-9">
                <textarea
                  ref={ref => ref ? this.longDescriptionInput = ref : null}
                  type="text"
                  className="form-control"
                  name="longDescription"
                  onBlur={e => (this.longDescriptionInput.value !== '' && validation.longDescription.isValid === undefined) && this.changeInput('longDescription', e.target.value)}
                  onChange={e => (validation.longDescription.isValid !== undefined) && this.changeInput('longDescription', e.target.value)}
                />
                {!validation.longDescription.isValid && validation.longDescription.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {validation.longDescription.message}
                  </label>
                )}
              </div>
            </div>
            <div className="form-group">
              <label className="col-lg-3 control-label">ABN</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.abnInput = ref : null}
                  type="number"
                  className="form-control"
                  name="abn"
                  onBlur={e => (this.abnInput.value !== '' && validation.abn.isValid === undefined) && this.changeInput('abn', e.target.value)}
                  onChange={e => (validation.abn.isValid !== undefined) && this.changeInput('abn', e.target.value)}
                />
                {!validation.abn.isValid && validation.abn.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {validation.abn.message}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Bank Account Number</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.bankNumberInput = ref : null}
                  type="text"
                  className="form-control"
                  name="bankNumber"
                  onBlur={e => (this.bankNumberInput.value !== '' && validation.bankNumber.isValid === undefined) && this.changeInput('bankNumber', e.target.value)}
                  onChange={e => (validation.bankNumber.isValid !== undefined) && this.changeInput('bankNumber', e.target.value)}
                />
                {!validation.bankNumber.isValid && validation.bankNumber.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {validation.bankNumber.message}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Registered for GST</label>
              <div className="checkbox col-lg-9">
                <label className="check__label">
                  <Checkbox
                    name="causeTargetNewCustomer"
                    checked={validation.gst.value}
                    onCheck={name => {
                      this.changeInput('gst',!validation.gst.value)
                    }}
                  />
                </label>
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Xero account code</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.accountCodeInput = ref : null}
                  type="text"
                  className="form-control"
                  name="accountCode"
                  onBlur={e => (this.accountCodeInput.value !== '' && validation.accountCode.isValid === undefined) && this.changeInput('accountCode', e.target.value)}
                  onChange={e => (validation.accountCode.isValid !== undefined) && this.changeInput('accountCode', e.target.value)}
                />
                {!validation.accountCode.isValid && validation.accountCode.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`workspaceError.${validation.accountCode.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Payment Terms (Days)</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.paymentTermsInput = ref : null}
                  type="text"
                  className="form-control"
                  name="paymentTerms"
                  onBlur={e => (this.paymentTermsInput.value !== '' && validation.paymentTerms.isValid === undefined) && this.changeInput('paymentTerms', e.target.value)}
                  onChange={e => (validation.paymentTerms.isValid !== undefined) && this.changeInput('paymentTerms', e.target.value)}
                />
                {!validation.paymentTerms.isValid && validation.paymentTerms.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`workspaceError.${validation.paymentTerms.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Xero Id</label>
              <div className="col-lg-9">
                <input
                  ref={ref => ref ? this.xeroIdInput = ref : null}
                  type="text"
                  className="form-control"
                  name="xeroId"
                  value={validation.xeroId.value || ''}
                  disabled={true}
                />
                {!validation.xeroId.isValid && validation.xeroId.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`workspaceError.${validation.xeroId.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <div className='col-lg-3'>
                <label className="control-label">
                  Xero contacts
                  <i className='icon-reload-alt pl-10' onClick={() => getXeroContacts()}/>
                  <label className='pl-10'>
                    <ul className="icons-list">
                      <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <i className="icon-filter4"></i>
                          <span className="caret"></span>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                          <li
                            onClick={() => setFilter()}
                          >
                            <div className="m-10">All</div>
                          </li>
                          <li
                            onClick={() => setFilter({isBuyer: true})}
                          >
                            <div className="m-10">Buyers</div>
                          </li>
                          <li
                            onClick={() => setFilter({isContractor: true})}
                          >
                            <div className="m-10">Contactos</div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </label>
                </label>
              </div>
              {_.size(xeroContacts) ? (
                <div className='pre-scrollable border'>
                  { _.map(xeroContacts, xcItem => (
                    <div
                      className={`cursor-pointer m-5 p-10 ${xcItem.ContactID === validation.xeroId.value ? 'bg-blue' : ''}`}
                      key={xcItem.ContactID}
                      onClick={() => this.changeInput('xeroId', xcItem.ContactID)}
                    >
                      {xcItem.FirstName ? `${xcItem.FirstName} ${xcItem.LastName ? xcItem.LastName : ''}` : xcItem.Name}
                    </div>
                  ))}
                </div>
              ) : null}
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Analytics Project</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onSearch={e => this.onChangeSelectInput(e, 'analyticsProjects')}
                  onSelect={value => {
                    this.changeInput('analyticsProjectId', value)
                    this.setState({
                      currentAnalyticsProject: value
                    })
                  }}
                  items={currentAnalyticsProjects}
                  value={_.get(currentAnalyticsProject, 'name')}
                />    
                {!_.isNil(validation.analyticsProjectId.isValid) && !validation.analyticsProject.isValid && validation.analyticsProject.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {validation.analyticsProjectId.message}
                  </label>
                )}
              </div>
            </div>

            {isBuyerWorkspace &&
              <div className="form-group">
                <label className="col-lg-3 control-label">Copy product card from Orginization</label>
                <div className="col-lg-9">
                  <Select
                    isSearch={true}
                    onClick={e => {
                      e.stopPropagation()
                      this.setState({
                        isOpen: true
                      })
                    }}
                    onSearch={e => this.onChangeSelectInput(e, 'workspace')}
                    onSelect={value => this.changeInput('copyProductCardOrganization', value)}
                    isOpen={this.state.isOpen}
                    items={search.workspace || []}
                    value={_.get(validation, 'copyProductCardOrganization.value.name', null)}
                  />
                </div>
              </div>}

            <div className="form-group">
              <label className="col-lg-3 control-label">Create Amplify products from organisation</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={e => {
                    e.stopPropagation()
                    this.setState({
                      isOpenAmplify: true
                    })
                  }}
                  onSearch={e => this.onChangeSelectInput(e, 'workspace', true )}
                  onSelect={value => this.changeInput('createAmplifyProductsOrganisation', value)}
                  isOpen={this.state.isOpenAmplify}
                  items={search.amplifyProduct || []}
                  value={_.get(validation, 'createAmplifyProductsOrganisation.value.name', null)}
                />
              </div>
            </div>

            <div className="text-right">
              <button
                type="button"
                className="btn btn-primary"
                onClick={e => this.onSubmit(e)}
                disabled={isGetFetching}
              >
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Create
