import React from 'react'

import './Input.css'

const Input = props => {
  const { name, value, isEdit, onChange } = props
  const currentValue = value || ''
  return isEdit ? (
    <input type="text" className="form-control" name={name} value={currentValue} onChange={onChange} />
  ) : (
    <span className="input-text">{currentValue}</span>
  )
}

export default Input
