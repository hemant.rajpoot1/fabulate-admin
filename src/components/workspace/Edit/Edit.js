import React, { Component } from 'react'
import Dropzone from 'react-dropzone'

import { workspaceTypes, workspaceTypesValues } from 'helpers/params'

import _ from 'lodash'

import Checkbox from 'components/ui/Checkbox'
import TagField from 'components/ui/TagField'
import Select from 'components/ui/Select'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import FileLoad from 'components/ui/File'

import Input from './Input'
import Textarea from './Textarea'

import './Edit.css'
class Edit extends Component {
  constructor(props) {
    super(props)

    this.state = {
      analyticsProject: null
    }
  }

  handleChange({ target }) {
    const { workspaceActions } = this.props
    workspaceActions.edit({ [target.name]: target.value })
  }

  searchContacts = value => {
    const { setFilter } = this.props
    setFilter({ name: value })
  }

  onSubmit() {
    const { workspaceActions, workspace } = this.props
    workspaceActions.update(workspace)
  }

  onClickCancel = () => {
    const { id, workspaceActions } = this.props

    workspaceActions.setIsEdit(false)
    workspaceActions.getId(id, true)
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { workspaceActions } = this.props
    if(this.timer) {
      clearTimeout(this.timer)
    }
    if (value !== '') {
      this.timer = setTimeout(() => {
        workspaceActions.search(type, { q: value })
      }, 800)
    }
  }

  handleOnSelect = (value) => {
    const { workspaceActions, workspace } = this.props

    workspaceActions.edit({
      users: {
        ...workspace.users,
        added: {
          ...workspace.users.added,
          [value.id]: {
            id: value.id,
            userName: value.name,
          }
        }
      }
    })
  }

  handleOnRemove = (value) => {
    const { workspace, workspaceUsers, workspaceActions } = this.props
    if(_.has(workspaceUsers, value)) {
      workspaceActions.edit({
        users: {
          ...workspace.users,
          deleted: [...workspace.users.deleted, value],
        }
      })
    } else {
      workspaceActions.edit({
        users: {
          ...workspace.users,
          added: _.keyBy(_.filter(workspace.users.added, user => !(user.id === value)), 'id'),
        }
      })
    }
  }

  renderButtons = () => {
    const { isEdit, workspaceActions, isGetFetching } = this.props
    return isEdit ? (
      <div>
        <button
          type="button"
          className="btn btn-primary cancel-button"
          disabled={isGetFetching}
          onClick={this.onClickCancel}
        >
          Cancel
        </button>
        <button
          type="button"
          className="btn btn-primary"
          disabled={isGetFetching}
          onClick={() => this.onSubmit()}
        >
          Update
        </button>
      </div>
    ) : (
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => workspaceActions.setIsEdit(true)}
      >
        Edit
      </button>
    )
  }

  drop = (files) => {
    const { workspaceActions } = this.props

    if (files && files[0]) {
      workspaceActions.attachFile(files)
    }
  }

  uploadFile = (uri, preview) => {
    const { workspaceActions, change } = this.props

    workspaceActions.uploadFile({ uri, preview })
    workspaceActions.edit({ 'icon': uri })  }

  removeAttach = file => {
    const { workspaceActions } = this.props
    workspaceActions.removeAttach(file.preview)
  }

  renderPanelBody = () => {
    const {
      workspace,
      workspaceUsers,
      xeroContacts,
      setFilter,
      getXeroContacts,
      xeroContactsFilter,
      isEdit,
      search,
      analyticsProjects,
      attachments
    } = this.props

    const users = {
      ..._.keyBy(
        _.filter(
          workspaceUsers,
          user => !_.includes(workspace.users.deleted, user.id)
        ),
        'id'
      ),
      ...workspace.users.added
    }

    const currentAnalyticsProjects = _.size(search.analyticsProjects) ? search.analyticsProjects : analyticsProjects
    const currentAnalyticsProject = _.get(this.state.analyticsProject || workspace.analyticsProject, 'name')
    const workspaceTypeName = _.get(workspaceTypes, `[${workspace.workspaceType}].name`, '')
    const isDefaultWorkspace = workspace.workspaceType === workspaceTypesValues.default
    const isBuyerWorkspace = workspace.workspaceType === workspaceTypesValues.buyer

    return (
      <div className="panel-body">
        <form className="form-horizontal">
          <div className="form-group">
            <label className="col-lg-3 control-label">Name</label>
            <div className="col-lg-9">
              <Input
                name="name"
                value={workspace.name}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          <div className="form-group center-flex">
            <label className="col-lg-3 control-label">Workspace Type</label>
            <div className="col-lg-9">
              {isEdit ? <Select
                cantReset={true}
                isSearch={false}
                onSelect={item =>
                  this.handleChange({ target: {
                    name: 'workspaceType',
                    value: item.value
                  }})
                }
                items={workspaceTypes}
                value={workspaceTypeName} 
              /> : workspaceTypeName }
            </div>
          </div>
          
          {(isDefaultWorkspace || isBuyerWorkspace) &&
            <div className="form-group">
              <label className="col-lg-3 control-label">Unlimited</label>
              <div className="col-lg-9">
                {isEdit ? (
                  <label className="check__label">
                    <Checkbox
                      name="isUnlimited"
                      checked={workspace.isUnlimited}
                      onCheck={name => {
                        this.handleChange({
                          target: {
                            name: 'isUnlimited',
                            value: !workspace.isUnlimited
                          }
                        })
                      }}
                    />
                  </label>
                ) : (
                  <span className="input-text">
                    {workspace.isUnlimited ? 'Yes' : 'No'}
                  </span>
                )}
              </div>
            </div>}

          <div className="form-group">
            <label className="col-lg-3 control-label">Commission</label>
            <div className="col-lg-9">
              <Input
                name="commission"
                value={workspace.commission}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">Icon</label>
            <div className="col-lg-9">
              {isEdit && <Dropzone onDrop={this.drop} className="dropzone-asset">
                {attachments.length > 0 && attachments[0].preview ? (
                  <div className="tokenfield">
                    {_.map(attachments, (item, i) => (
                      <FileLoad
                        key={item.preview}
                        file={item}
                        uploadFile={this.uploadFile}
                        removeAttach={this.removeAttach}
                        isImagesShow={true}
                      />
                    ))}
                  </div>
                ) : (
                  <div className="drop-text">Click or drop files</div>
                )}
              </Dropzone>}

              {!isEdit && attachments.length > 0 && attachments[0].preview && <div className="tokenfield">
                {_.map(attachments, (item, i) => (
                  <FileLoad
                    key={item.preview}
                    file={item}
                    uploadFile={this.uploadFile}
                    removeAttach={this.removeAttach}
                    isImagesShow={true}
                    isRemoveShow={false}
                  />
                ))}
              </div>}
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">Short description</label>
            <div className="col-lg-9">
              <Input
                name="shortDescription"
                value={workspace.shortDescription}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          
          <div className="form-group">
            <label className="col-lg-3 control-label">Long description</label>
            <div className="col-lg-9">
              <Textarea
                name="longDescription"
                value={workspace.longDescription}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">ABN</label>
            <div className="col-lg-9">
              <Input
                name="abn"
                value={workspace.abn}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">
              Bank Account Number
            </label>
            <div className="col-lg-9">
              <Input
                name="bankNumber"
                value={workspace.bankNumber}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">Registered for GST</label>
            <div className="col-lg-9">
              {isEdit ? (
                <label className="check__label">
                  <Checkbox
                    name="gst"
                    checked={workspace.gst || false}
                    onCheck={name => {
                      this.handleChange({
                        target: {
                          name: 'gst',
                          value: !workspace.gst
                        }
                      })
                    }}
                  />
                </label>
              ) : (
                <span className="input-text">
                  {workspace.gst ? 'Yes' : 'No'}
                </span>
              )}
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">Xero account code</label>
            <div className="col-lg-9">
              <Input
                name="accountCode"
                value={workspace.accountCode}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">
              Payment Terms (Days)
            </label>
            <div className="col-lg-9">
              <Input
                name="paymentTerms"
                value={workspace.paymentTerms}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3 control-label">Xero Id</label>
            <div className="col-lg-9">
              <Input
                name="xeroId"
                value={workspace.xeroId}
                isEdit={isEdit}
                onChange={e => this.handleChange(e)}
              />
            </div>
          </div>

          <div className="form-group">
            <div className="col-lg-3">
              <label className="control-label">
                Xero contacts
                <i
                  className="icon-reload-alt pl-10"
                  onClick={() => getXeroContacts()}
                />
                <label className="pl-10">
                  <ul className="icons-list">
                    <li className="dropdown">
                      <a
                        className="dropdown-toggle"
                        data-toggle="dropdown"
                        aria-expanded="false"
                      >
                        <i className="icon-filter4" />
                        <span className="caret" />
                      </a>
                      <ul className="dropdown-menu dropdown-menu-right">
                        <li onClick={() => setFilter()}>
                          <div className="m-10">All</div>
                        </li>
                        <li onClick={() => setFilter({ isBuyer: true })}>
                          <div className="m-10">Buyers</div>
                        </li>
                        <li onClick={() => setFilter({ isContractor: true })}>
                          <div className="m-10">Contactos</div>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </label>
              </label>
            </div>
            <div className="col-lg-9">
              <Input
                name="xeroId"
                value={xeroContactsFilter}
                isEdit={isEdit}
                onChange={e => this.searchContacts(e.target.value)}
              />
              {_.size(xeroContacts) ? (
                <div className="pre-scrollable border">
                  {_.map(xeroContacts, xcItem => (
                    <div
                      className={`cursor-pointer m-5 p-10 ${
                        xcItem.ContactID === workspace.xeroId ? 'bg-blue' : ''
                      }`}
                      key={xcItem.ContactID}
                      onClick={() =>
                        this.handleChange({
                          target: {
                            name: 'xeroId',
                            value: xcItem.ContactID
                          }
                        })
                      }
                    >
                      {xcItem.Name ||
                        `${xcItem.FirstName} ${
                          xcItem.LastName ? xcItem.LastName : ''
                        }`}
                    </div>
                  ))}
                </div>
              ) : null}
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">
              Analytics Project
            </label>
            <div className="col-lg-9">
              {isEdit ? <Select
                isSearch={true}
                onSearch={e => this.onChangeSelectInput(e, 'analyticsProjects')}
                onSelect={value => {
                  const isResetValue = _.isNil(value)
                  this.handleChange({ target: { name: 'analyticsProjectId', value: isResetValue ? value : value.id } })
                  this.setState({
                    analyticsProject: isResetValue ? {
                      name: ''
                    } : value
                  })
                }}
                items={currentAnalyticsProjects}
                value={currentAnalyticsProject}
              /> : currentAnalyticsProject}
            </div>
          </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">Users</label>
            <div className="col-lg-9">
              <TagField
                data={_.keys(users)}
                tagObj={users}
                onlyTags={true}
                isSelectEnabled={false}
                isStatic={!isEdit}
                labelKey='userName'
                onRemove={this.handleOnRemove}
              />
              {isEdit && <Select
                isSearch={true}
                onSearch={e => this.onChangeSelectInput(e, 'user')}
                onSelect={this.handleOnSelect}
                items={search.user}
                value={''}
              /> }
            </div>
          </div>

          <div className="text-right">{this.renderButtons()}</div>
        </form>
      </div>
    )
  }

  render() {
    const { isEdit, isGetIdFetching } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">{isEdit ? 'Edit' : 'View'}</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>

        {isGetIdFetching ? <SpinnerLoadData isAbsolute={false} delay={0} /> : this.renderPanelBody()}
      </div>
    )
  }
}

export default Edit
