import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import * as workspaceActions from 'store/actions/workspace'
import * as xeroContactActions from 'store/actions/xeroContact'
import * as analyticsProjectActions from 'store/actions/analyticsProject'

class EditContainer extends Component {

  componentDidMount() {
    const { workspaceActions, id, analyticsProjectActions } = this.props

    workspaceActions.getId(id, true)
    analyticsProjectActions.get()
  }

  setFilter = (filter) => {
    const { xeroContactActions } = this.props
    xeroContactActions.getXeroContact(filter)
  }

  getXeroContacts = () => {
    const {
      xeroContactActions
    } = this.props
    
    xeroContactActions.getXeroContact()
  }

  render() {
    return (
      <Edit 
        { ...this.props } 
        getXeroContacts={this.getXeroContacts}  
        setFilter={this.setFilter}    
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    xeroContactsFilter: _.get (state, 'xeroContact.filter.name'),
    workspace: state.workspace.edit,
    workspaceUsers: state.workspace.users,
    analyticsProjects: state.analyticsProject.origin,
    xeroContacts: state.xeroContact.contacts,
    search: state.workspace.search,
    isEdit: state.workspace.edit.isEdit,
    isGetFetching: state.workspace.isGetFetching,
    isGetIdFetching: state.workspace.isGetIdFetching,
    translate: getTranslate(state.locale),
    id: props.match.params.id,
    attachments: _.get(state, 'workspace.attachments', [])
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    xeroContactActions: bindActionCreators(xeroContactActions, dispatch),
    analyticsProjectActions: bindActionCreators(analyticsProjectActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
