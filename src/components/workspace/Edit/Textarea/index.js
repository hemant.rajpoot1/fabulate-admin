import React from 'react'

import './Textarea.css'

const Textarea = props => {
  const { name, value, isEdit, onChange } = props
  const currentValue = value || ''
  return isEdit ? (
    <textarea className='form-control' name={name} onChange={onChange} value={currentValue} />
  ) : (
    <span className='textarea-text'>{currentValue}</span>
  )
}

export default Textarea
