import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import PNotify from 'pnotify'

import Filter from 'components/ui/Filter'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

import 'pnotify/dist/pnotify.confirm'

class WorkflowList extends Component {

  onDelete(id) {
    const { templateActions } = this.props

    let notice = new PNotify({
      title: 'Remove workflow',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      templateActions.remove(id)
    })

  }

  setSearchFilter = ({ target }) => {    
    const { value } = target
    const { templateActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { workflowSearch: value !== '' ? value : undefined }
      templateActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { templateActions } = this.props
    templateActions.resetFilter()
  }


  renderSearchFilter = () => (
    <Filter
      searchItem="Product card"
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.workflowSearch || ''}
      resetFilters={this.resetFilters}
      customClasses="float-none m-10p-0"
    />
  )


  render() {

    const {
      templates,
      isGetFetching
    } = this.props

    return isGetFetching ? <SpinnerLoadData/> :  (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Workflow</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Workflow name</th>
                <th>Milestones count</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>

            <tbody>
              {
                _.map(templates, (template, i) =>
                  (
                    <tr key={`${template.id}_index_${i}`}>
                      <td>{template.id}</td>
                      <td>{template.templateName}</td>
                      <td>{template.templateTask ? template.templateTask.length : '-'}</td>
                      <td className="text-center">
                        <ul className="icons-list">
                          <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown">
                              <i className="icon-menu9"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-menu-right">
                              <li><Link to={`/workflow-edit/${template.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                              <li onClick={() => this.onDelete(template.id)}>
                                <a><i className="icon-cross"></i>Delete</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </td>
                    </tr>
                  )
                )
              }
            </tbody>
          </table>

        </div>
      </div>
    )

  }

}

export default withRouter(WorkflowList)
