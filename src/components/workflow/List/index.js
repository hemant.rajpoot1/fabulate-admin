import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import _ from 'lodash'

import WorkflowList from './WorkflowList'

import * as templateActions from 'store/actions/template'

class WorkflowListContainer extends Component {

  componentDidMount() {
    const { templateActions } = this.props
    templateActions.get()
  }

  componentDidUpdate(prevProps){
    const { filter } = this.props
    
    if(!_.isEqual(prevProps.filter, filter)){
      this.fetch(filter)
    }
  }

  fetch = filter => {
    const { templateActions } = this.props

    templateActions.get(filter)
  }

  render() {
    return (
      <WorkflowList { ...this.props }/>
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    templates: state.template.origin,
    translate: getTranslate(state.locale),
    filter: _.get(state, 'template.filter', {}),
    isGetFetching: _.get(state, 'template.isGetFetching')
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    templateActions: bindActionCreators(templateActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkflowListContainer))
