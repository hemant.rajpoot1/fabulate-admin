import WorkflowCreate from './WorkflowCreate'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as templateActions from 'store/actions/template'
import { getField } from 'store/selectors/modelSelectors'

import { formattingValidObj } from 'helpers/tools'

class WorkflowCreateContainer extends Component {

  change = (type, value) => {
    const { validation, templateActions, validRules } = this.props
    templateActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  } 

  removeTask = index => {
    const { templateActions } = this.props
    templateActions.removeTask(index, true)
  }

  addTask = data => {
    const { templateActions } = this.props
    templateActions.addTask(data, true)
  }  

  updateTask = (data, index) => {
    const { templateActions } = this.props
    templateActions.updateTask(data, index, true)
  } 

  onSubmit = () => {
    const { templateActions, validation, tasks, currentUser } = this.props
    
    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
      tasks,
      createrId: currentUser.id
    }
    if (formData.templateName) {
      templateActions.create(formData)
    } else {
      this.change('templateName', validation.templateName.value)
    }
  }  

  render() {
    return (
      <WorkflowCreate
        {...this.props}
        change={this.change}
        removeTask={this.removeTask}
        addTask={this.addTask}
        updateTask={this.updateTask}
        onSubmit={this.onSubmit}
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.template.validationCreate))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const token = state.auth.accessToken || null
  let { id } = props.match.params

  return {
    tasks: state.template.tasksCreate,
    validation: state.template.validationCreate,
    currentUser,
    validRules,
    token,
    id
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    templateActions: bindActionCreators(templateActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkflowCreateContainer))

