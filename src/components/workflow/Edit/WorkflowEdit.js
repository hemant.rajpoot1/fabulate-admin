import React, { Component } from 'react'
import _ from 'lodash'
import Select from 'components/ui/Select'
import { roles } from 'helpers/params'

class WorkflowEdit extends Component {
  addTask = () => {
    const { currentUser, addTask } = this.props
    const initTask = {
      userId: currentUser.id,
      taskName: null,
      dueDate: new Date(),
      time: null, 
      amount: null,
      taskRole: 3
    }

    addTask(initTask)
  }

  handleOnSelect = (field, item, index) => {
    const { updateTask, tasks } = this.props
    const nextTasks = {
      ...tasks[index],
      [field]: item ? item.value : null
    }

    if(item && item.value === 2) {
      nextTasks.amount = 0
    }

    updateTask(nextTasks, index)
  }

  handleChangeText(e, name, itemTask, index) {
    const trimValue = e.target.value.trim()
    if (trimValue) {
      const data = { ...itemTask.notificationText }
      data[name] = e.target.value
      this.handleChange({ target: { name: 'notificationText', value: data } }, index)
    }
  }

  handleChange({ target }, index) {
    const { updateTask, tasks } = this.props
    const nextTasks = {
      ...tasks[index],
      [target.name]: target.value
    }


    updateTask(nextTasks, index)
  }

  modificatedContent = (text) => {
    if (!_.isNull(text)) {
      return text.replace(/\\"/g, '"')
    }

    return ''
  }

  render() {
    const { tasks, change, removeTask, onSubmit, validation } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Edit</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Workflow name</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="templateName"
                  ref={ref => { this.templateName = ref }}
                  value={validation.templateName.value}
                  onChange={e => change('templateName', e.target.value)}
                />
                {!validation.templateName.isValid && validation.templateName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    Workflow name can not be null
                  </label>
                )}                 
              </div>
            </div>

            <div>
              <h5>
                <label>
                  Milestones
                </label> 
              </h5>           
            </div>
            
            <div className="form-group">
              {tasks.length > 0 && (
                <div>
                  {_.map(tasks, (itemTask, i) => (
                    <div key={`item_task_${i}`}>
                      <h6 className="ml-10">
                        <label>
                          Milestone #{i + 1}
                        </label> 
                      </h6>        
                      <div className="form-group row" style={{ width: '100%' }}>
                        <div className="col-sm-12 col-md-6 label__pad">
                          <label>
                            Description
                          </label>
                          <textarea
                            rows="1"
                            className="form-control"
                            type="text"
                            name="taskName"
                            ref={ref => { this.causeName = ref }}
                            value={itemTask.taskName || ''}
                            onChange={e => this.handleChange(e, i)}
                          />

                        </div>
                        <div className="col-sm-12 col-md-6 label__pad">
                          <label className="pb-14">
                            Time, %
                          </label>
                          <input
                            className="form-control"
                            type="text"
                            name="time"
                            ref={ref => { this.time = ref }}
                            value={itemTask.time || ''}
                            onChange={e => this.handleChange(e, i)}
                          />

                        </div>
                        <div className="col-sm-12 col-md-6 label__pad">
                          <label className="pb-14">
                            Amount, %
                          </label>
                          <input
                            readOnly={itemTask.taskRole === 2}
                            className="form-control"
                            type="text"
                            name="amount"
                            ref={ref => { this.amount = ref }}
                            value={itemTask.amount || ''}
                            onChange={e => this.handleChange(e, i)}
                          />                       
                        </div>

                        <div className="col-sm-12 col-md-6 label__pad">
                          <label className="pb-14">
                            Role
                          </label>
                          <Select
                            keyValue="taskRole"
                            isSearch={false}
                            placeholder={roles[3].name}
                            onSelect={value => {
                              this.handleOnSelect('taskRole', value, i)
                            }}
                            items={roles}
                            value={!_.isNil(itemTask.taskRole)
                              ? roles[itemTask.taskRole].name
                              : undefined
                            } 
                          />                           
                        </div> 

                        <div className="col-sm-12 col-md-6 label__pad">
                          <label className="pb-14">
                            Content (HTML)
                          </label>
                          <textarea
                            rows='1'
                            className="form-control"
                            type="text"
                            name="content"
                            onChange={e => this.handleChange(e, i)}
                            value={this.modificatedContent(_.get(itemTask, 'content', null))}
                          />                       
                        </div>

                        <div className="col-sm-12 col-md-12">
                          <div className="col-sm-12 col-md-6 label__pad">
                            <label className="pb-14">
                              Notification text for{itemTask.taskRole === 3 ? ' review' : ' approve'}
                            </label>
                            <textarea
                              className="form-control"
                              type="text"
                              name="notificationText"
                              onChange={e => this.handleChangeText(e, 'firstText' ,itemTask, i)}
                              value={_.get(itemTask, 'notificationText.firstText', '')}
                              rows={10}
                            ></textarea>
                          </div>

                          {itemTask.taskRole !== 3 && 
                            <div className="col-sm-12 col-md-6 label__pad">
                              <label className="pb-14">
                                Notification text for decline
                              </label>
                              <textarea
                                className="form-control"
                                type="text"
                                name="notificationText"
                                onChange={e => this.handleChangeText(e, 'secondText', itemTask, i)}
                                value={_.get(itemTask, 'notificationText.secondText', '')}
                                rows={10}
                              ></textarea>
                            </div>    
                          }
                        </div>             
                      </div>
                      <div className="text-right">
                        <button className="btn legitRipple" onClick={() => removeTask(i)}>
                          <i className="mr-10 icon-stack-minus"> </i>
                          Remove
                        </button>
                      </div>
                    </div>
                  ))}
                </div>
              )}
            </div>
          </div>

          <div className="text-right">
            <button className="btn" style={{marginRight: 20}} onClick={() => this.addTask()}>add task</button>
            <button className="btn btn-primary" onClick={() => onSubmit()}>update template</button>
          </div>
        </div>

      </div>
    )
  }
}

export default WorkflowEdit
