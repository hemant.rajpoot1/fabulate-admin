import Milestone from './Milestone'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

class MilestoneContainer extends Component {

  render() {
    return (
      <Milestone {...this.props} />
    )
  }

}

let mapStateToProps = (state, props) => {
  return {
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MilestoneContainer))

