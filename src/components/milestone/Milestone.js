import React, { Component } from 'react'

import MilestoneItem from './MilestoneItem'
import MilestoneList from './MilestoneList'

import './Milestone.css'

class Milestone extends Component {

  render() {
    const { 
      qty,
      selectedMilestone
    } = this.props

    const renderMilestone = []
    for (let i = 0; i <= qty; i++) {
      renderMilestone.push(
        <MilestoneList
          key={`milestones_container_${i}`}
          {...this.props}
          qty={i + 1}
          onSelectTask={this.onSelectTask}
        />
      )
    }
    return (
      <div className="milestone-container">
        <div className="panel panel-body milestone-list-container">
          { renderMilestone }
        </div>

        {selectedMilestone && (
          <div className="milestone-item-container">
            <MilestoneItem 
              selectedMilestone={selectedMilestone} 
              {...this.props}            
            />
          </div>
        )}
      </div>
    )
  }
}

export default Milestone
