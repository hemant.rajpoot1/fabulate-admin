import React, { Component } from 'react'
import moment from 'moment'
import _ from 'lodash'

import Chat from 'components/form/Chat'
import avatar from 'assets/images/image.png'

import { milestoneStatus } from 'helpers/params'

class MilestoneItem extends Component {

  render() {
    const {
      selectedMilestone,
      productCost,
      hiredUser,
      status,
      taskInfo,
      isCurrentUserHired,
      isCurrentUserOwner,
      change,
      onSelectStatus,
    } = this.props
    const information = taskInfo && taskInfo.taskInfo && taskInfo.taskInfo[0] 
      ? taskInfo.taskInfo[0]
      : {}
    const statusTask = milestoneStatus[information.taskStatus || selectedMilestone.statusTask || 0]

    const causeTaskId = _.isEmpty(information) 
      ? 'createNew'
      : information.id

    return (
      <div className="panel panel-white">
        <div className="panel-heading">
          <h6 className="panel-title text-semibold">
            {selectedMilestone.taskName}
            <a className="heading-elements-toggle">
              <i className="icon-more"></i>
            </a>
          </h6>
        </div>
        <div className="panel-heading milestone-info-padding">
          <div>
            <div className="media-left media-middle milestone-pr">
              <a>
                <img src={hiredUser.image || avatar} className="img-circle img-md" alt="" />
              </a>
            </div>

            <div className="media-body milestone-contractor">
              <div className="media-heading text-semibold">{ hiredUser.userName }</div>
            </div>
          </div>
          <div className="heading-elements">
            <ul className="list-inline list-inline-separate heading-text">
              <li>
                {/* Cost:&nbsp; */}
                <span className="text-semibold">
                  ${productCost * selectedMilestone.amount / 100}
                </span>
              </li>
              <li>
                {/* Due date:&nbsp; */}
                <span className="text-semibold">
                  {selectedMilestone.dueDate ? moment(selectedMilestone.dueDate).format('MM/DD/YYYY') : '-'}
                </span>
              </li>
              <li>
                {(isCurrentUserHired || isCurrentUserOwner) && status && (
                  <ul className="list list-unstyled text-right">
                    <li data-toggle="dropdown" aria-expanded="false" className="dropdown">
                      <a className={`label ${statusTask && statusTask.color} dropdown-toggle`}>{statusTask && statusTask.name}</a>
                    </li>
                    {isCurrentUserHired && statusTask && statusTask.value < 3 && (
                      <ul className="dropdown-menu dropdown-menu-right">
                        {_.map(milestoneStatus, statusItem => statusItem.value <= 2 ? (
                          <li
                            onClick={async () => {
                              await change('currentMilestoneTask', selectedMilestone.milestoneTask)
                              await change('currentMilestoneStatus', statusItem.value)
                              onSelectStatus(selectedMilestone.id, causeTaskId)
                            }}
                            key={`selectedMilestone.milestoneTask_${selectedMilestone.milestoneTask}_${statusItem.name}`}
                          >
                            <a>
                              <span className={`status-mark border-${statusItem.border} position-left`}></span>
                              {statusItem.name}
                            </a>
                          </li>
                        ) : null)}
                      </ul>
                    )}
                    {isCurrentUserOwner && statusTask && statusTask.value >= 2 && (
                      <ul className="dropdown-menu dropdown-menu-right">
                        {_.map(milestoneStatus, statusItem => statusItem.value >= 3 ? (
                          <li
                            onClick={async () => {
                              await change('currentMilestoneTask', selectedMilestone.milestoneTask)
                              await change('currentMilestoneStatus', statusItem.value)
                              onSelectStatus(selectedMilestone.id, causeTaskId)
                            }}
                            key={`selectedMilestone.milestoneTask_${selectedMilestone.milestoneTask}_${statusItem.name}`}
                          >
                            <a>
                              <span className={`status-mark border-${statusItem.border} position-left`}></span>
                              {statusItem.name}
                            </a>
                          </li>
                        ) : null)}
                      </ul>
                    )}
                  </ul>
                )}
              </li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <Chat />
        </div>
      </div>
    )
  }
}

export default MilestoneItem
