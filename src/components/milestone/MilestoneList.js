import React, { Component } from 'react'
import _ from 'lodash'

import { milestoneStatus } from 'helpers/params'

class MilestoneList extends Component {

  render() {
    const {
      tasks,
      status,
      productCost,
      qty,
      currentMilestoneTask,
      currentMilestoneStatus,
      isCurrentUserHired,
      isCurrentUserOwner,
      change,
      onSelectStatus,
      selectMilestone,
      hiredUserId
    } = this.props

    return (
      <div>
        <div className="row">
          <div className="col-sm-12 col-md-12">
            <h5 className="mb-zero">
              Milestone #{qty}
            </h5>
          </div>
        </div>

        <div>
          {tasks && tasks.length > 0 && (
            <ul className="list-feed media-list list-feed-time">
              {_.map(tasks, (taskItem, i) => {
                const milestoneTask = parseInt(`${qty}${i}`, 10)
                const isCurrentTask = milestoneTask === currentMilestoneTask
                const causeTask = taskItem.taskInfo 
                  && taskItem.taskInfo.length > 0 
                  && taskItem.taskInfo.find(item => item.milestoneQty === milestoneTask)
                const statusTask = isCurrentTask 
                  ? milestoneStatus[currentMilestoneStatus] 
                  : milestoneStatus[causeTask ? causeTask.taskStatus :  0]

                const causeTaskId = causeTask
                  ? causeTask.id
                  : 'createNew'

                return (
                  <li className={`media border-${hiredUserId && (isCurrentUserHired || isCurrentUserOwner) && isCurrentTask ? 'warning' : 'success'}`} key={`qty_${qty}_taskItem_${i}`}>
                    <span className="feed-time text-muted">
                      ${productCost * taskItem.amount / 100}
                    </span>
                    <div className="media-body">
                      {taskItem.taskName}
                    </div>

                    <div className="media-right">
                      <ul className="icons-list icons-list-extended text-nowrap">

                        <li>
                          {hiredUserId && (isCurrentUserHired || isCurrentUserOwner) && status && (
                            <ul className="list list-unstyled text-right">
                              <li data-toggle="dropdown" aria-expanded="false" className="dropdown">
                                <a className={`label ${statusTask && statusTask.color} dropdown-toggle`}>{statusTask && statusTask.name}</a>
                              </li>
                              {isCurrentUserHired && statusTask && (statusTask.value < 3 || statusTask.value === 4) && (
                                <ul className="dropdown-menu dropdown-menu-right">
                                  {_.map(milestoneStatus, statusItem => statusItem.value <= 2 ? (
                                    <li
                                      onClick={async () => {
                                        if (!isCurrentTask) {
                                          await change('currentMilestoneTask', milestoneTask)
                                        }
                                        await change('currentMilestoneStatus', statusItem.value)
                                        onSelectStatus(taskItem.id, causeTaskId)
                                      }}
                                      key={`milestoneTask_${milestoneTask}_${statusItem.name}`}
                                    >
                                      <a>
                                        <span className={`status-mark border-${statusItem.border} position-left`}></span>
                                        {statusItem.name}
                                      </a>
                                    </li>
                                  ) : null)}
                                </ul>
                              )}
                              {isCurrentUserOwner && statusTask && statusTask.value >= 2 && (
                                <ul className="dropdown-menu dropdown-menu-right">
                                  {_.map(milestoneStatus, statusItem => statusItem.value >= 3 ? (
                                    <li
                                      onClick={async () => {
                                        if (!isCurrentTask) {
                                          await change('currentMilestoneTask', milestoneTask)
                                        }
                                        await change('currentMilestoneStatus', statusItem.value)
                                        onSelectStatus(taskItem.id, causeTaskId)
                                      }}
                                      key={`milestoneTask_${milestoneTask}_${statusItem.name}`}
                                    >
                                      <a>
                                        <span className={`status-mark border-${statusItem.border} position-left`}></span>
                                        {statusItem.name}
                                      </a>
                                    </li>
                                  ) : null)}
                                </ul>
                              )}
                            </ul>
                          )}                      
                        </li>

                        <li 
                          onClick={() => selectMilestone({
                            ...taskItem,
                            milestoneTask,
                            statusTask: statusTask.value
                          })}
                        >
                          <a><i className="icon-circle-right2"></i></a>
                        </li>
                      </ul>
                    </div>
                  </li>
                )
              })}
            </ul>
          )}
        </div>
      </div>
    )
  }
}

export default MilestoneList
