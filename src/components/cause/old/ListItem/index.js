import ListItem from './ListItem'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as causeActions from 'store/actions/cause'
import * as causeUserActions from 'store/actions/causeUser'
import * as productActions from 'store/actions/product'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'
import _ from 'lodash'

class ListItemContainer extends Component {

  componentDidMount() {
    const { 
      id, 
      // currentUser, 
      // products,
      causeActions, 
      // productActions
    } = this.props

    // const isContractor = currentUser.permissions === 3
    // let query = {}

    // if(currentUser.permissions === 3) {
    //   query = {
    //     contractorId: currentUser.id
    //   }
    // }

    causeActions.getId(id)

    // if(_.isEmpty(products)) {
    //   productActions.get({})
    // }

    // if(isContractor) {
    //   causeActions.getRequest({
    //     causeId: id,
    //     contractorId: currentUser.id
    //   })
    // }

  }

  componentDidUpdate(prevProps) {
    if(!_.isEqual(this.props.selectedMilestone, prevProps.selectedMilestone)) {
      this.getTask(this.props)
    }
  }

  download = (id, ref) => {
    const { causeActions } = this.props
    causeActions.downloadFile(id, ref)
  }

  request = (data) => {
    const { id, edit, currentUser, causeUserActions } = this.props
    if (edit.buyerId && id) {
      causeUserActions.create({
        contractorId: currentUser.id,
        buyerId: edit.buyerId,
        causeId: id,
        causeUserStatus: 3,
        ...data
      })
    }
  }

  updateRequest = (data) => {
    const { id, currentUser, causeUserActions } = this.props
    if (id) {
      causeUserActions.update({
        id,
        ...data
      }, currentUser)
    }
  }  

  requestToInvite = (id, status, index) => {
    if(id) {
      const { causeUserActions, currentUser } = this.props
      causeUserActions.update({
        id,
        causeUserStatus: status
      }, currentUser)
    }
  } 

  removeRequest = (id) => {
    if(id) {
      const { causeUserActions, causeActions } = this.props
      causeUserActions.remove(id)
      causeActions.requestData({})      
    }
  }

  change = (type, value) => {
    const { validation, causeActions, validRules } = this.props
    causeActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }  

  onSelectStatus = (taskId, causeTaskId ) => {
    const {
      causeActions,
      validation,
      id,
      selectedMilestone
    } = this.props

    const validObj = formattingValidObj({
      currentMilestoneTask: validation.currentMilestoneTask,
      currentMilestoneStatus: validation.currentMilestoneStatus
    })


    const formData = {
      ...validObj,
      id
    }
    causeActions.update(formData, undefined, { causeTaskId, taskId })

    if(selectedMilestone && taskId === selectedMilestone.id) {
      this.updateSelectedMilestone({
        ...selectedMilestone,
        statusTask: validation.currentMilestoneStatus.value
      })
    }
  }  

  selectMilestone = data => {
    const { causeActions } = this.props
    causeActions.selectMilestone(data)
  }

  updateSelectedMilestone = data => {
    const { causeActions } = this.props

    causeActions.updateSelectedMilestone(data)
  }  


  getTask = (props) => {
    const { causeActions, selectedMilestone, id } = props
    if(selectedMilestone && selectedMilestone.id) {
      causeActions.getTask(selectedMilestone.id, {
        causeId: id,
        milestoneQty: selectedMilestone.milestoneTask
      })
    }
  }

  render() {

    return (
      <ListItem 
        {...this.props} 
        download={this.download} 
        request={this.request} 
        requestToInvite={this.requestToInvite}
        removeRequest={this.removeRequest}
        change={this.change}
        updateRequest={this.updateRequest}
        onSelectStatus={this.onSelectStatus}
        selectMilestone={this.selectMilestone}
        updateSelectedMilestone={this.updateSelectedMilestone}
      />
    )
  }

}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.cause.validationEdit))  
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  let { id } = props.match.params

  return {
    translate: getTranslate(state.locale),
    validation: state.cause.validationEdit,
    attachments: state.cause.attachmentsEdit,
    search: state.cause.search,
    milestoneCause: state.cause.edit.milestoneCause,
    edit: state.cause.edit,
    taskInfo: state.cause.taskInfo,    
    products: state.product.origin,        
    selectedMilestone: state.cause.selectedMilestone,
    isGetIdFetching: state.cause.isGetIdFetching,
    currentUser,
    id,
    validRules,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    causeUserActions: bindActionCreators(causeUserActions, dispatch),    
    productActions: bindActionCreators(productActions, dispatch)    
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListItemContainer))

