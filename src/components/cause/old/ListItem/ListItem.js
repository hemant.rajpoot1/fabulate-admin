import React, { Component } from 'react'
import _ from 'lodash'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'

import BriefFormOneShow from 'components/form/BriefFormOneShow'
import BriefFormTwoShow from 'components/form/BriefFormTwoShow'
import BriefFormThreeShow from 'components/form/BriefFormThreeShow'

import Milestone from 'components/milestone'



import { causeUserStatus } from 'helpers/params'
import './ListItem.css'

class ListItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tab: 0
    }
  }

  setTab = index => {
    const { tab } = this.state

    if (index !== tab) {
      this.setState({
        tab: index
      })
    }
  }

  renderSwitch = (causeUserId, tasks) => {
    const { tab } = this.state
    const {
      id,
      currentUser,
      request,
      updateRequest,
      edit,
      selectedMilestone,
      requestToInvite,
      products,
      validation,
      change,
      onSelectStatus,
      selectMilestone,
      updateSelectedMilestone,
      taskInfo,
      isGetIdFetching
    } = this.props

    const isContractor = currentUser.permissions === 3
    const productCost = products && edit.causeProduct
      ? edit.causeProduct.productCost
      : 0

    if(isGetIdFetching) {
      return ( <SpinnerLoadData /> )
    }

    switch (tab) {
    case 2: {
      return (
        <Milestone 
          status={true}
          tasks={tasks}
          productCost={productCost}
          currentMilestoneTask={validation.currentMilestoneTask.value}
          currentMilestoneStatus={validation.currentMilestoneStatus.value}
          isCurrentUserHired={edit.hiredUserId === currentUser.id}
          isCurrentUserOwner={validation.ownerId.value === currentUser.id}
          change={change}
          onSelectStatus={onSelectStatus}
          qty={edit.causeFormatQty || 0}
          hiredUser={edit.hiredUserCause || {}}
          selectMilestone={selectMilestone}
          updateSelectedMilestone={updateSelectedMilestone}
          selectedMilestone={selectedMilestone}
          taskInfo={taskInfo}
          hiredUserId={edit.hiredUserId}
        />
      )
    }
    default: {
      return (
        <div className="container-custom brief-form brief-main-form panel panel-flat">
          <BriefFormOneShow
            {...this.props}
          />
          <BriefFormTwoShow
            {...this.props}
          />
          <BriefFormThreeShow
            product={edit.causeProduct || {}}
            {...this.props}
          />
        </div>
      )
    }
    }
  }


  render() {
    const { tab } = this.state
    const {
      currentUser,
      edit,
      requestToInvite,
      removeRequest
    } = this.props

    const tasks = _.has(edit, 'causeProduct.productTemplate.templateTask')
      ? edit.causeProduct.productTemplate.templateTask
      : []

    const isMilestone = tasks.length > 0 ? true : false

    const isContractor = currentUser.permissions === 3
    let isInvitedContractor = false
    let statusId = null
    let causeUserCauseStatus = null
    let index = null

    if (isContractor && edit.causeUserCause && edit.causeUserCause.length > 0) {
      const causeUserCause = edit.causeUserCause
      index = causeUserCause.findIndex((item) => {
        const invitedUser = item.contractorCauseUser || {}
        return currentUser.id === invitedUser.id
      })
      if (index !== -1) {
        causeUserCauseStatus = causeUserCause[index].causeUserStatus || 0
        statusId = causeUserCause[index].id || null
        isInvitedContractor = index !== -1
      }
    }

    return (
      <div className="page-container">
        <div className="page-content">
          <div className="content-wrapper">
            {/*Toolbar start*/}
            <div className="navbar navbar-default navbar-component navbar-xs navbar">
              <ul className="nav navbar-nav visible-xs-block">
                <li className="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i className="icon-menu7"></i></a></li>
              </ul>

              <div className="navbar-collapse collapse" id="navbar-filter">
                <ul className="nav navbar-nav">
                  <li className={tab === 0 ? 'active' : ''} onClick={() => this.setTab(0)}><a data-toggle="tab"><i className="icon-briefcase position-left"></i> Description </a></li>
                  {isMilestone && (
                    <li className={tab === 2 ? 'active' : ''} onClick={() => this.setTab(2)}>
                      <a data-toggle="tab">
                        <i className=" icon-git-branch position-left"></i>
                        Milestones
                        <span className="badge badge-success badge-inline position-right">
                          {edit.causeFormatQty + 1}
                        </span>
                      </a>
                    </li>
                  )}
                </ul>

                <div className="navbar-right">
                  {isContractor && isInvitedContractor && (
                    <ul className="nav navbar-nav">
                      <li className="dropdown-toggle cursor-pointer" data-toggle="dropdown" aria-expanded="false">
                        <span className={`label ${causeUserStatus[causeUserCauseStatus].color} invite-container__span`}>
                          {causeUserStatus[causeUserCauseStatus].name}
                          &nbsp;&nbsp;
                          <i className="icon-menu7"></i>
                        </span>
                      </li>
                      {causeUserCauseStatus === 0 ? (
                        <ul className="dropdown-menu dropdown-menu-right">
                          <li onClick={() => statusId && requestToInvite(statusId, 1, index)}><a><i className="icon-folder-plus"></i> Accept invite </a></li>
                          <li onClick={() => statusId && requestToInvite(statusId, 2, index)}><a><i className="icon-folder-plus"></i> Decline invite </a></li>
                        </ul>
                      ) : (
                        <ul className="dropdown-menu dropdown-menu-right">
                          <li onClick={() => statusId && removeRequest(statusId)}><a><i className="icon-folder-plus"></i> Remove request </a></li>
                        </ul>
                      )}
                    </ul>
                  )}
                </div>
              </div>
            </div>
            {/*Toolbar end*/}

            {this.renderSwitch(statusId, tasks)}
          </div>
        </div>
      </div>


    )
  }
}

export default ListItem
