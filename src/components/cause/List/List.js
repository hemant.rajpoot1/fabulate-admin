import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { DateRangePicker } from 'react-date-range'

import Pagination from 'react-js-pagination'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'

import 'react-date-range/dist/styles.css'
import 'react-date-range/dist/theme/default.css'

import _ from 'lodash'
import PNotify from 'pnotify'

import 'pnotify/dist/pnotify.confirm'

import './List.css'

class List extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isInvoiceDatePickerOpen: false,
      isBillDatePickerOpen: false,

      xeroInvoicesDate: {
        startDate: new Date(),
        endDate: null,
        key: ''
      },
      xeroBillDate: {
        startDate: new Date(),
        endDate: null,
        key: ''
      },
    }
  }

  onDelete(id) {
    const { causeActions } = this.props

    const notice = new PNotify({
      title: 'Remove brief',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      causeActions.remove(id)
    })

  }

  handleGenerateAnalyticsPageviewsData = id => () => {
    const { causeActions } = this.props
    causeActions.generateAnalyticsPageviewsData(id)
  }

  handleGenerateAnalyticsFormSubmitData = id => () => {
    const { causeActions } = this.props
    causeActions.generateAnalyticsFormSubmitData(id)
  }

  setSearchFilter = (event, ranges) => {
    const target = _.get(event, 'target')
    const value = _.get(target, 'value')
    const { causeActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { causeSearchData: value !== '' ? value : undefined }
      query.ranges = ranges
      causeActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { causeActions } = this.props
    this.setState({isInvoiceDatePickerOpen: false})
    this.setState({isBillDatePickerOpen: false})
    causeActions.resetFilters()
  }

  renderSearchFilter = () => (
    <Filter
      searchItem="briefs"
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.causeSearchData || ''}
      resetFilters={this.resetFilters}
      customClasses="float-none m-10p-0"
    />
  )


  handleSelectDate = (ranges) => {
    _.map(ranges, (range) => {
      this.setState({[range.key]: {
        startDate: range.startDate,
        endDate: range.endDate
      }})
    })
    
    this.setSearchFilter(null, ranges)
  }

  renderDatePicker = (type) => {
    const selectionRange = {
      startDate: this.state[type].startDate,
      endDate: this.state[type].endDate,
      key: type,
    }
    return (
      <DateRangePicker
        ranges={[selectionRange]}
        onChange={this.handleSelectDate}
        showSelectionPreview={true}
        moveRangeOnFirstSelection={false}
        editableDateInputs={true}
      />
    )
  }

  renderDropdown = () => {
    return (
      <div className='brief-list-date-pickers'>
        <div className='brief-list-date-pickers-item mr-20'>
          <button className='btn btn-primary' onClick={() => this.setState({isInvoiceDatePickerOpen: !this.state.isInvoiceDatePickerOpen})}>
            Choose Due date for invoice
          </button>
          {this.state.isInvoiceDatePickerOpen && (
            <ul className='brief-date-picker'>
              <a>
                {this.renderDatePicker('xeroInvoicesDate')}
              </a>
            </ul>
          )}
          
        </div>
        <div className='brief-list-date-pickers-item mr-20'>
          <button className='btn btn-primary' onClick={() => this.setState({isBillDatePickerOpen: !this.state.isBillDatePickerOpen})}>
            Choose Due date for bill
          </button>
          {this.state.isBillDatePickerOpen && (
            <ul className='brief-date-picker'>
              <a>
                {this.renderDatePicker('xeroBillDate')}
              </a>
            </ul>
          )}
        </div>
      </div>
      
    )
  }

  rendererTable = () => {
    const { isGetFetching, causes } = this.props
    return isGetFetching
      ? (
        <tr>
          <td colSpan='11'>
            <SpinnerLoadData isAbsolute={false} delay="0" />
          </td>
        </tr>
      )
      : _.map(causes, (cause, i) =>
        (
          <tr key={`${cause.id}_index_${i}`}>
            <td>{cause.id}</td>
            <td><Link to={`/cause-show/${cause.id}`}> {cause.causeName} </Link></td>
            <td>{cause.hiredUserCause ? cause.hiredUserCause.userName : '-'}</td>
            <td>{cause.userCause ? cause.userCause.userName : '-'}</td>
            <td>{cause.xeroInvoicesId ? cause.xeroInvoicesId : '-'}</td>
            <td>{cause.xeroBillId ? cause.xeroBillId : '-'}</td>
            <td>{cause.xeroInvoicesStatus ? cause.xeroInvoicesStatus : '-'}</td>
            <td>{cause.xeroBillStatus ? cause.xeroBillStatus : '-'}</td>
            <td>{cause.xeroInvoicesDate ? cause.xeroInvoicesDate.slice(0, -9) : '-'}</td>
            <td>{cause.xeroBillDate ? cause.xeroBillDate.slice(0, -9) : '-'}</td>
            <td className="text-center">
              <ul className="icons-list">
                <li className="dropdown">
                  <a className="dropdown-toggle" data-toggle="dropdown">
                    <i className="icon-menu9"></i>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li><Link to={`/cause-show/${cause.id}/edit`}><i className="icon-pencil"></i>Edit</Link></li>
                    <li onClick={() => this.onDelete(cause.id)}>
                      <a><i className="icon-cross"></i>Delete</a>
                    </li>
                    <li onClick={this.handleGenerateAnalyticsPageviewsData(cause.id)}>
                      <a>
                        Generate analytics pageviews data (generates 15 records)
                      </a>
                    </li>
                    <li onClick={this.handleGenerateAnalyticsFormSubmitData(cause.id)}>
                      <a>
                        Generate analytics formSubmit data (generates 15 records)
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </td>
          </tr>
        )
      )

  }

  render() {
    const {
      count,
      page,
      fetchList,
    } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">

          <h5 className="panel-title">Briefs List</h5>

          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>

        <div ref={ref => { this.cardsWrapper = ref }}>
          <div style={{display: 'flex', justifyContent: 'space-between'}}>
            {this.renderSearchFilter()}
            {this.renderDropdown()}
          </div>
          
          <table className="table datatable-select-basic" >
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Hired contractor</th>
                <th>Owner</th>
                <th>Xero invoice id</th>
                <th>Xero bill Id</th>
                <th>Xero invoice status</th>
                <th>Xero bill status</th>
                <th>Due date for invoice</th>
                <th>Due date for bill</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>

            <tbody>
              {this.rendererTable()}
            </tbody>
          </table>
        </div>
        {
          count > 20 &&
          <div className="pager p-20">
            <Pagination
              hideNavigation
              pageRangeDisplayed={3}
              activePage={page + 1}
              itemsCountPerPage={20}
              totalItemsCount={count}
              onChange={page => fetchList(page - 1)}
            />
          </div>
        }
      </div>
    )

  }

}

export default withRouter(List)
