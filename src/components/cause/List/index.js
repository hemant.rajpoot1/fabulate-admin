import List from './List'

import React, { Component } from 'react'

import _ from 'lodash'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as causeActions from 'store/actions/cause'
import * as userActions from 'store/actions/user'

class ListContainer extends Component {
  
  componentDidMount() {
    const { filter } = this.props
    this.fetch(filter)
  }

  componentDidUpdate(prevProps) {
    const { filter } = prevProps

    if (!_.isEqual(this.props.filter, filter)) {
      this.fetch(this.props.filter)
    }
  }

  fetch = filter => {
    const { causeActions } = this.props

    causeActions.get(filter)
  }

  fetchList = page => {
    const { causeActions, filter } = this.props

    causeActions.get(filter, page)
  }

  render() {
    return (
      <List 
        { ...this.props }
        fetchList={this.fetchList}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    causes: state.cause.list.origin,
    count: state.cause.list.count,
    translate: getTranslate(state.locale),
    page: state.cause.list.page, 
    isAll: state.cause.list.isAll,
    isGetFetching: state.cause.isGetFetching,
    filter: state.cause.filter,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
