import BriefFormsCreate from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.buttons'

import * as causeActions from 'store/actions/cause'
import * as productActions from 'store/actions/product'
import * as contentCategoryActions from 'store/actions/contentCategory'
import * as userActions from 'store/actions/user'
import * as authActions from 'store/actions/auth'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj, checkValidObj, calculateSum } from 'helpers/tools'

import _ from 'lodash'

class BriefFormsCreateContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentPage: 1,
      changeBrand: false,
      saveBrandChecked: false
    }
  }

  componentDidMount() {
    const { contentCategoryActions, productActions, products, contentCategories } = this.props

    if (_.isEmpty(products)) {
      productActions.get({})
    }

    if (_.isEmpty(contentCategories)) {
      contentCategoryActions.get({})
    }

  }

  setBrandInfo = () => {
    const { currentUser, causeActions } = this.props

    if (currentUser.brandInformation) {
      causeActions.setBrandInformation({ ...currentUser.brandInformation })
    }
    this.setState({ saveBrandChecked: false })
  }

  updateBrandState = () => {
    this.setState({
      changeBrand: !this.state.changeBrand
    })
  }

  changePage = i => {
    const {
      currentPage,
      validation,
    } = this.props

    if (!validation.causeName.value
      || !validation.causeDueDate.value
      || !validation.causePitchDueDate.value      
      || !_.size(validation.products.value)
    ) {

      this.change('causeName', validation.causeName.value)
      this.change('causeDueDate', validation.causeDueDate.value)
      this.change('causePitchDueDate', validation.causePitchDueDate.value)
      this.change('causeType', validation.causeType.value)


      let text = 'Need to fill fields: \n'
      if (!validation.causeName.value) {
        text += '- Brief name \n'
      }
      if (!validation.causeDueDate.value) {
        text += '- Date of completion \n'
      }
      if(!validation.causePitchDueDate) {
        text += '- Pitch date of completion \n'
      }           
      if (!_.size(validation.products.value)) {
        text += '- Choose content type \n'
      }
      if(!validation.causeType.value) {
        text += '- Choose brief type \n'
      }

      this.setState({ currentPage: 1 })

      new PNotify({
        addclass: 'bg-warning',
        text: text,
        buttons: {
          closer: true,
          sticker: true,
        },
      })
    } else {
      if (currentPage !== i) {
        this.setState({ currentPage: i })
      }
    }
  }

  change = (type, value) => {
    const { validation, causeActions, validRules } = this.props
    causeActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }

  onSubmit = causeStatus => {
    const {
      causeActions,
      authActions,
      currentUser,
      validation,
      attachments,
      search,
      history
    } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
      ownerId: currentUser.id,
      templateId: search.selectedCreate.template ? search.selectedCreate.template.id : null,
      causeStatus
    }

    if (formData.causeName && formData.causeDueDate && formData.causePitchDueDate 
      && _.size(formData.products) && checkValidObj(validation)
    ) {
      causeActions.create({
        ...formData,
        causeSum: calculateSum(validation),
        //TODO: stub. in finale delete this
        causeFormat: formData.products[0].id,
        causeFormatQty: formData.products[0].count - 1
      }, attachments, history)
      if (this.state.changeBrand && currentUser) {
        const allBrandsItems = currentUser.allBrands || []
        allBrandsItems.push({
          causeBrand: formData.causeBrand,
          causeBrandURL: formData.causeBrandURL,
          causeIndustry: formData.causeIndustry,
          causeBrandAbout: formData.causeBrandAbout
        })

        authActions.updateUserInfo({
          id: currentUser.id,
          brandInformation: {
            causeBrand: formData.causeBrand,
            causeBrandURL: formData.causeBrandURL,
            causeIndustry: formData.causeIndustry,
            causeBrandAbout: formData.causeBrandAbout
          },
          allBrands: allBrandsItems
        })
      }
    } else {
      this.change('causeName', validation.causeName.value)
      this.change('causeDueDate', validation.causeDueDate.value)
      this.change('causePitchDueDate', validation.causePitchDueDate.value)      

      let text = 'Need to fill fields: \n'
      if (!formData.causeName) {
        text += '- Brief name \n'
      }
      if (!formData.causeDueDate) {
        text += '- Date of completion \n'
      }
      if(!formData.causePitchDueDate) {
        text += '- Pitch date of completion \n'
      }          
      if (!_.size(formData.products)) {
        text += '- Choose content type \n'
      }

      this.changePage(1)
      window.scrollTo(0, 0)

      new PNotify({
        addclass: 'bg-warning',
        text: text,
        buttons: {
          closer: true,
          sticker: true,
        },
      })
    }
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { causeActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        this.setState({
          isSearch: true
        })
        causeActions.search(type, { q: value })
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  render() {
    const { currentPage } = this.state

    return (
      <BriefFormsCreate
        {...this.props}
        change={this.change}
        changePage={this.changePage}
        onSubmit={this.onSubmit}
        currentPage={currentPage}
        isCreate={true}
        onChangeSelectInput={this.onChangeSelectInput}
        updateBrandState={this.updateBrandState}
        changeBrand={this.state.changeBrand}
        setBrandInfo={this.setBrandInfo}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.cause.validationCreate))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const currentWorkspace = state.auth && state.auth.workspace ? state.auth.workspace : {}

  return {
    translate: getTranslate(state.locale),
    validation: state.cause.validationCreate,
    attachments: state.cause.attachmentsCreate,
    search: state.cause.search,
    products: state.product.origin,
    contentCategories: state.contentCategory.origin,
    currentUser,
    currentWorkspace,
    validRules,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    productActions: bindActionCreators(productActions, dispatch),
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BriefFormsCreateContainer))
