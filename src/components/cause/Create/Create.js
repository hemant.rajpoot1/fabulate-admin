import React, { Component, Fragment } from 'react'

import BriefFormOne from 'components/form/Brief/BriefFormOne'
import BriefFormTwo from 'components/form/Brief/BriefFormTwo'
import BriefFormThree from 'components/form/Brief/BriefFormThree'
import BriefFormFour from 'components/form/Brief/BriefFormFour'
import BriefFormFive from 'components/form/Brief/BriefFormFive'

import BriefTabOne from 'components/form/BriefTabs/BriefTabOne'
import BriefTabTwo from 'components/form/BriefTabs/BriefTabTwo'
import BriefTabThree from 'components/form/BriefTabs/BriefTabThree'
import BriefTabFour from 'components/form/BriefTabs/BriefTabFour'
import BriefTabFive from 'components/form/BriefTabs/BriefTabFive'
import TabForm from 'components/form/TabForm'

class BriefFormsCreate extends Component {
  renderForm = () => {
    const {
      currentPage,
    } = this.props

    switch (currentPage) {
    case 2: {
      return (
        <BriefFormTwo
          {...this.props}
        />
      )
    }

    case 3: {
      return (
        <BriefFormThree
          {...this.props}
        />
      )
    }

    case 4: {
      return (
        <BriefFormFour
          {...this.props}
        />
      )
    }

    case 5: {
      return (
        <BriefFormFive
          {...this.props}
        />
      )
    }

    default: {
      return (
        <BriefFormOne
          {...this.props}
        />
      )
    }
    }
  }

  render() {
    const currentForm = this.renderForm()

    return (
      <Fragment>
        {currentForm}
        <div id="modal_preview_brief" className="modal fade">
          <div className="modal-dialog modal-full">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal">×</button>
                <h5 className="modal-title">Brief preview</h5>
              </div>

              <div className="modal-body pb-20">
                <TabForm
                  tabs={[
                    { tabChild: 'About the brand' },
                    { tabChild: 'The content' },
                    { tabChild: 'Target audience' },
                    { tabChild: 'Tone, voice and style' },
                    { tabChild: 'References' }
                  ]}
                  disablePanelClass={true}
                >
                  {[
                    <BriefTabOne {...this.props} key='BriefTabOne' isBlockEdit={true} />,
                    <BriefTabTwo {...this.props} key='BriefTabTwo' isBlockEdit={true} />,
                    <BriefTabThree {...this.props} key='BriefTabThree' isBlockEdit={true} />,
                    <BriefTabFour {...this.props} key='BriefTabFour' isBlockEdit={true} />,
                    <BriefTabFive {...this.props} key='BriefTabFive' isBlockFiles={true} isBlockEdit={true} />
                  ]}
                </TabForm>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-default legitRipple"
                  data-dismiss="modal"
                >
                  close
                  <span className="legitRipple-ripple"></span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default BriefFormsCreate
