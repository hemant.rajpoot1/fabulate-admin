import BriefForms from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.buttons'

import * as causeActions from 'store/actions/cause'
import * as contentCategoryActions from 'store/actions/contentCategory'
import * as productActions from 'store/actions/product'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj, checkValidObj, calculateSum } from 'helpers/tools'
import _ from 'lodash'


class BriefFormsContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentPage: 1
    }
  }

  componentDidMount() {
    const { causeActions, match, productActions, products, contentCategoryActions, contentCategories } = this.props
    let { id } = match.params
    causeActions.getId(id)

    if (_.isEmpty(products)) {
      productActions.get({})
    }
    if (_.isEmpty(contentCategories)) {
      contentCategoryActions.get({})
    }
  }

  setBrandInfo = () => {
    const { causeActions, currentUser } = this.props

    if (currentUser.brandInformation) {
      causeActions.setBrandInformation({ ...currentUser.brandInformation })
    }
  }

  changePage = i => {
    const { currentPage } = this.state

    if (currentPage !== i) {
      this.setState({ currentPage: i })
    }
  }

  change = (type, value) => {
    const { validation, causeActions, validRules } = this.props

    causeActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }

  onSubmit = causeStatus => {
    const {
      causeActions,
      currentUser,
      validation,
      attachments,
      search
    } = this.props

    const validObj = formattingValidObj(validation)
    const checkNames = [
      'causeDwellTime',
      'causePageImpressions',
      'causeTargetNewCustomer',
      'causeIncrease',
      'causeEngagement',
      'causeBrandConsideration',
      'causeLaunchProduct',
      'causeSuccessOther'
    ]
    const formData = {
      ...validObj,
      ownerId: currentUser.id,
      templateId: search.selectedEdit.template ? search.selectedEdit.template.id : null,
      causeStatus,
      causeContentBoosterBundle: validation.causeContentBoosterBundle.value,
      causeEcpertHelp: validation.causeEcpertHelp.value,
      causeGetSooner: validation.causeGetSooner.value,
      causeNonDisclosure: validation.causeNonDisclosure.value,
    }

    _.forEach(checkNames, checkItem => {
      if (!_.isNil(validation[checkItem].value)) {
        formData[checkItem] = validation[checkItem].value
      }
    })

    const isPost = causeStatus === 1 && validation.causeStatus.value !== 1

    if (formData.causeName && formData.causeDueDate && formData.causePitchDueDate 
      && _.size(formData.products) && checkValidObj(validation)
    ) {
      causeActions.update({
        ...formData,
        causeSum: calculateSum(validation),
        //TODO: stub. in finale delete this
        causeFormat: formData.products[0].id,
        causeFormatQty: formData.products[0].count - 1,
      }, attachments, null, isPost)
    } else {
      this.change('causeName', validation.causeName.value)
      this.change('causeDueDate', validation.causeDueDate.value)
      this.change('causePitchDueDate', validation.causePitchDueDate.value)      

      let text = 'Need to fill fields: \n'
      if (!formData.causeName) {
        text += '- Brief name \n'
      }
      if (!formData.causeDueDate) {
        text += '- Date of completion \n'
      }
      if(!formData.causePitchDueDate) {
        text += '- Pitch date of completion \n'
      }         
      if (!_.size(formData.products)) {
        text += '- Choose content type \n'
      }

      this.changePage(1)
      window.scrollTo(0, 0)

      new PNotify({
        addclass: 'bg-warning',
        text: text,
        buttons: {
          closer: true,
          sticker: true,
        },
      })
    }
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { causeActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        this.setState({
          isSearch: true
        })
        causeActions.search(type, { q: value })
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  render() {
    const { currentPage } = this.state

    return (
      <BriefForms
        {...this.props}
        change={this.change}
        changePage={this.changePage}
        onSubmit={this.onSubmit}
        currentPage={currentPage}
        isCreate={false}
        onChangeSelectInput={this.onChangeSelectInput}
        setBrandInfo={this.setBrandInfo}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.cause.validationEdit))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const currentWorkspace = state.auth && state.auth.workspace ? state.auth.workspace : {}

  return {
    translate: getTranslate(state.locale),
    validation: state.cause.validationEdit,
    attachments: state.cause.attachmentsEdit,
    search: state.cause.search,
    products: state.product.origin,
    contentCategories: state.contentCategory.origin,
    currentUser,
    currentWorkspace,
    validRules
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BriefFormsContainer))

