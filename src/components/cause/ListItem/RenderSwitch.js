import React, { Component } from 'react'
import _ from 'lodash'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import PitchesList from 'components/pitch/PitchesList'
import BriefTabOne from 'components/form/BriefTabs/BriefTabOne'
import BriefTabTwo from 'components/form/BriefTabs/BriefTabTwo'
import BriefTabThree from 'components/form/BriefTabs/BriefTabThree'
import BriefTabFour from 'components/form/BriefTabs/BriefTabFour'
import BriefTabFive from 'components/form/BriefTabs/BriefTabFive'
import BriefTabsWrapper from './BriefTabsWrapper'

import WorkroomForm from 'components/form/WorkroomForm'

import './ListItem.css'

class RenderSwitch extends Component {

  render() {
    const {
      tab,
      id,
      isGetIdFetching,
      edit,
      validation,
      isShowChat
    } = this.props
    const tasks = _.get(edit, 'causeProduct.productTemplate.templateTask', [])
    if (isGetIdFetching) {
      return (<SpinnerLoadData />)
    }
    switch (tab) {
    case 4: {
      return (
        <PitchesList
          causeId={id}
        />
      )
    }
    case 5: {
      return (
        <WorkroomForm
          causeId={id}
          tasks={tasks}
          causeInfo={{
            id,
            causeName: validation.causeName.value,
            causeFormatQty: validation.causeFormatQty.value
          }}
          isShowChat={isShowChat}
        />
      )
    }
    default: {
      return (
        <BriefTabsWrapper>
          <BriefTabOne {...this.props} key='BriefTabOne' isBlockEdit={true} />
          <BriefTabTwo {...this.props} key='BriefTabTwo' isBlockEdit={true} />
          <BriefTabThree {...this.props} key='BriefTabThree' isBlockEdit={true} />
          <BriefTabFour {...this.props} key='BriefTabFour' isBlockEdit={true} />
          <BriefTabFive {...this.props} key='BriefTabFive' isBlockEdit={true} />
        </BriefTabsWrapper>
      )
    }
    }
  }
}

export default RenderSwitch
