import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import { getBriefSum } from 'helpers/tools'
import RenderSwitch from './RenderSwitch'

import { Link } from 'react-router-dom'
import 'pnotify/dist/pnotify.buttons'

import { causeUserStatus, causeStatus } from 'helpers/params'
import './ListItem.css'

class ListItem extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tab: this.setStartPage(),
      briefTab: 1,
      isShowChat: false,
    }
  }

  setStartPage = () => {
    switch (this.props.location.search) {
    case '?tab=creators':
      return 1
    case '?tab=my-pitch':
      return 3
    case '?tab=pitches':
      return 4
    case '?tab=workroom':
      return 5
    case '?tab=feedback':
      return 6
    default:
      return 0
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    this.showConfirm = true
  }

  generateProduct = (product) => {
    return _.map(product, (productItem, index) => {
      if (!_.size(productItem.filteredData)) {
        return null
      }
      return (
        <React.Fragment key={`product_${productItem.contentCategoryId}`}>
          <span className="active border-double mr-10">
            <span className="m-0 text-semibold">
              {productItem.contentCategoryName}&nbsp;
            </span>
            {
              _.map(productItem.filteredData, (filterItem, index) => (
                <React.Fragment key={filterItem.id}>
                  <span>
                    {`${filterItem.count}x ${filterItem.productDisplayName}`}
                  </span>
                  {productItem.filteredData.length - 1 !== index && <span>,&nbsp;</span>}
                </React.Fragment>
              ))
            }
          </span>
        </React.Fragment>
      )
    })
  }

  updateStatus = (id, causeStatus) => {
    const { causeActions } = this.props

    causeActions.update({
      id,
      causeStatus
    })
  }

  setTab = index => {
    const { tab } = this.state

    if (index !== tab) {
      this.setState({ tab: index })
    }
  }

  toggleChat = () => {
    const { isShowChat } = this.state
    this.setState({ isShowChat: !isShowChat })
  }

  setBriefTab = index => {
    const { briefTab } = this.state

    if (index !== briefTab) {
      this.setState({ briefTab: index })
    }
  }

  getProductData = () => {
    const { contentCategories, validation } = this.props
    return _.map(contentCategories, contentCategoryItem => {
      const filteredData = _.filter(validation.products.value, productItem => productItem && productItem.contentCategoryId === contentCategoryItem.id)
      return {
        contentCategoryName: contentCategoryItem.contentCategoryName,
        contentCategoryId: contentCategoryItem.id,
        filteredData
      }
    })
  }

  render() {
    const { tab } = this.state
    const {
      currentUser,
      edit,
      requestToInvite,
      removeRequest,
      id,
      validation,
      isGetIdFetching,
    } = this.props
    const product = this.getProductData()

    let statusId = null
    let causeUserCauseStatus = null
    let index = null
    if (_.size(edit.causeUserCause)) {
      const causeUserCause = edit.causeUserCause
      index = causeUserCause.findIndex((item) => {
        const invitedUser = _.get(item, 'contractorCauseUser', {})
        return currentUser.id === invitedUser.id
      })
      if (index !== -1) {
        causeUserCauseStatus = _.get(causeUserCause, `[${index}].causeUserStatus`, 0)
        statusId = _.get(causeUserCause, `[${index}].id`, null)
      }
    }
    const itemStatus = edit.causeStatus <= 4 ? edit.causeStatus : 0
    const causeProducts = _.get(validation, 'products.value')
    const briefSumBuyer = getBriefSum(causeProducts, { permissions: 2 }, edit.causeType) || 0
    const briefSumContractor = getBriefSum(causeProducts, { permissions: 3 }, edit.causeType) || 0
    if (isGetIdFetching) {
      return (<SpinnerLoadData />)
    }
    return (
      <div className="page-container">
        <div className="page-content">
          <div className="content-wrapper">
            {/*Toolbar start*/}
            {!!validation.id.value &&
              (
                <div className="navbar navbar-default navbar-component navbar-xs navbar">
                  <ul className="nav navbar-nav visible-xs-block">
                    <li className="full-width text-center">
                      <a
                        data-toggle="collapse"
                        data-target="#navbar-filter">
                        <i className="icon-menu7"></i>
                      </a>
                    </li>
                  </ul>
                  <div id="navbar-filter" className="navbar-collapse collapse">
                    <ul className="nav navbar-nav navbar-nav-material">
                      <li
                        className={tab === 0 ? 'active': ''}
                        onClick={() => this.setTab(0)}
                      >
                        <Link to='?tab=brief' replace>
                          <i className="icon-briefcase position-left"></i>
                            Brief
                        </Link>
                      </li>

                      {_.get(edit, 'causeStatus') !== 0 && (
                        <li
                          className={tab === 4 ? 'active': ''}
                          onClick={() => this.setTab(4)}
                        >
                          <Link to='?tab=pitches' replace>
                            <i className="icon-reading position-left"></i>
                            Pitches
                          </Link>
                        </li>
                      )}

                      {
                        !_.isNil(edit.hiredUserId)
                        && currentUser
                          && (
                            <li
                              className={tab === 5 ? 'active': ''}
                              onClick={() => this.setTab(5)}
                            >
                              <Link to={'?tab=workroom'} replace>
                                <i className="icon-stack position-left"></i>
                                  Workroom
                              </Link>
                            </li>
                          )}
                    </ul>
                    <div className="navbar-right">
                      <ul className="nav navbar-nav navbar-nav-material navbar-right">
                        {tab === 5 && (
                          <li>
                            <a onClick={() => this.toggleChat()}>
                              <span className="mr-10">Open chat</span>
                              <i className=" icon-bubbles7 position-left"></i>
                            </a>
                          </li>
                        )}
                        {( itemStatus < 2 &&
                          <li>
                            {!!causeUserCauseStatus && causeStatus[itemStatus].value !== 3 && (
                              <div
                                className="dropdown-toggle cursor-pointer"
                                data-toggle="dropdown"
                                aria-expanded="false"
                                style={{padding: '13px'}}
                              >
                                <span className={`label ${_.get(causeUserStatus, `[${causeUserCauseStatus}].color`)} badge-col-blue border-radius-10 border-none`}>
                                  {_.get(causeUserStatus, `[${causeUserCauseStatus}].name`)}
                                &nbsp;&nbsp;
                                  <i className="icon-arrow-down32"></i>
                                </span>
                              </div>
                            )}
                            {!causeUserCauseStatus ? (
                              <ul className="dropdown-menu dropdown-menu-right">
                                <li onClick={() => statusId && requestToInvite(statusId, 1, index)}>
                                  <a>
                                    <i className="icon-folder-plus"></i>
                                    Accept invite
                                  </a>
                                </li>
                                <li onClick={() => statusId && requestToInvite(statusId, 2, index)}>
                                  <a>
                                    <i className="icon-folder-plus"></i>
                                    Decline invite
                                  </a>
                                </li>
                              </ul>
                            ) : (
                              <ul className="dropdown-menu dropdown-menu-right">
                                {!edit.hiredUserId && (
                                  <li onClick={() => statusId && removeRequest(statusId, causeUserCauseStatus)}>
                                    <a className="p-0">
                                      <i className="icon-cross3"></i>
                                      Remove request
                                    </a>
                                  </li>
                                )}
                              </ul>
                            )}
                          </li>
                        )}
                        <li>
                          <div className="btn-group" style={{padding: '13px 13px'}} >
                            <div data-toggle="dropdown" aria-expanded="false">
                              <div className={`label ${causeStatus[itemStatus].color} border-radius-10 text-lowercase badge-col-green border-none label-padding`}>
                                <span className="token-label token-label-padding" style={{ maxWidth: 840 }}>
                                  {!_.isNil(edit.causeStatus) ? causeStatus[itemStatus].name : '-'}
                                </span>
                              </div>
                            </div>

                          </div>
                        </li>
                        <li>
                          <Link to={`/cause-show/${id}/edit`} className="">
                            <i className="icon-quill4 position-left"></i>
                              Edit
                          </Link>
                        </li>
                        <li
                          onClick={() => this.updateStatus(id, 4)}
                        >
                          <a>
                            <i className="icon-stack-cancel position-left"></i>
                              Cancel
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              )
            }
            {/*Toolbar end*/}
            <div className="panel-c panel-body panel-collapsed brief-page-padding">
              <div className="display-block">
                <p className="pull-right mt-10">
                  {_.has(this.props, 'validation.causeDueDate.value') && moment(this.props.validation.causeDueDate.value).isValid()
                    ? 'Material Deadline: ' + moment(this.props.validation.causeDueDate.value).format('MMM DD YYYY')
                    : ''
                  }
                  <br />
                  {_.has(this.props, 'validation.causePitchDueDate.value') && moment(this.props.validation.causePitchDueDate.value).isValid()
                    ? 'Pitch Deadline: ' + moment(this.props.validation.causePitchDueDate.value).format('MMM DD YYYY')
                    : ''
                  }                  
                </p>
                <h2 className="mt-0">{_.get(validation, 'causeName.value', '')}</h2>
              </div>

              <div>
                <div className="row mb-10">
                  <div className="col-md-3 mt-10 text-semibold">
                    Brand:
                  </div>
                  <div className="col-md-3 mt-10">
                    {_.get(validation, 'causeBrand.value') || 'No brand'}
                  </div>
                </div>

                <div className="row mb-10">
                  <div className="col-md-3 mt-10 text-semibold">
                    Budget:
                  </div>
                  <div className="col-md-4 mt-10">
                    {'Buyer sum: $' + briefSumBuyer.toFixed(2)}
                    &nbsp;
                    {'Contractor sum: $' + briefSumContractor.toFixed(2)}
                  </div>
                </div>

                <div className="row mb-10">
                  <div className="col-md-3 mt-10 text-semibold">
                    Product cards:
                  </div>
                  <div className="col-md-9 mt-10">
                    {this.generateProduct(product)}
                  </div>
                </div>

                <div className="row mb-10">
                  <div className="col-md-3 mt-10 text-semibold">
                    Xero invoice Id (creator):
                  </div>
                  <div className="col-md-4 mt-10">
                    {_.get(validation, 'xeroInvoicesId.value')}
                  </div>
                </div>

                <div className="row mb-10">
                  <div className="col-md-3 mt-10 text-semibold">
                    Xero bill Id (buyer):
                  </div>
                  <div className="col-md-4 mt-10">
                    {_.get(validation, 'xeroBillId.value')}
                  </div>
                </div>

              </div>
            </div>
            <hr/>

            <RenderSwitch
              causeUserId={statusId}
              tab={tab}
              isShowChat = {this.state.isShowChat}
              {...this.props}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default ListItem
