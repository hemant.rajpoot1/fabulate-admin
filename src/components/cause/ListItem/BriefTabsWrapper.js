import React, { Component } from 'react'
import _ from 'lodash'

import './ListItem.css'

class BriefTabsWrapper extends Component {
  render() {
    const { children } = this.props
    return (
      <div className="panel-c panel-body panel-collapsed brief-page-padding">
        {_.map(children, (child, index) => (
          <div className="border-bottom" key={`brief-tab-${index}`}>
            { child }
          </div>
        ))}
      </div>
    )
  }
}

export default BriefTabsWrapper
