import ListItem from './ListItem'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.buttons'

import * as causeActions from 'store/actions/cause'
import * as causeUserActions from 'store/actions/causeUser'
import * as productActions from 'store/actions/product'
import * as contentCategoryActions from 'store/actions/contentCategory'
import * as genreActions from 'store/actions/genre'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj, checkValidObj } from 'helpers/tools'
import _ from 'lodash'
import moment from 'moment'

class ListItemContainer extends Component {

  componentDidMount() {
    const { 
      id, 
      causeActions, 
      productActions, 
      products,
      contentCategoryActions, 
      contentCategories,
      genreActions,
      genreList
    } = this.props

    causeActions.getId(id)

    if(_.isEmpty(products)) {
      productActions.get({})
    }    
    if(_.isEmpty(contentCategories)) {
      contentCategoryActions.get({})
    }
    if(_.isEmpty(genreList)) {
      genreActions.get({})
    }
    window.zE && window.zE.show && window.zE.show()
  }

  componentDidUpdate(prevProps) {
    if(!_.isEqual(this.props.selectedMilestone, prevProps.selectedMilestone)) {
      this.getTask(this.props)
    }

    if(this.props.id !== prevProps.id) {
      const { id, causeActions } = this.props
      causeActions.getId(id)
    }
  }

  download = (id, ref) => {
    const { causeActions } = this.props
    causeActions.downloadFile(id, ref)
  }

  request = (data) => {
    const { id, edit, currentUser, causeUserActions, validation } = this.props
    const causePitchDueDate = _.get(validation, 'causePitchDueDate.value', null)
    if (causePitchDueDate && new Date(causePitchDueDate) < new Date()) {
      return new PNotify({
        type: 'error',
        text: `Can not submit pitch after deadline ${moment(causePitchDueDate).format('MMM DD YYYY')}`
      })
    }
    if (edit.buyerId && id) {
      causeUserActions.create({
        ...data,
        contractorId: currentUser.id,
        buyerId: edit.buyerId,
        causeId: id,
        causeUserStatus: 3,
      })
    }
  }

  updateRequest = (data) => {
    const { id, currentUser, causeUserActions } = this.props
    if (id) {
      causeUserActions.update({
        id,
        ...data
      }, currentUser)
    }
  }  

  requestToInvite = (id, status, index) => {
    if(id) {
      const { causeUserActions, currentUser } = this.props
      causeUserActions.update({
        id,
        causeUserStatus: status
      }, currentUser)
    }
  } 

  removeRequest = (id, status) => {
    if (status === 4) {
      new PNotify({
        addclass: 'alert alert-warning alert-styled-right',
        // title: 'Information',
        text: 'Please Contact Fabulate Admin for help',
        buttons: {
          closer: true,
          sticker: false,
        },
      })
    } else {
      if (id) {
        const { causeUserActions, causeActions } = this.props

        const notice = new PNotify({
          title: 'Remove pitch',
          text: 'Are you sure?',
          hide: false,
          confirm: {
            confirm: true,
            buttons: [
              {
                text: 'Yes',
                addClass: 'btn btn-sm btn-primary'
              },
              {
                text: 'No',
                addClass: 'btn btn-sm btn-link'
              }
            ]
          },
        })

        notice.get().on('pnotify.confirm', () => {
          causeUserActions.remove(id)
          causeActions.requestData({})
        })
      }
    }
  }

  change = (type, value) => {
    const { validation, causeActions, validRules } = this.props
    causeActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }  

  onSubmit = causeStatus => {
    const {
      causeActions,
      currentUser,
      validation,
      attachments
    } = this.props

    const validObj = formattingValidObj(validation, currentUser.permissions)
    const checkNames = [
      'causeDwellTime',
      'causePageImpressions',      
      'causeTargetNewCustomer',
      'causeIncrease',
      'causeEngagement', 
      'causeBrandConsideration',
      'causeLaunchProduct', 
      'causeSuccessOther'
    ]
    const formData = {
      ...validObj,
      ownerId: currentUser.id,
      // templateId: search.selectedEdit.template ? search.selectedEdit.template.id : null,      
      causeStatus,
      causeContentBoosterBundle: validation.causeContentBoosterBundle.value,
      causeEcpertHelp: validation.causeEcpertHelp.value,
      causeGetSooner: validation.causeGetSooner.value,
      causeNonDisclosure: validation.causeNonDisclosure.value,
    }

    _.forEach(checkNames, checkItem => {
      if(!_.isNil(validation[checkItem].value)) {
        formData[checkItem] = validation[checkItem].value
      }
    })

    const isPost = causeStatus === 1 && validation.causeStatus.value !== 1

    if (formData.causeName && formData.causeDueDate && formData.causePitchDueDate 
      && _.size(formData.products) && checkValidObj(validation)
    ) {
      causeActions.update({
        ...formData,
        //TODO: stub. in finale delete this
        causeFormat: formData.products[0].id,
        causeFormatQty: 1     
      }, attachments, null, isPost)
    } else {
      this.change('causeName', validation.causeName.value)
      this.change('causeDueDate', validation.causeDueDate.value)
      this.change('causePitchDueDate', validation.causePitchDueDate.value)      

      let text = 'Need to fill fields: \n'
      if(!formData.causeName) {
        text += '- Brief name \n'
      }
      if(!formData.causeDueDate) {
        text += '- Date of completion \n'
      }
      if(!formData.causePitchDueDate) {
        text += '- Pitch date of completion \n'
      }      
      if(!_.size(formData.products)) {
        text += '- Choose content type \n'
      }
      
      new PNotify({
        addclass: 'bg-warning',
        text: text,
        buttons: {
          closer: true,
          sticker: true,
        },   
      })      
    }
  }

  onSelectStatus = (taskId, causeTaskId ) => {
    const {
      causeActions,
      validation,
      id,
      selectedMilestone
    } = this.props

    const validObj = formattingValidObj({
      currentMilestoneTask: validation.currentMilestoneTask,
      currentMilestoneStatus: validation.currentMilestoneStatus
    })


    const formData = {
      ...validObj,
      id
    }
    causeActions.update(formData, undefined, { causeTaskId, taskId })

    if(selectedMilestone && taskId === selectedMilestone.id) {
      this.updateSelectedMilestone({
        ...selectedMilestone,
        statusTask: validation.currentMilestoneStatus.value
      })
    }
  }  

  selectMilestone = data => {
    const { causeActions } = this.props
    causeActions.selectMilestone(data)
  }

  updateSelectedMilestone = data => {
    const { causeActions } = this.props

    causeActions.updateSelectedMilestone(data)
  }  


  getTask = (props) => {
    const { causeActions, selectedMilestone, id } = props
    if(selectedMilestone && selectedMilestone.id) {
      causeActions.getTask(selectedMilestone.id, {
        causeId: id,
        milestoneQty: selectedMilestone.milestoneTask
      })
    }
  }

  render() {
    return (
      <ListItem 
        key={`list_item_${this.props.id}`}
        {...this.props} 
        download={this.download} 
        request={this.request} 
        requestToInvite={this.requestToInvite}
        removeRequest={this.removeRequest}
        change={this.change}
        updateRequest={this.updateRequest}
        onSelectStatus={this.onSelectStatus}
        selectMilestone={this.selectMilestone}
        updateSelectedMilestone={this.updateSelectedMilestone}
        onSubmit={this.onSubmit}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.cause.validationEdit))  
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const currentWorkspace = state.auth && state.auth.workspace ? state.auth.workspace : {}
  
  const { id } = props.match.params

  return {
    translate: getTranslate(state.locale),
    validation: state.cause.validationEdit,
    attachments: state.cause.attachmentsEdit,
    search: state.cause.search,
    milestoneCause: state.cause.edit.milestoneCause,
    edit: state.cause.edit,
    taskInfo: state.cause.taskInfo,    
    products: state.product.origin,     
    genreList: state.genre,
    selectedMilestone: state.cause.selectedMilestone,
    isGetIdFetching: state.cause.isGetIdFetching,
    currentUser,
    currentWorkspace,
    id,
    validRules,
    contentCategories: state.contentCategory.origin,  
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    causeUserActions: bindActionCreators(causeUserActions, dispatch),    
    productActions: bindActionCreators(productActions, dispatch),
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),    
    genreActions: bindActionCreators(genreActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListItemContainer))

