import PNotify from 'pnotify'

export const accessNotification = () => 
  new PNotify({
    addclass: 'bg-warning alert alert-styled-left',
    title: 'Work is in progress.',
    text: 'Please, contact support.',
    hide: true,
    buttons: {
      closer: true,
      sticker: true,
    },
  })


export const notice = () =>  
  new PNotify({
    title: 'Become an editor?',
    text: 'Are you sure you want to be an editor for this brief?',
    hide: false,
    confirm: {
      confirm: true,
      buttons: [
        {
          text: 'Yes',
          addClass: 'btn btn-sm btn-primary'
        },
        {
          text: 'No',
          addClass: 'btn btn-sm btn-link'
        }
      ]
    },
  })