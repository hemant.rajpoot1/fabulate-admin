import React, { Component, Fragment } from 'react'
import _ from 'lodash'
import { pnotifyValue } from 'helpers/params'
import classNames from 'classnames'

class PitchMenu extends Component {
  getHiddenPitchMenu = (pitchItem) => {
    const { updatePitch } = this.props

    return (
      <Fragment>
        <li>
          <a
            onClick={() => updatePitch({
              id: pitchItem.id,
              isHide: false
            })}
          >
            <i className="icon-eye"></i> Unhide
          </a>
        </li>
        <li>
          <a
            onClick={() => updatePitch({
              id: pitchItem.id,
              causeUserStatus: 5,
            },
            pnotifyValue.isDecline)}
          >
            <i className="icon-thumbs-down2"></i> Decline
          </a>
        </li>
      </Fragment>
    )
  }

  getVisiblePitchMenu = (pitchItem, ) => {
    const { updatePitch, causeHire, causeId, hidePitch } = this.props
    const pitchStar = classNames({
      'icon-star-full2': !pitchItem.isStar,
      'icon-star-empty3': pitchItem.isStar
    })
    return (
      <Fragment>
        {_.isNil(pitchItem.causeInfo.hiredUserId) && (
          <li>
            <a
              onClick={() => {
                updatePitch({
                  id: pitchItem.id,
                  causeUserStatus: 4,
                },
                pnotifyValue.isAward(pitchItem.contractorCauseUser.userName))
                causeHire(
                  causeId,
                  pitchItem.contractorId
                )
              }}
            >
              <i className="icon-checkmark3"></i> Assign brief
            </a>
          </li>
        )}
        <li>
          <a
            onClick={() => updatePitch({
              id: pitchItem.id,
              causeUserStatus: 5,
            },
            pnotifyValue.isDecline)}
          >
            <i className="icon-cross2"></i> Decline
          </a>
        </li>
        <li>
          <a
            onClick={() => updatePitch({
              id: pitchItem.id,
              isStar: !pitchItem.isStar,
            },
            !pitchItem.isStar ? pnotifyValue.isStar : null
            )}
          >
            <i className={pitchStar}></i> {pitchItem.isStar ? 'Unstar' : 'Star'}
          </a>
        </li>
        <li>
          <a
            onClick={() => hidePitch({
              id: pitchItem.id,
              isHide: true
            })}
          >
            <i className="icon-eye-blocked"></i> Hide
          </a>
        </li>
      </Fragment>
    )
  }

  render() {
    const { pitchItem, isContractorHired } = this.props

    return (
      <div className="media-right text-nowrap">
        <ul className="list-inline list-inline-condensed heading-text pull-right">
          {!isContractorHired && (
            <li className="dropdown">
              <a className="text-default dropdown-toggle" data-toggle="dropdown">
                <i className="icon-more2"></i>
              </a>
              <ul className="dropdown-menu dropdown-menu-right">
                {pitchItem.isHide ? (
                  this.getHiddenPitchMenu(pitchItem)
                ) : (
                  this.getVisiblePitchMenu(pitchItem)
                )}
              </ul>
            </li>
          )}
        </ul>
      </div >
    )
  }
}

export default PitchMenu
