import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

import _ from 'lodash'
import classNames from 'classnames'

import avatar from 'assets/images/image.png'
import { inviteHC } from 'helpers/params'
import PitchMenu from './PitchMenu'
import './styles.css'

class Pitch extends Component {
  render() {
    const {
      pitchItem,
      updatePitch,
      causeHire,
      causeId,
      hidePitch,
      isContractorHired
    } = this.props

    const contractorId = _.get(pitchItem, 'contractorCauseUser.id', '')
    const avatarImage = _.get(pitchItem, 'contractorCauseUser.image') || avatar
    const taglineClass = classNames({
      'text-semibold': true,
      'text-muted-important': pitchItem.isHide,
      'text-transform-none-important': true,
      'link-color': !pitchItem.isHide,
      'overflow-auto': true
    })
    const contractorClass = classNames({
      'text-muted-important': pitchItem.isHide
    })
    const avatarClass = classNames({
      'img-circle': true,
      'img-lg': true,
      'muted-img': pitchItem.isHide
    })
    const outlineClass = classNames({
      'media-body': true,
      'text-muted-important': pitchItem.isHide
    })

    return (
      <Fragment>
        {pitchItem.tagline !== inviteHC && (
          <li
            key={`pitch_item_${pitchItem.id}`}
            className="media border-bottom stack-media-on-mobile pb-20"
          >
            <div className="mb-10 display-flex pl-20">
              <div className="media-left p-0 center-align min-width-25">
                <div className="col-md-1 col-xs-1  p-0">
                  <Link
                    to={`/creator/${contractorId}`}
                  >
                    <img
                      src={avatarImage}
                      className={avatarClass}
                      alt="avatar"
                    />
                  </Link>
                </div>
                <span className="media-heading p-absolute pitch-contractor-name">
                  <Link id="hidden-pitch" className={contractorClass} to={`/creator/${contractorId}`}>
                    {_.get(pitchItem, 'contractorCauseUser.userName', '-')}
                  </Link>
                </span>
              </div>
              <div className="media-body pb-10">
                {pitchItem.tagline && (
                  <React.Fragment>
                    {pitchItem.isStar &&
                        <i className="icon-star-full2 text-warning-300 pr-10"></i>
                    }
                    <Link id="hidden-pitch" to={`/pitch/${pitchItem.id}`} className={taglineClass}>
                      {pitchItem.tagline}
                    </Link>
                  </React.Fragment>
                )}
                <div className={outlineClass}>
                  <ul className="list-inline list-inline-separate text-muted mb-10">
                  </ul>
                  {pitchItem.outline}
                </div>
              </div>
              <PitchMenu
                pitchItem={pitchItem}
                updatePitch={updatePitch}
                causeHire={causeHire}
                causeId={causeId}
                hidePitch={hidePitch}
                isContractorHired={isContractorHired}
              />
            </div>
          </li>
        )}
      </Fragment>
    )
  }
}

export default Pitch
