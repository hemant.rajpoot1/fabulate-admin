import React, { Component } from 'react'
import _ from 'lodash'

import Pitch from './Pitch'

class PitchesList extends Component {
  render() {
    const {
      pitches,
      updatePitch,
      causeHire,
      causeId,
      isEmpty,
      hidePitch,
      isContractorHired
    } = this.props
    const applyFilter = pitches
    return (
      <ul className="panel media-list content-group brief-page-padding">
        <div className="pt-20">
          <span className="text-semibold h5 pl-20">Pitches</span>
        </div>
        {_.size(applyFilter) ? (
          _.map(applyFilter, pitchItem => (
            <Pitch
              key={pitchItem.id}
              pitchItem={pitchItem}
              updatePitch={updatePitch}
              causeHire={causeHire}
              causeId={causeId}
              hidePitch={hidePitch}
              isContractorHired={isContractorHired}
            />
          ))
        ) : (
          <div className="media panel-body stack-media-on-mobile text-center">
            {isEmpty ? 'No pitches yet' : 'No pitches for choosen filter. Please, choose another one'}
          </div>
        )}
      </ul>
    )
  }
}

export default PitchesList
