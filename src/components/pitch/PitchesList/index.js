import PitchesList from './PitchesList'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import _ from 'lodash'

import * as causeUserActions from 'store/actions/causeUser'
import * as causeActions from 'store/actions/cause'

import { getField } from 'store/selectors/modelSelectors'

class PitchesListContainer extends Component {
  componentDidMount() {
    this.fetch(this.props)
    this.showConfirm = true
  }

  componentDidUpdate(prevProps) {
    if(!_.isEqual(this.props.filter, prevProps.filter)) {
      this.fetch(this.props)
    }
  }

  fetch = (props) => {
    const { 
      causeId, 
      causeUserActions,
      filter
    } = props
    causeUserActions.get({
      causeId,
      pitchCount: true,
      ...filter
    })
  }

  setFilter = (fieldName, data) => {
    const { causeUserActions } = this.props
    causeUserActions.setFilter({[fieldName]: data})
  }  

  updatePitch = (data, pnotifyText) => {
    const { causeUserActions } = this.props

    if (this.showConfirm && data.causeUserStatus === 5) {
      this.showConfirm = false

      const notice = new PNotify({
        title: 'Decline pith?',
        text: 'Are you sure you want to decline this pitch?',
        hide: false,
        confirm: {
          confirm: true,
          buttons: [
            {
              text: 'Yes',
              addClass: 'btn btn-sm btn-primary'
            },
            {
              text: 'No',
              addClass: 'btn btn-sm btn-link'
            }
          ]
        },
      })

      notice.get().on('pnotify.confirm', () => {
        this.showConfirm = true
        causeUserActions.update(data, pnotifyText)
      })
      notice.get().on('pnotify.cancel', () => {
        this.showConfirm = true
      })
    } else {
      causeUserActions.update(data, pnotifyText)
    }  
  }

  hidePitch = (data) => {
    const { causeUserActions } = this.props
    try {
      causeUserActions.update(data)
      return new PNotify({
        title: 'Success!',
        text: 'You successfully hide a pitch.',
        type: 'success'
      })
    } catch (e) {
      return new PNotify({
        title: 'Ooops!',
        text: e.message,
        type: 'error'
      })
    }
  }

  causeHire = (causeId, contractorId) => {
    const { 
      causeActions, 
    } = this.props

    if(causeId && contractorId) {
      causeActions.update({
        id: causeId,
        hiredUserId: contractorId,
        causeStatus: 2
      })
    }
  } 

  render() {
    return (
      <PitchesList 
        {...this.props} 
        setFilter={this.setFilter}
        updatePitch={this.updatePitch}
        causeHire={this.causeHire}
        hidePitch={this.hidePitch}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.cause.validationEdit))  
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const { id } = props.match.params

  return {
    translate: getTranslate(state.locale),
    pitches: state.causeUser.causePitches[id],
    isEmpty: state.causeUser.isEmpty,
    filter: state.causeUser.filter,
    currentUser,
    causeId: id,
    validRules,
    isGetFetching: state.causeUser.isGetFetching,
    isContractorHired: _.get(state, 'cause.edit.hiredUserId', false)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    causeUserActions: bindActionCreators(causeUserActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PitchesListContainer))

