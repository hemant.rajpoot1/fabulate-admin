import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import moment from 'moment'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import avatar from 'assets/images/image.png'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Select from 'components/ui/Select'
import Checkbox from 'components/ui/Checkbox'

import './List.css'

class List extends Component {

  componentDidMount(){
    window.addEventListener('scroll', this.onHandleScroll)
  }

  onHandleScroll = e => {
    const { isGetFetching, fetchList } = this.props
    if(this.cardsWrapper) {
      const heigthToBottom = parseInt(this.cardsWrapper.getBoundingClientRect().bottom - window.innerHeight, 10)
      const isLoad = !isGetFetching && heigthToBottom < 150

      if(isLoad) {
        fetchList()
      }
    }
  }

  render() {
    const {
      payments,
      isGetFetching, 
    } = this.props

    return (
      <div>
        <div className="panel panel-flat">
          <div>

            <table className="table datatable-select-basic">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Commission</th>
                  <th>Summary</th>
                  <th>Contractor</th>
                  <th>Buyer</th>
                  <th className="text-center">Actions</th>
                </tr>
              </thead>

              <tbody>
                {_.size(payments) &&
                  _.map(payments, paymentItem => (
                    <tr key={ paymentItem.id }>
                      <td>{ paymentItem.id }</td>
                      <td>{ paymentItem.commission || '-' }</td>
                      <td>{ paymentItem.summary || '-' }</td>
                      <td>{ _.has(paymentItem, 'contractorPayment.userName') ? paymentItem.contractorPayment.userName : '-' }</td>
                      <td>{ _.has(paymentItem, 'buyerPayment.userName') ? paymentItem.buyerPayment.userName : '-' }</td>
                      <td className="text-center">
                        <ul className="icons-list">
                          <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown">
                              <i className="icon-menu9"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-menu-right">
                              <li>
                                <Link to={`payment/${paymentItem.id}`}><i className="icon-pencil"></i>Edit</Link>
                              </li>
                              <li>
                                <a><i className="icon-cross"></i>Delete</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>

          </div>
        </div>
      </div>
    )
  }
}

export default List