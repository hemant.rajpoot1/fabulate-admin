import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as paymentActions from 'store/actions/payment'

import List from './List'

class ListContainer extends Component {
  componentDidMount() {
    const { paymentActions } = this.props
    paymentActions.get({})
  }

  fetchList = (filter) => {
    const { page, isAll, paymentActions } = this.props
    if(!isAll) {
      paymentActions.get({...filter}, page)
    }
  }

  render() {
    return (
      <List 
        {...this.props} 
        fetchList={this.fetchList}
      />
    )
  }

}

let mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : null
 
  return {
    translate: getTranslate(state.locale),
    payments: state.payment.list.origin,
    isGetFetching: state.payment.isGetFetching,
    page: state.payment.list.page, 
    isAll: state.payment.list.isAll,
    currentUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    paymentActions: bindActionCreators(paymentActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))

