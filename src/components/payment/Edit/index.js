import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as paymentActions from 'store/actions/payment'
import { getField } from 'store/selectors/modelSelectors'

import { checkValidObj } from 'helpers/tools'

class EditContainer extends Component {

  componentDidMount() {

    let { paymentActions } = this.props

    let { id } = this.props.match.params

    paymentActions.getId(id)

  }

  change = (type, value) => {
    const { validation, paymentActions, validRules } = this.props
    paymentActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type
    )
  }  

  onSubmit = () => {
    const { validation, paymentActions } = this.props

    if(checkValidObj(validation)) {
      paymentActions.update({
        id: validation.id.value,
        summary: validation.summary.value,
        commission: validation.commission.value
      })
    }
  }

  render() {

    return (
      <Edit 
        { ...this.props } 
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )

  }

}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.user.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}

  return {
    validation: state.payment.validationEdit,
    info: state.payment.info,
    translate: getTranslate(state.locale),
    products: state.product.origin,
    validRules,
    currentUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    paymentActions: bindActionCreators(paymentActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
