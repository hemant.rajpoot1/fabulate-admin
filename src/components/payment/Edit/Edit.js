import React, { Component } from 'react'
import _ from 'lodash'

class Edit extends Component {  

  handleChange({ target }) {
    this.props.change(target.name, target.value)
  }

  costValue = item => {
    const { validation } = this.props
    let cost = 0

    if (item.causeContentBoosterBundle) {
      cost += 319
    }
    if (item.causeEcpertHelp) {
      cost += 99
    }
    if (item.causeGetSooner) {
      cost += 199
    }
    if (item.causeNonDisclosure) {
      cost += 99
    }

    const causeFormatQty = item.causeFormatQty ? item.causeFormatQty + 1 : 1
    if (item.causeFormat) {
      const countFormat = causeFormatQty
      const productCost = _.has(item, 'causeProduct.productCost')
        ? item.causeProduct.productCost
        : 0
      cost += countFormat * productCost
    }
    if (item.causeOtherServices) {
      const countFormat = item.causeOtherServicesQty >= 0 ? item.causeOtherServicesQty + 1 : 1
      cost += countFormat * item.causeOtherServices
    }
    const percentSum = !_.isEmpty(validation.taskInfo.value) 
      ? validation.taskInfo.value.amount
      : 1
    return  {
      cost,
      taskCost: (cost / 100 * percentSum).toFixed(0, 10)
    }
  }

  render() {

    const { validation, onSubmit, info } = this.props
    const costObj = this.costValue(info.causePayment || {})
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Edit</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">
            <div className="form-group">
              <label className="col-lg-3 control-label">Cause name</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  value={ _.has(info, 'causePayment.causeName')
                    ? info.causePayment.causeName
                    : 'No information'}
                  readOnly={true}
                />
              </div>
            </div>           
            <div className="form-group">
              <label className="col-lg-3 control-label">Buyer name</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  value={ _.has(info, 'buyerPayment.userName')
                    ? info.buyerPayment.userName
                    : 'No information'}
                  readOnly={true}
                />
              </div>
            </div>          
            <div className="form-group">
              <label className="col-lg-3 control-label">Contractor name</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  value={ _.has(info, 'contractorPayment.userName')
                    ? info.contractorPayment.userName
                    : 'No information'}
                  readOnly={true}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Brief summary</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  value={ costObj.cost || ''}
                  readOnly={true}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Task summary</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  value={ costObj.taskCost || ''}
                  readOnly={true}
                />
              </div>
            </div>
            
            <div className="form-group">
              <label className="col-lg-3 control-label">Commission from contrator</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  name="commission"
                  value={ validation.commission.value || ''}
                  onChange={ e => this.handleChange(e) }
                />
                {!validation.commission.isValid && validation.commission.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="commission"
                  >
                    {validation.commission.message}
                  </label>
                )}                  
              </div>
            </div>
            <div className="form-group">
              <label className="col-lg-3 control-label">Commission from buyer</label>
              <div className="col-lg-9">
                <input 
                  type="text" 
                  className="form-control" 
                  name="summary"
                  value={ validation.summary.value || ''}
                  onChange={ e => this.handleChange(e) }
                />
                {!validation.summary.isValid && validation.summary.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="summary"
                  >
                    {validation.summary.message}
                  </label>
                )}                  
              </div>
            </div>            

            <div className="text-right">
              <button type="button" className="btn btn-primary"
                onClick={ () => onSubmit() }
              >
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Edit
