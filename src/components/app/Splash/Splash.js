import React, { Component } from 'react'

import './Splash.css'

class Splash extends Component {

  render() {

    return (
      <section className="splash text-center">
        Fabulate loading
        &nbsp;&nbsp;&nbsp;
        <i className="icon-spinner4 spinner"></i>
      </section>
    )

  }

}

export default Splash
