import React, { Component } from 'react'

class Reset extends Component {

  sendEmail = e => {

    // const reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    // reg.test(this.emailInput.value);
    const { authActions, validation } = this.props

    if(validation.email.isValid && validation.email.value) {
      authActions.resetPassword({
        email: validation.email.value
      })
    } else {
      this.changeInput('email', this.emailInput.value)
    }
  }

  changeInput = (type, value) => {
    const { validation, setValid, validRules } = this.props
    setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  render() {
    const { translate, validation } = this.props

    return (
      <div>
        <div className="panel panel-body login-form">
          <div className="text-center">
            <div className="icon-object border-warning text-warning">
              <i className="icon-spinner11"></i>
            </div>
            <h5 className="content-group">
              Password recovery
              <small className="display-block">{'We\'ll send you instructions in email'}</small>
            </h5>
          </div>

          <div className="form-group has-feedback">
            <input
              ref={ref => ref ? this.emailInput = ref : null}
              type="email"
              className="form-control"
              placeholder="Your email"
              onBlur={e => (this.emailInput.value !== '' && validation.email.isValid === undefined) && this.changeInput('email', e.target.value)}
              onChange={e => (validation.email.isValid !== undefined) && this.changeInput('email', e.target.value)}
              required
            />
            <div className="form-control-feedback">
              <i className="icon-mail5 text-muted"></i>
            </div>

            {!validation.email.isValid && validation.email.message && (
              <label
                id="email-error"
                className="validation-error-label"
                forhtml="email-reset"
              >
                {translate(`userError.${validation.email.message}`)}
              </label>
            )}
          </div>

          <button type="submit" onClick={(e) => this.emailInput ? this.sendEmail(e) : null} className="btn bg-pink-400 btn-block" >
            Reset password <i className="icon-arrow-right14 position-right"></i>
          </button>
        </div>
      </div>
    )

  }
}

export default Reset
