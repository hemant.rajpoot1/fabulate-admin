import React, { Component } from 'react'

import 'utils/jquery'

import 'normalize.css'
import 'assets/css/app.css'
import 'assets/css/icons/icomoon/styles.css'
import 'assets/css/bootstrap.css'
import 'assets/css/core.css'
import 'assets/css/components.css'
import 'assets/css/colors.css'
import 'assets/css/main/main.css'

import ripple from 'limitless/ripple'
import socket from 'store/realtime'

import LoggedApp from 'components/routing/LoggedApp'
import NotLoggedApp from 'components/routing/NotLoggedApp'
import AppLayout from 'components/layout/App'
import LoginLayout from 'components/layout/Login'
 
class App extends Component {

  componentDidMount() {
    const { currentUser, history } = this.props
    ripple.init()

    if(currentUser) {
      socket(currentUser, history)
    }
  }

  componentDidUpdate() {

    ripple.init()

  }

  render() {

    const {
      accessToken,
      loggedUser,
      translate,
      currentUser
    } = this.props


    return accessToken && loggedUser ?
      (
        <AppLayout translate={translate} currentUser={currentUser}>
          <LoggedApp />
        </AppLayout>
      ) : (
        <LoginLayout translate={translate}>
          <NotLoggedApp />
        </LoginLayout>
      )

  }

}

export default App
