import React, { Component } from 'react'

import _ from 'lodash'

import App from './App'
import Splash from '../Splash'
import OfflineError from 'components/errors/Offline'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getTranslate } from 'react-localize-redux'

import * as initActions from 'store/actions/init'
import * as authActions from 'store/actions/auth'



import backend from 'store/api/feathers'

class AppContainer extends Component {

  componentDidMount() {

    const {
      initActions,
    } = this.props

    initActions.init()

  }

  componentDidUpdate() {

    const {
      initActions,
      authActions,
      appStatus,
      authStatus,
    } = this.props

    if(appStatus && !authStatus) {

      backend.authenticate().then((payload) => {

        authActions.signIn(null, null, payload.accessToken)

      }).catch(() => {

        authActions.signOut()

        initActions.initAuth()

      })

    }

  }

  render() {

    const {
      appStatus,
      authStatus,
    } = this.props

    return appStatus && authStatus ?
      (
        <App {...this.props} />
      ) : (
        appStatus || _.isNull(appStatus) ? (
          <Splash />
        ) : (
          <OfflineError />
        )
      )

  }

}

let mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : undefined
  
  return {
    appStatus:   state.init.status,
    authStatus:  state.init.authStatus,
    accessToken: state.auth.accessToken,
    loggedUser:  state.auth.user,
    translate: getTranslate(state.locale),
    currentUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    initActions: bindActionCreators(initActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContainer))
