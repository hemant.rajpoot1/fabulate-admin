import React, { Component } from 'react'
import SignIn from '../SignIn'

class Entry extends Component {
  render() {
    return (
      <div className="tabbable panel login-form">
        <div className="tab-content panel-body">
          <div className="tab-pane fade in active" id="basic-tab1">
            <SignIn />
          </div>
        </div>
      </div>
    )
  }
}

export default Entry