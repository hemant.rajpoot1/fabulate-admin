import Restore from './Restore'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { getTranslate } from 'react-localize-redux'
import { bindActionCreators } from 'redux'

import { validation } from 'helpers/validation'

import * as authActions from 'store/actions/auth'
import { getField } from 'store/selectors/modelSelectors'


class RestoreContainer extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      validation: {
        password: {
          value: '',
          isValid: undefined,
        }       
      }  
    }
  }

  setValid = (model, rules, type) => {
    const newModel = validation(model, rules)
    const currentState = { ...this.state }

    this.setState({
      ...currentState,
      validation: {
        ...currentState.validation,
        [type]: {
          ...newModel
        }
      }
    })
  }

  render() {
    const { validation } = this.state

    return (
      <Restore 
        { ...this.props }
        validation={validation}
        setValid={this.setValid}  
      />
    )
  }
}

let mapStateToProps = (state, props) => {

  const { params } = props.match
  const validRules = getField(state, ['password'])

  return {
    translate: getTranslate(state.locale),
    id: params.id,
    validRules
  }

}

let mapDispatchToProps = (dispatch) => {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RestoreContainer))
