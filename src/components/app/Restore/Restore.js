import React, { Component } from 'react'
import Passy from 'components/ui/Passy'

class Restore extends Component {

  sendPassword = e => {
    const { id, validation, authActions } = this.props

    if ( validation.password.value && validation.password.isValid && id ) {
      authActions.updatePassword(id, { password: validation.password.value })
    }  else {
      this.changeInput('password', this.passwordInputRestore.getValue())
    }
  }

  changeInput = (type, value) => {
    const { validation, setValid, validRules } = this.props
    setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  render() {
    const { translate, validation } = this.props

    return (
      <div>
        <div className="panel panel-body login-form">
          <div className="text-center">
            <div className="icon-object border-warning text-warning">
              <i className="icon-spinner11"></i>
            </div>
            <h5 className="content-group">
              Add new password
              <small className="display-block">{'We\'ll update password'}</small>
            </h5>
          </div>


          <div className="form-group has-feedback has-feedback-left">
            <Passy
              ref={ref => ref ? this.passwordInputRestore = ref : null}
              placeholder="Create password"
              nameInput="password-restore"
              onBlur={e => (this.passwordInputRestore.getValue() !== '' && validation.password.isValid === undefined) && this.changeInput('password', e.target.value)}
              onChange={(e, value) => (validation.password.isValid !== undefined) && this.changeInput('password', value || e.target.value)}
            >
              <div className="form-control-feedback">
                <i className="icon-user-lock text-muted"></i>
              </div>
            </Passy>
            {!validation.password.isValid && validation.password.message && (
              <label
                id="password-error"
                className="validation-error-label"
                forhtml="password-restore"
              >
                {translate(`userError.${validation.password.message}`)}
              </label>
            )}
          </div>

          <button onClick={e => this.sendPassword(e)} className="btn bg-pink-400 btn-block" >
            Change password
          </button>
        </div>
      </div>
    )

  }
}

export default Restore
