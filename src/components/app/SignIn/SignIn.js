import React, { Component } from 'react'

import { Link } from 'react-router-dom'
import Checkbox from 'components/ui/Checkbox'

class SignIn extends Component {

  signIn() {
    const { authActions, validation } = this.props

    this.changeInput('email', this.emailInputSignIn.value)
    this.changeInput('password', this.passwordInputSignIn.value)
    const isValid = !!(validation.email.value
      && validation.email.isValid
      && validation.password.value
      && validation.password.isValid
    )
    if (isValid) {
      authActions.signIn(
        validation.email.value,
        validation.password.value
      )
    }
  }

  changeInput = (type, value) => {
    const { 
      validation, 
      signInActions, 
      validRules 
    } = this.props

    signInActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  render() {

    const { translate, validation } = this.props

    return (
      <section>
        <div>
          <div className="text-center">
            <div className="icon-object border-slate-300 text-slate-300">
              <i className="icon-user-tie"></i>
            </div>
          </div>

          <div className="form-group has-feedback has-feedback-left">
            <input
              type="text"
              className="form-control"
              placeholder="Your email"
              ref={ref => ref ? this.emailInputSignIn = ref : null}
              name="email-signIn"
              onBlur={e => (this.emailInputSignIn.value !== '' && validation.email.isValid === undefined) && this.changeInput('email', e.target.value)}
              onChange={e => (validation.email.isValid !== undefined) && this.changeInput('email', e.target.value)}
              required
            />
            <div className="form-control-feedback">
              <i className="icon-mention text-muted"></i>
            </div>
            {!validation.email.isValid && validation.email.message && (
              <label
                id="email-error"
                className="validation-error-label"
                forhtml="email-signIn"
              >
                {translate(`userError.${validation.email.message}`)}
              </label>
            )}
          </div>

          <div className="form-group has-feedback has-feedback-left">
            <input
              type="password"
              className="form-control"
              placeholder="Password"
              ref={ref => ref ? this.passwordInputSignIn = ref : null}
              name="password-singIn"
              onBlur={e => (this.passwordInputSignIn.value !== '' && validation.password.isValid === undefined) && this.changeInput('password', e.target.value)}
              onChange={e => (validation.password.isValid !== undefined) && this.changeInput('password', e.target.value)}
            />
            <div className="form-control-feedback">
              <i className="icon-lock2 text-muted"></i>
            </div>
            {!validation.password.isValid && validation.password.message && (
              <label
                id="password-error"
                className="validation-error-label"
                forhtml="password-singIn"
              >
                {translate(`userError.${validation.password.message}`)}
              </label>
            )}
          </div>

          <div className="form-group login-options">
            <div className="row">
              <div className="col-sm-6">
                <label className="checkbox-inline">
                  <Checkbox checked />
                  Remember
                </label>
              </div>

              <div className="col-sm-6 text-right">
                <Link to="/reset">Forgot password?</Link>
              </div>
            </div>
          </div>

          <div className="form-group">
            <button type="button" className="btn bg-pink-400 btn-block"
              onClick={() => this.signIn()}
            >
              Login
            </button>
          </div>
        </div>

        <span className="help-block text-center no-margin">
          By continuing, you're confirming that you've read our
          <a href="https://www.fabulate.com.au/terms-and-conditions/" className="ml-5 mr-5">Terms & Conditions</a>
          and
          <a href="https://www.fabulate.com.au/privacy-policy/" className="ml-5">Privacy Policy</a>
        </span>
      </section>
    )
  }
}

export default SignIn
