import SignIn from './SignIn'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as authActions from 'store/actions/auth'
import * as signInActions from 'store/actions/signIn'
import { getField } from 'store/selectors/modelSelectors'


let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.signIn.validation))
  
  return {
    translate: getTranslate(state.locale),
    validation: state.signIn.validation,    
    validRules    
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    signInActions: bindActionCreators(signInActions, dispatch)    
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignIn))
