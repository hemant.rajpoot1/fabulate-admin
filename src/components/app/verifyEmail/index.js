import verifyEmail from './verifyEmail'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as authActions from 'store/actions/auth'

let mapStateToProps = (state, props) => {

  const { params } = props.match
  const { auth } = state
  return {
    id: params.id,
    hasToken: auth && auth.accessToken  
  }

}

let mapDispatchToProps = (dispatch) => {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(verifyEmail))
