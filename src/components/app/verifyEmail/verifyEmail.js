import React, { Component } from 'react'

class verifyEmail extends Component {

  componentDidMount() {
    const { id, hasToken } = this.props
    const { verifyUserMail, signOut } = this.props.authActions
    if(hasToken){
      signOut()
    } else {      
      if(id) {
        verifyUserMail(id)
      }
    }
  }

  render() {
    return (
      <form>
        <div className="panel panel-body login-form">
          <div className="text-center">
            <div className="icon-object border-slate-300 text-slate-300">
              <i className="icon-megaphone"></i>
            </div>
            <h5 className="content-group">
                Verify by email
              <small className="display-block">Welcome!</small>
            </h5>
          </div>
        </div>
      </form>
    )

  }
}

export default verifyEmail
