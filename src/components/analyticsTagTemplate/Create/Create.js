import React from 'react'

import _ from 'lodash'

import { formattingQuery } from 'helpers/tools'
import Select from 'components/ui/Select'
import TagField from 'components/ui/TagField'

const Create = (props) => {

  const { validation, translate, search, change, analyticsTagTemplateActions, changeWorkspaces } = props

  const workspaces = _.get(validation, 'workspaces.value', {})

  const onChangeSelectInput = (e, type) => {
    const { value } = e.target

    let timer

    if (timer) {
      clearTimeout(timer)
    }

    if (value !== '') {
      timer = setTimeout(() => {
        analyticsTagTemplateActions.search(type, { q: value })
      }, 800)
    }
  }

  const onSubmit = e => {
    const formData = formattingQuery({
      workspaces: _.keys(workspaces),
      surveyScript: validation.surveyScript.value,
    })

    if (formData.workspaces || formData.surveyScript) {
      e.preventDefault()
      analyticsTagTemplateActions.create(formData)
    }

  }

  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Add analytics tag template</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li><a data-action="collapse">&nbsp;</a></li>
          </ul>
        </div>
      </div>

      <div className="panel-body">

        <div className="form-horizontal">
        <div className="form-group">
        <label className="col-lg-3 control-label">Organizations</label>
        <div className="col-lg-9">
          <TagField
            data={_.keys(workspaces)}
            tagObj={workspaces}
            onlyTags={true}
            isSelectEnabled={false}
            isStatic={false}
            labelKey='name'
            onRemove={(value) => {
              const workspace = _.get(workspaces, value)
              if(workspace) {
                changeWorkspaces('delete')(workspace)
              }
            }}
          />
          <Select
            isSearch={true}
            onSearch={e => onChangeSelectInput(e, 'workspace')}
            onSelect={changeWorkspaces('add')}
            items={search.workspace}
            value={null}
          />
        </div>
      </div>

          <div className="form-group">
            <label className="col-lg-3 control-label">Survey script</label>
            <div className="col-lg-9">
              <textarea
                className="form-control"
                name="surveyScript"
                value={validation.surveyScript.value}
                onChange={({ target }) => change(target)}
              />
              {!validation.surveyScript.isValid && validation.surveyScript.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="name"
                >
                  {translate(`analyticsTagTemplateError.${validation.surveyScript.message}`)}
                </label>
              )}
            </div>
          </div>

          <div className="text-right">
            <button
              type="button"
              className="btn btn-primary"
              onClick={e => onSubmit(e)}
            >
              Create
            </button>
          </div>

        </div>
      </div>
    </div>
  )
}

export default Create
