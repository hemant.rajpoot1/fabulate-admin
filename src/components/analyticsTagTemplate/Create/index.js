import Create from './Create'

import _ from 'lodash'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as analyticsTagTemplateActionss from 'store/actions/analyticsTagTemplate'

import { getField } from 'store/selectors/modelSelectors'

class CreateContainer extends Component {

  componentWillMount = () => {
    const { analyticsTagTemplateActions } = this.props

    analyticsTagTemplateActions.clearValidationSearch()
  }

  change = ({ name, value }) => {
    const { validation, analyticsTagTemplateActions, validRules } = this.props
    
    analyticsTagTemplateActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      true
    )
  } 

  changeWorkspaces = (typeChange) => (value) => {
    const { validation, analyticsTagTemplateActions } = this.props
    
    const workspaces = _.values(validation.workspaces.value)

    if(_.isEqual(typeChange, 'add')) {
      const isInclude = _.includes(_.keys(validation.workspaces.value), value.id)
      if(!isInclude) {
        analyticsTagTemplateActions.setValidationWorkspaces(_.concat(workspaces, value))
      }
    } else {
      analyticsTagTemplateActions.setValidationWorkspaces(_.differenceBy(workspaces, [value], 'id'))
    }
  }

  render() {
    return (
      <Create 
        { ...this.props }
        change={this.change}
        changeWorkspaces={this.changeWorkspaces}
        setFilter={this.setFilter}
      />
    )
  }
}

const mapStateToProps = (state) => {
  const validRules = getField(state, Object.keys(state.analyticsTagTemplate.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  
  return {
    translate: getTranslate(state.locale),
    validation: state.analyticsTagTemplate.validation,
    search: state.analyticsTagTemplate.search,
    currentUser,
    validRules
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    analyticsTagTemplateActions: bindActionCreators(analyticsTagTemplateActionss, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
