import List from './List'

import React, { Component } from 'react'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as analyticsTagTemplateActions from 'store/actions/analyticsTagTemplate'

class ListContainer extends Component {
  componentDidMount() {
    const { analyticsTagTemplateActions, page } = this.props
    analyticsTagTemplateActions.get({ page })
  }

  fetchList = (page) => {
    const { analyticsTagTemplateActions } = this.props
    analyticsTagTemplateActions.get({ page })
  }

  render() {
    return (
      <List
        {...this.props}
        fetchList={this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    analyticsTagTemplates: state.analyticsTagTemplate.origin,
    count: state.analyticsTagTemplate.count,
    page: state.analyticsTagTemplate.page,
  }
}

const  mapDispatchToProps = (dispatch) => {
  return {
    analyticsTagTemplateActions: bindActionCreators(analyticsTagTemplateActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
