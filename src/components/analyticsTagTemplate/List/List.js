import React from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import Pagination from 'react-js-pagination'

import './List.css'

const List = props => {

  const { analyticsTagTemplates, fetchList, page, count, analyticsTagTemplateActions } = props

  const onDelete = (id, page, templatesLength) => {

    const notice = new PNotify({
      title: 'Remove analytics tag template',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })
    
    const data = {
      id,
      page,
      templatesLength
    }

    notice.get().on('pnotify.confirm', () => {
      analyticsTagTemplateActions.remove(data)
    })
  }

  const templatesLength = Object.keys(analyticsTagTemplates).length

  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Analytics tag template list</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li>
              <a data-action="collapse">&nbsp;</a>
            </li>
          </ul>
        </div>
      </div>

      <table className="table datatable-select-basic">
        <thead>
          <tr>
            <th>#</th>
            <th>Workspaces</th>
            <th>Survey Script</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {_.map(analyticsTagTemplates, template => (
            <tr key={template.id}>
              <td>{template.id}</td>
              <td>
                <div className="d-flex flex-column">
                  {_.map(
                    _.get(template, 'workspacesAnalyticsTagTemplate', []),
                    workspace => <span key={workspace.name}>{workspace.name}</span>
                  )}   
                </div>
              </td>
              <td width="10%">
                <span className="survey-script">{template.surveyScript}</span>
              </td>
              <td className="text-center">
                <ul className="icons-list">
                  <li className="dropdown">
                    <a className="dropdown-toggle" data-toggle="dropdown">
                      <i className="icon-menu9"></i>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-right">
                      <li>
                        <Link
                          to={`/analytics-tag-template/${template.id}`}
                        >
                          <i className="icon-pencil"></i>
                          Edit
                        </Link>
                      </li>
                      <li
                        onClick={() =>
                          onDelete(template.id, page, templatesLength)
                        }
                      >
                        <a>
                          <i className="icon-cross"></i>Delete
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {count > 20 && (
        <div className="pager p-20">
          <Pagination
            hideNavigation
            pageRangeDisplayed={3}
            activePage={page}
            itemsCountPerPage={20}
            totalItemsCount={count}
            onChange={(page) => fetchList(page)}
          />
        </div>
      )}
    </div>
  )
}

export default withRouter(List)
