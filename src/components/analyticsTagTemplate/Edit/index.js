import Edit from './Edit'

import _ from 'lodash'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as analyticsTagTemplateActions from 'store/actions/analyticsTagTemplate'

import { getField } from 'store/selectors/modelSelectors'

class EditContainer extends Component {
  componentDidMount() {

    const { analyticsTagTemplateActions } = this.props
    
    const  { id } = this.props.match.params
    
    analyticsTagTemplateActions.getId(id)
    
  }

  change = ({ name, value }) => {
    const { validation, analyticsTagTemplateActions, validRules } = this.props
    
    analyticsTagTemplateActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      true
    )
  }

  changeWorkspaces = (typeChange) => (value) => {
    const { validation, analyticsTagTemplateActions } = this.props
    
    const workspaces = _.values(validation.workspaces.value)

    if(_.isEqual(typeChange, 'add')) {
      const isInclude = _.includes(_.keys(validation.workspaces.value), value.id)
      if(!isInclude) {
        analyticsTagTemplateActions.setValidationWorkspaces(_.concat(workspaces, value))
      }
    } else {
      analyticsTagTemplateActions.setValidationWorkspaces(_.differenceBy(workspaces, [value], 'id'))
    }
  }

  render() {
    const { id } = this.props.match.params
    return (
      <Edit
        { ...this.props }
        changeWorkspaces={this.changeWorkspaces}
        id={id}
        change={this.change}
      />
    )
  }
}

const mapStateToProps = (state) => {
  const validRules = getField(state, Object.keys(state.analyticsTagTemplate.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  
  return {
    translate: getTranslate(state.locale),
    validation: state.analyticsTagTemplate.validation,
    search: state.analyticsTagTemplate.search,
    isGetIdFetching: state.analyticsTagTemplate.isGetIdFetching,
    currentUser,
    validRules
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    analyticsTagTemplateActions: bindActionCreators(analyticsTagTemplateActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
