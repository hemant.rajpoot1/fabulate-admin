import Skills from './Skills'

import _ from 'lodash'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as skillsActions from 'store/actions/skills'

class SkillsContainer extends Component {

  componentDidMount() {
    const { skillsActions } = this.props
    skillsActions.get()
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props

    if (!_.isEqual(prevProps.filter, filter)) {
      this.fetch(this.props.filter)
    }
  }

  fetch = filter => {
    let { skillsActions } = this.props

    skillsActions.get(filter)
  }

  render() {
    return (
      <Skills { ...this.props }/>
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    skills: state.skill.skills,
    translate: getTranslate(state.locale),
    filter: state.skill.filter,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    skillsActions: bindActionCreators(skillsActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SkillsContainer))
