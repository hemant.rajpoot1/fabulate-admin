import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import Filter from 'components/ui/Filter'
import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

class Skills extends Component {

  onDelete(id) {
    let {
      skillsActions
    } = this.props

    let notice = new PNotify({
      title: 'Remove skill',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      skillsActions.remove(id)
    })
  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { skillsActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { skillName: value !== '' ? value : undefined }
      skillsActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { skillsActions } = this.props
    skillsActions.resetFilter()
  }

  renderSearchFilter = () => (
    <Filter
      searchItem="skills"
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.skillName || ''}
      resetFilters={this.resetFilters}
      customClasses="float-none m-10p-0"
    />
  )

  render() {
    const { skills } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">

          <h5 className="panel-title">Skills</h5>

          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>

        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Skill name</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>

            <tbody>
              {
                _.map(skills, (skill, i) =>
                  (
                    <tr key={`${skill.id}_index_${i}`}>
                      <td>{skill.id}</td>
                      <td>{skill.skillName}</td>
                      <td className="text-center">
                        <ul className="icons-list">
                          <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown">
                              <i className="icon-menu9"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-menu-right">
                              <li><Link to={`/skills/${skill.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                              <li onClick={() => this.onDelete(skill.id)}>
                                <a><i className="icon-cross"></i>Delete</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </td>
                    </tr>
                  )
                )
              }
            </tbody>
          </table>

        </div>
      </div>
    )

  }

}

export default withRouter(Skills)
