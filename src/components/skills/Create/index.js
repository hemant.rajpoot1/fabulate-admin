import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as skillsActions from 'store/actions/skills'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

import SkillsCreate from './SkillsCreate'

class SkillsCreateContainer extends Component {
  change = (type, value) => {
    const { validation, skillsActions, validRules } = this.props

    skillsActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }  

  onSubmit = () => {
    const { skillsActions, validation } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }
    if (formData.skillName) {
      skillsActions.create(formData.skillName)
      validation.skillName.value = ''
    } else {
      this.change('skillName', validation.skillName.value)  
    }
  }

  render() {
    return (
      <SkillsCreate
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.skill.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  return {
    validation: state.skill.validation,
    currentUser,
    validRules,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    skillsActions: bindActionCreators(skillsActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SkillsCreateContainer))
