import Edit from './SkillsEdit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as skillsActions from 'store/actions/skills'

class EditContainer extends Component {

  componentDidMount() {
    const { skillsActions } = this.props
    const { id } = this.props.match.params

    skillsActions.getId(id)
  }

  render() {

    return (
      <Edit { ...this.props } />
    )

  }

}

let mapStateToProps = (state, props) => {
  return {
    skill: state.skill.edit,
    translate: getTranslate(state.locale),
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    skillsActions: bindActionCreators(skillsActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
