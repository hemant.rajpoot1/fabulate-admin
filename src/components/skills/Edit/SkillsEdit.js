import React, { Component } from 'react'

class Edit extends Component {

  handleChange = ({ target }) => {
    const { skillsActions } = this.props
    skillsActions.edit({ [target.name]: target.value })
  }

  onSubmit = () => {
    const { skillsActions, skill } = this.props
    const request = {
      id: skill.id,
      skillName: skill.skillName
    }
    skillsActions.update(request)
  }

  render() {
    const { skill } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Edit</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Name</label>
              <div className="col-lg-9">
                <input type="text" className="form-control" name="skillName"
                  value={ skill.skillName || '' }
                  onChange={ e => this.handleChange(e) }
                />
              </div>
            </div>

            <div className="text-right">
              <button type="button" className="btn btn-primary"
                onClick={this.onSubmit}
              >
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Edit
