import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import _ from 'lodash'

import * as amplifyProductActions from 'store/actions/amplifyProduct'
import AmplifyList from './AmplifyList'

class AmplifyProductListContainer extends Component {

  componentDidMount() {
    const { amplifyProductActions } = this.props
    amplifyProductActions.get()
    amplifyProductActions.setSort({
      name: 'Product Name',
      value: 'productName'
    })
  }

  componentDidUpdate(prevProps) {
    const { filter, sort, amplifyProductActions } = this.props

    if (!_.isEqual(prevProps.sort, sort)) {
      const nextfilter = {
        [sort.value]: Object.values(filter)[0]
      }
      amplifyProductActions.setFilter(nextfilter)
      this.fetch(nextfilter)
    } else if (!_.isEqual(prevProps.filter, filter)) {
      this.fetch(filter)
    }
  }

  fetch = filter => {
    const { amplifyProductActions } = this.props

    amplifyProductActions.get(filter)
  }

  render() {
    return (
      <AmplifyList {...this.props} />
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    amplifyProducts: state.amplifyProduct.origin,
    translate: getTranslate(state.locale),
    filter: _.get(state, 'amplifyProduct.filter', {}),
    sort: _.get(state, 'amplifyProduct.sort', {}),
    isGetFetching: _.get(state, 'amplifyProduct.isGetFetching')
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    amplifyProductActions: bindActionCreators(amplifyProductActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AmplifyProductListContainer))
