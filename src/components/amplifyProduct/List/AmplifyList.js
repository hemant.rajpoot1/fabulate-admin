import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'
import Select from 'components/ui/Select'


import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

class AmplifyList extends Component {

  onDelete(id) {
    const {
      amplifyProductActions
    } = this.props

    const notice = new PNotify({
      title: 'Remove this amplify product',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      amplifyProductActions.remove(id)
    })

  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { amplifyProductActions, sort } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const sortName = sort.value
      const query = { [sortName]: value !== '' ? value : undefined }
      amplifyProductActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { amplifyProductActions } = this.props
    amplifyProductActions.resetFilter()
  }


  renderSearchFilter = () => {
    const { amplifyProductActions } = this.props
    const { sort } = this.props
    const sortBy = [
      {
        name: 'Product Name',
        value: 'productName'
      },
      {
        name: 'Workspace Name',
        value: 'workspaceName'
      }
    ]

    return (
      <div className='d-flex'>
        <Filter
          searchItem="amplify product"
          onChangeHandler={this.setSearchFilter}
          defaultValue={this.props.filter.amplifyProductSearch || ''}
          resetFilters={this.resetFilters}
          customClasses="mr-20"
        />
        <Select
          items={sortBy}
          placeholder="Set sorting by"
          cantReset={true}
          onSelect={amplifyProductActions.setSort}
          value={_.get(sort, 'name', '')}
        />
      </div>
    )
  }

  renderTableBody = () => {
    const { amplifyProducts, isGetFetching } = this.props
    return (
      isGetFetching
        ? (
          <tbody>
            <tr>
              <td colSpan='8'>
                <SpinnerLoadData isAbsolute={false} delay="0" />
              </td>
            </tr>
          </tbody>
        )
        : (
          <tbody>
            {
              _.map(amplifyProducts, (product, i) =>
                (
                  <tr key={`${product.id}_index_${i}`}>
                    <td>{product.id}</td>
                    <td>{_.get(product, 'amplifyWorkspace.name', null)}</td>
                    <td>{product.name}</td>
                    <td>{product.shortDescription}</td>
                    <td>{product.icon ? <img className='width-100 height-100' src={product.icon} alt={`icon-${product.name}`}/> : <span className='d-flex_center'>-</span>}</td>
                    <td>{product.promotion}</td>
                    <td>{_.get(product, 'amplifyProductTemplate.templateName', null)}</td>
                    <td>{product.included}</td>
                    <td className="text-center">
                      <ul className="icons-list">
                        <li className="dropdown">
                          <a className="dropdown-toggle" data-toggle="dropdown">
                            <i className="icon-menu9"></i>
                          </a>
                          <ul className="dropdown-menu dropdown-menu-right">
                            <li><Link to={`/amplify-product/${product.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                            <li onClick={() => this.onDelete(product.id)}>
                              <a><i className="icon-cross"></i>Delete</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </td>
                  </tr>
                )
              )
            }
          </tbody>
        )
    )
  }


  render() {
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Product card list</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Workspace Name</th>
                <th>Amplify product name</th>
                <th>Short description</th>
                <th>Icon</th>
                <th>Promotion</th>
                <th>Template name</th>
                <th>Included</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>
            {this.renderTableBody()}
          </table>
        </div>
      </div>
    )
  }
}

export default withRouter(AmplifyList)
