import React, { Component } from 'react'
import Dropzone from 'react-dropzone'
import _ from 'lodash'

import PNotify from 'pnotify'

import Select from 'components/ui/Select'
import FileLoad from 'components/ui/File'

import { selectValueByField } from 'helpers/tools'
import { toAllOrganizations} from 'helpers/params'

class AmplifyCreate extends Component {
  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { amplifyProductActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    this.timer = setTimeout(() => {
      this.setState({
        isSearch: true
      })
      amplifyProductActions.search(type, { q: value })
    }, 500)
  }

  renderErrorLabel = fieldName => {
    return (
      <label
        id="name-error"
        className="validation-error-label"
        forhtml="name"
      >
        {fieldName} can't be null
      </label>
    )
  }


  onChangeWorkspaceSelectInput = e => {
    const { workspaceActions } = this.props
    const { value } = e.target

    clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      workspaceActions.get({ name: value, workspaceType: [1, 2] })
    }, 800)
  }


  drop = (files) => {
    const { amplifyProductActions, isCreate } = this.props

    const validationExamplesRatio = {
      width: 4,
      height: 3
    }

    if (files && files[0]) {
      const fileReader = new FileReader()
      fileReader.onload = function (event) { 
        const image = new Image()
        image.src = fileReader.result
        image.onload = function (e) {
          const ratioCheck = Math.abs(image.width/image.height) - Math.abs(validationExamplesRatio.width/validationExamplesRatio.height)
          if(ratioCheck <= -0.01 || ratioCheck >= 0.01) {
            new PNotify({
              addclass: 'bg-danger',
              title: 'Ooops!',
              text: `The image dimensions should be ${validationExamplesRatio.width} : ${validationExamplesRatio.height}! \n It will be resized automatically to fit the box.`,
            })
          }
          else {
            amplifyProductActions.attachFile(files, isCreate)
          }
        } 
      }
      fileReader.readAsDataURL(files[0])
    }
  }

  removeAttach = file => {
    const { amplifyProductActions, isCreate } = this.props
    amplifyProductActions.removeAttach(file.preview || file.id, isCreate)
  }

  uploadFile = (uri, preview, fileName, fieldType) => {
    const { amplifyProductActions, isCreate } = this.props
    amplifyProductActions.uploadFile({ uri, preview, fileName, fieldType }, isCreate)
  }

  dropIcon = (files) => {
    const { amplifyProductActions } = this.props
    
    if (files && files[0]) {
      amplifyProductActions.attachFile(files, null, true)
    }
  }

  uploadIcon = (uri, preview) => {
    const { amplifyProductActions, change } = this.props

    amplifyProductActions.uploadFile({ uri, preview }, null, true)

    change('icon', uri)
  }

  removeIcon = file => {
    const { amplifyProductActions } = this.props
    amplifyProductActions.removeAttach(file.preview, null, true)
  }


  render() {
    const {
      change,
      onSubmit,
      validation,
      search,
      templates,
      workspaces,
      attachments
    } = this.props

    const workspaceId = _.get(validation, 'workspaceId')
    const currentWorkspace = selectValueByField(workspaces, workspaceId.value, 'id')

    const selectArrTemplate = _.map(templates, templateItem => ({...templateItem, name: templateItem.templateName}))

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Create amplify product</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Name</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="name"
                  value={validation.name.value}
                  onChange={e => change('name', e.target.value)}
                />
                {!validation.name.isValid && validation.name.message
                  && !validation.name.value && this.renderErrorLabel('Name')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Workspace</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onSearch={this.onChangeWorkspaceSelectInput}
                  items={workspaces}
                  value={_.get(currentWorkspace, 'name', null)}
                  inputName={'workspaceId'}
                  onSelect={value => {
                    change('workspaceId', value && value.id)
                  }}
                  cantReset
                />
                {!validation.workspaceId.isValid && validation.workspaceId.message && (
                  <label
                    id="workspaceId-error"
                    className="validation-error-label"
                    forhtml="workspaceId"
                  >
                    {validation.workspaceId.message}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Icon</label>
              <div className="col-lg-9">
                <Dropzone
                  onDrop={this.dropIcon}
                  className="dropzone-asset"
                >
                  {attachments.length > 0 ? (
                    <div className="tokenfield">
                      {_.map(attachments, (item, i) => (
                        <FileLoad
                          key={item.preview}
                          file={item}
                          uploadFile={this.uploadIcon}
                          removeAttach={this.removeIcon}
                          isImagesShow={true}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="drop-text">
                      Click or drop files
                    </div>
                  )}
                </Dropzone>
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Short description</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="shortDescription"
                  value={_.get(validation, 'shortDescription.value', '')}
                  onChange={e => change('shortDescription', e.target.value)}
                />
                {!validation.shortDescription.isValid && validation.shortDescription.message 
                  && this.renderErrorLabel('Description')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Long description</label>
              <div className="col-lg-9">
                <textarea
                  className="form-control"
                  rows="5"
                  name="longDescription"
                  value={_.get(validation, 'longDescription.value', '')}
                  onChange={e => change('longDescription', e.target.value)}
                />
                {!validation.longDescription.isValid && validation.longDescription.message 
                  && this.renderErrorLabel('Long description')
                }
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Impression goal</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="impressionGoal"
                  value={_.get(validation, 'impressionGoal.value', '')}
                  onChange={e => change('impressionGoal', e.target.value)}
                />
                {!validation.impressionGoal.isValid && validation.impressionGoal.message 
                  && this.renderErrorLabel('Impression goal')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Click goal</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="clickGoal"
                  value={_.get(validation, 'clickGoal.value', '')}
                  onChange={e => change('clickGoal', e.target.value)}
                />
                {!validation.clickGoal.isValid && validation.clickGoal.message 
                  && this.renderErrorLabel('Click goal')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Promotion</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="promotion"
                  value={_.get(validation, 'promotion.value', '')}
                  onChange={e => change('promotion', e.target.value)}
                />
                {!validation.promotion.isValid && validation.promotion.message 
                  && this.renderErrorLabel('Promotion')
                }
              </div>
            </div>  

            <div className="form-group">
              <label className="col-lg-3 control-label">Included</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="included"
                  value={validation.included.value}
                  onChange={e => change('included', e.target.value)}
                />
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Basic workflow template</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onSearch={e => this.onChangeSelectInput(e, 'template')}
                  onSelect={value => {
                    change('templateId', value && value.id)
                  }}
                  items={_.size(search.template) ? search.template : selectArrTemplate}
                  value={_.get(templates, `${[validation.templateId.value]}.templateName`)}
                />             
                {!validation.templateId.isValid && validation.templateId.message 
                  && this.renderErrorLabel('Workflow')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Terms And Condition</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="termsAndCondition"
                  value={validation.termsAndCondition.value || ''}
                  onChange={e => change('termsAndCondition', e.target.value)}
                />
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Unique audience (Publishing)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="uniqueAudience"
                  value={validation.uniqueAudience.value || ''}
                  onChange={e => change('uniqueAudience', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Page views (Publishing)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="pageViews"
                  value={validation.pageViews.value || ''}
                  onChange={e => change('pageViews', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Sessions per person (Publishing)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="sessionsPerUser"
                  value={validation.sessionsPerUser.value || ''}
                  onChange={e => change('sessionsPerUser', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Per person monthly (Publishing)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="perPersonMonthly"
                  value={validation.perPersonMonthly.value || ''}
                  onChange={e => change('perPersonMonthly', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Ad im impressions per day (Distribution)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="impressionsPerDay"
                  value={validation.impressionsPerDay.value || ''}
                  onChange={e => change('impressionsPerDay', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Monthly active users (Distribution)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="monthlyActiveUsers"
                  value={validation.monthlyActiveUsers.value || ''}
                  onChange={e => change('monthlyActiveUsers', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Daily intent signal (Distribution)</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="dailyIntentSignal"
                  value={validation.dailyIntentSignal.value || ''}
                  onChange={e => change('dailyIntentSignal', e.target.value)}
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Examples</label>
              <div className="col-lg-9">
                <Dropzone
                  onDrop={this.drop}
                  className="dropzone-asset"
                  accept="image/*"
                  multiple={false}
                >
                  {validation.examples.value.length > 0 ? (
                    <div className="tokenfield">
                      {_.map(validation.examples.value, (item, i) => (
                        <FileLoad
                          id={item.id}
                          key={item.id || item.preview}
                          file={item}
                          uploadFile={this.uploadFile}
                          removeAttach={this.removeAttach}
                          isImagesShow={true}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="drop-text">
                      Click or drop files
                    </div>
                  )}
                </Dropzone>
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Create for all organizations</label>
              <div className="col-lg-9">
                <Select
                    cantReset={true}
                    isSearch={false}
                    items={toAllOrganizations}
                    value={_.get(toAllOrganizations, `${[validation.toAllOrganizations.value]}.name`)}
                    onSelect={item => {
                      change('toAllOrganizations', item.value)
                    }}
                />
              </div>
            </div>

            {validation.toAllOrganizations.value ? (
                <div>
                  <div className="form-group">
                    <label className="col-lg-3 control-label">Buyer price</label>
                    <div className="col-lg-9">
                      <input
                          className="form-control"
                          type="number"
                          name="buyerPrice"
                          value={_.get(validation, 'buyerPrice.value') || ''}
                          onChange={e => change('buyerPrice', e.target.value)}
                      />
                      {!validation.buyerPrice.isValid && !validation.buyerPrice.value
                        && this.renderErrorLabel('Buyer price')
                      }
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-lg-3 control-label">Amplify price</label>
                    <div className="col-lg-9">
                      <input
                          className="form-control"
                          type="number"
                          name="amplifyPrice"
                          value={_.get(validation, 'amplifyPrice.value') || ''}
                          onChange={e => change('amplifyPrice', e.target.value)}
                      />
                      {!validation.amplifyPrice.isValid && !validation.amplifyPrice.value
                        && this.renderErrorLabel('Amplify price')
                      }
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-lg-3 control-label">Fabulate commission</label>
                    <div className="col-lg-9" style={{marginTop: 8}}>
                      {validation.fabulateCommission = validation.amplifyPrice.value && validation.buyerPrice.value  &&
                      validation.buyerPrice.value - validation.amplifyPrice.value > 0  ?
                          validation.buyerPrice.value - validation.amplifyPrice.value : 0}
                      <hr style={{marginTop: 5}} />
                    </div>
                  </div>
                </div>
            ) : (
                <div>
                </div>
            )}
          </div>

          <div className="text-right">
            <button className="btn btn-primary" onClick={onSubmit}>Create product card</button>
          </div>
        </div>

      </div>
    )
  }
}

export default AmplifyCreate
