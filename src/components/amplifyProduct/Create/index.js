import AmplifyCreate from './AmplifyCreate'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as amplifyProductActions from 'store/actions/amplifyProduct'
import * as templateActions from 'store/actions/template'
import * as workspaceActions from 'store/actions/workspace'

import _ from 'lodash'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'
import { workspaceTypesValues } from 'helpers/params'
import PNotify from "pnotify";

class AmplifyProductCreateContainer extends Component {

  componentDidMount() {
    const {
      templateActions,
      templates,
      workspaces
    } = this.props
    if(_.isEmpty(templates)) {
      templateActions.get({})
    }

    const workspaceFilter = {
      workspaceType: [
        workspaceTypesValues.publishing,
        workspaceTypesValues.distribution,
        workspaceTypesValues.buyer,
        workspaceTypesValues.creator,
      ],
      withoutLimit: true
    }

    !_.size(workspaces) && workspaceActions.get(workspaceFilter)
  }

  change = (type, value) => {
    const { validation, amplifyProductActions, validRules } = this.props

    amplifyProductActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }

  onSubmit = () => {
    const { amplifyProductActions, validation, validRules } = this.props
    let examplesArray = validation.examples.value
    if (_.last(examplesArray) === '') {
      examplesArray.splice(-1, 1)
    }
    amplifyProductActions.setValid(
      {
        ...validation['examples'],
        value: examplesArray,
      },
      validRules['examples'],
      'examples',
      true,
    )

    const validObj = formattingValidObj({
      ...validation,
      examples: {
        ...validation.examples,
        value: examplesArray,
      }
    })

    const formData = {
      ...validObj,
    }
    formData.fabulateCommission = validation.fabulateCommission;

    const isValid = () => {
      validation.buyerPrice.isValid = !!(!formData.toAllOrganizations || formData.buyerPrice);
      validation.amplifyPrice.isValid = !!(!formData.toAllOrganizations || formData.amplifyPrice);
      return (
          formData.templateId &&
          formData.name &&
          (!formData.toAllOrganizations || formData.buyerPrice && formData.amplifyPrice)
      );
    }

    if (isValid()) {
      amplifyProductActions.create({
        ...formData
      })
    } else {
      this.change('name', validation.name.value)
      this.change('templateId', validation.templateId.value)
      return new PNotify({
        type: 'error',
        title: 'Error',
        text: 'Please, correctly fill all fields'
      })
    }
  }

  select = value => {
    const { amplifyProductActions } = this.props
    amplifyProductActions.select(value ? value.id : null, true)
  }

  render() {
    return (
      <AmplifyCreate
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
        isCreate={true}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.amplifyProduct.validationCreate))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}

  return {
    validation: state.amplifyProduct.validationCreate,
    search: state.amplifyProduct.search,
    templates: state.template.origin,
    currentUser,
    validRules,
    workspaces: state.workspace.origin,
    attachments: _.get(state, 'amplifyProduct.attachments', [])
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
    amplifyProductActions: bindActionCreators(amplifyProductActions, dispatch),
    templateActions: bindActionCreators(templateActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AmplifyProductCreateContainer))
