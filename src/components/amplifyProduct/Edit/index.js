import AmplifyEdit from './AmplifyEdit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as amplifyProductActions from 'store/actions/amplifyProduct'
import * as templateActions from 'store/actions/template'
import * as workspaceActions from 'store/actions/workspace'

import _ from 'lodash'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'
import { workspaceTypesValues } from 'helpers/params'

class AmplifyProductEditContainer extends Component {

  componentDidMount() {
    const {
      amplifyProductActions,
      templateActions,
      templates,
      id,
      workspaceActions
    } = this.props
    if(_.isEmpty(templates)) { 
      templateActions.get({})
    }
    amplifyProductActions.getId(id)

    const workspaceFilter = {
      workspaceType: [
        workspaceTypesValues.publishing,
        workspaceTypesValues.distribution,
        workspaceTypesValues.buyer,
        workspaceTypesValues.creator,
      ],
      withoutLimit: true
    }

    workspaceActions.get(workspaceFilter)
  }

  change = (type, value) => {
    const { validation, amplifyProductActions, validRules } = this.props

    amplifyProductActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }

  onSubmit = () => {
    const { amplifyProductActions, validation, validRules, id } = this.props
    let examplesArray = validation.examples.value
    if (_.last(examplesArray) === '') {
      examplesArray.splice(-1, 1)
    }

    amplifyProductActions.setValid(
      {
        ...validation['examples'],
        value: examplesArray,
      },
      validRules['examples'],
      'examples',
      false,
    )

    const validObj = formattingValidObj({
      ...validation,
      examples: {
        ...validation.examples,
        value: examplesArray,
      }
    })

    const formData = {
      ...validObj,
      id
    }

    if (formData.templateId && formData.name) {
      amplifyProductActions.update({
        ...formData
      })
    } else {
      this.change('name', validation.name.value)
      this.change('templateId', validation.templateId.value)
    }
  }

  select = value => {
    const { amplifyProductActions } = this.props
    amplifyProductActions.select(value ? value.id : null, true)
  }

  render() {
    return (
      <AmplifyEdit
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
        isCreate={false}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.amplifyProduct.validationEdit))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const { id } = props.match.params

  return {
    validation: state.amplifyProduct.validationEdit,
    search: state.amplifyProduct.search,
    templates: state.template.origin,
    currentUser,
    validRules,
    id,
    workspaces: state.workspace.origin,
    attachments: _.get(state, 'amplifyProduct.attachments', [])
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    amplifyProductActions: bindActionCreators(amplifyProductActions, dispatch),
    templateActions: bindActionCreators(templateActions, dispatch),
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AmplifyProductEditContainer))
