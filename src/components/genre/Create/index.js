import GenreCreate from './GenreCreate'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as genreActions from 'store/actions/genre'

import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

class GenreCreateContainer extends Component {
  change = ({ name, value }) => {
    const { validation, genreActions, validRules } = this.props

    genreActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      true
    )
  }  

  onSubmit = () => {
    const { genreActions, validation } = this.props

    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }
    if (formData.genreName) {
      genreActions.create(formData.genreName)
      validation.genreName.value = ''
    } else {
      this.change('genreName', validation.genreName.value)  
    }
  }

  render() {
    return (
      <GenreCreate
        {...this.props}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.genre.validation))
  return {
    validation: state.genre.validation,
    validRules,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    genreActions: bindActionCreators(genreActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GenreCreateContainer))
