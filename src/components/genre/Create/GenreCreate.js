import React, { Component } from 'react'

class GenreCreate extends Component {

  render() {
    const { change, onSubmit, validation } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Create</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">Genre name</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="genreName"
                  value={validation.genreName.value}
                  onChange={({ target }) => change(target)}
                />
                {!validation.genreName.isValid && validation.genreName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    Genre name can not be null
                  </label>
                )}                
              </div>
            </div>                                     
          </div>

          <div className="text-right">
            <button
              className="btn btn-primary"
              onClick={onSubmit}
            >
              create genre
            </button>
          </div>
        </div>

      </div>
    )
  }
}

export default GenreCreate
