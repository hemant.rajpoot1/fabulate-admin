import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import _ from 'lodash'

import * as genreActions from 'store/actions/genre'

import Genre from './Genre'

class GenreContainer extends Component {

  componentDidMount() {
    const { genreActions } = this.props
    genreActions.get({})
  }

  fetchList = (page) => {
    const { genreActions } = this.props
    genreActions.get({
      page
    })
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props

    if(!_.isEqual(prevProps.filter, filter)){
      this.fetch(filter)
    }
  }

  fetch = filter => {
    const { genreActions } = this.props

    genreActions.get(filter)
  }

  render() {
    return (
      <Genre
        {...this.props }
        fetchList={this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    genreList: state.genre.list,
    count: state.genre.count,
    page: state.genre.page,
    translate: getTranslate(state.locale),
    filter: _.get(state, 'genre.filter', {}),
    isGetFetching: _.get(state, 'genre.isGetFetching')
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    genreActions: bindActionCreators(genreActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GenreContainer))
