import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'
import Pagination from 'react-js-pagination'

import PNotify from 'pnotify'

import Filter from 'components/ui/Filter'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

import 'pnotify/dist/pnotify.confirm'

class Genre extends Component {
  onDelete(id) {
    const { genreActions } = this.props

    const notice = new PNotify({
      title: 'Remove genre',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary',
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link',
          },
        ],
      },
    })
    notice.get().on('pnotify.confirm', () => {
      genreActions.remove(id)
    })
  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { genreActions } = this.props

    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { genreSearch: value !== '' ? value : undefined }
      genreActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { genreActions } = this.props

    genreActions.resetFilter()
  }

  renderSearchFilter = () => (
    <Filter
      searchItem='Genre'
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.genreSearch || ''}
      resetFilters={this.resetFilters}
      customClass='float-none m-10p-0'
    />
  )

  render() {
    const { genreList, count, page, fetchList, isGetFetching } = this.props
    return isGetFetching ? (
      <SpinnerLoadData />
    ) : (
      <div className='panel panel-flat'>
        <div className='panel-heading'>
          <h5 className='panel-title'>Genre List</h5>
          <div className='heading-elements'>
            <ul className='icons-list'>
            </ul>
          </div>
        </div>
        <div>
          {this.renderSearchFilter()}
          <table className='table datatable-select-basic'>
            <thead>
              <tr>
                <th>#</th>
                <th>Genre name</th>
                <th className='text-center'>Actions</th>
              </tr>
            </thead>
            <tbody>
              {_.map(genreList, (genre, i) => (
                <tr key={`${genre.id}_index_${i}`}>
                  <td>{genre.id}</td>
                  <td>{genre.genreName}</td>
                  <td className='text-center'>
                    <ul className='icons-list'>
                      <li className='dropdown'>
                        <a className='dropdown-toggle' data-toggle='dropdown'>
                          <i className='icon-menu9'></i>
                        </a>
                        <ul className='dropdown-menu dropdown-menu-right'>
                          <li>
                            <Link to={`/genre/${genre.id}`}>
                              <i className='icon-pencil' />
                              Edit
                            </Link>
                          </li>
                          <li onClick={() => this.onDelete(genre.id)}>
                            <a>
                              <i className='icon-cross'></i>Delete
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          {count > 20 && (
            <div className='pager p-20'>
              <Pagination
                hideNavigation
                pageRangeDisplayed={3}
                activePage={page}
                itemsCountPerPage={20}
                totalItemsCount={count}
                onChange={(page) => fetchList(page)}
              />
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default withRouter(Genre)
