import Edit from './GenreEdit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as genreActions from 'store/actions/genre'

class EditContainer extends Component {

  componentDidMount() {
    const { genreActions, id } = this.props
    genreActions.getId(id)
  }

  render() {
    return (
      <Edit { ...this.props } />
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    genre: state.genre.edit,
    translate: getTranslate(state.locale),
    id: props.match.params.id
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    genreActions: bindActionCreators(genreActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
