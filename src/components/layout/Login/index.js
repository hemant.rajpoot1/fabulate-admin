import React, { Component } from 'react'

class Login extends Component {

  render() {

    return (
      <div className="login-container login-cover">

        {/* Page container */}
        <div className="page-container">

          {/* Page content */}
          <div className="page-content">

            {/* Main content */}
            <div className="content-wrapper">

              {/* Content area */}
              <div className="content pb-20">

                {this.props.children}

              </div>
              {/* /content area */}

            </div>
            {/* /main content */}

          </div>
          {/* /page content */}

        </div>
        {/* /page container */}

      </div>
    )

  }

}

export default Login
