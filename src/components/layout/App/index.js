import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import moment from 'moment'

import Breadcrumbs from 'components/routing/Breadcrumbs'
import Logout from 'components/ui/Logout'

import limitless from 'limitless/app.js'
import nicescroll from 'limitless/nicescroll'

import logo from 'assets/images/logo/Fabulate_white_transparent.png'
import avatar from 'assets/images/image.png'

import './index.css'

const navigationHeadLinks = [
  {
    name: 'user',
    iconClassName: 'icon-user',
    route: '/user'
  },
  {
    name: 'workspace',
    iconClassName: 'icon-briefcase',
    route: '/workspace'
  },
  {
    name: 'workspaceUser',
    iconClassName: 'icon-users',
    route: '/workspace-user'
  },
  {
    name: 'causeShow',
    iconClassName: 'icon-folder',
    route: '/cause-show'
  },
  {
    name: 'amplifyCause',
    iconClassName: 'glyphicon glyphicon-duplicate',
    route: '/amplify-cause'
  },

  {
    name: 'asset',
    iconClassName: 'icon-file-text',
    route: '/asset'
  },
  {
    name: 'workflow',
    iconClassName: 'icon-task',
    route: '/workflow'
  },
  {
    name: 'contentCategory',
    iconClassName: 'icon-paste4',
    route: '/content-category'
  },
  {
    name: 'productCard',
    iconClassName: 'icon-cart4',
    route: '/product-card'
  },
  {
    name: 'amplifyProduct',
    iconClassName: 'icon-cart5',
    route: '/amplify-product',
  },
  {
    name: 'skills',
    iconClassName: 'icon-grid5',
    route: '/skills'
  },
  {
    name: 'genre',
    iconClassName: 'icon-wrench',
    route: '/genre'
  },
  {
    name: 'workspaceProductCard',
    iconClassName: 'icon-bookmarks',
    route: '/workspace-product-card'
  },
  {
    name: 'amplifyWorkspaceProduct',
    iconClassName: 'icon-chess-king',
    route: '/amplify-workspace-product'
  },
  {
    name: 'analyticsTagTemplate',
    iconClassName: 'icon-circle-code',
    route: '/analytics-tag-template',
  },
  {
    name: 'publicationHistory',
    iconClassName: 'icon-book',
    route: '/publication-history',
  },
  {
    name: 'analyticsProject',
    iconClassName: 'icon-stats-bars2',
    route: '/analytics-project'
  },
  {
    name: 'legalPages',
    iconClassName: 'glyphicon glyphicon-list',
    route: '/legal-pages'
  },
  {
    name: 'activeUserSessions',
    iconClassName: 'glyphicon glyphicon-log-in',
    route: '/active-user-sessions'
  },
  {
    name: 'pastUserSessions',
    iconClassName: 'glyphicon glyphicon-log-out',
    route: '/past-user-sessions'
  },
  {
    name: 'settings',
    iconClassName: 'icon-cog2',
    route: '/settings'
  }
]

class App extends Component {

  componentDidMount() {

    limitless.init()

    nicescroll.init()

  }

  componentWillUpdate() {

    limitless.reset()

    nicescroll.reset()

  }

  componentDidUpdate() {

    limitless.update()

    nicescroll.init()

  }


  render() {
    const { translate, currentUser } = this.props
    return (
      <div className="navbar-top">

        {/*NOTE: Main navbar */}
        <div className="navbar navbar-default navbar-fixed-top header-highlight">
          <div className="navbar-header">
            <a className="navbar-brand">
              <img className="logo" src={logo} alt="" />
            </a>

            <ul className="nav navbar-nav pull-right visible-xs-block">
              <li>
                <a data-toggle="collapse" data-target="#navbar-mobile">
                  <i className="icon-tree5"></i>
                </a>
              </li>
              <li>
                <a className="sidebar-mobile-main-toggle">
                  <i className="icon-paragraph-justify3"></i>
                </a>
              </li>
            </ul>
          </div>

          <div className="navbar-collapse collapse" id="navbar-mobile">
            <ul className="nav navbar-nav">
              <li>
                <a className="sidebar-control sidebar-main-toggle hidden-xs">
                  <i className="icon-paragraph-justify3"></i>
                </a>
              </li>
            </ul>

            <ul className="nav navbar-nav navbar-right">
              <li>
                <Link to="/calendar">
                  <i className="icon-calendar2" title="Calendar"></i>
                </Link>
              </li>

              <li>
                <a data-toggle="dropdown" aria-expanded="false">
                  <i className="icon-plus-circle2"></i>
                  <span className="visible-xs-inline-block position-right">Icon link</span>
                </a>

                <ul className="dropdown-menu dropdown-menu-right header-dropdown-menu">
                  <li><Link to='/create-cause'><i className="icon-folder-plus"></i> {translate('navigationCreateBtn.causeShow')} </Link></li>
                  <li><Link to='/create-asset'><i className="icon-file-plus"></i> {translate('navigationCreateBtn.asset')} </Link></li>
                  <li><Link to='/create-user'><i className="icon-user-plus"></i> {translate('navigationCreateBtn.user')} </Link></li>
                  <li><Link to='/create-workspace'><i className="icon-briefcase"></i> {translate('navigationCreateBtn.workspace')} </Link></li>
                  <li><Link to='/create-relation'><i className="icon-users"></i> {translate('navigationCreateBtn.workspaceUser')} </Link></li>
                  <li><Link to='/create-workflow'><i className="icon-task"></i> {translate('navigationCreateBtn.workflow')} </Link></li>
                  <li><Link to='/create-product-card'><i className="icon-cart4"></i> {translate('navigationCreateBtn.productCard')} </Link></li>
                  <li><Link to='/create-amplify-product'><i className="icon-cart5"></i> {translate('navigationCreateBtn.createAmplifyProduct')} </Link></li>
                  <li><Link to='/create-content-category'><i className="icon-paste4"></i> {translate('navigationCreateBtn.contentCategory')} </Link></li>
                  <li><Link to='/create-skills'><i className="icon-grid5"></i> {translate('navigationCreateBtn.skills')} </Link> </li>
                  <li><Link to='/create-limits'><i className="icon-task"></i> {translate('navigationCreateBtn.limits')} </Link> </li>
                  <li><Link to='/create-genre'><i className="icon-wrench" />{translate('navigationCreateBtn.genre')}</Link></li>
                  <li><Link to='/create-analytics-tag-template'><i className="icon-circle-code" />{translate('navigationCreateBtn.analyticsTagTemplate')}</Link></li>
                  <li><Link to='/create-workspace-product-card'><i className="icon-bookmarks" />{translate('navigationCreateBtn.workspaceProductCard')}</Link></li>
                  <li><Link to='/create-publication-history'><i className="icon-book" />{translate('navigationCreateBtn.publicationHistory')}</Link></li>
                  <li><Link to='/create-analytics-project'><i className="icon-stats-bars2" />{translate('navigationCreateBtn.analyticsProject')}</Link></li>
                  <li><Link to='/create-amplify-workspace-product'><i className="icon-chess-king" />{translate('navigationCreateBtn.amplifyWorkspaceProduct')}</Link></li>
                  <li><Link to='/create-legal-page'><i className='glyphicon glyphicon-list' />{translate('navigationCreateBtn.createLegalPages')}</Link></li>
                </ul>
              </li>

              <li className="dropdown dropdown-user">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  <img src={currentUser.image || avatar} alt="" />
                  <span>{currentUser ? currentUser.userName.split(' ')[0] : ''}</span>
                  <i className="caret"></i>
                </a>

                <ul className="dropdown-menu dropdown-menu-right">
                  <li>
                    <Link to='/profile'>
                      <i className="icon-user-plus"></i>
                      My profile
                    </Link>
                  </li>
                  <li>
                    <Logout />
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        {/*NOTE: /main navbar */}

        {/* Page container */}
        <div className="page-container">

          {/*NOTE:Page content */}
          <div className="page-content">

            {/*NOTE: Main sidebar */}
            <div className="sidebar sidebar-main sidebar-fixed">
              <div className="sidebar-content">

                {/*NOTE: User menu */}
                <div className="sidebar-user">
                  <div className="category-content">
                    <div className="media">
                      <a className="media-left">
                        <img src={currentUser.image || avatar} className="img-circle img-sm" alt="" />
                      </a>
                      <div className="media-body">
                        <span className="media-heading text-semibold">{currentUser ? currentUser.userName : ''}</span>
                        <div className="text-size-mini text-muted">
                          <i className="icon-envelop2 text-size-small"></i>
                          &nbsp;{currentUser ? currentUser.email : ''}
                        </div>
                      </div>

                      <div className="media-right media-middle">
                        <ul className="icons-list">
                          <li>
                            <a>
                              <i className="icon-cog3"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                {/*NOTE: /user menu */}

                {/*NOTE: Main navigation */}
                <div className="sidebar-category sidebar-category-visible">
                  <div className="category-content no-padding">
                    <ul className="navigation navigation-main navigation-accordion">
                      {/*NOTE: Main */}
                      <li className="navigation-header">
                        <span>{translate('navigationLeftPanel.main')}</span>
                        <i className="icon-menu" title="Main pages"></i>
                      </li>

                      {_.map(navigationHeadLinks, link => (
                        <li key={`navigate_panel_${link.name}`}>
                          <Link to={link.route} className={`${link.name}-navigation-panel`}>
                            {link.iconClassName && <i className={link.iconClassName} title={translate(`navigationLeftPanel.${link.name}`)}></i>}
                            <span>{translate(`navigationLeftPanel.${link.name}`)}</span>
                          </Link>
                        </li>
                      ))}
                      {/*NOTE: /main */}

                    </ul>
                  </div>
                </div>
                {/* NOTE: /main navigation */}

              </div>
            </div>
            {/*NOTE: /main sidebar */}


            {/*NOTE: Main content */}
            <div className="content-wrapper">

              {/*NOTE: Page header */}
              <div className="page-header">
                <div className="page-header-content">
                  <div style={{ height: 30 }} />
                </div>

                <Breadcrumbs />

              </div>
              {/*NOTE: /page header */}

              {/*NOTE: Content area */}
              <div className="content">

                {this.props.children}

                {/*NOTE: Footer */}
                <div className="footer text-muted">
                  <span>@Fabulate, {moment(Date.now()).year()}. </span>
                  <a className="text-muted" href="https://www.fabulate.com.au/terms-and-conditions/" target="_blanc"> &nbsp;Terms and Conditions. </a>
                  <a className="text-muted" href="https://www.fabulate.com.au/privacy-policy" target="_blanc"> &nbsp;Privacy Policy</a>
                </div>
                {/*NOTE: /footer */}

              </div>
              {/*NOTE: /content area */}

            </div>
            {/*NOTE: /main content */}

          </div>
          {/*NOTE: /page content */}

        </div>
        {/*NOTE: /page container */}

      </div>
    )

  }

}

export default App
