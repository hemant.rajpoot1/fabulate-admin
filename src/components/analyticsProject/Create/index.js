import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import _ from 'lodash'

import * as analyticsProjectActions from 'store/actions/analyticsProject'

import { getField } from 'store/selectors/modelSelectors'

import { formattingValidObj } from 'helpers/tools'

class CreateContainer extends Component {

  componentDidMount = () => {
    const { analyticsProjectActions } = this.props

    analyticsProjectActions.clearValidationSearch()
  }

  change = ({ name, value }) => {
    const { validation, analyticsProjectActions, validRules } = this.props
    
    analyticsProjectActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      true
    )
  }

  onSubmit = () => {
    const { validation, analyticsProjectActions, validRules } = this.props
    const { change } = this

    const formData = formattingValidObj(validation)

    const keysRequiredFields = _.filter(_.map(validRules, (item, key) => {
      if (_.includes(_.keys(item), 'notEmpty')) {
        return key
      }
    }), item => {
      if(!formData[item]) {
        return true
      }
    })

    if (_.isEmpty(keysRequiredFields)) {
      analyticsProjectActions.create(formData)
    } else {
      _.forEach(keysRequiredFields, item => {
        change({ name: item, value: validation[item].value })
      })
    }
  }

  render() {
    return (
      <Create 
        { ...this.props }
        change={this.change}
        setFilter={this.setFilter}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = (state) => {
  const validRules = getField(state, Object.keys(state.analyticsProject.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  
  return {
    translate: getTranslate(state.locale),
    validation: state.analyticsProject.validation,
    search: state.analyticsProject.search,
    currentUser,
    validRules
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    analyticsProjectActions: bindActionCreators(analyticsProjectActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
