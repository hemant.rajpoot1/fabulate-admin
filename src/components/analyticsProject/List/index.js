import List from './List'

import React, { Component } from 'react'

import _ from 'lodash'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as analyticsProjectActions from 'store/actions/analyticsProject'

class ListContainer extends Component {
  componentDidMount() {
    const { analyticsProjectActions, page } = this.props
    analyticsProjectActions.get({ page })
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props

    if (!_.isEqual(prevProps.filter.projectName, filter.projectName)) {
      this.fetch(filter)
    }
  }

  fetch = filter => {
    const { analyticsProjectActions } = this.props

    analyticsProjectActions.get(filter)
  }

  fetchList = (page) => {
    const { analyticsProjectActions } = this.props
    analyticsProjectActions.get({ page })
  }

  render() {
    return (
      <List
        {...this.props}
        fetchList={this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    analyticsProjects: state.analyticsProject.origin,
    count: state.analyticsProject.count,
    page: state.analyticsProject.page,
    filter: state.analyticsProject.filter,
    isGetFetching: state.analyticsProject.isGetFetching,
  }
}

const  mapDispatchToProps = (dispatch) => {
  return {
    analyticsProjectActions: bindActionCreators(analyticsProjectActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))