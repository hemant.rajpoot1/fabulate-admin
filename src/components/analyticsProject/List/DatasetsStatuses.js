import React, { Fragment, useMemo} from 'react'

import _ from 'lodash'
import classNames from 'classnames'

const StatusColorsDatasets = () => {
  return (
    <Fragment>
      <span>Status: </span>
      <div className="datasets-list-popup-colors">
        <div className="datasets-list-popup-colors-item datasets-list-popup-colors-success">
          <span>Success</span>
          <span className="datasets-list-popup-colors-color"/>
        </div>
        <div className="datasets-list-popup-colors-item datasets-list-popup-colors-bootstrapping">
          <span>Bootstrapping</span>
          <span className="datasets-list-popup-colors-color"/>
        </div>
        <div className="datasets-list-popup-colors-item datasets-list-popup-colors-created">
          <span>Created</span>
          <span className="datasets-list-popup-colors-color"/>
        </div>
      </div>
    </Fragment>
  )
}
  
const DatasetStatus = props => {
  const { status, datasetName } = props

  const statusIsSuccess = useMemo(() => {
    return _.isEqual(status, 'OK')
  }, [status])

  const statusIsBootstrapping = useMemo(() => {
    return _.isEqual(status, 'Bootstrapping')
  }, [status])

  const statusIsCreated = useMemo(() => {
    return _.isEqual(status, 'Created')
  }, [status])

  return (
    <span
      className={
        classNames(
          'result-get-dataset',
          {
            'result-get-dataset-success': statusIsSuccess,
            'result-get-dataset-bootstrapping': statusIsBootstrapping,
            'result-get-dataset-created': statusIsCreated
          }
        )
      }
    >
      {datasetName}
    </span>
  )
}
  
const DatasetsStatuses = props => {
  const { resultGetsDatasets } = props

  const renderedDatasets = useMemo(() => {
    return _.map(resultGetsDatasets,
      ({ status, templateDatasetName }) => <DatasetStatus key={templateDatasetName} status={status} datasetName={templateDatasetName} />
    )
  }, [resultGetsDatasets])

  return (
    <div className="datasets-list-popup panel panel-flat">
      <StatusColorsDatasets />
      {renderedDatasets}
    </div>
  )
}

export default DatasetsStatuses