import React, { useState, useCallback, useRef, useMemo} from 'react'
import { Link } from 'react-router-dom'

import _ from 'lodash'
import classNames from 'classnames'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import Pagination from 'react-js-pagination'

import DatasetsStatuses from './DatasetsStatuses'

import SpinnerLoadData from 'components/ui/SpinnerLoadData'
import Filter from 'components/ui/Filter'

import './List.css'

const ItemList = props => {
  const { id, name, projectId, readKey, writeKey, masterKey, onDelete, resultGetsDatasets, onUpdateDatasets} = props

  const allStatusSuccess = useMemo(() => {
    return _.every(
      resultGetsDatasets,
      ['status', 'OK']
    )
  }, [resultGetsDatasets])

  const [isOpenDatasetsPopup, setIsOpenDatasetsPopup] = useState(false)

  const handleChangeOpenPopup = useCallback(value => () => {
    setIsOpenDatasetsPopup(value)
  }, [isOpenDatasetsPopup])

  return (
    <tr key={id} className="analytics-project-list-item">
      <td>{id}</td>
      <td>{name}</td>
      <td>{projectId}</td>
      <td>{readKey}</td>
      <td>{writeKey}</td>
      <td>{masterKey}</td>
      <td className="datasets-status">
        <div
          className="icon-update-datasets-wrapper"
          onMouseOver={handleChangeOpenPopup(true)}
          onMouseOut={handleChangeOpenPopup(false)}
          onClick={onUpdateDatasets(id)}
        >
          <i className={classNames('icon-reload-alt', 'icon-update-datasets', { 'icon-update-datasets-success': allStatusSuccess })} />
          {isOpenDatasetsPopup && <DatasetsStatuses resultGetsDatasets={resultGetsDatasets} />}
        </div>
      </td>
      <td className="text-center analytics-project-list-item-actions">
        <ul className="icons-list">
          <li className="dropdown">
            <a className="dropdown-toggle" data-toggle="dropdown">
              <i className="icon-menu9"></i>
            </a>
            <ul className="dropdown-menu dropdown-menu-right">
              <li>
                <Link
                  to={`/analytics-project/${id}`}
                >
                  <i className="icon-pencil"></i>
                  Edit
                </Link>
              </li>
              <li
                onClick={onDelete(id)}
              >
                <a>
                  <i className="icon-cross"></i>Delete
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </td>
    </tr>
  )
}

const List = props => {
  const timer = useRef()

  const { analyticsProjectActions, count, page, fetchList, analyticsProjects, isGetFetching } = props

  const updateDatasetsForProject = useCallback(id => () => {
    analyticsProjectActions.updateDatasets(id)
  }, [analyticsProjectActions])

  const size = useMemo(() => {
    return _.keys(analyticsProjects).length
  }, [analyticsProjects])

  const onDelete = id => () => {

    const notice = new PNotify({
      title: 'Remove analytics project',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })
    
    const data = { id, page, size }

    notice.get().on('pnotify.confirm', () => {
      analyticsProjectActions.remove(data)
    })
  }

  const setSearchFilter = ({ target }) => {
    const { value } = target
    const { analyticsProjectActions } = props
    if (timer.current) {
      clearTimeout(timer.current)
    }
    timer.current = setTimeout(() => {
      const query = { projectName: value !== '' ? value : undefined }
      analyticsProjectActions.setFilter(query)
    }, 800)
  }

  const resetFilters = () => {
    const { analyticsProjectActions } = props
    analyticsProjectActions.resetFilter()
  }

  const renderSearchFilter = () => (
    <Filter
      searchItem="analytic-projects"
      onChangeHandler={setSearchFilter}
      defaultValue={props.filter.projectName || ''}
      resetFilters={resetFilters}
      customClasses="float-none m-10p-0"
    />
  )

  const renderedItemsList = useMemo(() => {
    if (isGetFetching) {
      return (
        <tr>
          <td colSpan='8'>
            <SpinnerLoadData isAbsolute={false} delay="0" />
          </td>
        </tr>
      )
    }
    return _.map(analyticsProjects,
      project => <ItemList key={project.id} {...project} onDelete={onDelete} onUpdateDatasets={updateDatasetsForProject} />
    )
  }, [analyticsProjects, isGetFetching])

  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Analytics projects list</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li>
              <a data-action="collapse">&nbsp;</a>
            </li>
          </ul>
        </div>
      </div>

      <div className="panel-body">
        {renderSearchFilter()}
        <table className="table datatable-select-basic">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Project Id</th>
              <th>Read Key</th>
              <th>Write Key</th>
              <th>Master Key</th>
              <th>Status datasets</th>
              <th className="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            {renderedItemsList}
          </tbody>
        </table>
        {count > 20 && (
          <div className="pager p-20">
            <Pagination
              hideNavigation
              pageRangeDisplayed={3}
              activePage={page}
              itemsCountPerPage={20}
              totalItemsCount={count}
              onChange={(page) => fetchList(page)}
            />
          </div>
        )}
      </div>
    </div>
  )
}

export default List