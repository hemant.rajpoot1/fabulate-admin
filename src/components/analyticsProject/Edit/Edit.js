import React from "react"

const Edit = props => {
  const { validation, change, onSubmit } = props

  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h5 className="panel-title">Edit analytics project</h5>
        <div className="heading-elements">
          <ul className="icons-list">
            <li>
              <a data-action="collapse">&nbsp;</a>
            </li>
          </ul>
        </div>
      </div>

      <div className="panel-body">
        <div className="form-group">
          <label className="col-lg-3 control-label">Project name</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="name"
              value={validation.name.value}
              onChange={e => change(e.target)}
            />
            {!validation.name.isValid && validation.name.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="name"
              >
                {validation.name.message}
              </label>
            )}
          </div>
        </div>

        <div className="form-group">
          <label className="col-lg-3 control-label">Project ID</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="projectId"
              value={validation.projectId.value}
              onChange={e => change(e.target)}
            />
            {!validation.projectId.isValid && validation.projectId.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="projectId"
              >
                {validation.projectId.message}
              </label>
            )}
          </div>
        </div>

        <div className="form-group">
          <label className="col-lg-3 control-label">Read key</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="readKey"
              value={validation.readKey.value}
              onChange={e => change(e.target)}
            />
            {!validation.readKey.isValid && validation.readKey.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="readKey"
              >
                {validation.readKey.message}
              </label>
            )}
          </div>
        </div>

        <div className="form-group">
          <label className="col-lg-3 control-label">Write key</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="writeKey"
              value={validation.writeKey.value}
              onChange={e => change(e.target)}
            />
            {!validation.writeKey.isValid && validation.writeKey.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="writeKey"
              >
                {validation.writeKey.message}
              </label>
            )}
          </div>
        </div>

        <div className="form-group">
          <label className="col-lg-3 control-label">Master key</label>
          <div className="col-lg-9">
            <input
              className="form-control"
              type="text"
              name="masterKey"
              value={validation.masterKey.value}
              onChange={e => change(e.target)}
            />
            {!validation.masterKey.isValid && validation.masterKey.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="masterKey"
              >
                {validation.masterKey.message}
              </label>
            )}
          </div>
        </div>

        <div className="text-right">
          <button
            type="button"
            className="btn btn-primary"
            onClick={onSubmit}
          >
            Update
          </button>
        </div>
      </div>
    </div>
  )
}

export default Edit
