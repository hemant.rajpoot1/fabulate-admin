import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _ from 'lodash'

import * as analyticsProjectActions from 'store/actions/analyticsProject'

import { getField } from 'store/selectors/modelSelectors'

import { formattingValidObj } from 'helpers/tools'

class EditContainer extends Component {
  componentDidMount() {

    const { analyticsProjectActions } = this.props
    
    const  { id } = this.props.match.params
    
    analyticsProjectActions.getId(id)
    
  }

  change = ({ name, value }) => {
    const { validation, analyticsProjectActions, validRules } = this.props
    
    analyticsProjectActions.setValid(
      {
        ...validation[name],
        value
      },
      validRules[name],
      name,
      true
    )
  } 

  onSubmit = () => {
    const { validation, analyticsProjectActions, validRules, match: { params: { id } } } = this.props
    const { change } = this

    const formData = formattingValidObj(validation)

    const keysRequiredFields = _.filter(_.map(validRules, (item, key) => {
      if (_.includes(_.keys(item), 'notEmpty')) {
        return key
      }
    }), item => {
      if(!formData[item]) {
        return true
      }
    })

    if (_.isEmpty(keysRequiredFields)) {
      analyticsProjectActions.update({...formData, id})
    } else {
      _.forEach(keysRequiredFields, item => {
        change({ name: item, value: validation[item].value })
      })
    }
  }

  render() {
    const { id } = this.props.match.params
    return (
      <Edit
        { ...this.props }
        id={id}
        change={this.change}
        onSubmit={this.onSubmit}
      />
    )
  }
}

const mapStateToProps = (state) => {
  const validRules = getField(state, Object.keys(state.analyticsProject.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  
  return {
    validation: state.analyticsProject.validation,
    search: state.analyticsProject.search,
    isGetIdFetching: state.analyticsProject.isGetIdFetching,
    currentUser,
    validRules
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    analyticsProjectActions: bindActionCreators(analyticsProjectActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
