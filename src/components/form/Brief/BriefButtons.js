import React from 'react'
import propTypes from 'prop-types'

const BriefButtons = (props) => {
  const {
    isCreate,
    onSubmit,
    changePage,
    leaveCreatePage,
    currentPage,
    isDraft,
    postBrief,
    shouldBeDisabled
  } = props
  const buttonsCreate = postBrief
    ? (
      <React.Fragment>
        <div className="form__brief-btn mb-15">
          <button className="btn btn-default legitRipple w-auto" onClick={() => onSubmit(0)}>Save and continue later</button>
          <button
            className="btn btn-primary legitRipple w-auto"
            onClick={() => postBrief(1)}
            disabled={shouldBeDisabled}
          >
            post brief
          </button>
        </div>
        <span className="text-info"> The brief will be available on the marketplace when posted</span>
      </React.Fragment>
    )
    : (
      <div className="form__brief-btn">
        <button className="btn btn-default legitRipple w-auto mr-20" onClick={() => onSubmit(0)}>Save and continue later</button>
        <button className="btn btn-primary legitRipple w-auto mr-20" onClick={() => changePage(currentPage + 1)}>next</button>
        { leaveCreatePage && (
          <button className="btn legitRipple w-auto" onClick={() => leaveCreatePage()}>cancel</button>
        )}
      </div>
    )
  const buttonsEdit = postBrief
    ?  (
      <div className="form__brief-btn mb-15" >
        <button className="btn btn-primary legitRipple w-auto" onClick={() => onSubmit()}>update brief</button>
        { isDraft && (
          <button className="btn btn-primary legitRipple w-auto" onClick={() => onSubmit(1)}>post brief</button>
        )}
      </div>
    )
    : (
      <div className="form__brief-btn">
        <button className="btn btn-primary legitRipple w-auto" onClick={() => onSubmit()}>update brief</button>
        { isDraft && (
          <button className="btn btn-primary legitRipple w-auto" onClick={() => onSubmit(1)}>post brief</button>
        )}
        <button className="btn btn-default legitRipple w-auto" onClick={() => changePage(currentPage + 1)}>next</button>
      </div>
    )
  return (
    <div className="row">
      <div className="col-sm-12 text-center">
        {isCreate ? (
          buttonsCreate
        ) : (
          buttonsEdit
        )}
      </div>
    </div>
  )
}

BriefButtons.protoTypes = {
  isCreate: propTypes.bool.isRequired,
  onSubmit: propTypes.func.isRequired,
  changePage: propTypes.func,
  currentPage: propTypes.number,
  leaveCreatePage: propTypes.func,
  isDraft: propTypes.bool.isRequired,
  postBrief: propTypes.func,
  shouldBeDisabled: propTypes.bool
}

export default BriefButtons