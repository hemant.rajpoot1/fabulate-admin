import React, { Component } from 'react'
import 'react-datepicker/dist/react-datepicker.css'

import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'
import Select from 'components/ui/Select'
import Checkbox from 'components/ui/Checkbox'
import Radiobutton from 'components/ui/Radiobutton'
import BriefButtons from '../BriefButtons'

import _ from 'lodash'

import { selectFieldBriefFormOne, briefStepNames } from 'helpers/params'


class BriefFormTwo extends Component {

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  handleChange = ({ target }) => {
    this.props.change(target.name, target.value)
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : null)
  }

  render() {
    const {
      validation,
      onSubmit,
      changePage,
      isCreate,
      currentPage,
    } = this.props
    const isDraft = _.get(validation, 'causeStatus.value') === 0
    
    return (
      <div className="container-custom brief-form brief-main-form panel panel-flat brief_forms">
        <div className="form-group row">
          <div className="col-sm-12">
            <BriefProgressBar
              changePage={changePage}
              itemCount={5}
              current={currentPage}
              stepNames={briefStepNames}
            />
          </div>
        </div>

        <div className="form-group row">
          <div className="col-md-12">
            <h2 className="text-default">
              The Content
            </h2>
          </div>

          <div className="col-sm-12 col-md-12 label__pad">
            <label>
              Content type
            </label>
            <Select
              isSearch={false}
              onSelect={value => {
                this.handleOnSelect('causeContentType', value)
              }}
              items={selectFieldBriefFormOne['causeContentType']}
              value={selectFieldBriefFormOne['causeContentType'][validation.causeContentType.value]
                ? selectFieldBriefFormOne['causeContentType'][validation.causeContentType.value].name
                : undefined
              }
            />
          </div>

          <div className="col-sm-12 col-md-12 label__pad">
            <label>
              Campaign objectives
            </label>
            <textarea
              className="form-control"
              rows="5"
              name="causeWorkspaceObjective"
              onChange={this.handleChange}
              value={validation.causeWorkspaceObjective.value || ''}
            >
            </textarea>

            {!validation.causeWorkspaceObjective.isValid && validation.causeWorkspaceObjective.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="causeWorkspaceObjective"
              >
                {validation.causeWorkspaceObjective.message}
              </label>
            )}
          </div>

          <div className="col-sm-12 col-md-6 label__pad">
            <label>
              Marketing objectives
            </label>

            <div className="row position-relative">
              <div className="col-sm-12 col-md-6">

                {_.map(selectFieldBriefFormOne['causeMarketing'], radioItem => (
                  <div key={`radio_marketing_index_${radioItem.value}`}>
                    <label className="check__label">
                      <Radiobutton
                        name={radioItem.name}
                        checked={validation.causeMarketing.value === radioItem.value}
                        onCheck={() => {
                          this.handleOnSelect('causeMarketing', radioItem)
                        }}
                      />
                      <span className="radiobutton-margin">
                        {radioItem.name}
                      </span>
                    </label>
                  </div>
                ))}
              </div>
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad">
            <label>
              Content idea
            </label>
            <textarea
              className="form-control"
              rows="5"
              name="causeContentIdea"
              onChange={this.handleChange}
              value={validation.causeContentIdea.value || ''}
            >
            </textarea>

            {!validation.causeContentIdea.isValid && validation.causeContentIdea.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="causeContentIdea"
              >
                {validation.causeContentIdea.message}
              </label>
            )}
          </div> 

          <div className="col-sm-12 col-md-12 label__pad">
            <label>
              Content purpose
            </label>
            <textarea
              className="form-control"
              rows="5"
              name="causePurpose"
              value={validation.causePurpose.value || ''}
              onChange={this.handleChange}
            >
            </textarea>

            {!validation.causePurpose.isValid && validation.causePurpose.message && (
              <label
                id="name-error"
                className="validation-error-label"
                forhtml="causePurpose"
              >
                {validation.causePurpose.message}
              </label>
            )}
          </div>

          <div className="col-sm-12 col-md-12 label__pad">
            <label>
              Content objectives
            </label>
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causeDwellTime"
                      checked={validation.causeDwellTime.value}
                      onCheck={() => {
                        this.props.change('causeDwellTime', !validation.causeDwellTime.value)
                      }}
                    />
                    Dwell time
                  </label>
                </div>
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causePageImpressions"
                      checked={validation.causePageImpressions.value}
                      onCheck={() => {
                        this.props.change('causePageImpressions', !validation.causePageImpressions.value)
                      }}
                    />
                    Page impressions
                  </label>
                </div>
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causeTargetNewCustomer"
                      checked={validation.causeTargetNewCustomer.value}
                      onCheck={() => {
                        this.props.change('causeTargetNewCustomer', !validation.causeTargetNewCustomer.value)
                      }}
                    />
                    Target new customers
                  </label>
                </div>
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causeIncrease"
                      checked={validation.causeIncrease.value}
                      onCheck={() => {
                        this.props.change('causeIncrease', !validation.causeIncrease.value)
                      }}
                    />
                    Increase sales
                  </label>
                </div>
              </div>
              <div className="col-sm-12 col-md-6">
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causeEngagement"
                      checked={validation.causeEngagement.value}
                      onCheck={() => {
                        this.props.change('causeEngagement', !validation.causeEngagement.value)
                      }}
                    />
                    Engagement
                  </label>
                </div>
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causeBrandConsideration"
                      checked={validation.causeBrandConsideration.value}
                      onCheck={() => {
                        this.props.change('causeBrandConsideration', !validation.causeBrandConsideration.value)
                      }}
                    />
                    Brand consideration
                  </label>
                </div>
                <div className="checkbox">
                  <label className="check__label">
                    <Checkbox
                      name="causeLaunchProduct"
                      checked={validation.causeLaunchProduct.value}
                      onCheck={() => {
                        this.props.change('causeLaunchProduct', !validation.causeLaunchProduct.value)
                      }}
                    />
                    Launch product
                  </label>
                </div>
                <div className="other">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="other"
                    name="causeSuccessOther"
                    onChange={this.handleChange}
                    value={validation.causeSuccessOther.value || ''}
                  />
                  {!validation.causeSuccessOther.isValid && validation.causeSuccessOther.message && (
                    <label
                      id="name-error"
                      className="validation-error-label"
                      forhtml="causeSuccessOther"
                    >
                      {validation.causeSuccessOther.message}
                    </label>
                  )}
                </div>
              </div>
            </div>
          </div>

        </div>

        <BriefButtons 
          isCreate={isCreate}
          onSubmit={onSubmit}
          changePage={changePage}
          currentPage={currentPage}
          isDraft={isDraft}
        />

      </div>
    )
  }
}

export default BriefFormTwo
