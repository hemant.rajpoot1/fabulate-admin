import React, { Component } from 'react'

import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'
import Checkbox from 'components/ui/Checkbox'
import Radiobutton from 'components/ui/Radiobutton'
import Select from 'components/ui/Select'
import BriefButtons from '../BriefButtons'

import Slider from 'rc-slider'
import { selectFieldBriefFormOne, briefStepNames, causeBiline } from 'helpers/params'

import _ from 'lodash'
import 'rc-slider/assets/index.css'


class BriefFormThree extends Component {

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  handleChange = ({ target }) => {
    this.props.change(target.name, target.value)
  }

  handleOnSelect = (field, item) => {
    field === 'causeWriter'
      ? this.props.change(field, _.get(item, 'value', null))
      : this.props.change(field, _.get(item, 'value', 12))
  }

  handleOnRadio = (field, item) => {
    this.props.change(field, !_.isNil(item) ? item : null)
  }

  change = (type, value) => {
    const { validation, causeActions, validRules, isCreate } = this.props
    causeActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      isCreate
    )
  }

  onCheck = (fieldName, value) => {
    const { validation, change } = this.props

    const updateArr = new Set(_.get(validation, `${[fieldName]}.value`, []))
    const isHasValue = updateArr.has(value)
    isHasValue ? updateArr.delete(value) : updateArr.add(value)

    change(fieldName, Array.from(updateArr))
  }

  renderErrorLabel = (fieldName, errorMessage) => {
    return (
      <label
        id="name-error"
        className="validation-error-label"
        forhtml={fieldName}
      >
        {errorMessage}
      </label>
    )
  }

  render() {
    const { validation, onSubmit, changePage, isCreate, currentPage } = this.props
    const isDraft = _.get(validation, 'causeStatus.value') === 0
    return (
      <div className="container-custom brief-form brief-main-form panel panel-flat brief_forms">
        <div className="form-group row">
          <div className="col-sm-12 col-md-12">
            <BriefProgressBar
              changePage={changePage}
              itemCount={5}
              current={currentPage}
              stepNames={briefStepNames}
            />
          </div>
          <div className="col-sm-12 col-md-12">
            <div className="col-md-12">
              <h2 className="text-default">
                The Target Audience
              </h2>
            </div>
            <div className="col-sm-12 cselectFieldBriefFormOneol-md-6 label__pad">
              <label>GENDER</label>
              <div className="d-flex">
                {_.map(selectFieldBriefFormOne['causeAudienceGender'], genderItem => (
                  <div className="checkbox mr-20 mt-10" key={genderItem.name}>
                    <label className="check__label">
                      <Checkbox
                        name="causeAudienceGender"
                        checked={_.includes(validation.causeAudienceGender.value, genderItem.value)}
                        onCheck={() => {
                          this.onCheck('causeAudienceGender', genderItem.value)
                        }}
                      />
                      {genderItem.name}
                    </label>
                  </div>
                ))}
              </div>
            </div>
            <div className="col-sm-12 cselectFieldBriefFormOneol-md-6 label__pad">
              <label>AGE</label>
              {_.map(selectFieldBriefFormOne['causeAudienceAge'], ageItem => (
                <div className="checkbox" key={ageItem.name}>
                  <label className="check__label">
                    <Checkbox
                      name="causeAudienceAge"
                      checked={_.includes(validation.causeAudienceAge.value, ageItem.value)}
                      onCheck={() => {
                        this.onCheck('causeAudienceAge', ageItem.value)
                      }}
                    />
                    {ageItem.name}
                  </label>
                </div>
              ))}
            </div>
            <div className="col-sm-12 cselectFieldBriefFormOneol-md-6 label__pad">
              <label>LOCATION</label>
              <Select
                isSearch={false}
                placeholder={selectFieldBriefFormOne['causeAudienceLocationCountry'][0].name}
                onSelect={value => {
                  this.handleOnSelect('causeAudienceLocationCountry', value)
                }}
                items={selectFieldBriefFormOne['causeAudienceLocationCountry']}
                value={
                  _.get(selectFieldBriefFormOne, `[causeAudienceLocationCountry]${[validation.causeAudienceLocationCountry.value]}.name`)
                }
              />

              {validation.causeAudienceLocationCountry.value === 12 && (
                <div className="row">
                  <div className="col-md-6">
                    {_.map(_.take(selectFieldBriefFormOne['causeAudienceLocation'], 4), locationItem => (
                      <div className="checkbox" key={locationItem.name}>
                        <label className="check__label">
                          <Checkbox
                            name="causeAudienceLocation"
                            checked={_.includes(validation.causeAudienceLocation.value, locationItem.value)}
                            onCheck={() => {
                              this.onCheck('causeAudienceLocation', locationItem.value)
                            }}
                          />
                          {locationItem.name}
                        </label>
                      </div>
                    ))}
                  </div>
                  <div className="col-md-6">
                    {_.map(_.takeRight(selectFieldBriefFormOne['causeAudienceLocation'], 4), locationItem => (
                      <div className="checkbox" key={locationItem.name}>
                        <label className="check__label">
                          <Checkbox
                            name="causeAudienceLocation"
                            checked={_.includes(validation.causeAudienceLocation.value, locationItem.value)}
                            onCheck={() => {
                              this.onCheck('causeAudienceLocation', locationItem.value)
                            }}
                          />
                          {locationItem.name}
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>Other audience details</label>
              <input
                className="form-control"
                type="text"
                name="causeAudienceDetail"
                onChange={this.handleChange}
                value={validation.causeAudienceDetail.value || ''}
              />

              {!validation.causeAudienceDetail.isValid 
                && validation.causeAudienceDetail.message 
                && this.renderErrorLabel('causeAudienceDetail', validation.causeAudienceDetail.message)
              }
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>How should the audience feel about the content</label>
              <input
                className="form-control"
                type="text"
                name="causeAudienceFeel"
                onChange={this.handleChange}
                value={validation.causeAudienceFeel.value || ''}
              />
              {!validation.causeAudienceFeel.isValid 
                && validation.causeAudienceFeel.message 
                && this.renderErrorLabel('causeAudienceFeel', validation.causeAudienceFeel.message)
              }
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>Key message for the target audience</label>
              <input
                className="form-control"
                type="text"
                name="causeAudienceKeyMessage"
                onChange={this.handleChange}
                value={validation.causeAudienceKeyMessage.value || ''}
              />
              {!validation.causeAudienceKeyMessage.isValid 
                && validation.causeAudienceKeyMessage.message 
                && this.renderErrorLabel('causeAudienceKeyMessage', validation.causeAudienceKeyMessage.message)}
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>Published destination</label>
              <input
                className="form-control"
                type="text"
                name="causePublishedDestination"
                onChange={this.handleChange}
                value={validation.causePublishedDestination.value || ''}
              />
              {!validation.causePublishedDestination.isValid
                && validation.causePublishedDestination.message 
                && this.renderErrorLabel('causePublishedDestination', validation.causePublishedDestination.message)}
            </div>
          </div>

          <div className="col-sm-12 col-md-12">
            <div className="col-sm-12 col-md-12">
              <h2 className="text-default">
                Tone, voice and style
              </h2>
            </div>

            <div className="col-sm-12 col-md-6 label__pad">
              <label> Creator By Line required? </label>

              <div className="row position-relative">
                <div className="col-sm-12 col-md-6">
                  {_.map(causeBiline, (bilineItem, bilineIndex) => (
                    <div key={`radio_biline_index_${bilineItem}`}>
                      <label className="check__label">
                        <Radiobutton
                          name={bilineItem}
                          checked={`${validation.causeBiline.value}` === bilineIndex}
                          onCheck={() => {
                            this.handleOnRadio('causeBiline', bilineIndex)
                          }}
                        />
                        <span className="radiobutton-margin">
                          {bilineItem}
                        </span>
                      </label>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="col-sm-12 cselectFieldBriefFormOneol-md-6 label__pad">
              <label>Will this content be published as an article or ghost written by someone else?</label>
              <Select
                isSearch={false}
                onSelect={value => {
                  this.handleOnSelect('causeWriter', value)
                }}
                items={selectFieldBriefFormOne['causeWriter']}
                value={
                  _.get(selectFieldBriefFormOne, `[causeWriter]${[validation.causeWriter.value]}.name`)
                }
              />
            </div>
                
            <div className="col-sm-12 col-md-12 label__pad">
              <label>Content should always...</label>
              <input
                className="form-control"
                type="text"
                name="causeContentShould"
                onChange={this.handleChange}
                value={validation.causeContentShould.value || ''}
              />
              {!validation.causeContentShould.isValid 
                && validation.causeContentShould.message 
                && this.renderErrorLabel('causeContentShould', validation.causeContentShould.message)}
            </div>
            <div className="col-sm-12 col-md-12 label__pad">
              <label>Content should never...</label>
              <input
                className="form-control"
                type="text"
                name="causeContentNever"
                onChange={this.handleChange}
                value={validation.causeContentNever.value || ''}
              />
              {!validation.causeContentNever.isValid 
                && validation.causeContentNever.message 
                && this.renderErrorLabel('causeContentNever', validation.causeContentNever.message)}
            </div>
            <div className="col-sm-12 col-md-12">
              <label>Brand and content attributes</label>
              <div className="panel panel-body mb-0 p-0">
                <div className="slider__wrapper">
                  <span>Traditional</span>
                  <span>Modern</span>
                </div>
                <Slider
                  className="SliderTest"
                  min={0}
                  max={100}
                  step={1}
                  onChange={e => this.props.change('causeContentTraditional', e)}
                  defaultValue={validation.causeContentTraditional.value || 50}
                />

                <div className="slider__wrapper">
                  <span>Mature</span>
                  <span>Youthful</span>
                </div>
                <Slider
                  className="SliderTest"
                  min={0}
                  max={100}
                  step={1}
                  onChange={value => this.props.change('causeContentMature', value)}
                  defaultValue={validation.causeContentMature.value || 50}
                />
                <div className="slider__wrapper">
                  <span>Feminine</span>
                  <span>Masculine</span>
                </div>
                <Slider
                  className="SliderTest"
                  min={0}
                  max={100}
                  step={1}
                  onChange={value => this.props.change('causeContentFeminine', value)}
                  defaultValue={validation.causeContentFeminine.value || 50}
                />
                <div className="slider__wrapper">
                  <span>Playful</span>
                  <span>Sophisticated</span>
                </div>
                <Slider
                  className="SliderTest"
                  min={0}
                  max={100}
                  step={1}
                  onChange={value => this.props.change('causeContentPlayful', value)}
                  defaultValue={validation.causeContentTraditional.value || 50}
                />
              </div>
            </div>
          </div>
        </div>

        <BriefButtons 
          isCreate={isCreate}
          onSubmit={onSubmit}
          changePage={changePage}
          currentPage={currentPage}
          isDraft={isDraft}
        />

      </div>
    )

  }
}

export default BriefFormThree
