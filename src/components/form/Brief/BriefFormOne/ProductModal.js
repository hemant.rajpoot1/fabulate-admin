import React, { Component } from 'react'
import Modal from 'components/ui/Modal'
import Select from 'components/ui/Select'

import _ from 'lodash'
import { getPrice } from 'helpers/string'
import { getProductPrice } from 'helpers/tools'

class ProductModal extends Component {
  constructor(props) { 
    super(props)
    this.state = {
      products: []
    }
  }

  clearProductInformation = () => {
    this.props.handleChange({
      target: {
        name: 'genre',
        value: null
      }
    })
    this.props.handleChange({
      target: {
        name: 'workType',
        value: null
      }
    })
    this.props.handleChange({
      target: {
        name: 'product',
        value: null
      }
    })
    this.setState({ products: [] })
  }

  generateNextProducts = (prevProducts, nextProducts) => {
    const generatedData = _.reduce(nextProducts, (acc, productItem) => {
      const itemIndex = _.findIndex(acc, accProductItem => (
        accProductItem
        && accProductItem.id === productItem.id
        && accProductItem.contentCategoryId === productItem.contentCategoryId 
        && accProductItem.genreId === productItem.genreId
      ))
      if(itemIndex !== -1) {
        acc[itemIndex] = {...productItem}
        return acc
      }
      return [...acc, {...productItem}]
    }, [...prevProducts])
    return generatedData
  }

  updateBriefProducts = () => {
    const productsStore = _.get(this.props, 'validation.products.value')
    const productsData = [...this.state.products]
    const generatedProducts = this.generateNextProducts(productsStore, productsData)
    
    this.clearProductInformation()
    this.props.handleChange({
      target: {
        name: 'products',
        value: generatedProducts
      }
    })

  }

  getProductNamesSet = (products) => {
    const { validation: { genre, workType } } = this.props
    const filteredProductsData = _.filter(products, productItem => 
      _.includes(productItem.genre, genre.value) && _.includes(productItem.contentCategory, workType.value)
    )
    const productNamesSet = new Set()
    _.forEach(filteredProductsData, prItem => productNamesSet.add(prItem.productDisplayName))
    return productNamesSet
  }

  editContentCategoriesModal = (product) => {
    const { products } = this.state
    const { validation } = this.props
    let nextProducts = [...products]
    const productIndex = _.findIndex(nextProducts, productItem => (
      product.id === productItem.id
      && productItem.contentCategoryId === validation.workType.value
      && productItem.genreId === validation.genre.value
    ))
    if(productIndex === -1) {
      nextProducts = [
        ...products,
        {
          contentCategoryId: validation.workType.value,
          genreId: validation.genre.value,
          ...product,
        }
      ]
    } else {
      nextProducts[productIndex] = {...product}
    }

    this.setState({ products: nextProducts })
  }

  removeContentCategory = product => {
    const { change } = this.props
    const { products } = this.state

    const filteredProducts = _.filter(products, item => item !== undefined)
    const index = filteredProducts.findIndex(item => (
      item.productCardName === product.productCardName
      && item.contentCategoryId === product.contentCategoryId
      && item.genreId === product.genreId
    )) || 0
    if (index !== -1) {
      filteredProducts.splice(index, 1)
      change(
        'products',
        filteredProducts
      )
    }
  }

  generateProductTable = (product) => {
    const {
      validation,
      currentUser,
    } = this.props
    const { products } = this.state
    const validProducts = validation.products.value
    const validThisProduct = _.find(validProducts, valProductItem => (
      valProductItem
      && valProductItem.id === product.id
      && valProductItem.contentCategoryId === validation.workType.value
      && valProductItem.genreId === validation.genre.value
    )) 
    const filteredProducts = _.filter(products, item => item !== undefined)
    const productState = filteredProducts ? {..._.find(filteredProducts, prItem => (
      prItem.id === product.id
      && prItem.contentCategoryId === validation.workType.value
      && prItem.genreId === validation.genre.value
    ))} : []
    
    const productItem = {
      ...validThisProduct,
      ...product,
      ...productState,
    }
    const productPrice = getProductPrice(productItem, currentUser)
    const totalSum = productPrice * (productItem.count || 0)
    const productItemCount = productItem.count || 0

    return (
      <tr className="text-center" key={productItem.id}>
        {(
          <React.Fragment>
            <td className="price-type text-center">
              {productItem.sizeLabel || 0} words
            </td>
            <td className="price-type text-center">
              {getPrice(productPrice)}
            </td>
            <td className="empty-tr">
              <div className="input-group bootstrap-touchspin">
                <span className="input-group-btn p-0">
                  <button
                    className="btn bootstrap-touchspin-down legitRipple"
                    type="button"
                    onClick={() => {
                      this.editContentCategoriesModal({
                        ...productItem,
                        count: productItemCount - 1
                      })
                      productItemCount === 1 && this.removeContentCategory(productItem)
                    }
                    }
                    disabled={productItemCount === 0}
                  >
                    -
                  </button>
                </span>
                <input
                  type="text"
                  className="touchspin-set-value form-control ml-10 text-center"
                  style={{ width: 40 }}
                  value={productItemCount}
                  readOnly={true}
                />
                <span className="input-group-btn p-0">
                  <button
                    className="btn bootstrap-touchspin-down legitRipple"
                    type="button"
                    onClick={() => this.editContentCategoriesModal({
                      ...productItem,
                      count: productItemCount + 1
                    })}
                    disabled={productItemCount === 10}
                  >
                    +
                  </button>
                </span>
              </div>
            </td>
            <td className="price-type text-center">
              <span>Total: ${totalSum}</span>
            </td>
          </React.Fragment>
        )
        }
      </tr>
    )
  }

  render() {
    const {
      validation,
      products,
      contentCategories,
      genreList,
      filteredProducts,
      filteredContentCategories,
      handleOnSelect
    } = this.props
    const productsState = this.state.products
    const productNamesSet = this.getProductNamesSet(products)
    const productNames = Array.from(productNamesSet).map(item => ({name: item}))
    return (
      <Modal idModal="contentCategoriesModal">
        <div className="modal-content text-left">
          <div className="m-20">
            <h5 className="modal-title">
            NEW PRODUCT
            </h5>
          </div>

          <div className="m-20">
            <div className="row m-20">
              <div className="col-sm-1">
                <button
                  type="button"
                  className="btn btn-primary btn-icon btn-rounded legitRipple"
                >
                  <span className="letter-icon">1</span> 
                </button>
              </div>
              <div className="col-sm-2">
                <label className="mt-10">
                Content genre
                </label>
              </div>
              <div className="col-sm-9">
                <Select
                  isSearch={false}
                  placeholder='Select content genre'
                  onSelect={value => {
                    handleOnSelect('genre', value && value.id)
                  }}
                  items={genreList}
                  selectFieldName="genreName"
                  value={_.get(genreList, `${validation.genre.value}.genreName`, '')}
                />
              </div>
            </div>

            <div className="row m-20">
              <div className="col-sm-1">
                <button
                  type="button"
                  className="btn btn-primary btn-icon btn-rounded legitRipple"
                >
                  <span className="letter-icon">2</span> 
                </button>
              </div>
              <div className="col-sm-2">
                <label className="mt-10">
                Type of work
                </label>
              </div>
              <div className="col-sm-9">
                <Select
                  isSearch={false}
                  placeholder='Select type of work'
                  onSelect={value => {
                    handleOnSelect('workType', value && value.id)
                  }}
                  items={filteredContentCategories}
                  value={_.get(contentCategories, `${validation.workType.value}.contentCategoryName`, '')}
                  selectFieldName="contentCategoryName"
                />
              </div>
            </div>

            <div className="row m-20">
              <div className="col-sm-1">
                <button
                  type="button"
                  className="btn btn-primary btn-icon btn-rounded legitRipple"
                >
                  <span className="letter-icon">3</span> 
                </button>
              </div>
              <div className="col-sm-2">
                <label className="mt-15">
                Product
                </label>
              </div>
              <div className="col-sm-9">
                <Select
                  isSearch={false}
                  placeholder='Select product'
                  onSelect={value => {
                    handleOnSelect('product', value && value.name)
                  }}
                  items={productNames}
                  value={validation.product.value}
                />
              </div>
            </div>

            <div className="row m-20">
              <div className="col-sm-1">
                <button
                  type="button"
                  className="btn btn-primary btn-icon btn-rounded legitRipple"
                >
                  <span className="letter-icon">4</span> 
                </button>
              </div>
              <div className="col-sm-2">
                <label className="mt-15">
                Size and price
                </label>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-1"></div>
              <div className="col-sm-11">
                <table className="table text-nowrap table-back">
                  <tbody>
                    {_.map(filteredProducts, productItem => this.generateProductTable(productItem))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div className="form-inline">
            <div className="modal-body text-right">
              <button
                type="submit"
                className="btn btn-primary legitRipple mr-20"
                data-toggle="modal"
                data-target="#contentCategoriesModal"
                disabled={!_.size(productsState)}
                onClick={ () => this.updateBriefProducts() }
              >
              Add product
              </button>

              <button
                type="submit"
                className="btn btn-default legitRipple"
                data-toggle="modal"
                data-target="#contentCategoriesModal"
                onClick={() => this.clearProductInformation()}
              >
              Cancel
              </button>
            </div>
          </div>
        </div>
      </Modal>
    )
  }

}

export default ProductModal
