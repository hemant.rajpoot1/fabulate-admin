import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'
import BriefButtons from '../BriefButtons'

import _ from 'lodash'
import moment from 'moment'
import PNotify from 'pnotify'
import Select from 'components/ui/Select'
import Checkbox from 'components/ui/Checkbox'
import ProductModal from './ProductModal'


import { selectFieldBriefFormOne, briefStepNames } from 'helpers/params'
import { getProductPrice } from 'helpers/tools'

import './BriefFormOne.css'

class BriefFormOne extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activePriceType: 1,
      showDetails: false,
      contentType: 0,
      products: []
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  handleChange = ({ target }) => {
    this.props.change(target.name, target.value)
  }

  handleOnSelect = (field, item) => {
    const { change } = this.props
    change(field, !_.isNil(item) ? item : null)
    if (field === 'genre') {
      change('workType', null)
      change('product', null)
    }
  }

  onCheck = (fieldName, value) => {
    const { validation, change } = this.props

    const updateArr = new Set(_.get(validation, `${[fieldName]}.value`, []))
    const isHasValue = updateArr.has(value)
    isHasValue ? updateArr.delete(value) : updateArr.add(value)

    change(fieldName, Array.from(updateArr))
  }

  setContent = number => {
    if (this.state.contentType !== number) {
      this.setState({ contentType: number })
    }
  }

  addContentCategories = (product, isModal) => {
    const { validation, change } = this.props
    if (_.some(validation.products.value, product)) {
      return new PNotify({
        type: 'error',
        text: 'You have already selected this product!'
      })
    }
    const oldProducts = new Set(validation.products.value)
    !oldProducts.has(product)
      && oldProducts.add({ ...product, count: 1 })
      && change(
        'products',
        Array.from(oldProducts)
      )
  }

  removeContentCategory = product => {
    const { validation, change } = this.props
    const products = _.filter(validation.products.value, item => item !== undefined)
    const index = products.findIndex(item => (
      item.productCardName === product.productCardName
      && item.contentCategoryId === product.contentCategoryId
      && item.genreId === product.genreId
    )) || 0
    if (index !== -1) {
      products.splice(index, 1)
      change(
        'products',
        products
      )
    }
  }

  editContentCategories = product => {
    const { validation, change } = this.props
    const products = validation.products.value || []
    const oldProducts = [...products]
    const index = oldProducts.findIndex(item => (item 
      && item.id === product.id
      && item.contentCategoryId === product.contentCategoryId
      && item.genreId === product.genreId
    ))

    if (!_.isEqual(product, oldProducts[index]) && index !== -1) {
      oldProducts[index] = ({
        ...product,
      })
      
      change(
        'products',
        oldProducts
      )
    }
  }

  generateTable = products =>  _.map(
    products, 
    (productItem) => this.generateProductTable(productItem)
  )


  generateProductTable = (product) => {
    const {
      validation,
      genreList,
      contentCategories
    } = this.props
    const filteredProducts = _.filter(validation.products.value, item => item !== undefined)
    const productBrief = filteredProducts ? {..._.find(filteredProducts, prItem => (
      prItem.id === product.id
      && prItem.contentCategoryId === product.contentCategoryId
      && prItem.genreId === product.genreId
    ))} : []

    const productItem = {
      ...product,
      ...productBrief
    }
    const genreName = _.get(genreList, `${product.genreId}.genreName`, '')
    const workName = _.get(contentCategories, `${product.contentCategoryId}.contentCategoryName`, '')
    const productName = product.productDisplayName
    const productPrice = getProductPrice(productItem, { permissions: 2}, validation.causeType.value)
    const totalSum = productPrice * (productItem.count ? productItem.count : 0)
    const productItemCount = productItem.count || 0
    return (
      <tr className="text-center" key={productItem.genreList + productItem.contentCategoryId + productItem.id}>
        {!_.size(productItemCount) && (
          <React.Fragment>
            <td className="price-type text-center">
              {`${genreName} | ${workName} | ${productName} | ${productItem.sizeLabel || 0} words`}
            </td>
            <td className="price-type text-center"></td>
            <td className="price-type text-center">
              <div className="input-group bootstrap-touchspin">
                <span className="input-group-btn p-0">
                  <button
                    className="btn bootstrap-touchspin-down legitRipple"
                    type="button"
                    onClick={() => this.editContentCategories({
                      ...productItem,
                      count: productItemCount - 1
                    })}
                    disabled={productItemCount === 1}
                  >
                    -
                  </button>
                </span>
                <input
                  type="text"
                  className="touchspin-set-value form-control ml-10 text-center"
                  style={{ width: 40 }}
                  value={productItemCount}
                  readOnly={true}
                />
                <span className="input-group-btn p-0">
                  <button
                    className="btn bootstrap-touchspin-down legitRipple"
                    type="button"
                    onClick={() => this.editContentCategories({
                      ...productItem,
                      count: productItemCount + 1
                    })}
                    disabled={productItemCount === 10}
                  >
                    +
                  </button>
                </span>
              </div>
            </td>
            <td className="price-type text-center">
              <span>
                Total: ${totalSum}
              </span>
            </td>
            <td className="empty-tr text-left pt-0">
              <i
                className="fa fa-remove mr-15 btn-default cursor-pointer"
                onClick={() => this.removeContentCategory(productItem)}
              >
              </i>
            </td>
          </React.Fragment>
        )}
      </tr>
    )
  }

  selectBrand = (brand) => {
    const {
      changeBrand,
      updateBrandState,
      change
    } = this.props
    change('causeBrand', brand ? brand.causeBrand : null)
    change('causeBrandURL', brand ? brand.causeBrandURL : null)
    change('causeIndustry', brand ? brand.causeIndustry : null)
    change('causeBrandAbout', brand ? brand.causeBrandAbout : null)
    changeBrand && updateBrandState()
  }

  isBrandExist = (brand) => {
    const { validation } = this.props
    return _.includes(brand.causeBrand, validation.causeBrand.value)
  }

  leaveCreatePage = () => {
    const { history } = this.props
    history.push('/dashboard')
  }

  getProductNamesSet = (products) => {
    const { validation: { genre, workType } } = this.props
    const filteredProductsData = _.filter(products, productItem => 
      _.includes(productItem.genre, genre.value) && _.includes(productItem.contentCategory, workType.value)
    )
    const productNamesSet = new Set()
    _.forEach(filteredProductsData, prItem => productNamesSet.add(prItem.productDisplayName))
    return productNamesSet
  }

  render() {
    const {
      validation,
      onSubmit,
      changePage,
      isCreate,
      currentPage,
      contentCategories,
      currentUser,
      changeBrand,
      updateBrandState,
    } = this.props

    const allBrands = currentUser.allBrands
      && validation.causeBrand.value
      && validation.causeBrand.value !== ''
      ? currentUser.allBrands.filter(item => this.isBrandExist(item))
      : currentUser.allBrands

    const product = _.map(contentCategories, contentCategoryItem => {
      const filteredData = _.filter(validation.products.value, productItem => productItem && productItem.contentCategoryId === contentCategoryItem.id)
      return {
        contentCategoryName: contentCategoryItem.contentCategoryName,
        filteredData
      }
    })
    const isDraft = _.get(validation, 'causeStatus.value') === 0
    const causeTypes = selectFieldBriefFormOne['causeType']
    const currentCauseType = _.find(causeTypes, item => _.isEqual(item.value, validation.causeType.value))

    return (
      <div className="container-custom brief-form brief-main-form panel panel-flat brief_forms">
        <div className="form-group row">
          <div className="col-sm-12">
            <BriefProgressBar
              changePage={changePage}
              itemCount={5}
              current={currentPage}
              stepNames={briefStepNames}
            />
          </div>

          <div className="col-sm-12 col-md-12">
            <h2 className="text-default">
              The project
            </h2>
          </div>
          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Name of the project
              </label>
            </div>

            <div className="col-sm-9">
              <input
                className="form-control"
                type="text"
                name="causeName"
                onChange={this.handleChange}
                value={validation.causeName.value || ''}
              />

              {!validation.causeName.isValid && validation.causeName.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeName"
                >
                  {validation.causeName.message}
                </label>
              )}
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Deadline
              </label>
            </div>

            <div className="col-sm-9">
              <div className="input-group">
                <DatePicker
                  className="form-control full-width"
                  calendarClassName="full-width"
                  popperClassName="z-index-10"
                  minDate={validation.causePitchDueDate.value ? moment(validation.causePitchDueDate.value) : new Date()}
                  selected={validation.causeDueDate.value && moment(validation.causeDueDate.value)}
                  onChange={value => this.handleChange({
                    target: {
                      name: 'causeDueDate',
                      value
                    }
                  })}
                />

                <span className="input-group-addon"><i className="icon-calendar22"></i></span>
              </div>
              {!validation.causeDueDate.isValid && validation.causeDueDate.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeDueDate"
                >
                  {validation.causeDueDate.message}
                </label>
              )}
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Pitch Deadline
              </label>
            </div>

            <div className="col-sm-9">
              <div className="input-group">
                <DatePicker
                  className="form-control full-width"
                  calendarClassName="full-width"
                  popperClassName="z-index-10"
                  minDate={new Date()}
                  maxDate={validation.causeDueDate.value && moment(validation.causeDueDate.value)}
                  selected={validation.causePitchDueDate.value && moment(validation.causePitchDueDate.value)}
                  onChange={value => this.handleChange({
                    target: {
                      name: 'causePitchDueDate',
                      value
                    }
                  })}
                />

                <span className="input-group-addon"><i className="icon-calendar22"></i></span>
              </div>
              {!validation.causePitchDueDate.isValid && validation.causePitchDueDate.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causePitchDueDate"
                >
                  {validation.causePitchDueDate.message}
                </label>
              )}
            </div>
          </div> 

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Xero invoice id
              </label>
            </div>

            <div className="col-sm-9">
              <input
                className="form-control"
                type="text"
                name="xeroInvoicesId"
                onChange={this.handleChange}
                value={validation.xeroInvoicesId.value || ''}
              />
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Xero bill id
              </label>
            </div>

            <div className="col-sm-9">
              <input
                className="form-control"
                type="text"
                name="causeName"
                onChange={this.handleChange}
                value={validation.xeroBillId.value || ''}
              />
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                PO number
              </label>
            </div>

            <div className="col-sm-9">
              <input
                className="form-control"
                type="text"
                name="causePO"
                onChange={this.handleChange}
                value={validation.causePO.value || ''}
              />

              {!validation.causePO.isValid && validation.causePO.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causePO"
                >
                  {validation.causePO.message}
                </label>
              )}
            </div>
          </div>  
          
          
          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
              Brief type
              </label>
            </div>
            <div className="col-sm-9">
              <Select
                isSearch={false}
                onSelect={(item) => {
                  this.handleOnSelect('causeType', item && !_.isNil(item.value) ? item.value : null)
                }}
                items={causeTypes}
                value={currentCauseType
                  ? currentCauseType.name
                  : undefined
                }
                inputName={'causeType'}
              />

              {!validation.causeType.isValid && validation.causeType.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeType"
                >
                  {validation.causeType.message}
                </label>
              )}
            </div>
          </div>

          <div className="col-sm-12 col-md-12">
            <h2 className="text-default">
              The brand
            </h2>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Brand name
              </label>
            </div>
            <div className="col-sm-9">
              <Select
                isInput={true}
                isSearch={false}
                inputChange={this.handleChange}
                inputName={'causeBrand'}
                selectFieldName={'causeBrand'}
                value={validation.causeBrand.value}
                items={allBrands}
                onSelect={brand => this.selectBrand(brand)}
              />

              {!validation.causeBrand.isValid && validation.causeBrand.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeBrand"
                >
                  {validation.causeBrand.message}
                </label>
              )}
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Brand URL
              </label>
            </div>

            <div className="col-sm-9">
              <input
                className="form-control"
                type="text"
                name="causeBrandURL"
                onChange={this.handleChange}
                value={validation.causeBrandURL.value || ''}
              />

              {!validation.causeBrandURL.isValid && validation.causeBrandURL.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeBrandURL"
                >
                  {validation.causeBrandURL.message}
                </label>
              )}
            </div>
          </div>

          <div className="col-sm-12 col-md-12 label__pad row">
            <div className="col-sm-3">
              <label className="mt-15 pull-right">
                Brand industry
              </label>
            </div>
            <div className="col-sm-9">
              <Select
                isSearch={false}
                onSelect={item => {
                  this.handleOnSelect('causeIndustry', item && !_.isNil(item.value) ? item.value : null)
                }}
                items={selectFieldBriefFormOne['causeIndustry']}
                value={selectFieldBriefFormOne['causeIndustry'][validation.causeIndustry.value]
                  ? selectFieldBriefFormOne['causeIndustry'][validation.causeIndustry.value].name
                  : undefined
                }
              />
              {!_.isNil(changeBrand) && (
                <div className="checkbox position-relative mb-20 col-sm-12 col-md-12">
                  <label className="check__label">
                    <Checkbox
                      name="causeDwellTime"
                      checked={changeBrand}
                      onCheck={updateBrandState}
                    />
                    Save this brand information for other briefs
                  </label>
                </div>
              )}
            </div>
          </div>


          <div className="col-sm-12 col-md-12">
            <h2 className="text-default">
              What kind of content do you need?
            </h2>
          </div>
        </div>

        <div>
          <ul className="icons-list">
            <li className="dropdown full-width">
              <a
                className="dropdown-toggle d-flex cursor-pointer"
                data-toggle="modal"
                data-target="#contentCategoriesModal"
              >
                <button
                  type="button"
                  className="btn btn-primary btn-icon btn-rounded legitRipple"
                >
                  <i className="icon-plus3"></i>
                </button>
                <span className="ml-10 mb-0">
                  Add product
                </span>
              </a>
              <ul className="dropdown-menu">
                {_.size(contentCategories) && _.map(contentCategories, contentCategoryItem => (
                  <li key={`contentCategory_${contentCategoryItem.id}`}>
                    <a
                      data-toggle="modal"
                      data-target="#contentCategoriesModal"
                      onClick={() => this.setContent(contentCategoryItem.id)}
                    >
                      <i className="icon-image2"></i>
                      Add {contentCategoryItem.contentCategoryName}
                    </a>
                  </li>
                ))}
              </ul>
            </li>
          </ul>
        </div>
        
        <div className="table-responsive">
          {validation.products.value ? (
            <table className="table text-nowrap mt-10">
              <tbody>
                {_.map(product, productItem => _.size(productItem.filteredData) ? [
                  this.generateTable(productItem.filteredData)
                ] : null)}
              </tbody>
            </table>
          ) : (
            <div className="table-back mt-20">
              <div className="p-10">
                <p className="text-style">
                  <strong>Your list is empty. Add products to your brieft to see them here.</strong>
                </p>
                <p className="text-style">Define what type of work you need from our creators. You can add different type of work for your brief.</p>
              </div>
            </div>
          )}
        </div>

        <BriefButtons 
          isCreate={isCreate}
          onSubmit={onSubmit}
          changePage={changePage}
          leaveCreatePage={this.leaveCreatePage}
          currentPage={currentPage}
          isDraft={isDraft}
        />

        <ProductModal 
          {...this.props} 
          handleOnSelect={this.handleOnSelect}
          handleChange={this.handleChange}
        />
      </div>
    )
  }
}

export default BriefFormOne
