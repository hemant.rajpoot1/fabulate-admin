import BriefFormOne from './BriefFormOne'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getFilteredProducts } from 'store/selectors/causeSelector'
import { getFilteredContentCategories } from 'store/selectors/filteredContentCategoriesSelector'
import * as genreActions from 'store/actions/genre'

class BriefFormOneContainer extends Component {
  componentDidMount() {
    const { genreActions } = this.props
    genreActions.get()
  }

  render() {
    return (
      <BriefFormOne 
        { ...this.props } 
        productFilterByGenre = {this.productFilterByGenre}
      />
    )
  }

}

const mapStateToProps = (state, props) => {
  const filteredProducts = getFilteredProducts(state)
  const filteredContentCategories = getFilteredContentCategories(state)
  return {
    genreList: state.genre.list,
    filteredProducts,
    filteredContentCategories
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    genreActions: bindActionCreators(genreActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BriefFormOneContainer))

