import BriefFormFive from './BriefFormFive'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

import { getFilteredProducts } from 'store/selectors/causeSelector'
import { getFilteredContentCategories } from 'store/selectors/filteredContentCategoriesSelector'

class BriefFormFiveContainer extends Component {
  render() {
    return (
      <BriefFormFive { ...this.props } />
    )
  }

}

const mapStateToProps = (state, props) => {
  const filteredProducts = getFilteredProducts(state)
  const filteredContentCategories = getFilteredContentCategories(state)
  return {
    genreList: state.genre,
    filteredProducts,
    filteredContentCategories
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BriefFormFiveContainer))

