import React, { Component } from 'react'
import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'
import BriefButtons from '../BriefButtons'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

import './BriefFormFive.css'
import { briefStepNames } from 'helpers/params'
import { calculateSum, getProductPrice } from 'helpers/tools'

import _ from 'lodash'

class BriefFormFive extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
    this.showConfirm = true
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : null)
  }

  post = () => {
    const { onSubmit } = this.props

    if (this.showConfirm) {
      this.showConfirm = false

      this.notice = new PNotify({
        title: 'Post brief',
        text: 'Are you sure you would like to post this brief to the marketplace?',
        hide: false,
        confirm: {
          confirm: true,
          buttons: [
            {
              text: 'Yes',
              addClass: 'btn btn-sm btn-primary'
            },
            {
              text: 'No',
              addClass: 'btn btn-sm btn-link'
            }
          ]
        },
      })

      this.notice.get().on('pnotify.confirm', () => {
        onSubmit(1)
        this.showConfirm = true
      })
      this.notice.get().on('pnotify.cancel', () => {
        this.showConfirm = true
      })
    }
  }

  editContentCategories = product => {
    const { validation, change } = this.props
    const products = validation.products.value || []
    const oldProducts = [...products]
    const index = oldProducts.findIndex(item => (item 
      && item.id === product.id
      && item.contentCategoryId === product.contentCategoryId
      && item.genreId === product.genreId
    ))

    if (!_.isEqual(product, oldProducts[index]) && index !== -1) {
      oldProducts[index] = ({
        ...product,
      })
      
      change(
        'products',
        oldProducts
      )
    }
  }

  removeContentCategory = product => {
    const { validation, change } = this.props
    const products = _.filter(validation.products.value, item => item !== undefined)
    const index = products.findIndex(item => (
      item.productCardName === product.productCardName
      && item.contentCategoryId === product.contentCategoryId
      && item.genreId === product.genreId
    )) || 0
    if (index !== -1) {
      products.splice(index, 1)
      change(
        'products',
        products
      )
    }
  }

  generateTable = products =>  _.map(
    products, 
    (productItem) => this.generateProductTable(productItem)
  )


  generateProductTable = (product) => {
    const {
      validation,
      genreList,
      contentCategories,
    } = this.props
    const filteredProducts = _.filter(validation.products.value, item => item !== undefined)
    const productBrief = filteredProducts ? {..._.find(filteredProducts, prItem => (
      prItem
      && prItem.id === product.id
      && prItem.contentCategoryId === product.contentCategoryId
      && prItem.genreId === product.genreId
    ))} : []

    const productItem = {
      ...product,
      ...productBrief
    }
    const genreName = _.get(genreList, `${product.genreId}.genreName`, '')
    const workName = _.get(contentCategories, `${product.contentCategoryId}.contentCategoryName`, '')
    const productName = product.productDisplayName
    const productPrice = getProductPrice(productItem, { permissions: 2 }, validation.causeType.value)
    const totalSum = productPrice * (productItem.count ? productItem.count : 0)
    const productItemCount = productItem.count || 0
    return (
      <tr className="text-center" key={productItem.genreList + productItem.contentCategoryId + productItem.id}>
        {!_.size(productItemCount) && (
          <React.Fragment>
            <td className="price-type text-center">
              {`${genreName} | ${workName} | ${productName} | ${productItem.sizeLabel || 0} words`}
            </td>
            <td className="price-type text-center"></td>
            <td className="price-type text-center">
              <div className="input-group bootstrap-touchspin">
                <span className="input-group-btn p-0">
                  <button
                    className="btn bootstrap-touchspin-down legitRipple"
                    type="button"
                    onClick={() => this.editContentCategories({
                      ...productItem,
                      count: productItemCount - 1
                    })}
                    disabled={productItemCount === 1}
                  >
                    -
                  </button>
                </span>
                <input
                  type="text"
                  className="touchspin-set-value form-control ml-10 text-center"
                  style={{ width: 40 }}
                  value={productItemCount}
                  readOnly={true}
                />
                <span className="input-group-btn p-0">
                  <button
                    className="btn bootstrap-touchspin-down legitRipple"
                    type="button"
                    onClick={() => this.editContentCategories({
                      ...productItem,
                      count: productItemCount + 1
                    })}
                    disabled={productItemCount === 10}
                  >
                    +
                  </button>
                </span>
              </div>
            </td>
            <td className="price-type text-center">
              <span>
                Total: ${totalSum}
              </span>
            </td>
            <td className="empty-tr text-left pt-0">
              <i
                className="fa fa-remove mr-15 btn-default cursor-pointer"
                onClick={() => this.removeContentCategory(productItem)}
              >
              </i>
            </td>
          </React.Fragment>
        )}
      </tr>
    )
  }

  componentWillUnmount() {
    this.notice && this.notice.remove()
  }

  render() {
    const {
      changePage,
      validation,
      isCreate,
      onSubmit,
      currentPage,
      currentUser,
      currentWorkspace
    } = this.props

    const checks = {
      causeContentBoosterBundle: validation.causeContentBoosterBundle.value ? 319 : 0,
      causeEcpertHelp: validation.causeEcpertHelp.value ? 99 : 0,
      causeGetSooner: validation.causeGetSooner.value ? 199 : 0,
      causeNonDisclosure: validation.causeNonDisclosure.value ? 99 : 0
    }

    const sum = calculateSum(validation, currentUser, currentWorkspace)
    const isDraft = _.get(validation, 'causeStatus.value') === 0
    return (
      <div className="form__brief-three panel panel-flat brief_forms">
        <div className="container-custom">
          <div className="row mb-20">
            <div className="col-sm-12 col-md-12">
              <BriefProgressBar
                changePage={changePage}
                itemCount={5}
                current={currentPage}
                stepNames={briefStepNames}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-12">
              <div>
                <h2 className="text-default">
                  Your project summary

                  <small className="display-block"> All costs excludes GST </small>
                </h2>
                <div className="form__brief-summary_box">
                  {_.size(validation.products.value) ? (
                    <div className="table-responsive">
                      <table className="table text-nowrap">
                        <tbody>
                          {this.generateTable(validation.products.value)}
                        </tbody>
                      </table>
                    </div>
                  ) : null}

                  {checks.causeContentBoosterBundle !== 0 && (
                    <div className="form__brief-summary_item">
                      <span>Content Boster Bundle</span>
                      <span>${checks.causeContentBoosterBundle}</span>
                    </div>
                  )}
                  {checks.causeEcpertHelp !== 0 && (
                    <div className="form__brief-summary_item">
                      <span>Recruiter</span>
                      <span>${checks.causeEcpertHelp}</span>
                    </div>
                  )}
                  {checks.causeGetSooner !== 0 && (
                    <div className="form__brief-summary_item">
                      <span>Urgent</span>
                      <span>${checks.causeGetSooner}</span>
                    </div>
                  )}
                  {checks.causeNonDisclosure !== 0 && (
                    <div className="form__brief-summary_item">
                      <span>NDA</span>
                      <span>${checks.causeNonDisclosure}</span>
                    </div>
                  )}

                  <div className="form__brief-summary_item">
                    <h5>Project total</h5>
                    <h5>${sum}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <BriefButtons
            isCreate={isCreate}
            postBrief={this.post}
            shouldBeDisabled={currentWorkspace.id ? false : true}
            onSubmit={onSubmit}
            isDraft={isDraft}
          />

          <div className="row mt-20">
            <div className="col-sm-12 text-center">
              <span className="form__brief-click">
                By clicking 'Post Project', you are indicating that you have read
                  and agreed to the&nbsp;
                <span><a href="https://www.fabulate.com.au/terms-and-conditions/" target="_blanc">Terms & Conditions</a></span>&nbsp;
                  and&nbsp;
                <span><a href="https://www.fabulate.com.au/privacy-policy" target="_blanc">Privacy Policy.</a></span>
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default BriefFormFive

