import React, { Component } from 'react'
import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'
import FileLoad from 'components/ui/File'
import Dropzone from 'react-dropzone'
import BriefButtons from '../BriefButtons'

import 'pnotify/dist/pnotify.confirm'

import { briefStepNames } from 'helpers/params'
import _ from 'lodash'

class BriefFormFour extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
    this.showConfirm = true
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : null)
  }

  drop = (files, fieldType) => {
    const { causeActions, isCreate } = this.props
    if (files && files[0]) {
      causeActions.attachFile(_.map(files, file => { file.fieldType = fieldType; return file }), isCreate)
    }
  }

  removeAttach = file => {
    const { causeActions, isCreate } = this.props
    causeActions.removeAttach(file.preview || file.id, isCreate)
  }

  uploadFile = (uri, preview, fileName, fieldType) => {
    const { causeActions, isCreate } = this.props
    causeActions.uploadFile({ uri, preview, fileName, fieldType }, isCreate)
  }

  handleChange = ({ target }) => {
    this.props.change(target.name, target.value)
  }

  renderErrorLabel = (fieldName, errorMessage) => {
    return (
      <label
        id="name-error"
        className="validation-error-label"
        forhtml={fieldName}
      >
        {errorMessage}
      </label>
    )
  }

  render() {

    const {
      changePage,
      validation,
      isCreate,
      onSubmit,
      currentPage,
      attachments
    } = this.props

    const existing = []
    const brandGuidelines = []
    const favourite = []
    _.forEach(attachments, attachItem => {
      switch (attachItem.fieldType) {
      case 1: {
        favourite.push(attachItem)
        break
      }
      case 2: {
        brandGuidelines.push(attachItem)
        break
      }
      default: {
        existing.push(attachItem)
        break
      }
      }
    })
    const isDraft = _.get(validation, 'causeStatus.value') === 0
    return (
      <div className="form__brief-three panel panel-flat brief_forms">
        <div className="container-custom">
          <div className="row mb-20">
            <div className="col-sm-12 col-md-12">
              <BriefProgressBar
                changePage={changePage}
                itemCount={5}
                current={currentPage}
                stepNames={briefStepNames}
              />
            </div>
          </div>

          <div className="form-group row">
            <div className="col-md-12">
              <h2 className="text-default">
                References
              </h2>
            </div>
                
            <div className="col-sm-12 col-md-12 label__pad">
              <label>Required SEO terms</label>
              <input
                className="form-control"
                type="text"
                name="causeSeo"
                onChange={this.handleChange}
                value={validation.causeSeo.value || ''}
              />
              {!validation.causeSeo.isValid 
                && validation.causeSeo.message 
                && this.renderErrorLabel('causeSeo', validation.causeSeo.message)
              }
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>Further URLs for reference</label>
              <input
                className="form-control"
                type="text"
                name="causeFurtherUrl"
                onChange={this.handleChange}
                value={validation.causeFurtherUrl.value || ''}
              />
              {!validation.causeFurtherUrl.isValid 
                && validation.causeFurtherUrl.message 
                && this.renderErrorLabel('causeFurtherUrl', validation.causeFurtherUrl.message)
              }
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>
                EXISTING RESOURCES
              </label>
              <div>
                First draft of the content, notes or research results that may be helpful for your project.
                <div className="display-block text-muted">
                  You may include links and/or attached documents.
                </div>
              </div>
              <textarea
                className="form-control"
                rows="5"
                name="causeExitingResources"
                onChange={this.handleChange}
                value={validation.causeExitingResources.value || ''}
              >
              </textarea>
              {!validation.causeExitingResources.isValid && validation.causeExitingResources.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeExitingResources"
                >
                  {validation.causeExitingResources.message}
                </label>
              )}
            </div>
            <div className="col-sm-12 col-md-12">
              <Dropzone
                onDrop={files => this.drop(files, 0)}
                accept=".doc, .docx, .ppt, .pdf"
                className="dropzone-asset"
              >
                <form className="dropzone dz-clickable mb-12">
                  {_.size(existing) ? (
                    <div className="tokenfield">
                      {_.map(existing, (item, i) => (
                        <FileLoad
                          key={item.preview}
                          file={item}
                          uploadFile={this.uploadFile}
                          removeAttach={this.removeAttach}
                          type={0}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="dz-default dz-message">
                      <span>
                        Click or drag and drop to attach images or documents.
                      </span>
                    </div>
                  )}
                </form>
              </Dropzone>
            </div>

            <div className="col-sm-12 col-md-12 label__pad">
              <label>
                BRAND GUIDELINES/STYLE GUIDE
              </label>
              <div>
                Include your brand style guide, to help the creator understand the specifics of your brand.
                <div className="display-block text-muted">
                  You may include links and/or attached documents.
                </div>
              </div>
              <textarea
                className="form-control"
                rows="5"
                name="causeBrandGuidelines"
                onChange={this.handleChange}
                value={validation.causeBrandGuidelines.value || ''}
              >
              </textarea>
              {!validation.causeBrandGuidelines.isValid && validation.causeBrandGuidelines.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeBrandGuidelines"
                >
                  {validation.causeBrandGuidelines.message}
                </label>
              )}
            </div>
            <div className="col-sm-12 col-md-12">
              <Dropzone
                onDrop={files => this.drop(files, 2)}
                accept=".doc, .docx, .ppt, .pdf"
                className="dropzone-asset"
              >
                <form className="dropzone dz-clickable mb-12">
                  {!!_.size(brandGuidelines) ? (
                    <div className="tokenfield">
                      {_.map(brandGuidelines, item => (
                        <FileLoad
                          key={item.preview}
                          file={item}
                          uploadFile={this.uploadFile}
                          removeAttach={this.removeAttach}
                          type={2}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="dz-default dz-message">
                      <span>
                        Click or drag and drop to attach images or documents.
                      </span>
                    </div>
                  )}
                </form>
              </Dropzone>
            </div>

            <div className="col-sm-12 col-md-12 label__pad pt-30">
              <label>
                FAVOURITE EXAMPLES OF SPONSORED CONTENT
              </label>
              <div>
                References and examples of other brand content  you like and can be used as reference for this project.
                <div className="display-block text-muted">
                  You may include links and/or attached documents.
                </div>
              </div>
              <textarea
                className="form-control"
                rows="5"
                name="causeFavoriteExamples"
                onChange={this.handleChange}
                value={validation.causeFavoriteExamples.value || ''}
              >
              </textarea>
              {!validation.causeFavoriteExamples.isValid && validation.causeFavoriteExamples.message && (
                <label
                  id="name-error"
                  className="validation-error-label"
                  forhtml="causeFavoriteExamples"
                >
                  {validation.causeFavoriteExamples.message}
                </label>
              )}
            </div>
            <div className="col-sm-12 col-md-12">
              <Dropzone
                onDrop={files => this.drop(files, 1)}
                accept=".doc, .docx, .ppt, .pdf"
                className="dropzone-asset"
              >
                <form className="dropzone dz-clickable mb-12">
                  {!!_.size(favourite) ? (
                    <div className="tokenfield">
                      {_.map(favourite, (item, i) => (
                        <FileLoad
                          key={item.preview}
                          file={item}
                          uploadFile={this.uploadFile}
                          removeAttach={this.removeAttach}
                          type={1}
                        />
                      ))}
                    </div>
                  ) : (
                    <div className="dz-default dz-message">
                      <span>
                        Click or drag and drop to attach images or documents.
                      </span>
                    </div>
                  )}
                </form>
              </Dropzone>
            </div>
          </div>

          <BriefButtons 
            isCreate={isCreate}
            onSubmit={onSubmit}
            changePage={changePage}
            currentPage={currentPage}
            isDraft={isDraft}
          />

        </div>
      </div>
    )
  }
}

export default BriefFormFour

