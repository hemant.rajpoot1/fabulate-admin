import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'

import Chat from './Chat'

class ChatContainer extends Component {

  render() {
    return(
      <Chat { ...this.props } />
    )
  }
}


const mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  
  return {
    currentUser
  }
}

export default withRouter(connect(mapStateToProps, null)(ChatContainer))
