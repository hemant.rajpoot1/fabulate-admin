import React, { Component } from 'react'
import _ from 'lodash'

class TabForm extends Component {
  constructor(props) {
    super(props)

     this.state = {
      selectedTab: 0
    }
  }

  setBriefTab = index => {
    const { selectTab } = this.props

    if(index !== this.state.selectedTab) {
      this.setState({
        selectedTab: index
      }, () => {
        selectTab && selectTab(index)
      })
    }
  }

  render() {
    const { 
      tabs, 
      children, 
      btns, 
      isTop, 
      header,
      disablePanelClass
    } = this.props

    return (
      <div className={disablePanelClass ? 'custom-size-page background-inherit pb-20' : 'custom-size-page panel pb-20'}>
      {header}
      <div className="row">
        <div className={disablePanelClass ? 'pt-10' : 'mt-40 pt-10'}>
          <div className={disablePanelClass ? '' : 'container-custom'}>
            <div className="col-md-12">
              <div className={`tabbable ${isTop ? '' : 'nav-tabs-vertical nav-tabs-left'}`}>
                <ul className={`nav nav-tabs ${isTop ? 'bottom-divided' : 'nav-tabs-highlight'}`}>
                  {tabs && _.map(tabs, (item, i) => (
                    <li 
                      className={`${this.state.selectedTab === i ? 'active' : ''} ${item.class || ''} `} 
                      key={`tab_item_${i}`}
                    >
                      <a onClick={() =>  !item.disable && this.setBriefTab(i)} data-toggle="tab">
                        {item.tabChild}
                      </a>
                    </li>
                  ))}
                </ul>
                <div className="tab-content custom-size-main">
                  {children 
                    ? children[this.state.selectedTab]
                    : null
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {btns}
    </div>    
    )
  }
}

export default TabForm