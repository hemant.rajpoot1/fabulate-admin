import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'
import html2pdf from 'html2pdf.js'
import html2canvas from 'html2canvas'
import classNames from 'classnames'

import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'

import TabForm from 'components/form/TabForm'
import TinyEditor from 'components/ui/TinyEditor'
import Select from 'components/ui/Select'
import Dropzone from 'react-dropzone'
import FileLoad from 'components/ui/File'

import 'pnotify/dist/pnotify.confirm'
import {
  roles,
  fileExtensions
} from 'helpers/params'

class WorkroomItem extends Component {

  showConfirm = true
  sortTask(sortItemA, sortItemB) {
    return sortItemA.index > sortItemB.index ? 1 : -1
  }

  download = async () => {
    const { info } = this.props
    let text = 'Not Found'
    if (window.editor && _.has(window, 'editor.editor.dom.doc')) {
      text = `
        <!doctype html>\n
        ${window.editor.editor.dom.doc.documentElement.innerHTML}
      `
    }
    window.html2canvas = html2canvas
    const {
      REACT_APP_API_HOST,
      REACT_APP_API_PORT,
      REACT_APP_API_SCHEME      
    } = process.env
    var opt = {
      margin: 1,
      filename: `${info.causeWorkroom.causeName}.pdf`,
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: {
        dpi: 300,
        useCORS: true,
        letterRendering: true,
        proxy: `${REACT_APP_API_SCHEME}://${REACT_APP_API_HOST}:${REACT_APP_API_PORT}`
      },
      jsPDF: {
        unit: 'in',
        format: 'letter',
        orientation: 'portrait'
      }
    }
    // New Promise-based usage:
    await html2pdf().from(text).set(opt).save()
  }
  
  downloadFile = (id, ref) => {
    const { workroomActions } = this.props
    workroomActions.downloadFile(id, ref)
  }

  getFieldName = productsSelectModel => {
    const { currentProduct } = this.props
    const currentProductData = productsSelectModel[currentProduct]
    if (currentProductData.productDisplayName) {
      return 'productDisplayName'
    } else if (currentProductData.productCardName) {
      return 'productCardName'
    } else {
      return 'productName'
    }
  }

  handleProductSelect = (item) => {
    const { id, contentCategoryId, genreId } = item
    const { setCurrentProduct } = this.props
    id && contentCategoryId && genreId
      ? setCurrentProduct(genreId + contentCategoryId + id)
      : setCurrentProduct(id)
  }

  getModel = (workItem, tasksInfo) => {
    const { 
      workroom,
      workroomActions,
      info,
      currentUser,
      productInfo,
      currentProduct,
      guestComments,
      currentIndex
    } = this.props
    let tabs = []
    const workItemsCount = _.size(workItem) || 1
    const tasks = tasksInfo
      ? tasksInfo.sort((sortItemA, sortItemB) => this.sortTask(sortItemA, sortItemB))
      : []
    const currentDate = moment()
    const causeDueDate = moment(_.get(info, 'causeWorkroom.causeDueDate')).isValid()
      ? moment(_.get(info, 'causeWorkroom.causeDueDate'))
      : moment()
    let diffDate = causeDueDate.diff(currentDate, 'days')
    if (diffDate < 0) {
      diffDate = diffDate * -1
    }
    const taskDays = diffDate / workItemsCount
    const children = _.map(workItem, (workItem) => {
      const { index: i } = workItem
      const currentTask = tasks[workItem.milestoneStatus] || {}
      const time = currentTask.time || 0
      const days = taskDays / 100 * time
      const taskRole = !_.isNil(currentTask.taskRole)
        ? currentTask.taskRole
        : 3
      const isFinish = workItem.milestoneStatus === tasks.length
      const color = (() => {
        if (days > 5) {
          return 'border-success'
        }
        if (days < 2) {
          return 'border-danger'
        } return 'border-warning-300'
      })()
      tabs = [
        ...tabs,
        {
          tabChild: (
            <div key={`key_tab_${workItem.id}`}>
              {isFinish ? (
                <i className="mb-1 mr-5 icon-checkmark2 text-success" />
              ) : (
                <span className={`mb-1 mr-5 status-mark ${color}`} />
              )}
              {`Product card ${i + 1}`}
            </div>
          )
        }
      ]
      const data = workItem ? { ...workItem } : null
      const productsSelectModel = workroom.workroomItems.value || {}
      const existing = _.isArray(data.attachments) 
        ? data.attachments.filter(attachItem => !attachItem.fieldType) 
        : []
      const dzClass = classNames({
        'dropzone': true,
        'dz-clickable': true,
        'mb-12': true,
        'default-cursor': isFinish
      })
      const hasGuestComments = _.size(guestComments) && _.find(guestComments, userComments => {
        return userComments.productId === currentProduct
          && userComments.productIndex === currentIndex
          && userComments.comments
      })
      const fieldName = this.getFieldName(productsSelectModel)
      return (
        <div key={currentProduct}>
          <div className="pull-right d-flex">
            <div className="mr-15 help-select-workroom">
              <Select
                items={productsSelectModel}
                value={_.get(productsSelectModel, `${[currentProduct]}.${fieldName}`)}
                cantReset={true}
                onSelect={item => this.handleProductSelect(item)}
                selectFieldName={fieldName}
              />
            </div>
            <div className="mr-15">
              <span>Download </span>
              <i
                className="icon-file-download2 mr-5 cursor-pointer"
                onClick={() => this.download(_.get(data, 'value', 'Not found'))}
              >
              </i>
            </div>
            {hasGuestComments ? this.renderGuestComments() : null}
            <div className="bg-slate-300 label">
              <span
                className="token-label token-label-padding"
              >
                {isFinish ? 'Finished' : roles[taskRole].name + ' in progress'}
              </span>
            </div>
          </div>
          <div className="pt-30">
            <TinyEditor
              history={this.props.history}
              key={`key_tiny_editor_${i + productInfo.genreId + productInfo.contentCategoryId + productInfo.id}`}
              item={i}
              data={data}
              workroom={workroom}
              workroomActions={workroomActions}
              milestone={workroom.milestone.value}
              isChange={false}
              isEdit={false}
              isReply={false}
              currentUser={currentUser}
              onUpdateComments={this.onUpdateComments}
            />
          </div>
          <Dropzone
            disabled={true}
            accept={fileExtensions}
            className={dzClass}
          >
            {_.size(existing)
              ? (
                <div className="tokenfield">
                  {_.map(existing, item => (
                    <FileLoad
                      key={item.preview}
                      file={item}
                      downloadFile={this.downloadFile}
                      type={0}
                    />
                  ))}
                </div>
              ) : (
                <div className="dz-default dz-message default-cursor">
                  <span>
                    There are no shared attachments
                  </span>
                </div>
              )
            }
          </Dropzone>
        </div>
      )
    })
    return { tabs, children }
  }

  handleChange = (data, i) => {
    const { workroom, change, productInfo } = this.props
    const nextWorkItems = productInfo.workItems
      ? productInfo.workItems.slice()
      : []
    nextWorkItems[i] = {
      ...nextWorkItems[i],
      ...data
    }

    const nextWorkroomItems = {
      ...workroom.workroomItems.value,
      [productInfo.id]: {
        ...productInfo,
        workItems: nextWorkItems
      }
    }
    change('workroomItems', nextWorkroomItems)
  }

  onUpdateComments = (params, cb, cbParam) => {
    const {
      currentIndex
    } = this.props

    this.handleChange({
      ...params,
    }, currentIndex)

    if(cb) {
      if(cbParam) {
        cb(cbParam)
      } else {
        cb()
      }
    }
  }

  generateLables = tasks => {
    return _.map(tasks, taskItem => taskItem.taskName)
  }

  generateVariables = () => {
    const {
      productInfo,
      currentIndex,
      currentProduct,
      workroom,
    } = this.props
    const workItem = !_.isEmpty(productInfo.workItems)
      ? { ...productInfo.workItems }
      : {}
    const tasks = _.get(productInfo, 'productTemplate.templateTask', [])
    const currentItem = _.find(workItem, { index: currentIndex })
    const model = !_.isNil(productInfo.count)
      ? this.getModel(workItem, tasks)
      : {
        tabs: [],
        children: []
      }
    const headerLables = _.size(tasks) && this.generateLables(tasks)
    const currentStep = currentItem.milestoneStatus || 0
    const currentProductName = 
      workroom.workroomItems.value[currentProduct].productDisplayName
      || workroom.workroomItems.value[currentProduct].productCardName
      || workroom.workroomItems.value[currentProduct].productName

    return {
      tasks,
      model,
      headerLables,
      currentStep,
      currentProductName,
    }
  }

  render() {
    const {
      causeInfo,
      isShowChat,
      setIndex,
      currentIndex,
    } = this.props
    const {
      tasks,
      model,
      headerLables,
      currentStep,
      currentProductName,
    } = this.generateVariables()
    const renderWorkroomHeader = () => (
      <React.Fragment>
        <h6 className="pt-20 pl-20 text-semibold d-block">Current product card: &nbsp;</h6>
        <h6 className="d-block">{currentProductName}</h6>
        <BriefProgressBar
          itemCount={_.size(tasks)}
          current={currentStep + 1}
          stepNames={headerLables}
        />
      </React.Fragment>
    )

    return (
      <div className="row">
        <div className={`col-md-${isShowChat ? '8' : '12'}`}>
          {!_.isNil(causeInfo.causeFormatQty) ? (
            <TabForm
              tabs={model.tabs}
              header={!!_.size(tasks)
                && renderWorkroomHeader()}
              isTop={true}
              selectTab={index => setIndex(index)}
              currentIndex={currentIndex}
            >
              {model.children}
            </TabForm>
          ) : (
            <h4> Milestones have not been selected yet </h4>
          )}
        </div>
      </div>
    )


  }
}

export default WorkroomItem