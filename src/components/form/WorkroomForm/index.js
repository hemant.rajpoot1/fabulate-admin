import WorkroomForm from './WorkroomForm'
import Workroom from './Workroom'


import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getField } from 'store/selectors/modelSelectors'
import _ from 'lodash'
import { formattingValidObj, checkValidObj } from 'helpers/tools'

import * as workroomActions from 'store/actions/workroom'
import * as causeActions from 'store/actions/cause'

class WorkroomFormContainer extends Component {
  componentDidMount() {
    const { causeId } = this.props

    if(causeId) {
      this.fetch({ causeId })
    }
  }

  componentDidUpdate(prevProps) {
    if(prevProps.causeId !== this.props.causeId) {
      const { causeId } = this.props

      if(causeId) {
        this.fetch({ causeId })
      }      
    }
  }

  change = (type, value) => {
    const {  workroomActions, workroom, validRules } = this.props
    workroomActions.setValid(
      {
        ...workroom[type],
        value
      },
      validRules[type],
      type
    )
  } 

  fetch = query => {
    const { workroomActions } = this.props

    workroomActions.getId(query)
  }

  onSubmit = (payment) => {
    const {
      workroomActions,
      workroom,
      causeActions
    } = this.props

    const validObj = formattingValidObj(workroom)
    const formData = {
      ...validObj,
    }

    if (checkValidObj(workroom)) {
      workroomActions.update(formData)
    }

    if(workroom.causeId.value){
      causeActions.update({
        id: workroom.causeId.value,
      })
    }
  }  

  render() { 
    const {workroom} = this.props
    return !_.isNil(workroom.workroomItems.value)
      ? (
        <Workroom 
          { ...this.props } 
          change={this.change}
          onSubmit={this.onSubmit}
        />
      ) 
      : (
        <WorkroomForm 
          { ...this.props } 
          change={this.change}
          onSubmit={this.onSubmit}
        />
      )
  }
}


let mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  const workroom = state.workroom.validationEdit || {}
  const info = state.workroom.info || {}
  const validRules = getField(state, Object.keys(state.cause.validationEdit))
  const attachments = state.workroom.attachments || []
  const isAdmin = currentUser.permissions <= 1 
 
  let { id } = props.match.params
  const causeId = id

  return {
    isGetIdFetching: state.workroom.isGetIdFetching,
    info,
    workroom,
    validRules,
    currentUser,
    isAdmin,
    causeId,
    attachments
  }
}


let mapDispatchToProps = (dispatch) => {
  return {
    workroomActions: bindActionCreators(workroomActions, dispatch),
    causeActions: bindActionCreators(causeActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkroomFormContainer))
