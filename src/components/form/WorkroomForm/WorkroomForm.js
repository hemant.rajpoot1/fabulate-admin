import React, { Component, Fragment } from 'react'

import _ from 'lodash'
import moment from 'moment'
import PNotify from 'pnotify'
import html2pdf from 'html2pdf.js'
import html2canvas from 'html2canvas'

import { roles } from 'helpers/params'

import TabForm from 'components/form/TabForm'
import TinyEditor from 'components/ui/TinyEditor'
import BriefProgressBar from 'components/ui/BriefProgressBar/BriefProgressBar'
import Chat from 'components/form/Chat'

import 'pnotify/dist/pnotify.confirm'

class WorkroomForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentIndex: 0
    }
    this.showConfirm = true
  }

  setIndex = index => {
    this.setState({
      currentIndex: index
    })
  }

  sortTask(sortItemA, sortItemB) {
    return sortItemA.index > sortItemB.index ? 1 : -1
  }

  download = async () => {
    const { info } = this.props
    let text = 'Not Found'
    if (window.editor && _.has(window, 'editor.editor.dom.doc')) {
      text = `
        <!doctype html>\n
        ${window.editor.editor.dom.doc.documentElement.innerHTML}
      `
    }
    window.html2canvas = html2canvas
    const {
      REACT_APP_API_HOST,
      REACT_APP_API_PORT,
      REACT_APP_API_SCHEME      
    } = process.env
    var opt = {
      margin: 1,
      filename: `${info.causeWorkroom.causeName}.pdf`,
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: {
        dpi: 300,
        useCORS: true,
        letterRendering: true,
        proxy: `${REACT_APP_API_SCHEME}://${REACT_APP_API_HOST}:${REACT_APP_API_PORT}`
      },
      jsPDF: {
        unit: 'in',
        format: 'letter',
        orientation: 'portrait'
      }
    }
    // New Promise-based usage:
    await html2pdf().from(text).set(opt).save()
  }

  permissionToEdit = (taskRole) => {
    const { isContractor, isEditor, isBuyer } = this.props

    if (taskRole === 3 || taskRole === 4){
      if (isBuyer || (isContractor && taskRole === 4) || (isEditor && taskRole === 3)) {
        return false
      } else return true
    } else return false
  }

  getModel = () => {
    const { workroom, isContractor, isEditor, info, currentUser } = this.props
    const tabs = []
    const workItemsCount = workroom.workItems.value
      ? workroom.workItems.value.length
      : 1
    const tasks = _.has(workroom.milestone.value, 'productTemplate.templateTask')
      ? workroom.milestone.value.productTemplate.templateTask.sort((sortItemA, sortItemB) => this.sortTask(sortItemA, sortItemB))
      : []
    const currentDate = moment()
    const causeDueDate = _.has(info, 'causeWorkroom.causeDueDate')
      ? moment(info.causeWorkroom.causeDueDate)
      : moment()

    let diffDate = causeDueDate.diff(currentDate, 'days')
    if (diffDate < 0) {
      diffDate = diffDate * -1
    }
    const taskDays = diffDate / workItemsCount

    const children = _.map(workroom.workItems.value, (workItem, i) => {
      const currentTask = tasks[workItem.milestoneStatus] || {}
      const time = currentTask.time || 0
      const days = taskDays / 100 * time
      const taskRole = !_.isNil(currentTask.taskRole)
        ? currentTask.taskRole
        : 3

      const isFinish = workItem.milestoneStatus === tasks.length

      let color = 'border-warning-300'
      if (days > 5) {
        color = 'border-success'
      }
      if (days < 2) {
        color = 'border-danger'
      }

      tabs.push({
        tabChild: (
          <div key={`key_tab_${i}`}>
            {isFinish ? (
              <i
                style={{ marginBottom: 1 }}
                className="mr-5 icon-checkmark2 text-success"
              ></i>
            ) : (
              <span
                style={{ marginBottom: 1 }}
                className={`mr-5 status-mark ${color}`}
              ></span>
            )}
            {`Article ${i + 1}`}
          </div>
        )
      })

      return (
        <div key={`key_tiny_editor_${i}`} style={{marginTop: '-51px'}}>
          <a style={{display: 'block'}} ref={ref => {this.link = ref}}><Fragment> </Fragment></a>
          <div className="pull-right d-flex">
            <div className="mr-15">
              <span>Download </span>
              <i
                className="icon-file-download2 mr-5 cursor-pointer"
                onClick={() => this.download(workItem.value)}
              >
              </i>
            </div>
           
            <div className="bg-slate-300 label">
              <span
                className="token-label token-label-padding"
              >
                {isFinish ? 'Finished' : roles[taskRole].name + ' in progress'}
              </span>
            </div>
          </div>
          <div className="pt-30">
            <TinyEditor
              item={i}
              data={{ ...workItem}}
              milestone={workroom.milestone.value}
              handleChange={content => this.handleChange({ value: content }, i)}
              isChange={!isFinish && (isContractor || isEditor)}
              isEdit={this.permissionToEdit(taskRole)}
              isReply={taskRole === currentUser.permissions}
              conversation={workItem.conversation}
              currentUser={currentUser}
              onUpdateComments={this.onUpdateComments}
            />
          </div>
        </div>
      )
    })


    return { tabs, children }
  }

  generateLables = tasks => {
    return _.map(tasks, taskItem => taskItem.taskName)
  }

  handleChange = (data, i) => {
    const { workroom, change } = this.props
    const nextWorkItems = workroom.workItems.value
      ? workroom.workItems.value.slice()
      : []

    nextWorkItems[i] = {
      ...nextWorkItems[i],
      ...data
    }

    change('workItems', nextWorkItems)
  }

  handleChangeControlItem = (data, i) => {
    const { workroom, change } = this.props
    const nextWorkItems = workroom.workItems.value
      ? workroom.workItems.value.slice()
      : []

    nextWorkItems[i] = {
      ...nextWorkItems[i],
      ...data
    }

    change('workItemsBuyer', nextWorkItems)
  }

  onUpdateComments = (params, cb, cbParam) => {
    this.handleChange({
      ...params,
    }, this.state.currentIndex)

    if(cb) {
      if(cbParam) {
        cb(cbParam)
      } else {
        cb()
      }
    }
  }

  onSubmitToCheck = (params, currentTaskName) => {
    const {
      onSubmit,
      workroom
    } = this.props
    if(this.showConfirm) {
      this.showConfirm = false
      let notice = new PNotify({
        title: 'Submit work for review',
        text: `Are you sure you want to submit your "${currentTaskName}" draft for review? \nTip: Have you addressed all client questions and feedback?`,
        hide: false,
        history: {
          history: false
        },
    
        confirm: {
          confirm: true,
          buttons: [
            {
              text: 'Yes',
              addClass: 'btn btn-sm btn-primary'
            },
            {
              text: 'No',
              addClass: 'btn btn-sm btn-link'
            }
          ]
        },
      })
  
      notice.get().on('pnotify.confirm', () => {
        this.handleChangeControlItem({
          ...workroom.workItems.value[this.state.currentIndex],
          ...params
        },
        this.state.currentIndex
        )
        this.handleChange({
          ...params,
        },
        this.state.currentIndex
        )
        onSubmit()
        this.showConfirm = true
      })
      notice.get().on('pnotify.cancel', () => {
        this.showConfirm = true
      })       
    }
  }

  render() {
    const {
      causeInfo,
      workroom,
      info,
      isContractor,
      isEditor,
      currentUser,
      isShowChat
    } = this.props

    let interlocuter = isContractor
      ? info.buyerWorkroom
      : info.contractorWorkroom
    let owner
    if(isEditor) {
      interlocuter = info.contractorWorkroom
      owner = info.buyerWorkroom    
    }

    const { currentIndex } = this.state
    const model = !_.isNil(causeInfo.causeFormatQty)
      ? this.getModel()
      : {
        tabs: [],
        children: []
      }


    const tasks = _.has(workroom.milestone.value, 'productTemplate.templateTask')
      ? workroom.milestone.value.productTemplate.templateTask
      : []
    const headerLables = _.size(tasks) && this.generateLables(tasks)
    const workItem = workroom.workItems.value[currentIndex]
      ? { ...workroom.workItems.value[currentIndex] }
      : {}

    return (
      <div className="row">
        <div className={`col-md-${isShowChat ? '8' : '12'}`}>
          {!_.isNil(causeInfo.causeFormatQty) ? (
            <TabForm
              header={_.size(tasks)
                ? (
                  <BriefProgressBar
                    itemCount={_.size(tasks)}
                    current={(workItem.milestoneStatus || 0) + 1}
                    stepNames={headerLables}
                  />
                ) : undefined}
              tabs={model.tabs}
              isTop={true}
              selectTab={index => this.setIndex(index)}
            >
              {model.children}
            </TabForm>
          ) : (
            <h4> Milestones have not been selected yet </h4>
          )}
        </div>
        <div className={`col-md-4 ${isShowChat ? '' : 'd-none'}`}>
          {currentUser.id
            && interlocuter 
            && interlocuter.id
            && (
              <Chat
                interlocuter={{ ...interlocuter }}
                owner={owner ? {...owner} : undefined}
                isChatbox={true}
                workroom={{
                  id: workroom.id.value,
                  subject: _.has(info, 'causeWorkroom.causeName')
                    ? info.causeWorkroom.causeName
                    : '',
                  causeId: workroom.causeId.value
                }}
              />
            )}
        </div>
      </div>
    )


  }
}

export default WorkroomForm