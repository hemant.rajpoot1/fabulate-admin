import React, { Component } from 'react'
import _ from 'lodash'
import WorkroomItem from './WorkroomItem'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

class WorkroomForm extends Component {
  constructor(props) {
    super(props)
    const products = _.keys(props.workroom.workroomItems.value)
    this.state = {
      currentIndex: 0, 
      currentProduct: products[0]
    }
    this.showConfirm = true
  }

  componentDidUpdate(prevProps) {
    if(!_.isEqual(prevProps.workroom.workroomItems.value, this.props.workroom.workroomItems.value) && _.isNil(this.state.currentProduct)) {
      const products = _.keys(this.props.workroom.workroomItems.value)
      this.setState({
        currentProduct: products[0]
      })
    }
  }

  setIndex = index => {
    this.setState({
      currentIndex: index
    })
  }

  setCurrentProduct = id => {
    this.setState({
      currentProduct: id,
      currentIndex: 0
    })
  }

  render() {
    const { workroom, isGetIdFetching } = this.props
    const { currentIndex, currentProduct } = this.state

    if(isGetIdFetching && !workroom.workroomItems.value) {
      return (<SpinnerLoadData />)
    }
    
    const productItem = workroom.workroomItems.value 
      ? workroom.workroomItems.value[currentProduct]
      : {}
    return(
      <div>
        <WorkroomItem
          {...this.props}
          productInfo={productItem}
          setIndex={this.setIndex}
          currentIndex={currentIndex}
          currentProduct={currentProduct}
          setCurrentProduct={this.setCurrentProduct}
        />
      </div>
    )
  }
}

export default WorkroomForm