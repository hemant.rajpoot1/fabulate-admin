import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import moment from 'moment'

import TabForm from 'components/form/TabForm'
import PitchForm from './PitchForm'
import BrandItem from './BrandItem'
import SupportingEvidence from './SupportingEvidence'

import './RequestForm.css'

class RequestForm extends Component {

  renderForm = () => {
    const { validation } = this.props
    const tabs = [{
      tabChild: 'Pitch Summary'
    }]
    const children = [
      <PitchForm 
        key="pitchForm"
        validation={validation} 
        isShow={true}
      />]

    if(!_.isNil(validation.brandItem)) {
      for(let i = 0; i < validation.brandItem.length; i++) {
        tabs.push({
          tabChild: `Article ${i + 1}`
        })
        children.push(
          <BrandItem 
            key={`brandItem_${i}`}
            validation={validation} 
            index={i} 
            isShow={true}
          />)
      }
    }

    tabs.push({
      tabChild: 'Supporting Evidence'
    })
    children.push(
      <SupportingEvidence 
        key="supportingEvidence"
        validation={validation} 
        isShow={true}
      />)

    return { tabs, children }
  }

  renderHeader = () => {
    const { validation } = this.props
    return (
      <h6 className="content-group text-semibold">
        <Link to={`/cause-show/${_.has(validation, 'causeInfo.id') && validation.causeInfo.id}`}>
          {_.has(validation, 'causeInfo.causeName') ? validation.causeInfo.causeName : '' }
        </Link>
        <small className="display-block">
          {_.has(validation, 'causeInfo.causeDueDate') && moment(validation.causeInfo.causeDueDate).isValid()
            ? 'Date of completion: ' + moment(validation.causeInfo.causeDueDate).format('YYYY-MM-DD') 
            : '' 
          }
        </small>
      </h6>      
    )
  }

  render() {
    const form = this.renderForm()

    return [
      this.renderHeader(),
      <div className="row">
        <div className="col-md-12">
          <TabForm
            tabs={form.tabs}
          >
            {form.children}
          </TabForm>
        </div>
      </div>      
    ]
  }
}

export default RequestForm