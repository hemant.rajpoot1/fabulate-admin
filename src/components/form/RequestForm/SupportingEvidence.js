import React, { Component } from 'react'

class SupportingEvidence extends Component {
  render() {
    const { 
      validation
    } = this.props

    return (
      <div className="form-group row pl-20">     
        <div className="col-sm-12 col-md-12 label__pad">
          <label>CREATE CONTENT LIKE
          </label>
          <textarea
            className="form-control"
            rows="5"
            name="contentLike"
            value={validation.contentLike || ''}
            readOnly={true}
          />
        </div>  

        <div className="col-sm-12 col-md-12 label__pad">
          <label>PREVIOUS CONTENT
          </label>
          <textarea
            className="form-control"
            rows="5"
            name="previousContent"
            value={validation.previousContent || ''}
            readOnly={true}
          />
        </div>                      
      </div>
    )
  }
}

export default SupportingEvidence