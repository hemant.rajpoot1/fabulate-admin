import React, { Component } from 'react'
import _ from 'lodash'

const brandItemModel = {
  headline: '',
  outline: '',
  brandIntegration: '',
} 

class BrandItem extends Component {
  render() {
    const { 
      validation,
      index
    } = this.props
    const brandItemByIndex = !_.isNil(validation.brandItem)
      ? validation.brandItem[index]
      : {} 

    const brandItem = {
      ...brandItemModel,
      ...brandItemByIndex
    }       

    return (
      <div className="form-group row pl-20">
        <div className="col-sm-12 col-md-12 label__pad">
          <label>HEADLINE
            {/* <i className="icon-question4"></i> */}
          </label>
          <input
            className="form-control"
            type="text"
            name="headline"
            value={brandItem.headline || ''}
            readOnly={true}
          />
        </div>      

        <div className="col-sm-12 col-md-12 label__pad">
          <label>OUTLINE
            {/* <i className="icon-question4"></i> */}
          </label>
          <textarea
            className="form-control"
            rows="3"
            name="outline"
            value={brandItem.outline || ''}
            readOnly={true}
          />           
        </div>  

        <div className="col-sm-12 col-md-12 label__pad">
          <label>BRAND INTEGRATION
            {/* <i className="icon-question4"></i> */}
          </label>
          <textarea
            className="form-control"
            rows="5"
            name="brandIntegration"
            value={brandItem.brandIntegration || ''}
            readOnly={true}
          />           
        </div>                      
      </div>
    )
  }
}

export default BrandItem