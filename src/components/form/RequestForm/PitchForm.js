import React, { Component } from 'react'

class PitchForm extends Component {
  render() {
    const { validation } = this.props

    return (
      <div className="form-group row pl-20">
        <div className="col-sm-12 col-md-12 label__pad">
          <label>PITCH TITLE/TAGLINE
          </label>
          <input
            className="form-control"
            type="text"
            name="tagline"
            value={validation.tagline|| ''}
            readOnly={true}
          />
        </div>      

        <div className="col-sm-12 col-md-12 label__pad">
          <label>OUTLINE
          </label>
          <textarea
            className="form-control"
            rows="3"
            name="outline"
            value={validation.outline|| ''}
            readOnly={true}            
          />
        </div>  

        <div className="col-sm-12 col-md-12 label__pad">
          <label>BRAND INTEGRATION
          </label>
          <textarea
            className="form-control"
            rows="5"
            name="brandIntegration"
            value={validation.brandIntegration|| ''}
            readOnly={true}
          />
        </div>                      
      </div>
    )
  }
}

export default PitchForm