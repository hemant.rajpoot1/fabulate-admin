import RequestForm from './RequestForm'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import _ from 'lodash'

import * as causeUserActions from 'store/actions/causeUser'
import * as causeActions from 'store/actions/cause'

class RequestFormContainer extends Component {

  componentDidMount(){
    const { causeUserActions, id } = this.props
    causeUserActions.getId(id)
  }

  render() {
    return(
      <RequestForm 
        { ...this.props } 
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  let id = props.match.params.id
  let causeInfo = props.causeInfo
  if(!_.size(causeInfo)) {
    causeInfo = {
      ...state.causeUser.edit.causeInfo
    }
  }
  const contractorCauseUser = {
    ...state.causeUser.edit.contractorCauseUser
  }
  const buyerCauseUser = {
    ...state.causeUser.edit.buyerCauseUser
  }  

  const isShowMode = props.match.path === '/pitch/:id'
  return {
    requestContractor: state.cause.requestContractor,  
    validation: state.causeUser.edit,
    id,
    causeInfo,
    isShowMode,
    currentUser,
    contractorCauseUser,
    buyerCauseUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    causeActions: bindActionCreators(causeActions, dispatch),
    causeUserActions: bindActionCreators(causeUserActions, dispatch),       
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RequestFormContainer))
