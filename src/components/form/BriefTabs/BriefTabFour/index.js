import React, { Component } from 'react'

import Slider from 'rc-slider'
import _ from 'lodash'

import CollapseButton from 'components/ui/CollapseButton'
import Radiobutton from 'components/ui/Radiobutton'
import Select from 'components/ui/Select'
import TextArea from '../TextArea'

import { causeBiline, selectFieldBriefFormOne } from 'helpers/params'
import { generatingObjectFromArray } from 'helpers/tools'

const arrCause = [
  'causeContentShould', 'causeContentNever',
  'causeContentTraditional', 'causeContentMature',
  'causeContentFeminine', 'causeContentPlayful'
]

class BriefTabFour extends Component {
  constructor(props) {
    super(props)
    this.state = { isEdit: false }
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  updateInitModel = () => {
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  handleOnRadio = (field, item) => {
    this.props.change(field, !_.isNil(item) ? item : null)
  }

  handleUpdate = () => {
    const { onSubmit } = this.props
    this.setState({ isEdit: false })
    this.updateInitModel()
    onSubmit()
  }

  resetModel = () => {
    this.setState({ isEdit: !this.state.isEdit })
    if (this.state.isEdit) {
      _.forEach(arrCause, item => {
        this.props.change(item, this.initValue[item])
      })
    }
  }

  buttonUpdate = () => (
    <div className="form__brief-btn tab__brief-btn">
      <button
        className="w-auto btn btn-primary legitRipple button_length"
        onClick={this.handleUpdate}
      >
        update brief
      </button>
      <button className="w-auto btn btn-default legitRipple button_length" onClick={() => this.resetModel()}>cancel</button>
    </div>
  )

  renderSlide = (nameLeft, nameRight, validationtype) => {
    const { validation, change } = this.props
    return (
      <div>
        <div className="slider__wrapper">
          <span>{nameLeft}</span>
          <span>{nameRight}</span>
        </div>
        <div>
          <Slider
            className="SliderTest"
            min={0}
            max={100}
            step={1}
            disabled={!this.state.isEdit}
            onChange={e => change(validationtype, e)}
            value={validation[validationtype].value || 50}
          />
        </div>
      </div>
    )
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, _.get(item, 'value', null))
  }


  render() {
    const { isBlockEdit, validation, change } = this.props
    return (
      <React.Fragment>
        <CollapseButton
          title="TONE, VOICE AND STYLE"
          isBlockEdit={isBlockEdit}
          resetModel={this.resetModel}
          isEdit={this.state.isEdit}
          currentTab="tab-4"
        />
        <div className="collapse in" id="collapse-link-tab-4" aria-expanded="true">
          <div className="row">
            <div className="col-md-3 mt-10 text-semibold">Creator By Line required? </div>
            {
              (this.state.isEdit) ? (
                <div className="text-box mt-30">
                  {_.map(causeBiline, (bilineItem, bilineIndex) => (
                    <div key={`radio_biline_index_${bilineItem}`}>
                      <label className="check__label ml-10">
                        <Radiobutton
                          name={bilineItem}
                          checked={validation.causeBiline.value.toString() === bilineIndex}
                          onCheck={() => {
                            this.handleOnRadio('causeBiline', bilineIndex)
                          }}
                        />
                        <span
                          style={{
                            margin: '9px 0px 0px 8px'
                          }}
                        >
                          {bilineItem}
                        </span>
                      </label>
                    </div>
                  ))}
                </div>
              ) : (
                <div className="col-md-3 mt-10 mb-10">
                  {validation.causeBiline.value ? 'Yes' : 'No'}
                </div>
              )
            }
          </div>
          {
            (!_.isNil(validation.causeWriter.value) || this.state.isEdit) && (
              <div className="row">
                <div className="col-md-3 mt-10 text-semibold">Will this content be published as an article or ghost written by someone else? </div>
                {(this.state.isEdit) ? (
                  <div className="col-md-12">
                    <Select
                      isSearch={false}
                      onSelect={value => {
                        this.handleOnSelect('causeWriter', value)
                      }}
                      items={selectFieldBriefFormOne['causeWriter']}
                      value={
                        _.get(selectFieldBriefFormOne, `[causeWriter]${[validation.causeWriter.value]}.name`)
                      }
                    />
                  </div>
                ) : (
                  <div className="col-md-3 mt-10 mb-10">
                    {_.get(selectFieldBriefFormOne, `[causeWriter]${[validation.causeWriter.value]}.name`)}
                  </div>
                )}
              </div>
            )
          }
          <TextArea
            validation={validation}
            name="Content should always"
            validationType="causeContentShould"
            isEdit={this.state.isEdit}
            change={change}
          />
          <TextArea
            validation={validation}
            name="Content should never"
            validationType="causeContentNever"
            isEdit={this.state.isEdit}
            change={change}
          />
          <div className="row mb-20">
            <div className="text-semibold col-md-3 mt-10">Brand and content attributes:</div>
            <div className="col-md-8">
              {this.renderSlide('Traditional', 'Modern', 'causeContentTraditional')}
              {this.renderSlide('Mature', 'Youthful', 'causeContentMature')}
              {this.renderSlide('Feminine', 'Masculine', 'causeContentFeminine')}
              {this.renderSlide('Playful', 'Sophisticated', 'causeContentPlayful')}
            </div>
          </div>
          {this.state.isEdit && this.buttonUpdate()}
        </div>
      </React.Fragment>
    )
  }
}

export default BriefTabFour
