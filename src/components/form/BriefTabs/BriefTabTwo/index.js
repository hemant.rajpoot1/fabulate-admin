import React, { Component } from 'react'

import Select from 'components/ui/Select'
import Checkbox from 'components/ui/Checkbox'
import Radiobutton from 'components/ui/Radiobutton'
import CollapseButton from 'components/ui/CollapseButton'
import TextArea from '../TextArea'

import { selectFieldBriefFormOne } from 'helpers/params'
import { generatingObjectFromArray } from 'helpers/tools'
import _ from 'lodash'
import classNames from 'classnames'

const arrCause = [
  'causeContent', 'causeWorkspaceObjective',
  'causeMarketing', 'causePurpose',
  'causeDwellTime', 'causePageImpressions',
  'causeTargetNewCustomer', 'causeIncrease',
  'causeEngagement', 'causeBrandConsideration',
  'causeLaunchProduct', 'causeSuccessOther'
]

class BriefTabTwo extends Component {
  constructor(props) {
    super(props)
    this.state = { isEdit: false }
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  updateInitModel = () => {
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  resetModel = () => {
    this.setState({ isEdit: !this.state.isEdit })
    if (this.state.isEdit) {
      _.forEach(arrCause, item => {
        this.props.change(item, this.initValue[item])
      })
    }
  }

  handleChange({ target }) {
    this.props.change(target.name, target.value)
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : null)
  }

  handleUpdate = () => {
    const { onSubmit } = this.props
    this.setState({ isEdit: false })
    this.updateInitModel()
    onSubmit()
  }

  renderCheckbox = (name, validationtype) => {
    const { validation } = this.props
    if (validation[validationtype].value || this.state.isEdit) {
      return (
        (this.state.isEdit) ? (
          <div className="checkbox">
            <label className="check__label">
              <Checkbox
                name={validationtype}
                checked={validation[validationtype].value}
                onCheck={name => {
                  this.props.change(validationtype, !validation[validationtype].value)
                }}
              />
              {name}
            </label>
          </div>
        ) :
          <div className="col-md-2">
            {name}
          </div>
      )
    }
    return null
  }

  buttonUpdate = () => (
    <div className="form__brief-btn tab__brief-btn">
      <button
        className="w-auto btn btn-primary legitRipple button_length"
        onClick={this.handleUpdate}
      >
        update brief
      </button>
      <button className="w-auto btn btn-default legitRipple button_length" onClick={() => this.resetModel()}>cancel</button>
    </div>
  )

  render() {
    const { validation, isBlockEdit, change } = this.props
    const leastOneOf = ((validation.causeDwellTime.value || validation.causePageImpressions.value
      || validation.causeTargetNewCustomer.value || validation.causeIncrease.value
      || validation.causeEngagement.value || validation.causeBrandConsideration.value
      || validation.causeLaunchProduct.value || validation.causeSuccessOther.value
    ) || this.state.isEdit)
    const checkboxDivClass = classNames({
      'mt-10': true,
      'mt-30': this.state.isEdit,
      'ml-10': this.state.isEdit,
      'pt-5': this.state.isEdit
    })
    return (
      <React.Fragment>
        <CollapseButton
          title="THE CONTENT"
          isBlockEdit={isBlockEdit}
          resetModel={this.resetModel}
          isEdit={this.state.isEdit}
          currentTab="tab-2"
        />
        <div className="collapse in" id="collapse-link-tab-2" aria-expanded="true">
          {(selectFieldBriefFormOne['causeContentType'][validation.causeContentType.value] || this.state.isEdit) && (
            <div className="row">
              <div className="col-md-3 mt-10 text-semibold">Content Type:</div>
              <div className="col-md-8">
                <Select
                  readOnly={!this.state.isEdit}
                  isSearch={false}
                  onSelect={value => {
                    this.handleOnSelect('causeContentType', value)
                  }}
                  items={selectFieldBriefFormOne['causeContentType']}
                  value={selectFieldBriefFormOne['causeContentType'][validation.causeContentType.value]
                    ? selectFieldBriefFormOne['causeContentType'][validation.causeContentType.value].name
                    : undefined
                  }
                />
              </div>
            </div>
          )}

          <TextArea
            validation={validation}
            name="Campaign Objectives"
            validationType="causeWorkspaceObjective"
            isEdit={this.state.isEdit}
            change={change}
          />

          {(selectFieldBriefFormOne['causeMarketing'][validation.causeMarketing.value] || this.state.isEdit) && (
            <div className="row">
              <div className="col-md-3 mt-10 text-semibold">Marketing Objectives:</div>
              {
                (this.state.isEdit) ? (
                  <div className="text-box mt-30">
                    {_.map(selectFieldBriefFormOne['causeMarketing'], radioItem => (
                      <div key={`radio_marketiong_index_${radioItem.value}`}>
                        <label className="check__label ml-10">
                          <Radiobutton
                            name={radioItem.name}
                            checked={validation.causeMarketing.value === radioItem.value}
                            onCheck={name => {
                              this.handleOnSelect('causeMarketing', radioItem)
                            }}
                          />
                          <span
                            style={{
                              margin: '9px 0px 0px 8px'
                            }}
                          >
                            {radioItem.name}
                          </span>
                        </label>
                      </div>
                    ))}
                  </div>
                ) :
                  <div className="col-md-3 mt-10 mb-10">
                    {selectFieldBriefFormOne['causeMarketing'][validation.causeMarketing.value].name}
                  </div>
              }
            </div>
          )}

          <TextArea
            validation={validation}
            name="Content Idea"
            validationType="causeContentIdea"
            isEdit={this.state.isEdit}
            change={change}
          />

          <TextArea
            validation={validation}
            name="Content purpose"
            validationType="causePurpose"
            isEdit={this.state.isEdit}
            change={change}
          />

          {leastOneOf && (
            <div className="row mb-20">
              <div className="col-md-3 text-semibold mt-10">Content objectives:</div>
              <div className={checkboxDivClass}>
                {this.renderCheckbox('Dwell time', 'causeDwellTime')}
                {this.renderCheckbox('Page impressions', 'causePageImpressions')}
                {this.renderCheckbox('Target new customers', 'causeTargetNewCustomer')}
                {this.renderCheckbox('Increase sales', 'causeIncrease')}
                {this.renderCheckbox('Engagement', 'causeEngagement')}
                {this.renderCheckbox('Brand consideration', 'causeBrandConsideration')}
                {this.renderCheckbox('Launch product', 'causeLaunchProduct')}
              </div>
              {(validation.causeSuccessOther.value || this.state.isEdit) && (
                <div className="other">
                  <textarea
                    readOnly={!this.state.isEdit}
                    className="form-control ml-10"
                    rows="5"
                    type="text"
                    placeholder="other"
                    name="causeSuccessOther"
                    onChange={e => this.handleChange(e)}
                    value={validation.causeSuccessOther.value || ''}
                  />
                </div>
              )}
            </div>
          )}
          {this.state.isEdit && this.buttonUpdate()}
        </div>
      </React.Fragment>
    )
  }
}

export default BriefTabTwo
