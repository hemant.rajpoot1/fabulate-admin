import React from 'react'
import propTypes from 'prop-types'

class TextArea extends React.Component {

  handleChange = ({ target }) => {
    this.props.change(target.name, target.value)
  }

  render() {
    const { validation, name, validationType, isEdit } = this.props
    if (!isEdit && validation[validationType].value) {
      return (
        <div className="row mb-10">
          <div className="col-md-3 text-semibold mt-10">{name}:</div>
          <p className="col-md-8 mt-10 plain-text">
            {validation[validationType].value}
          </p>
        </div>
      )
    }
    
    if (isEdit) {
      return (
        <div className="row mb-10">
          <div className="col-md-3 text-semibold">{name}:</div>
          <textarea
            className="form-control ml-10"
            rows="5"
            name={validationType}
            onChange={e => this.handleChange(e)}
            value={validation[validationType].value || ''}
          />
        </div>
      )
    }
    return null
  }
}

TextArea.propTypes = {
  validation: propTypes.object.isRequired,
  name: propTypes.string.isRequired,
  validationType: propTypes.string.isRequired,
  isEdit: propTypes.bool.isRequired,
  change: propTypes.func.isRequired,
}

export default TextArea

