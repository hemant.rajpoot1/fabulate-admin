import React, { Component } from 'react'

import { selectFieldBriefFormOne } from 'helpers/params'
import { generatingObjectFromArray } from 'helpers/tools'
import Select from 'components/ui/Select'
import _ from 'lodash'

import CollapseButton from 'components/ui/CollapseButton'

const arrCause=['causeBrand', 'causeBrandURL', 'causeIndustry']

class BriefTabOne extends Component {
  constructor(props) {
    super(props)
    this.state = { isEdit: false }
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  resetModel = () => {
    this.setState({ isEdit: !this.state.isEdit })
    if (this.state.isEdit) {
      _.forEach(arrCause, item => {
        this.props.change(item, this.initValue[item])
      })
    }
  }

  updateInitModel = () => {
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  handleChange({ target }) {
    this.props.change(target.name, target.value)
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : null)
  }

  handleUpdate = () => {
    const { onSubmit } = this.props
    this.setState({ isEdit: false })
    this.updateInitModel()
    onSubmit()
  }

  renderInput = (name, validationtype) => {
    const { validation } = this.props
    if (validation[validationtype].value || this.state.isEdit) {
      return (
        <div className="row">
          <div className="col-md-3 mt-10 text-semibold">
            {name}:
          </div>
          <div className="col-md-8">
            <input
              className="form-control"
              type="text"
              name={validationtype}
              onChange={e => this.handleChange(e)}
              value={validation[validationtype].value || ''}
              readOnly={!this.state.isEdit}
            />
          </div>
        </div>
      )}
    return null
  }

  buttonUpdate = () => (
    <div className="form__brief-btn tab__brief-btn">
      <button
        className="w-auto btn btn-primary legitRipple button_length"
        onClick={this.handleUpdate}
      >
        update brief
      </button>
      <button
        className="w-auto btn btn-default legitRipple button_length"
        onClick={() => this.resetModel()}
      >
        cancel
      </button>
    </div>
  )

  render() {
    const { validation, isBlockEdit } = this.props
    return (
      <React.Fragment>
        <CollapseButton
          title="ABOUT THE BRAND"
          isBlockEdit={isBlockEdit}
          resetModel={this.resetModel}
          isEdit={this.state.isEdit}
          currentTab="tab-1"
        />
        <div className="collapse in" id="collapse-link-tab-1" aria-expanded="true">
          {this.renderInput('Brand Name', 'causeBrand')}
          {this.renderInput('Brand URL', 'causeBrandURL')}

          {(selectFieldBriefFormOne['causeIndustry'][validation.causeIndustry.value] || this.state.isEdit) && (
            <div className="row mb-10">
              <div className="col-md-3 mt-10 text-semibold">Brand Industry:</div>
              <div className="col-md-8">
                <Select
                  readOnly={!this.state.isEdit}
                  isSearch={false}
                  onSelect={value => {
                    this.handleOnSelect('causeIndustry', value)
                  }}
                  items={selectFieldBriefFormOne['causeIndustry']}
                  value={_.get(selectFieldBriefFormOne, `causeIndustry.[${validation.causeIndustry.value}].name`, undefined)}
                />
              </div>
            </div>
          )}
          {this.state.isEdit && this.buttonUpdate()}
        </div>
      </React.Fragment>
    )
  }
}

export default BriefTabOne
