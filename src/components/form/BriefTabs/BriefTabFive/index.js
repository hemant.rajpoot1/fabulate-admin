import React, { Component, Fragment } from 'react'

import FileLoad from 'components/ui/File'
import Dropzone from 'react-dropzone'
import CollapseButton from 'components/ui/CollapseButton'

import { generatingObjectFromArray } from 'helpers/tools'
import _ from 'lodash'
import TextArea from '../TextArea'

const arrCause=['causeExitingResources', 'causeFavoriteExamples']

class BriefTabFive extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isEdit: false,
      attachedFiles: this.props.attachments
    }
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  componentDidMount() {
    this.setState({attachedFiles: this.props.attachments})
  }

  resetModel = () => {
    this.setState({ isEdit: !this.state.isEdit })
    if (this.state.isEdit) {
      this.props.causeActions.resetAttachChanges(this.state.attachedFiles)
      _.forEach(arrCause, item => {
        this.props.change(item, this.initValue[item])
      })
    }
  }

  renderInput = (name, validationtype) => {
    const { validation, change } = this.props
    if (validation[validationtype].value || this.state.isEdit) {
      return (
        <div className="row mb-10">
          <div className="col-md-3 mt-10 text-semibold">{name}:</div>
          {this.state.isEdit ? (
            <div className="col-md-8">
              <input
                className="form-control"
                type="text"
                name={validationtype}
                onChange={({target}) => change(target.name, target.value)}
                value={validation[validationtype].value || ''}
              />
            </div>
          ) : (
            <div className="col-md-8 mb-20">
              {validation[validationtype].value || ''}
            </div>
          )}
        </div>
      )
    }
    return null
  }

  drop = (files, fieldType) => {
    const { causeActions, isCreate } = this.props
    const isFiles = _.map(files, file => { file.fieldType = fieldType; return file })
    if (files && files[0]) {
      causeActions.attachFile(isFiles, isCreate)
    }
  }

  uploadFile = (uri, preview, fileName, fieldType) => {
    const { causeActions, isCreate } = this.props
    causeActions.uploadFile({ uri, preview, fileName, fieldType }, isCreate)
  }

  removeAttach = file => {
    const { causeActions, isCreate } = this.props
    causeActions.removeAttach(file.preview || file.id, isCreate)
  }

  handleUpdate = () => {
    const { onSubmit } = this.props
    this.setState({ isEdit: false })
    this.updateInitModel()
    onSubmit()
  }

  updateInitModel = () => {
    const { validation } = this.props
    this.initValue = {
      causeExitingResources: validation.causeExitingResources.value,
      causeFavoriteExamples: validation.causeFavoriteExamples.value
    }
  }

  renderFileDropzone = (value, typeValue) => {
    const { download, isBlockFiles } = this.props
    if ((value && value.length > 0) || this.state.isEdit) {
      return (
        <div className="row mb-10">
          {(this.state.isEdit) ? (
            <Dropzone
              onDrop={files => this.drop(files, typeValue)}
              accept=".doc, .docx, .ppt, .pdf"
              className="dropzone-asset"
            >
              <form className="dropzone dz-clickable mb-12" id="dropzone_multiple">
                {value.length > 0 ? (
                  <div className="tokenfield">
                    {_.map(value, (item, i) => (
                      <FileLoad
                        key={item.preview + i}
                        file={item}
                        uploadFile={this.uploadFile}
                        removeAttach={this.removeAttach}
                        type={typeValue}
                      />
                    ))}
                  </div>
                ) : (
                  <div className="dz-default dz-message">
                    <span>
                      Click or drag and drop to attach images or documents.
                    </span>
                  </div>
                )}
              </form>
            </Dropzone>
          ) : (
            <React.Fragment>
              <div className="col-md-3" />
              {_.map(value, (item, i) => (
                <span className="ml-10" key={`show_attach_${i}`}>
                  <a onClick={() => !isBlockFiles && download(item.storeId, this.assetDownload)} className="legitRipple">
                    <i className="icon-file-text3 icon-margin"></i>
                    <span className="mr-10">{item.mediaName || item.name}</span>
                  </a>
                </span>
              ))}
            </React.Fragment>
          )}
        </div>
      )}
    return null
  }

  buttonUpdate = () => (
    <div className="form__brief-btn tab__brief-btn">
      <button
        className="w-auto btn btn-primary legitRipple button_length"
        onClick={this.handleUpdate}
      >
        update brief
      </button>
      <button className="w-auto btn btn-default legitRipple button_length" onClick={() => this.resetModel()}>cancel</button>
    </div>
  )

  render() {
    const { attachments, isBlockEdit, validation, change } = this.props
    const existing = []
    const favourite = []
    const brandGuidelines = []

    _.forEach(attachments, attachItem => {
      switch (attachItem.fieldType) {
      case 1: {
        favourite.push(attachItem)
        break
      }
      case 2: {
        brandGuidelines.push(attachItem)
        break
      }
      default: {
        existing.push(attachItem)
        break
      }
      }
    })

    return (
      <React.Fragment>
        <CollapseButton
          title="REFERENCES"
          isBlockEdit={isBlockEdit}
          resetModel={this.resetModel}
          isEdit={this.state.isEdit}
          currentTab="tab-5"
        />
        <div className="collapse in" id="collapse-link-tab-5" aria-expanded="true">
          <a className="downloadLink" ref={ref => { this.assetDownload = ref }}><Fragment> </Fragment></a>
          {this.renderInput('Required SEO terms', 'causeSeo')}
          {this.renderInput('Further URLs for reference', 'causeFurtherUrl')}
          <TextArea
            validation={validation}
            name="Existing resources"
            validationType="causeExitingResources"
            isEdit={this.state.isEdit}
            change={change}
          />
          {this.renderFileDropzone(existing, 0)}
          <TextArea
            validation={validation}
            name="Brand guidelines/style guide"
            validationType="causeBrandGuidelines"
            isEdit={this.state.isEdit}
            change={change}
          />
          {this.renderFileDropzone(brandGuidelines, 2)}
          <TextArea
            validation={validation}
            name="Favourite examples of content"
            validationType="causeFavoriteExamples"
            isEdit={this.state.isEdit}
            change={change}
          />
          {this.renderFileDropzone(favourite, 1)}
          {this.state.isEdit && this.buttonUpdate()}
        </div>
      </React.Fragment>
    )
  }
}

export default BriefTabFive
