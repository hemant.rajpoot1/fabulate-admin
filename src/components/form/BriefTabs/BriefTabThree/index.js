import React, { Component } from 'react'

import Checkbox from 'components/ui/Checkbox'
import Select from 'components/ui/Select'
import CollapseButton from 'components/ui/CollapseButton'

import { selectFieldBriefFormOne } from 'helpers/params'
import { generatingObjectFromArray } from 'helpers/tools'
import 'rc-slider/assets/index.css'
import _ from 'lodash'
import TextArea from '../TextArea'

const arrCause = [
  'causeAudienceDetail', 'causeAudienceFeel',
  'causeAudienceKeyMessage', 'causeAudienceGender',
  'causeAudienceAge', 'causeAudienceLocationCountry',
  'causeAudienceLocation'
]


class BriefTabThree extends Component {
  constructor(props) {
    super(props)
    this.state = { isEdit: false }
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  resetModel = () => {
    this.setState({ isEdit: !this.state.isEdit })
    if (this.state.isEdit) {
      _.forEach(arrCause, item => {
        this.props.change(item, this.initValue[item])
      })
    }
  }

  updateInitModel = () => {
    const { validation } = this.props
    this.initValue = generatingObjectFromArray(arrCause, validation)
  }

  onCheck = (fieldName, value) => {
    const { validation, change } = this.props
    const nextFilterSet = new Set(validation[fieldName].value)

    if (nextFilterSet.has(value)) {
      nextFilterSet.delete(value)
    } else {
      nextFilterSet.add(value)
    }

    change(fieldName, Array.from(nextFilterSet))
  }

  handleOnSelect = (field, item) => {
    this.props.change(field, item ? item.value : 12)
  }

  handleUpdate = () => {
    const { onSubmit } = this.props
    this.setState({ isEdit: false })
    this.updateInitModel()
    onSubmit()
  }

  renderCheckbox = (name, validationtype) => {
    const { validation } = this.props
    if (this.state.isEdit) {
      return (
        <div>
          <span className="text-semibold">{name}:</span>
          <div className="flex-column">
            {_.map(selectFieldBriefFormOne[validationtype], Item => (
              <div className="checkbox" key={Item.name}>
                <label className="check__label">
                  <Checkbox
                    name="causeAudienceAge"
                    checked={_.includes(validation[validationtype].value, Item.value)}
                    onCheck={name => {
                      this.onCheck(validationtype, Item.value)
                    }}
                  />
                  {Item.name}
                </label>
              </div>
            ))}
          </div>
        </div>
      )
    }
    if (!this.state.isEdit && validation[validationtype].value
      && !!_.size(validation[validationtype].value)) {
      return (
        <div className="row mb-10">
          <span className="text-semibold col-md-3 mt-10">{name}:</span>
          {_.map(selectFieldBriefFormOne[validationtype], Item => {
            const isCheck = _.includes(validation[validationtype].value, Item.value)

            return isCheck ? (
              <div className="col-md-1 mt-10" key={Item.name}>
                {Item.name}
              </div>
            ) : null
          })}
        </div>
      )
    }
    return null
  }

  buttonUpdate = () => (
    <div className="form__brief-btn tab__brief-btn">
      <button
        className="w-auto btn btn-primary legitRipple button_length"
        onClick={this.handleUpdate}
      >
        update brief
      </button>
      <button className="w-auto btn btn-default legitRipple button_length" onClick={() => this.resetModel()}>cancel</button>
    </div>
  )

  render() {
    const { validation, isBlockEdit, change } = this.props
    return (
      <React.Fragment>
        <CollapseButton
          title="TARGET AUDIENCE"
          isBlockEdit={isBlockEdit}
          resetModel={this.resetModel}
          isEdit={this.state.isEdit}
          currentTab="tab-3"
        />
        <div className="collapse in" id="collapse-link-tab-3" aria-expanded="true">
          {this.renderCheckbox('Genre', 'causeAudienceGender')}
          {this.renderCheckbox('Age', 'causeAudienceAge')}

          {(this.state.isEdit) ? (
            <div>
              <Select
                isSearch={false}
                placeholder={selectFieldBriefFormOne['causeAudienceLocationCountry'][0].name}
                onSelect={value => {
                  this.handleOnSelect('causeAudienceLocationCountry', value)
                }}
                items={selectFieldBriefFormOne['causeAudienceLocationCountry']}
                value={selectFieldBriefFormOne['causeAudienceLocationCountry'][validation.causeAudienceLocationCountry.value]
                  ? selectFieldBriefFormOne['causeAudienceLocationCountry'][validation.causeAudienceLocationCountry.value].name
                  : undefined
                }
              />
              {validation.causeAudienceLocationCountry.value === 12 && (
                <div className="row">
                  <div className="col-md-6">
                    {_.map(_.take(selectFieldBriefFormOne['causeAudienceLocation'], 4), locationItem => (
                      <div className="checkbox" key={locationItem.name}>
                        <label className="check__label">
                          <Checkbox
                            name="causeAudienceLocation"
                            checked={
                              validation.causeAudienceLocation.value
                                ? (_.findIndex(validation.causeAudienceLocation.value, item => item === locationItem.value) !== -1)
                                : false
                            }
                            onCheck={name => {
                              this.onCheck('causeAudienceLocation', locationItem.value)
                            }}
                          />
                          {locationItem.name}
                        </label>
                      </div>
                    ))}
                  </div>
                  <div className="col-md-6">
                    {_.map(_.takeRight(selectFieldBriefFormOne['causeAudienceLocation'], 4), locationItem => (
                      <div className="checkbox" key={locationItem.name}>
                        <label className="check__label">
                          <Checkbox
                            name="causeAudienceLocation"
                            checked={
                              validation.causeAudienceLocation.value
                                ? (_.findIndex(validation.causeAudienceLocation.value, item => item === locationItem.value) !== -1)
                                : false
                            }
                            onCheck={name => {
                              this.onCheck('causeAudienceLocation', locationItem.value)
                            }}
                          />
                          {locationItem.name}
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>
          ) : (
            <React.Fragment>
              <div className="row">
                <div className="text-semibold mt-10 col-md-3">
                    Country:
                </div>
                <div className="col-md-3 mt-10">
                  <label className="check__label">
                    {selectFieldBriefFormOne['causeAudienceLocationCountry'][validation.causeAudienceLocationCountry.value]
                      ? selectFieldBriefFormOne['causeAudienceLocationCountry'][validation.causeAudienceLocationCountry.value].name
                      : undefined}
                  </label>
                </div>
              </div>

              <div className='row mb-10'>
                {(validation.causeAudienceLocationCountry.value === 12 && _.size(validation.causeAudienceLocation.value))
                  ? (
                    <React.Fragment>
                      <div className="text-semibold mt-10 col-md-3">
                        Location:
                      </div>
                      {_.map(selectFieldBriefFormOne['causeAudienceLocation'], locationItem => {
                        const isCheck = validation.causeAudienceLocation.value
                          ? (_.findIndex(validation.causeAudienceLocation.value, item => item === locationItem.value) !== -1)
                          : false
                        return isCheck && (
                          <div className="col-md-1 mt-10" key={locationItem.name}>
                            {locationItem.name}
                          </div>
                        )
                      })}
                    </React.Fragment>
                  )
                  : null
                }
              </div>
            </React.Fragment>
          )}
          <TextArea
            validation={validation}
            name="Other audience details"
            validationType="causeAudienceDetail"
            isEdit={this.state.isEdit}
            change={change}
          />
          <TextArea
            validation={validation}
            name="How should the audience feel about the content"
            validationType="causeAudienceFeel"
            isEdit={this.state.isEdit}
            change={change}
          />
          <TextArea
            validation={validation}
            name="Key message for the target audience"
            validationType="causeAudienceKeyMessage"
            isEdit={this.state.isEdit}
            change={change}
          />
          <TextArea
            validation={validation}
            name="Published destination"
            validationType="causePublishedDestination"
            isEdit={this.state.isEdit}
            change={change}
          />
          {this.state.isEdit && this.buttonUpdate()}
        </div>
      </React.Fragment>
    )
  }
}

export default BriefTabThree
