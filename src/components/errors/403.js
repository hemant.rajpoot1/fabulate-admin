import React, { Component } from 'react'

import { Link } from 'react-router-dom'

class AppError extends Component {
  render() {

    return (
      <div className="text-center content-group">
        <h1 className="error-title">403</h1>
        <h5>Oops, an error has occurred. Forbidden!</h5>
        <Link to="/" className="btn bg-pink-400">
          <i className="icon-circle-left2 position-left"></i> Back to dashboard
        </Link>
      </div>
    )
  }
}

export default AppError
