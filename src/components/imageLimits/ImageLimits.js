import React, { Component } from 'react'

const errStyle = {
  'height': '20px',
  'color': 'red',
}

class ImageLimits extends Component {
  render() {
    const { 
      change, 
      onSubmit, 
      limit, 
      validError 
    } = this.props
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Set a limit on the number of used Getty images</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">
            <div className="form-group">
              <label className="col-lg-3 control-label">Images limit</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="skillName"
                  value={limit}
                  onChange={change}
                />            
              </div>
            </div>                                     
          </div>
          <div style={errStyle}>{validError}</div>
          <div className="text-right">
            <button className="btn btn-primary" onClick={onSubmit} >set limit</button>
          </div>
        </div>

      </div>
    )
  }
}

export default ImageLimits
