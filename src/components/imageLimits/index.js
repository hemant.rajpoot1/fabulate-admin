import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as gettyActions from 'store/actions/gettyLimit'
import { getField } from 'store/selectors/modelSelectors'

import ImageLimits from './ImageLimits'


class ImageLimitsContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      limit: '',
      validError: '',
      limitWasUpdate: true,
    }
    this.handleChange = this.handleChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    const {
      gettyActions,
    } = this.props

    gettyActions.getGettyLimit()
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.gettyLimit !== prevState.limit && prevState.limitWasUpdate) {
      return {
        limit: nextProps.gettyLimit,
        limitWasUpdate: false,
      }
    }
  }

  handleChange({target}) {
    if (/^\d*$/.test(Number(target.value))) {
      this.setState({
        validError: '',
        limit: target.value,
      })
    }
  }

  onSubmit() {
    const {
      limit,
    } = this.state
    const {
      gettyActions,
    } = this.props
    if (Number(limit) > 0) {
      gettyActions.setGettyLimit(limit)
    } else {
      this.setState({
        validError: 'Value must be a positive number',
      })
    }
  }

  render() {
    const {
      handleChange,
      onSubmit,
      props,
    } = this
    const {
      limit,
      validError,
    } = this.state
 
    return (
      <ImageLimits
        {...props}
        change={handleChange}
        onSubmit={onSubmit}
        limit={limit}
        validError={validError}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.skill.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  return {
    validation: state.skill.validation,
    currentUser,
    validRules,
    gettyLimit: state.getty.gettyLimit,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gettyActions: bindActionCreators(gettyActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ImageLimitsContainer))
