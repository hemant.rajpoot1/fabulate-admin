import Edit from './Edit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as workspaceActions from 'store/actions/workspace'

class EditContainer extends Component {

  componentDidMount() {

    let { workspaceActions } = this.props

    let { id } = this.props.match.params

    workspaceActions.getId(id)

  }

  render() {

    return (
      <Edit { ...this.props } />
    )

  }

}

let mapStateToProps = (state, props) => {
  return {
    workspace: state.workspace.edit,
    translate: getTranslate(state.locale),
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    workspaceActions: bindActionCreators(workspaceActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditContainer))
