import React, { Component } from 'react'

class Edit extends Component {

  handleChange({ target }) {

    const { workspaceActions } = this.props

    workspaceActions.edit({ [target.name]: target.value })
    
  }

  onSubmit() {

    const { workspaceActions, workspace } = this.props

    workspaceActions.update(workspace)

  }

  render() {
    const { workspace, translate } = this.props
    
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">{ translate('workspaceEdit.title') }</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <form className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">{ translate('workspaceEdit.name') }</label>
              <div className="col-lg-9">
                <input type="text" className="form-control" name="name"
                  value={ workspace.name || ''}
                  onChange={ e => this.handleChange(e) }
                />
              </div>
            </div>

            <div className="text-right">
              <button type="button" className="btn btn-primary"
                onClick={ () => this.onSubmit() }
              >
                { translate('workspaceEdit.button') }
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

export default Edit
