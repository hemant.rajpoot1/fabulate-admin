import React, { Component } from 'react'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import * as workspaceUserActions from 'store/actions/workspaceUser'

import List from './List'

class ListContainer extends Component {
  componentDidMount() {
    this.fetchList()
  }

  componentDidUpdate(prevProps){
    const { filter } = this.props

    if(!_.isEqual(prevProps.filter, filter)){
      this.fetch(this.props.filter)
    }
  }

  fetch = filter => {
    const { workspaceUserActions } = this.props

    workspaceUserActions.getSearchUsersOfWorkspace(filter)
  }

  fetchList = (page) => {
    const { workspaceUserActions } = this.props
    workspaceUserActions.get({ page })
  }

  render() {
    return (
      <List
        {...this.props}
        fetchList = {this.fetchList}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    workspaceUser: state.workspaceUser.origin,
    page: state.workspaceUser.page,
    count: state.workspaceUser.count,
    translate: getTranslate(state.locale),
    filter: _.get(state, 'workspaceUser.filter', {})
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    workspaceUserActions: bindActionCreators(workspaceUserActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListContainer))
