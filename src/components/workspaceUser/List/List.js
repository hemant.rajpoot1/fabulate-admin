import React, { Component } from 'react'

import _ from 'lodash'

import Filter from 'components/ui/Filter'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'
import Pagination from 'react-js-pagination'
class List extends Component {
  onDelete(id) {
    const { workspaceUserActions } = this.props

    let notice = new PNotify({
      title: 'Remove user from organization',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      workspaceUserActions.remove({
        id,
        isRelation: true
      })
    })
  }

  setSearchFilter = ({ target }) => {    
    const { value } = target
    const { workspaceUserActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = { workspaceUserSearch: value !== '' ? value : undefined }
      workspaceUserActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { workspaceUserActions } = this.props
    workspaceUserActions.resetFilter()
  }


  renderSearchFilter = () => (
    <Filter
      searchItem="users of organization"
      onChangeHandler={this.setSearchFilter}
      defaultValue={this.props.filter.causeSearch || ''}
      resetFilters={this.resetFilters}
      customClasses="float-none m-10p-0"
    />
  )

  render() {
    const { 
      workspaceUser, 
      fetchList,
      page,
      count 
    } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Users list of organization</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Organization name</th>
                <th>User name</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              {_.map(workspaceUser, item => (
                <tr key={item.id}>
                  <td>{item.id}</td>
                  <td>{item.workspace && item.workspace.name ? item.workspace.name : '-'}</td>
                  <td>
                    <span>
                      {item.user && item.user.userName ? item.user.userName : '-'}
                      {item.user
                        && item.workspace
                        && (item.user.id === item.workspace.ownerId)
                        && (<i className="icon-user-tie" title="Owner organization"></i>)
                      }
                    </span>
                  </td>
                  <td className="text-center">
                    <ul className="icons-list">
                      <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown">
                          <i className="icon-menu9"></i>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                          <li onClick={() => this.onDelete(item.id)}>
                            <a><i className="icon-cross"></i>Delete user from organization</a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </td>
                </tr>
              )
              )}
            </tbody>
          </table>
          {count > 20 &&
          <div className="pager p-20">
            <Pagination
              hideNavigation
              pageRangeDisplayed={3}
              activePage={page}
              itemsCountPerPage={20}
              totalItemsCount={count}
              onChange={(page) => fetchList(page)}
            />
          </div>
          }
        </div>
      </div>
    )
  }
}

export default List
