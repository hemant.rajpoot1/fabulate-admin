import React, { Component } from 'react'

import Select from 'components/ui/Select'

class Create extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openSelect: undefined
    }
  }

  componentDidMount() {
    window.addEventListener('click', () => this.windowClick())
  }

  windowClick = () => {
    if(this.state.openSelect) {
      this.setState({openSelect: undefined})
    }
  }


  onSubmit = e => {
    const { workspaceUserActions, search } = this.props
    const formData = {
      userId: search.selected.user.id,
      workspaceId: search.selected.workspace.id,
    }

    if ( formData.userId && formData.workspaceId ) {
      e.preventDefault()
      workspaceUserActions.create(formData)
    } else {
      this.changeInput('userName', search.selected.user.name || '')
      this.changeInput('name', search.selected.workspace.name || '')
    }
  }

  changeInput = (type, value) => {
    const { validation, workspaceUserActions, validRules } = this.props

    workspaceUserActions.setValid({
      ...validation[type],
      value
    }, validRules[type], type)
  }

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { workspaceUserActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    if (value !== '') {
      this.timer = setTimeout(() => {
        this.setState({
          isSearch: true
        })
        workspaceUserActions.search(type, { q: value })
      }, 800)
    } else {
      this.setState({
        isOpen: false
      })
    }
  }

  setOpenSelect = type => {
    let nextOpenSelect = this.state.openSelect

    if(nextOpenSelect === type) {
      nextOpenSelect = undefined
    } else {
      nextOpenSelect = type
    }

    this.setState({openSelect: nextOpenSelect})
  }

  render() {

    let { translate, validation, workspaceUserActions, search } = this.props

    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Add user to organization</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">

          <div className="form-horizontal">

            <div className="form-group">
              <label className="col-lg-3 control-label">User</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={e => {
                    e.stopPropagation()
                    this.setOpenSelect('user')
                  }}
                  onSearch={e => this.onChangeSelectInput(e, 'user')}
                  onSelect={value => {
                    workspaceUserActions.select({
                      user: value
                    })
                  }}
                  isOpen={this.state.openSelect === 'user'}
                  items={search.user}
                  value={search.selected.user.name}
                />
                {!validation.userName.isValid && validation.userName.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`userError.${validation.userName.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Organization name</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onClick={e => {
                    e.stopPropagation()
                    this.setOpenSelect('workspace')
                  }}
                  onSearch={e => this.onChangeSelectInput(e, 'workspace')}
                  onSelect={value => {
                    workspaceUserActions.select({
                      workspace: value
                    })
                  }}
                  isOpen={this.state.openSelect === 'workspace'}
                  items={search.workspace}
                  value={search.selected.workspace.name}
                />
                {!validation.name.isValid && validation.name.message && (
                  <label
                    id="name-error"
                    className="validation-error-label"
                    forhtml="name"
                  >
                    {translate(`workspaceError.${validation.name.message}`)}
                  </label>
                )}
              </div>
            </div>

            <div className="text-right">
              <button
                type="button"
                className="btn btn-primary"
                onClick={e => this.onSubmit(e)}
              >
                Create
              </button>
            </div>
          </div>
        </div>
      </div>
    )

  }

}

export default Create
