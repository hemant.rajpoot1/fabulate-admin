import Create from './Create'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'

import * as workspaceUserActions from 'store/actions/workspaceUser'
import { getField } from 'store/selectors/modelSelectors'

class CreateContainer extends Component {

  render() {

    return (
      <Create { ...this.props } />
    )

  }

}

let mapStateToProps = (state, props) => {

  const validRules = getField(state, Object.keys(state.workspaceUser.validation))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}

  return {
    translate: getTranslate(state.locale),
    validation: state.workspaceUser.validation,
    search: state.workspaceUser.search,
    currentUser,
    validRules
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    workspaceUserActions: bindActionCreators(workspaceUserActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateContainer))
