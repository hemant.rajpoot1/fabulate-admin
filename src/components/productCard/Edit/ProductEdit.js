import React, { Component } from 'react'
import Select from 'components/ui/Select'
import TagField from 'components/ui/TagField'
import _ from 'lodash'


class ProductEdit extends Component {

  onChangeSelectInput = (e, type) => {
    const { value } = e.target
    const { productActions } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }

    this.timer = setTimeout(() => {
      this.setState({
        isSearch: true
      })
      productActions.search(type, { q: value })
    }, 800)
  }  
  
  renderErrorLabel = (fieldName) => {
    return (
      <label
        id="name-error"
        className="validation-error-label"
        forhtml="name"
      >
        {fieldName} can't be null
      </label>
    )
  }

  removeItem = (itemName, value) => {
    const { validation, change } = this.props
    const newSet = new Set(validation[itemName].value || [])
    if (!_.isNil(value)) {
      newSet.delete(value)
      const nextData = Array.from(newSet)
      change(itemName, nextData)
    }
  }

  render() {
    const {
      change,
      onSubmit,
      validation,
      search,
      templates,
      contentCategories,
      addToArray,
      genres
    } = this.props
    const selectArrTemplate = _.map(templates, templateItem => ({...templateItem, name: templateItem.templateName}))
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Edit</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li><a data-action="collapse">&nbsp;</a></li>
            </ul>
          </div>
        </div>

        <div className="panel-body">
          <div className="form-horizontal">
            <div className="form-group">
              <label className="col-lg-3 control-label">Display name</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="productDisplayName"
                  value={validation.productDisplayName.value}
                  onChange={e => change('productDisplayName', e.target.value)}
                />
                {!validation.productDisplayName.isValid && validation.productDisplayName.message 
                  && this.renderErrorLabel('Display name')
                }
              </div>
            </div>  

            <div className="form-group">
              <label className="col-lg-3 control-label">Card name</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="productCardName"
                  value={_.get(validation, 'productCardName.value', '')}
                  onChange={e => change('productCardName', e.target.value)}
                />
                {!validation.productCardName.isValid && validation.productCardName.message 
                  && this.renderErrorLabel('Card name')
                }
              </div>
            </div>  

            <div className="form-group">
              <label className="col-lg-3 control-label">Label size</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="sizeLabel"
                  value={_.get(validation, 'sizeLabel.value', '')}
                  onChange={e => change('sizeLabel', e.target.value)}
                />
                {!validation.sizeLabel.isValid && validation.sizeLabel.message 
                  && this.renderErrorLabel('Label size')
                }
              </div>
            </div> 


            <div className="form-group">
              <label className="col-lg-3 control-label">Order size</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="text"
                  name="sizeOrder"
                  value={_.get(validation, 'sizeOrder.value', '')}
                  onChange={e => change('sizeOrder', e.target.value)}
                />
                {!validation.sizeOrder.isValid && validation.sizeOrder.message 
                  && this.renderErrorLabel('Order size')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Description text</label>
              <div className="col-lg-9">
                <textarea
                  className="form-control"
                  rows="5"
                  name="descriptionText"
                  value={_.get(validation, 'descriptionText.value', '')}
                  onChange={e => change('descriptionText', e.target.value)}
                />
                {!validation.descriptionText.isValid && validation.descriptionText.message 
                  && this.renderErrorLabel('Description text')
                }
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Default price</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="defaultPrice"
                  value={_.get(validation, 'defaultPrice.value')}
                  onChange={e => change('defaultPrice', e.target.value)}
                />             
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Basic workflow template</label>
              <div className="col-lg-9">
                <Select
                  isSearch={true}
                  onSearch={e => this.onChangeSelectInput(e, 'template')}
                  onSelect={value => {
                    change('templateId', _.get(value, 'id'))
                  }}
                  items={_.size(search.template) ? search.template : selectArrTemplate}
                  value={_.get(templates, `${[validation.templateId.value]}.templateName`)}
                />             
                {!validation.templateId.isValid && validation.templateId.message 
                  && this.renderErrorLabel('Workflow')
                }
              </div>
            </div> 

            <div className="form-group">
              <label className="col-lg-3 control-label">Content categories</label>
              <TagField
                data={_.get(validation, 'contentCategory.value')}
                tagObj={contentCategories}
                onlyTags={true}
                isSelectEnabled={false}
                onRemove={value => this.removeItem('contentCategory', value)}
                labelKey='contentCategoryName'
              />
              <Select
                onSelect={value => {
                  addToArray('contentCategory', _.get(value, 'id'))
                }} 
                items={
                  _.map(contentCategories, contentCategoryItem =>
                    ({ ...contentCategoryItem, name: contentCategoryItem.contentCategoryName })
                  )}
              />
              {!validation.contentCategory.isValid && validation.contentCategory.message
                && this.renderErrorLabel('Content category')
              }
            </div>

            <div className="form-group">
              <label className="col-lg-3 control-label">Genres</label>
              <TagField
                data={_.get(validation, 'genre.value')}
                tagObj={genres}
                onlyTags={true}
                isSelectEnabled={false}
                onRemove={value => this.removeItem('genre', value)}
                labelKey='genreName'
              />
              <Select
                onSelect={value => {
                  addToArray('genre', value && value.id)
                }} 
                items={
                  _.map(genres, genreItem =>
                    ({ ...genreItem, name: genreItem.genreName })
                  )}
              />
            </div>
            
            <div className="form-group">
              <label className="col-lg-3 control-label">Sequence</label>
              <div className="col-lg-9">
                <input
                  className="form-control"
                  type="number"
                  name="productSequence"
                  value={validation.productSequence.value || ''}
                  onChange={e => change('productSequence', e.target.value)}
                />
              </div>
            </div>                                                            
          </div>

          <div className="text-right">
            <button className="btn btn-primary" onClick={onSubmit}>update product card</button>
          </div>
        </div>

      </div>
    )
  }
}

export default ProductEdit
