import ProductEdit from './ProductEdit'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as productActions from 'store/actions/product'
import * as templateActions from 'store/actions/template'
import * as contentCategoryActions from 'store/actions/contentCategory'
import * as genreActions from 'store/actions/genre'


import _ from 'lodash'
import PNotify from 'pnotify'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

class ProductEditContainer extends Component {

  componentDidMount() {
    const {
      templateActions,
      contentCategoryActions,
      productActions,
      genreActions,
      templates,
      genres,
      id
    } = this.props
    if(_.isEmpty(templates)) { 
      templateActions.get({})
      contentCategoryActions.get({})
    }
    if (_.isEmpty(genres)) {
      genreActions.get()
    }
    productActions.getId(id)
  }

  change = (type, value) => {
    const { validation, productActions, validRules } = this.props
    productActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      false
    )
  }  

  addToArray = (type, value) => {
    const { validation, productActions, validRules } = this.props
    const currentValues = new Set(validation[type].value)
    if (currentValues.has(value)) {
      return new PNotify({
        text: 'You already added this item!',
        type: 'error'
      })
    }
    productActions.setValid(
      {
        ...validation[type],
        value: [
          ...validation[type].value,
          value
        ]
      },
      validRules[type],
      type,
      false
    )
  }

  onSubmit = () => {
    const { productActions, validation, id } = this.props
    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
      id
    }

    if (
      formData.productDisplayName 
      && formData.templateId
      && formData.contentCategory
    ) {
      productActions.update({
        ...formData
      })
    } else {
      this.change('productDisplayName', validation.productDisplayName.value)
      this.change('templateId', validation.templateId.value)
      this.change('contentCategory', validation.contentCategory.value)
    }
  }

  select = value => {
    const { productActions } = this.props
    productActions.select(value ? value.id : null, false)
  }

  render() {
    return (
      <ProductEdit
        {...this.props}
        change={this.change}
        removeTask={this.removeTask}
        addTask={this.addTask}
        updateTask={this.updateTask}
        onSubmit={this.onSubmit}
        select={this.select}
        addToArray={this.addToArray}
      />
    )
  }
}

const mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.product.validationEdit))
  const currentUser = state.auth && _.get(state, 'auth.user', {})
  const { id } = props.match.params

  return {
    validation: state.product.validationEdit,
    search: state.product.search,         
    templates: state.template.origin,
    genres: state.genre.list,
    contentCategories: state.contentCategory.origin,
    currentUser,
    validRules,
    id
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    productActions: bindActionCreators(productActions, dispatch),
    templateActions: bindActionCreators(templateActions, dispatch),
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
    genreActions: bindActionCreators(genreActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductEditContainer))