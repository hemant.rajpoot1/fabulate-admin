import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import _ from 'lodash'

import Filter from 'components/ui/Filter'
import Select from 'components/ui/Select'
import SpinnerLoadData from 'components/ui/SpinnerLoadData'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.confirm'

class ProductList extends Component {
  onDelete(id) {
    const {
      productActions
    } = this.props

    const notice = new PNotify({
      title: 'Remove product card',
      text: 'Are you sure?',
      hide: false,
      confirm: {
        confirm: true,
        buttons: [
          {
            text: 'Yes',
            addClass: 'btn btn-sm btn-primary'
          },
          {
            text: 'No',
            addClass: 'btn btn-sm btn-link'
          }
        ]
      },
    })

    notice.get().on('pnotify.confirm', () => {
      productActions.remove(id)
    })
  }

  setSearchFilter = ({ target }) => {
    const { value } = target
    const { productActions, sort } = this.props
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(() => {
      const query = {}
      if (sort.type === 'number') {
        if (_.isNaN(+value)) {
          new PNotify({
            addclass: 'bg-danger',
            title: 'Invalid type',
            text: `Value "${sort.name}" must be a ${sort.type}`,
          })
          return
        } else query[sort.value] = +value
      } else query[sort.value] = value

      productActions.setFilter(query)
    }, 800)
  }

  resetFilters = () => {
    const { productActions } = this.props
    productActions.resetFilter()
  }

  renderSearchFilter = () => {
    const { productActions, sort } = this.props
    const productFilter = React.createRef()

    const sortBy = [
      {
        name: 'Card Name',
        value: 'productCardName',
        type: 'string'
      },
      {
        name: 'Order Size',
        value: 'sizeOrder',
        type: 'number'
      }
    ]

    const setSort = (sort) => {
      productActions.resetFilter()
      productActions.setSort(sort)
      productFilter.current.value = ''
    }

    return (
      <div className='d-flex'>
        <Filter
          setRef={productFilter}
          searchItem="Product card"
          onChangeHandler={this.setSearchFilter}
          defaultValue=''
          resetFilters={this.resetFilters}
          customClasses="float-none m-10p-0 mr-20"
        />
        <Select
          items={sortBy}
          placeholder="Set sorting by"
          cantReset={true}
          onSelect={setSort}
          value={_.get(sort, 'name', '')}
        />
      </div>
    )
  }

  renderTableBody = () => {
    const { products, isGetFetching } = this.props

    return (
      isGetFetching
        ? (
          <tr>
            <td colspan='8'>
              <SpinnerLoadData isAbsolute={false} delay="0" />
            </td>
          </tr>
        )
        : (
          _.map(products, (product, i) =>
            (
              <tr key={`${product.id}_index_${i}`}>
                <td>{product.id}</td>
                <td>{product.productDisplayName}</td>
                <td>{product.productCardName}</td>
                <td>{product.sizeOrder}</td>
                <td>{product.productSequence}</td>
                <td>{product.defaultPrice}</td>
                <td className="text-center">
                  <ul className="icons-list">
                    <li className="dropdown">
                      <a className="dropdown-toggle" data-toggle="dropdown">
                        <i className="icon-menu9"></i>
                      </a>
                      <ul className="dropdown-menu dropdown-menu-right">
                        <li><Link to={`/product-card/${product.id}`}><i className="icon-pencil"></i>Edit</Link></li>
                        <li onClick={() => this.onDelete(product.id)}>
                          <a><i className="icon-cross"></i>Delete</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </td>
              </tr>
            )
          )
        )
    )
  }

  render() {
    return (
      <div className="panel panel-flat">
        <div className="panel-heading">
          <h5 className="panel-title">Product card list</h5>
          <div className="heading-elements">
            <ul className="icons-list">
              <li>
                <a data-action="collapse">&nbsp;</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          {this.renderSearchFilter()}
          <table className="table datatable-select-basic">
            <thead>
              <tr>
                <th>#</th>
                <th>Product card display name</th>
                <th>Card name</th>
                <th>Order size</th>
                <th>Sequence</th>
                <th>Card cost</th>
                <th className="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.renderTableBody()}
            </tbody>
          </table>

        </div>
      </div>
    )
  }
}

export default withRouter(ProductList)
