import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTranslate } from 'react-localize-redux'
import _ from 'lodash'

import ProductList from './ProductList'

import * as productActions from 'store/actions/product'

class ProductListContainer extends Component {

  componentDidMount() {
    const { productActions, sort } = this.props
    productActions.get()
    productActions.resetFilter()
    !_.keys(sort).length && productActions.setSort({
      name: 'Card Name',
      value: 'productCardName'
    })
  }

  componentDidUpdate(prevProps) {
    const { filter, sort } = this.props

    if (!_.isEqual(prevProps.sort, sort)) {
      const nextfilter = {
        [sort.value]: Object.values(filter)[0]
      }
      productActions.setFilter(nextfilter)
      this.fetch(nextfilter)

    } else if (!_.isEqual(prevProps.filter, filter)) {
      this.fetch(this.props.filter)
    }
  }

  fetch = filter => {
    const { productActions } = this.props

    productActions.get(filter)
  }

  render() {
    return (
      <ProductList {...this.props} />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.product.origin,
    translate: getTranslate(state.locale),
    filter: _.get(state, 'product.filter', {}),
    sort: _.get(state, 'product.sort', {}),
    isGetFetching: _.get(state, 'product.isGetFetching', false)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    productActions: bindActionCreators(productActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductListContainer))
