import ProductCreate from './ProductCreate'

import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as productActions from 'store/actions/product'
import * as templateActions from 'store/actions/template'
import * as contentCategoryActions from 'store/actions/contentCategory'
import * as genreActions from 'store/actions/genre'

import _ from 'lodash'
import PNotify from 'pnotify'
import { getField } from 'store/selectors/modelSelectors'
import { formattingValidObj } from 'helpers/tools'

class ProductCreateContainer extends Component {

  componentDidMount() {
    const {
      templateActions,
      contentCategoryActions,
      genreActions,
      templates,
      contentCategories,
      genres,
    } = this.props
    if(_.isEmpty(templates)) { 
      templateActions.get({})
    }
    if (_.isEmpty(contentCategories)) {
      contentCategoryActions.get()
    }
    if (_.isEmpty(genres)) {
      genreActions.get()
    }
  }

  change = (type, value) => {
    const { validation, productActions, validRules } = this.props
    productActions.setValid(
      {
        ...validation[type],
        value
      },
      validRules[type],
      type,
      true
    )
  }

  addToArray = (type, value) => {
    const { validation, productActions, validRules } = this.props
    const currentValues = new Set(validation[type].value)
    if (currentValues.has(value)) {
      return new PNotify({
        text: 'You already added this item!',
        type: 'error'
      })
    }
    productActions.setValid(
      {
        ...validation[type],
        value: [
          ...validation[type].value,
          value
        ]
      },
      validRules[type],
      type,
      true
    )
  }

  onSubmit = () => {
    const { productActions, validation } = this.props
    const validObj = formattingValidObj(validation)
    const formData = {
      ...validObj,
    }
    if (
      formData.productDisplayName 
      && formData.templateId
      && formData.contentCategory
    ) {
      productActions.create({
        ...formData
      })
    } else {
      this.change('productDisplayName', validation.productDisplayName.value)
      this.change('templateId', validation.templateId.value)
      this.change('contentCategory', validation.contentCategory.value)
    }
  }

  select = value => {
    const { productActions } = this.props
    productActions.select(value ? value.id : null, true)
  }

  render() {
    return (
      <ProductCreate
        {...this.props}
        change={this.change}
        removeTask={this.removeTask}
        addTask={this.addTask}
        updateTask={this.updateTask}
        onSubmit={this.onSubmit}
        select={this.select}
        addToArray={this.addToArray}
      />
    )
  }
}

let mapStateToProps = (state, props) => {
  const validRules = getField(state, Object.keys(state.product.validationCreate))
  const currentUser = state.auth && state.auth.user ? state.auth.user : {}
  return {
    validation: state.product.validationCreate,
    search: state.product.search,         
    templates: state.template.origin,
    genres: state.genre.list,
    contentCategories: state.contentCategory.origin,
    currentUser,
    validRules,
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    productActions: bindActionCreators(productActions, dispatch),
    templateActions: bindActionCreators(templateActions, dispatch),
    contentCategoryActions: bindActionCreators(contentCategoryActions, dispatch),
    genreActions: bindActionCreators(genreActions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductCreateContainer))
