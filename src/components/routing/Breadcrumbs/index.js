import React, { Component } from 'react'
import BreadcrumbsRouter from 'react-router-dynamic-breadcrumbs'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { getTranslate } from 'react-localize-redux'

import _ from 'lodash'

class Breadcrumbs extends Component {

  render() {

    const {
      user,
      workspace,
      cause,
      task,
      asset,
      translate,
      location: { pathname },
      amplifyCause
    } = this.props

    const routes = {
      '/': <i className="icon-home position-left"></i>,
      '/user': translate('navigationLeftPanel.user'),
      '/user/:id': (url, match) => _.size(user) && _.get(user[match[':id']], 'email', ''),
      '/workspace': translate('navigationLeftPanel.workspace'),
      '/workspace/:id': (url, match) => _.size(workspace) && _.get(workspace[match[':id']], 'name', ''),
      '/workspace-user': translate('navigationLeftPanel.workspaceUser'),
      '/causeShow': translate('navigationLeftPanel.causeShow'),
      '/cause/:id': (url, match) => _.size(cause) && _.get(cause[match[':id']], 'causeName', ''),
      '/task': translate('navigationLeftPanel.task'),
      '/task/:id': (url, match) => _.size(task) && _.get(task[match[':id']], 'taskName', ''),
      '/asset': translate('navigationLeftPanel.asset'),
      '/asset/:id': (url, match) => _.size(asset) && _.get(asset[match[':id']], 'assetName', ''),
      '/workflow': translate('navigationLeftPanel.workflow'),
      '/analytics-tag-template': translate('navigationLeftPanel.analyticsTagTemplate'),
      '/analytics-project': translate('navigationLeftPanel.analyticsProject'),
      '/legal-pages': translate('navigationLeftPanel.legalPages'),
      '/amplify-cause': translate('navigationLeftPanel.amplifyCause'),
      '/amplify-cause/:id': (url, match) => _.size(amplifyCause) && _.get(amplifyCause[match[':id']], 'causeName', ''),
    }

    return (pathname === '/') ?
      null : (
        <div className="breadcrumb-line breadcrumb-line-component">

          <BreadcrumbsRouter
            WrapperComponent={
              (props) => <ul className="breadcrumb">{props.children}</ul>
            }
            mappedRoutes={routes}
          />

        </div>
      )
  }
}

const mapStateToProps = (state, props) => {

  return {
    user: state.user.origin,
    workspace: state.workspace.origin,
    cause: state.cause.origin,
    asset: state.asset.origin,
    task: state.task.origin,
    translate: getTranslate(state.locale),
  }
}

export default withRouter(connect(mapStateToProps)(Breadcrumbs))
