import NotLoggedApp from './NotLoggedApp'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'

let mapStateToProps = (state, props) => {
  return {}
}

let mapDispatchToProps = (dispatch) => {
  return {}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NotLoggedApp))
