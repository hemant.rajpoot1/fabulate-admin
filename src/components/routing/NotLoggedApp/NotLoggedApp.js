import React, { Component } from 'react'

import {
  Switch,
  Route,
} from 'react-router-dom'

import Entry from 'components/app/Entry'
import Reset  from 'components/app/Reset'
import Restore  from 'components/app/Restore'
import verifyEmail  from 'components/app/verifyEmail'

class Notlogged extends Component {

  render() {

    return (
      <Switch className="wrapper">
        <Route exact path="/" component={Entry} />
        <Route path="/reset" component={Reset} />
        <Route path="/restore/:id" component={Restore}/>
        <Route path="/verify-email/:id" component={verifyEmail}/>
        <Route component={Entry} />
      </Switch>
    )

  }

}

export default Notlogged
