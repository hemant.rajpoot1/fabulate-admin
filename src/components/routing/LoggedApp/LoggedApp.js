import React, { Component } from 'react'

import {
  Switch,
  Route,
} from 'react-router-dom'

import DashBoard from 'components/app/DashBoard'

import UsersList from 'components/user/List'
import Edit from 'components/user/Edit'
import Create from 'components/user/Create'

import LegalPages from 'components/LegalPages'
import CreateLegalPages from 'components/LegalPages/Create'
import EditLegalPages from 'components/LegalPages/Edit'

import ActiveUserSessions from 'components/activeUserSessions/List'

import PastUserSessions from 'components/pastUserSessions/List'

import WorkspaceList from 'components/workspace/List'
import CreateWorkspace from 'components/workspace/Create'
import EditWorkspace from 'components/workspace/Edit'

import CauseList from 'components/cause/List'
import CreateCause from 'components/cause/Create'
import EditCause from 'components/cause/Edit'
import ShowCause from 'components/cause/ListItem'


import TaskList from 'components/task/List'
import CreateTask from 'components/task/Create'
import EditTask from 'components/task/Edit'

import AssetList from 'components/asset/List'
import CreateAsset from 'components/asset/Create'
import EditAsset from 'components/asset/Edit'

import WorkspaceUserList from 'components/workspaceUser/List'
import CreateWorkspaceUser from 'components/workspaceUser/Create'

import WorkflowList from 'components/workflow/List'
import WorkflowEdit from 'components/workflow/Edit'
import WorkflowCreate from 'components/workflow/Create'

import ProductCardList from 'components/productCard/List'
import ProductCardCreate from 'components/productCard/Create'
import ProductCardEdit from 'components/productCard/Edit'

import contentCategoryList from 'components/contentCategory/List'
import contentCategoryCreate from 'components/contentCategory/Create'
import contentCategoryEdit from 'components/contentCategory/Edit'

import Profile  from 'components/profile'
import RequestForm from 'components/form/RequestForm'

import Error404 from 'components/errors/404'
import verifyEmail  from 'components/app/verifyEmail'

import Skills from 'components/skills/List'
import SkillsEdit from 'components/skills/Edit'
import SkillsCreate from 'components/skills/Create'

import ImageLimits from 'components/imageLimits'
import Settings from 'components/settings'

import Genre from 'components/genre/List'
import GenreEdit from 'components/genre/Edit'
import GenreCreate from 'components/genre/Create'

import workspaceProductCard from 'components/workspaceProductCard/List'
import workspaceProductCardEdit from 'components/workspaceProductCard/Edit'
import workspaceProductCardCreate from 'components/workspaceProductCard/Create'

import analyticsTagTemplateList from 'components/analyticsTagTemplate/List'
import analyticsTagTemplateCreate from 'components/analyticsTagTemplate/Create'
import analyticsTagTemplateEdit from 'components/analyticsTagTemplate/Edit'

import publicationHistoryList from 'components/publicationHistory/List'
import publicationHistoryCreate from 'components/publicationHistory/Create'
import publicationHistoryEdit from 'components/publicationHistory/Edit'

import analyticsProjectList from 'components/analyticsProject/List'
import analyticsProjectCreate from 'components/analyticsProject/Create'
import analyticsProjectEdit from 'components/analyticsProject/Edit'

import amplifyWorkspaceProductList from 'components/amplifyWorkspaceProduct/List'
import amplifyWorkspaceProductCreate from 'components/amplifyWorkspaceProduct/Create'
import amplifyWorkspaceProductEdit from 'components/amplifyWorkspaceProduct/Edit'

import AmplifyProductListContainer from 'components/amplifyProduct/List'
import AmplifyProductCreateContainer from 'components/amplifyProduct/Create'
import AmplifyProductEditContainer from 'components/amplifyProduct/Edit'

import AmplifyCauseContainer from 'components/amplifyCause'
import AmplifyEditCauseContainer from 'components/amplifyCause/Edit'

import Calendar from 'components/calendar'


class LoggedApp extends Component {

  routeAsAdmin = () => {
    return (
      <Switch>
        <Route exact path="/" component={DashBoard} />
        <Route path="/dashboard" component={DashBoard} />
        <Route exact path="/user" component={UsersList} />
        <Route exact path="/workspace" component={WorkspaceList} />
        <Route path="/create-workspace" component={CreateWorkspace} />
        <Route path="/workspace/:id" component={EditWorkspace} />
        <Route path="/workspace-user" component={WorkspaceUserList} />
        <Route path="/create-relation" component={CreateWorkspaceUser} />
        <Route path="/verify-email/:id" component={verifyEmail}/>
        <Route exact path="/cause-show" component={CauseList} />
        <Route path="/create-cause" component={CreateCause} />
        <Route path="/cause-show/:id/edit" component={EditCause} />
        <Route path="/cause-show/:id" component={ShowCause} />
        <Route exact path="/task" component={TaskList} />
        <Route path="/create-task" component={CreateTask} />
        <Route path="/task/:id" component={EditTask} />
        <Route exact path="/asset" component={AssetList} />
        <Route path="/create-asset" component={CreateAsset} />
        <Route path="/asset/:id" component={EditAsset} />
        <Route path="/workflow" component={WorkflowList} />
        <Route path="/workflow-edit/:id" component={WorkflowEdit} />
        <Route path="/create-workflow" component={WorkflowCreate} />
        <Route exact path="/content-category" component={contentCategoryList} />
        <Route path="/content-category/:id" component={contentCategoryEdit} />
        <Route path="/create-content-category" component={contentCategoryCreate} />
        <Route exact path="/product-card" component={ProductCardList} />
        <Route path="/create-product-card" component={ProductCardCreate} />
        <Route path="/product-card/:id" component={ProductCardEdit} />
        <Route path="/profile" component={Profile} />
        {/* <Route exact path="/payment" component={PaymentList} />
        <Route path="/payment/:id" component={PaymentEdit} />  */}
        <Route path="/pitch/:id" component={RequestForm} />
        <Route exact path="/skills" component={Skills} />
        <Route path="/skills/:id" component={SkillsEdit} />
        <Route path="/create-skills" component={SkillsCreate} />
        <Route path="/create-limits" component={ImageLimits} />
        <Route path="/settings" component={Settings} />
        <Route exact path="/genre" component={Genre} />
        <Route path="/genre/:id" component={GenreEdit} />
        <Route path="/create-genre" component={GenreCreate} />
        <Route exact path="/workspace-product-card" component={workspaceProductCard} />
        <Route path="/workspace-product-card/:id" component={workspaceProductCardEdit} />
        <Route path="/create-workspace-product-card" component={workspaceProductCardCreate} />
        <Route path="/calendar" component={Calendar} />
        <Route exact path="/analytics-tag-template" component={analyticsTagTemplateList}/>
        <Route path="/create-analytics-tag-template" component={analyticsTagTemplateCreate}/>
        <Route path="/analytics-tag-template/:id" component={analyticsTagTemplateEdit} />
        <Route exact path="/publication-history" component={publicationHistoryList}/>
        <Route path="/create-publication-history" component={publicationHistoryCreate}/>
        <Route path="/publication-history/:id" component={publicationHistoryEdit} />
        <Route exact path="/analytics-project" component={analyticsProjectList} />
        <Route path="/create-analytics-project" component={analyticsProjectCreate}/>
        <Route path="/analytics-project/:id"component={analyticsProjectEdit} />
        
        <Route exact path="/amplify-workspace-product" component={amplifyWorkspaceProductList} />
        <Route path="/create-amplify-workspace-product" component={amplifyWorkspaceProductCreate}/>
        <Route path="/amplify-workspace-product/:id"component={amplifyWorkspaceProductEdit} />

        <Route exact path="/amplify-product" component={AmplifyProductListContainer} />
        <Route path="/amplify-product/:id" component={AmplifyProductEditContainer} />
        <Route path="/create-amplify-product" component={AmplifyProductCreateContainer} />
        <Route exact path="/legal-pages" component={LegalPages} />
        <Route path="/create-legal-page" component={CreateLegalPages} />
        <Route path="/legal-pages/:id" component={EditLegalPages} />
        <Route exact path="/active-user-sessions" component={ActiveUserSessions} />
        <Route exact path="/past-user-sessions" component={PastUserSessions} />
        <Route exact path="/amplify-cause" component={AmplifyCauseContainer} />
        <Route exact path="/amplify-cause/:id" component={AmplifyEditCauseContainer} />

        <Route component={Error404} />
      </Switch>
    )
  }

  routeAsSuperAdmin = () => {
    return (
      <Switch>
        <Route exact path="/" component={DashBoard} />
        <Route path="/dashboard" component={DashBoard} />
        <Route exact path="/user" component={UsersList} />
        <Route path="/user/:id" component={Edit} />
        <Route path="/create-user" component={Create} />
        <Route exact path="/workspace" component={WorkspaceList} />
        <Route path="/create-workspace" component={CreateWorkspace} />
        <Route path="/workspace/:id" component={EditWorkspace} />
        <Route path="/workspace-user" component={WorkspaceUserList} />
        <Route path="/create-relation" component={CreateWorkspaceUser} />
        <Route path="/verify-email/:id" component={verifyEmail}/>
        <Route exact path="/cause-show" component={CauseList} />
        <Route path="/create-cause" component={CreateCause} />
        <Route path="/cause-show/:id/edit" component={EditCause} />
        <Route path="/cause-show/:id" component={ShowCause} />
        <Route exact path="/task" component={TaskList} />
        <Route path="/create-task" component={CreateTask} />
        <Route path="/task/:id" component={EditTask} />
        <Route exact path="/asset" component={AssetList} />
        <Route path="/create-asset" component={CreateAsset} />
        <Route path="/asset/:id" component={EditAsset} />
        <Route path="/workflow" component={WorkflowList} />
        <Route path="/workflow-edit/:id" component={WorkflowEdit} />
        <Route path="/create-workflow" component={WorkflowCreate} />
        <Route exact path="/content-category" component={contentCategoryList} />
        <Route path="/content-category/:id" component={contentCategoryEdit} />
        <Route path="/create-content-category" component={contentCategoryCreate} />
        <Route exact path="/product-card" component={ProductCardList} />
        <Route path="/create-product-card" component={ProductCardCreate} />
        <Route path="/product-card/:id" component={ProductCardEdit} />
        <Route path="/profile" component={Profile} />
        {/* <Route exact path="/payment" component={PaymentList} />
        <Route path="/payment/:id" component={PaymentEdit} />  */}
        <Route path="/pitch/:id" component={RequestForm} />
        <Route exact path="/skills" component={Skills} />
        <Route path="/skills/:id" component={SkillsEdit} />
        <Route path="/create-limits" component={ImageLimits} />
        <Route path="/create-skills" component={SkillsCreate} />
        <Route path="/settings" component={Settings} />
        <Route exact path="/genre" component={Genre} />
        <Route path="/genre/:id" component={GenreEdit} />
        <Route path="/create-genre" component={GenreCreate} />
        <Route exact path="/workspace-product-card" component={workspaceProductCard} />
        <Route path="/workspace-product-card/:id" component={workspaceProductCardEdit} />
        <Route path="/create-workspace-product-card" component={workspaceProductCardCreate} />
        <Route path="/calendar" component={Calendar} />
        <Route exact path="/analytics-tag-template" component={analyticsTagTemplateList}/>
        <Route path="/create-analytics-tag-template" component={analyticsTagTemplateCreate}/>
        <Route path="/analytics-tag-template/:id" component={analyticsTagTemplateEdit} />
        <Route exact path="/publication-history" component={publicationHistoryList}/>
        <Route path="/create-publication-history" component={publicationHistoryCreate}/>
        <Route path="/publication-history/:id" component={publicationHistoryEdit} />
        <Route exact path="/analytics-project" component={analyticsProjectList} />
        <Route path="/create-analytics-project" component={analyticsProjectCreate}/>
        <Route path="/analytics-project/:id"component={analyticsProjectEdit} />
        <Route exact path="/amplify-workspace-product" component={amplifyWorkspaceProductList} />
        <Route path="/create-amplify-workspace-product" component={amplifyWorkspaceProductCreate}/>
        <Route path="/amplify-workspace-product/:id"component={amplifyWorkspaceProductEdit} />
        <Route exact path="/amplify-product" component={AmplifyProductListContainer} />
        <Route path="/amplify-product/:id" component={AmplifyProductEditContainer} />
        <Route path="/create-amplify-product" component={AmplifyProductCreateContainer} />
        <Route exact path="/legal-pages" component={LegalPages} />
        <Route path="/create-legal-page" component={CreateLegalPages} />
        <Route path="/legal-pages/:id" component={EditLegalPages} />
        <Route exact path="/active-user-sessions" component={ActiveUserSessions} />
        <Route exact path="/past-user-sessions" component={PastUserSessions} />
        <Route exact path="/amplify-cause" component={AmplifyCauseContainer} />
        <Route exact path="/amplify-cause/:id" component={AmplifyEditCauseContainer} />

        <Route component={Error404} />
      </Switch>
    )
  }

  render() {
    const { currentUser } = this.props
    return currentUser.permissions === 0
      ? this.routeAsSuperAdmin()
      : this.routeAsAdmin()
  }

}

export default LoggedApp
