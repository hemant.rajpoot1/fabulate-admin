import LoggedApp from './LoggedApp'

import { withRouter } from 'react-router'
import { connect } from 'react-redux'

let mapStateToProps = (state, props) => {
  const currentUser = state.auth && state.auth.user ? state.auth.user : null
  
  return {
    currentUser
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoggedApp))