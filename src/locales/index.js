import { initialize, addTranslationForLanguage } from 'react-localize-redux'
import { store } from 'store/store'

import en from './translations/en'

const languages = ['en']

store.dispatch(initialize(languages, { defaultLanguage: 'en' }))

store.dispatch(addTranslationForLanguage(en, 'en'))
