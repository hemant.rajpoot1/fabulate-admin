import _ from 'lodash'

export const getInitials = (name) => {

  if(_.isUndefined(name) || _.isNull(name)) {

    return '?' // TODO: Maybe Law.Tech logo is better?

  }

  const names = name.split(' ')

  let initials = names[0].substring(0, 1).toUpperCase()

  if (names.length > 1) {

    initials += names[names.length - 1].substring(0, 1).toUpperCase()

  }

  return initials

}
