import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from 'store/constants/legalPages'
import * as actions from 'store/actions/legalPages'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'



function* get(action) {

  try {
    const privacyList = yield backend.service('legal-url').find()

    yield put(actions.getSuccess(privacyList))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get privacy policy error'),
    })

    yield put(actions.getFailure(e))

  }

}

function* getId(action) {
  try {
    const {id } = action.payload
  
    const workspace = yield backend.service('legal-url').get(id)

    if (_.size(workspace)) {
      yield put(actions.getIdSuccess(workspace))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get legal page error'),
    })

    yield put(actions.getIdFailure(e))
  }
}

function* create(action) {

  try {

    const createLegalPages = yield backend.service('legal-url').create({ ...action.payload.data });

    if (_.size(createLegalPages)) {
      yield put(push('/legal-pages'))                                                                                                
      yield put(actions.createSuccess(createLegalPages))
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Create legal pages created',
      })
    }
    

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'message', 'Create legal pages error'),
    })

    yield put(actions.createFailure(e))
  }
}

function* update(action) {
  try {
    const { id, data } = action.payload
    const updateLegalPage = yield backend.service('legal-url').patch(id, data)
    if (_.size(updateLegalPage)) {
      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Legal page updated successfully',
      })
      yield put(actions.updateSuccess(updateLegalPage))
      yield put(push('/legal-pages'))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update legal page error'),
    })
    yield put(actions.updateFailure(e))
  }
}

function* remove(action) {

  try {

    const id = action.payload.id
     
    const deleteLegalPages = yield backend.service('legal-url').remove(id)

    if(_.size(deleteLegalPages)) {
      yield put(push('/legal-pages'))                                                                                                
      yield put(actions.removeSuccess(deleteLegalPages))
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Deleted privacy policy',
      })
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete privacy policy error'),
    })

    yield put(actions.removeFailure(e))

  }

}

function* legalPagesSaga() {
  yield takeEvery(types.GET, get)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.UPDATE, update)
  yield takeEvery(types.REMOVE, remove)
}

export default {
  legalPagesSaga,
}
