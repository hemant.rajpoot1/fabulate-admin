import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/amplifyProduct'
import * as actions from '../actions/amplifyProduct'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const amplifyProductList = yield backend.service('amplifyProduct').find({
      query: {
        ...action.payload
      }
    })

    if(_.size(amplifyProductList)) {
      yield put(actions.getSuccess(amplifyProductList))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get amplify product list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const product = yield backend.service('amplifyProduct').get(id)
    if(_.size(product)) {
      yield put(actions.getIdSuccess(product))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get amplify product card error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* create(action) {

  try {
    const createAmplifyProduct = yield backend.service('amplifyProduct').create({
      ...action.payload
    })

    if(_.size(createAmplifyProduct)) {

      yield put(actions.createSuccess(createAmplifyProduct))
      yield put(push('/amplify-product'))                                                                                                
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Amplify product created',
      })
      if(createAmplifyProduct.buyerWorkspacesCount)
        new PNotify({
          addclass: 'bg-info',
          title: 'Information',
          text: 'Added to ' + createAmplifyProduct.buyerWorkspacesCount + ' organisations',
        })
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create amplify product error'),
    })

    yield put(actions.createFailure(e))

  }

}

function* remove(action) {

  try {

    const id = action.payload

    const deleteAmplifyProduct = yield backend.service('amplifyProduct').remove(id)

    if(_.size(deleteAmplifyProduct)) {
      yield put(actions.removeSuccess(deleteAmplifyProduct))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete amplify product error'),
    })

    yield put(actions.removeFailure(e))

  }

}

function* uploadFile(action) {
  const formData = new FormData()
  formData.append('uri', action.payload.params.uri)
  try {
    const uploadData = yield backend.service('uploads').create(formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Content-Disposition': 'form-data',
      }
    })
    if(_.size(uploadData)) {
      yield put(actions.uploadFileSuccess(uploadData, action.payload.params.preview, action.payload.isCreate))
    }


  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.uploadFileFailure(e, action.payload.params.preview, action.payload.isCreate))

  }

}

function* downloadFile(action) {
  try {
    const id  = action.payload.id
    const aRef = action.payload.ref
    const downloadFileData = yield backend.service('uploads').get(id)

    if(downloadFileData.uri) {
      // document.location.href = downloadFileData.uri
      aRef.setAttribute('href', downloadFileData.uri)
      aRef.setAttribute('download', downloadFileData.id)
      aRef.click()

      yield put(actions.downloadFileSuccess())
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Download file error'),
    })

    yield put(actions.downloadFileFailure(e))

  }
}

function* update(action) {
  try {

    const { id } = action.payload
    const updateWorkspace = yield backend.service('amplifyProduct').patch(id, action.payload)

    if(_.size(updateWorkspace)) {

      yield put(actions.updateSuccess(updateWorkspace))
      yield put(push('/amplify-product'))  
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Amplify product updated',
      })      
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update amplify product error'),
    })

    yield put(actions.updateFailure(e))

  }

}


function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })
    if(_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.searchFailure(e))

  }

}

function* amplifyProductSaga() {

  yield takeEvery(types.GET, get)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.REMOVE, remove)
  yield takeEvery(types.UPLOAD, uploadFile)
  yield takeEvery(types.DOWNLOAD_FILE, downloadFile)
  yield takeEvery(types.SEARCH, search)
  yield takeEvery(types.UPDATE, update)
}

export default {
  amplifyProductSaga,
}
