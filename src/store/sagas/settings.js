import { put, takeEvery } from 'redux-saga/effects'

import _ from 'lodash'
import PNotify from 'pnotify'

import types from '../constants/settings'
import * as actions from '../actions/settings'

import backend from 'store/api/feathers'

function* get() {
  try {
    const xeroSettings = yield backend.service('settings').find()
    if(_.size(xeroSettings)) {
      yield put(actions.getSuccess(xeroSettings))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'An error occured!'),
    })
    yield put(actions.getFailure(e))
  }
}

function* update({ payload }) {
  try {
    const updateSettings = yield backend.service('settings').create(payload)
    if(_.size(updateSettings)) {
      yield put(actions.updateSuccess(updateSettings))
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Settings was successfully updated',
      })      
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update settings error'),
    })
    yield put(actions.updateFailure(e))
  }
}



function* settingsSaga() {
  yield takeEvery(types.SETTINGS_GET, get)
  yield takeEvery(types.SETTINGS_UPDATE, update)
}

export default {
  settingsSaga
}