import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'
import PNotify from 'pnotify'

import types from '../constants/contentCategory'
import * as actions from '../actions/contentCategory'

import backend from 'store/api/feathers'

function* get(action) {

  try {
    const contentCategoryList = yield backend.service('content-category').find({
      query: action.payload.queryParams
    })
    if(_.size(contentCategoryList)) {
      yield put(actions.getSuccess(contentCategoryList))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get content category list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const contentCategory = yield backend.service('content-category').get(id)
    if(_.size(contentCategory)) {
      yield put(actions.getIdSuccess(contentCategory))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get content category error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* create(action) {
  try {
    const createContentCategory = yield backend.service('content-category').create({
      ...action.payload
    })
    if(_.size(createContentCategory)) {
      yield put(actions.createSuccess(createContentCategory))
      yield put(push('/content-category'))                                                                                                
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Content category created',
      }) 
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create content category error'),
    })
    yield put(actions.createFailure(e))
  }
}

function* remove(action) {
  try {
    const id = action.payload
    const deleteContentCategory = yield backend.service('content-category').remove(id)
    if(_.size(deleteContentCategory)) {
      yield put(actions.removeSuccess(deleteContentCategory))
      yield put(push('/content-category'))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete content category error'),
    })
    yield put(actions.removeFailure(e))
  }
}

function* update(action) {
  try {
    const { id } = action.payload
    const updateContentCategory = yield backend.service('content-category').patch(id, action.payload)
    if(_.size(updateContentCategory)) {
      yield put(actions.updateSuccess(updateContentCategory))
      yield put(push('/content-category'))
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Content category updated',
      })      
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update content category error'),
    })
    yield put(actions.updateFailure(e))
  }
}

function* contentCategorySaga() {
  yield takeEvery(types.GET, get)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.REMOVE, remove)
  yield takeEvery(types.UPDATE, update)
}

export default {
  contentCategorySaga
}