import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/genre'
import * as actions from '../actions/genre'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {

  try {
    const page = _.get(action, 'payload.page', 1)
    const { genreSearch } = _.get(action, 'payload', {})

    const genreList = genreSearch ? yield backend.service('genre').find({
      query: {
        ...action.payload
      }
    }) : yield backend.service('genre').find({
      query: {
        page
      }
    }) 

    if(_.size(genreList)) {
      yield put(actions.getSuccess({
        ...genreList,
        page
      }))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get genre list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* create(action) {
  const newGenreBody = action.payload.params
  const request = {
    'genreName': newGenreBody
  }
  try {
    const newGenre = yield backend.service('genre').create(request)
    if(_.size(newGenre)) {
      yield put(actions.createSuccess(newGenre))
      yield put(push('/genre'))
    }
    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new genre',
    })
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create genre error'),
    })
    yield put(actions.createFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const genre = yield backend.service('genre').get(id)
    if(_.size(genre)) {
      yield put(actions.getIdSuccess(genre))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get genre error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* remove(action) {
  try {
    const id = action.payload
    const deleteGenre = yield backend.service('genre').remove(id)
    if(_.size(deleteGenre)) {
      yield put(actions.removeSuccess(deleteGenre))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete genre error'),
    })
    yield put(actions.removeFailure(e))
  }
}

function* update(action) {
  try {
    const { id } = action.payload
    const updateGenre = yield backend.service('genre').patch(id, action.payload)
    if(_.size(updateGenre)) {
      yield put(actions.updateSuccess(updateGenre))
      yield put(push('/genre'))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update genre error'),
    })
    yield put(actions.updateFailure(e))
  }
}

function* genreSaga() {
  yield takeEvery(types.GENRE_GET, get)
  yield takeEvery(types.GENRE_GET_ID, getId)
  yield takeEvery(types.GENRE_REMOVE, remove)
  yield takeEvery(types.GENRE_UPDATE, update)
  yield takeEvery(types.GENRE_CREATE, create)
}

export default {
  genreSaga,
}
