import { put, takeEvery } from 'redux-saga/effects'

import _ from 'lodash'
import PNotify from 'pnotify'

import backend from 'store/api/feathers'

import types from 'store/constants/gettyImage'
import * as actions from 'store/actions/gettyLimit'

function* setGettyLimit(action) {
  try {
    let limitNumber = action.payload
    const findLimitStatus = yield backend.service('getty-limit').find({
      query: {id: 'rzYPR0R7WL6YtHPtPXixLo'}, // random id for getty settings in data base
    })

    if (findLimitStatus.count === 0) {
      const setLimitStatus = yield backend.service('getty-limit').create({id: 'rzYPR0R7WL6YtHPtPXixLo', gettyImageLimit: limitNumber})
      if (setLimitStatus.gettyImageLimit === limitNumber) {
        new PNotify({
          addclass: 'bg-info',
          title: 'Information',
          text: 'Image limit successfully updated',
        })
      } else {
        new PNotify({
          addclass: 'bg-danger',
          title: 'Error',
          text: 'Something went wrong, try again',
        })
      }
    } else {
      const setLimitStatus = yield backend.service('getty-limit').patch('rzYPR0R7WL6YtHPtPXixLo', {gettyImageLimit: limitNumber})
      if (setLimitStatus.gettyImageLimit === limitNumber) {
        new PNotify({
          addclass: 'bg-info',
          title: 'Information',
          text: 'Image limit successfully updated',
        })
      } else {
        new PNotify({
          addclass: 'bg-danger',
          title: 'Error',
          text: 'Something went wrong, try again',
        })
      }
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Error retrieving data'),
    })
  }
}

function* getGettyLimit(action) {
  try {
    const findLimitStatus = yield backend.service('getty-limit').get('rzYPR0R7WL6YtHPtPXixLo')
    yield put(actions.saveGettyLimit(_.get(findLimitStatus, 'gettyLimit', '')))
  } catch (e) {
    if (e.code === 500 && e.type === 'FeathersError') {
      yield put(actions.saveGettyLimit(''))
    } else {
      new PNotify({
        addclass: 'bg-danger',
        title: 'Error',
        text: _.get(e, 'response.data.err', 'Error retrieving data'),
      })
    }
  }
}

function* gettySaga() {
  yield takeEvery(types.SET_GETTY_LIMIT, setGettyLimit)
  yield takeEvery(types.GET_GETTY_LIMIT, getGettyLimit)
}

export default {
  gettySaga,
}
