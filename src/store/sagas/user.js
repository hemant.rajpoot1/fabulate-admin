import { put, takeEvery, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'
import PNotify from 'pnotify'

import types from '../constants/user'
import * as actions from '../actions/user'

import backend from 'store/api/feathers'

import { getTranslate } from 'react-localize-redux'

function* translate() {
  let store = yield select()
  const translate = getTranslate(store.locale)
  return translate
}

function* get(action) {
  try {
    const page = action.payload.page
    const usersList = yield backend.service('user').find({
      query: {
        ...action.payload.queryParams,
        page
      }
    })
    if (_.size(usersList)) {
      yield put(actions.getSuccess(usersList, page))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get users list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* create(action) {
  try {
    const newUser = yield backend.service('user').create(action.payload.params)
    if (_.size(newUser)) {
      yield put(actions.createSuccess(newUser))
      yield put(push('/user'))
    }
    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new user',
    })
  } catch (e) {
    const translateFunc = yield translate()
    let err = e
    if (e && e.errors && e.errors.length > 0) {
      err = e.errors[0]
    }
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(err, 'response.data.err', translateFunc(`userError.${err.message}`))
    })
    yield put(actions.createFailure(err))
  }
}

function* remove(action) {
  try {
    const id = action.payload
    const deleteUser = yield backend.service('user').remove(id)
    if (_.size(deleteUser)) {
      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'User deleted successfully',
      })
      yield put(actions.removeSuccess(deleteUser))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete user error'),
    })
    yield put(actions.removeFailure(e))
  }
}

function* getId(action) {
  try {
    const user = yield backend.service('user').get(action.payload, {
      query: {
        isAdminPanel: true
      }
    })
    if (_.size(user)) {
      yield put(actions.getIdSuccess(user))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get user error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* update(action) {
  try {
    let { id } = action.payload
    const updateUser = yield backend.service('user').patch(id, _.omit(action.payload, 'skills'))
    if (_.size(updateUser)) {
      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'User data updated successfully',
      })
      yield put(actions.updateSuccess(updateUser))
      yield put(push('/user'))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update user error'),
    })
    yield put(actions.updateFailure(e))
  }
}

function* loginAsAnotherUser(action) {
  const translateFunc = yield translate()

  try {
    const response = yield backend.service('user').patch(action.payload, {})
    window.open(
      `${process.env['REACT_APP_CLIENT_URL']}/login-as-admin/${response.adminAccessKey}`,
      '_blank'
    )

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: translateFunc('saga.errorTitle'),
      text: _.get(e, 'response.data.err', 'Login error'),
    })
  }
}

function* usersListSaga() {
  yield takeEvery(types.GET, get)
  yield takeEvery(types.REMOVE, remove)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.UPDATE, update)
  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.USER_LOGIN_AS_ANOTHER_USER, loginAsAnotherUser)
}

export default {
  usersListSaga,
}
