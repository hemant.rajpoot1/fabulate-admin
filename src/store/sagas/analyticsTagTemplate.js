import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/analyticsTagTemplate'
import * as actions from '../actions/analyticsTagTemplate'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const { page } = action.payload
    const analyticsTagTemplateList = yield backend.service('analytics-tag-template').find({
      query: {
        ...action.payload
      }
    })

    if (_.size(analyticsTagTemplateList)) {
      yield put(actions.getSuccess(analyticsTagTemplateList, page))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organizations list error'),
    })

    yield put(actions.getFailure(e))
  }
}

function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })

    if (_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.searchFailure(e))
  }
}

function* create(action) {
  try {
    const newAnalyticsTagTemplate = yield backend.service('analytics-tag-template').create(action.payload.params)

    if (_.size(newAnalyticsTagTemplate)) {
      yield put(actions.createSuccess(newAnalyticsTagTemplate))
      yield put(push('/analytics-tag-template'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new relation',
    })
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.createFailure(e))
  }
}

function* remove(action) {
  try {
    const { id, templatesLength, page } = action.payload
    const renderPage = templatesLength === 1 ? page - 1 : page
    const deleteTemplate = yield backend.service('analytics-tag-template').remove(id)

    if (_.size(deleteTemplate)) {
      yield put(actions.removeSuccess(deleteTemplate))
      yield put(actions.get({ page: renderPage }))

      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Analytics tag template deleted success',
      })
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete analytics tag template error'),
    })

    yield put(actions.removeFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const analyticsTagTemplate = yield backend.service('analytics-tag-template').get(id)

    if (_.size(analyticsTagTemplate)) {
      yield put(actions.getIdSuccess(analyticsTagTemplate))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get analytics tag template info error'),
    })

    yield put(actions.getIdFailure(e))
  }
}

function* update(action) {
  try {
    const { id, ...otherParams} = action.payload
    const updateWorkspace = yield backend.service('analytics-tag-template').patch(id, otherParams)

    if (_.size(updateWorkspace)) {
      yield put(actions.updateSuccess(updateWorkspace))
      yield put(push('/analytics-tag-template'))

      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Analytics tag template updated success',
      })
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update analytics tag template error'),
    })

    yield put(actions.updateFailure(e))
  }
}

function* analyticsTagTemplateSaga() {
  yield takeEvery(types.ANALYTICS_TAG_TEMPLATE_GET, get)
  yield takeEvery(types.ANALYTICS_TAG_TEMPLATE_SEARCH, search)
  yield takeEvery(types.ANALYTICS_TAG_TEMPLATE_CREATE, create)
  yield takeEvery(types.ANALYTICS_TAG_TEMPLATE_REMOVE, remove)
  yield takeEvery(types.ANALYTICS_TAG_TEMPLATE_GET_ID, getId)
  yield takeEvery(types.ANALYTICS_TAG_TEMPLATE_UPDATE, update)
}

export default {
  analyticsTagTemplateSaga,
}
