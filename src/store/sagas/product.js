import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/product'
import * as actions from '../actions/product'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const productList = yield backend.service('product').find({
      query: {
        ...action.payload
      }
    })
    if(_.size(productList)) {
      yield put(actions.getSuccess(productList))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'message', 'Get product card list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const product = yield backend.service('product').get(id)
    if(_.size(product)) {
      yield put(actions.getIdSuccess(product))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get product card error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* create(action) {

  try {
    const createProduct = yield backend.service('product').create({
      ...action.payload
    })

    if(_.size(createProduct)) {

      yield put(actions.createSuccess(createProduct))
      yield put(push('/product-card'))                                                                                                
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Product card created',
      }) 
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create product card error'),
    })

    yield put(actions.createFailure(e))

  }

}

function* remove(action) {

  try {

    const id = action.payload

    const deleteCategory = yield backend.service('product').remove(id)

    if(_.size(deleteCategory)) {
      yield put(actions.removeSuccess(deleteCategory))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete product card error'),
    })

    yield put(actions.removeFailure(e))

  }

}

function* update(action) {
  try {

    let { id } = action.payload

    const updateWorkspace = yield backend.service('product').patch(id, action.payload)

    if(_.size(updateWorkspace)) {

      yield put(actions.updateSuccess(updateWorkspace))
      yield put(push('/product-card'))  
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Product card updated',
      })      
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update product card error'),
    })

    yield put(actions.updateFailure(e))

  }

}


function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })
    if(_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.searchFailure(e))

  }

}


function* productSaga() {

  yield takeEvery(types.GET, get)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.REMOVE, remove)
  yield takeEvery(types.SEARCH, search)
  yield takeEvery(types.UPDATE, update)
}

export default {
  productSaga,
}