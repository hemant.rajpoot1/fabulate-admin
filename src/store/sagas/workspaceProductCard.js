import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/workspaceProductCard'
import * as actions from '../actions/workspaceProductCard'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const page = action.payload.page
    const workspaceProductCardList = yield backend.service('workspace-product').find({
      query: {
        ...action.payload
      }
    })
    if(_.size(workspaceProductCardList)) {
      yield put(actions.getSuccess(workspaceProductCardList, page))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organization\'s product card list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* create(action) {
  try {
    const newWorkspaceProductCard = yield backend.service('workspace-product').create(action.payload)
    if(_.size(newWorkspaceProductCard)) {
      yield put(actions.createSuccess(newWorkspaceProductCard))
      yield put(push('/workspace-product-card'))
    }
    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Success!',
    })
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create product card for organization error'),
    })
    yield put(actions.createFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const workspaceProductCard = yield backend.service('workspace-product').get(id)
    if(_.size(workspaceProductCard)) {
      yield put(actions.getIdSuccess(workspaceProductCard))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organization\'s product card error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* remove(action) {
  try {
    const id = action.payload
    const deleteWorkspaceProductCard = yield backend.service('workspace-product').remove(id)
    if(_.size(deleteWorkspaceProductCard)) {
      yield put(actions.removeSuccess(deleteWorkspaceProductCard))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete error'),
    })
    yield put(actions.removeFailure(e))
  }
}

function* update(action) {
  try {
    const { id } = action.payload.formData

    const updateWorkspaceProductCard = yield backend.service('workspace-product').patch(id, action.payload.formData)

    if(_.size(updateWorkspaceProductCard)) {
      // yield put(actions.download(updateWorkspaceProductCard))
      yield put(actions.updateSuccess(updateWorkspaceProductCard))
      yield put(actions.get( action.payload.filter))
      yield put(push('/workspace-product-card'))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update error'),
    })
    yield put(actions.updateFailure(e))
  }
}

function* download(action) {
  try {
    const formData = action.payload
    const { id } = formData

    const updateWorkspaceProductCard = yield backend.service('workspace-product').patch(id, formData)

    const file = Buffer.from(updateWorkspaceProductCard)
    const blob = new Blob([file], {type: ''})

    const url = window.URL.createObjectURL(blob)
    const anchor = document.createElement('a')
    anchor.href = url
    anchor.download = 'Organization_product_cards.xlsx'
    anchor.click()
    window.URL.revokeObjectURL(url)

    yield put(actions.downloadSuccess())
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: e || 'Download error',
      hide: true,
      buttons: {
        closer: true,
        sticker: true,
      },
    })
    yield put(actions.downloadFailure())
  }
}


function* workspaceProductCardSaga() {
  yield takeEvery(types.WORKSPACE_PRODUCT_CARD_GET, get)
  yield takeEvery(types.WORKSPACE_PRODUCT_CARD_GET_ID, getId)
  yield takeEvery(types.WORKSPACE_PRODUCT_CARD_REMOVE, remove)
  yield takeEvery(types.WORKSPACE_PRODUCT_CARD_UPDATE, update)
  yield takeEvery(types.WORKSPACE_PRODUCT_CARD_CREATE, create)
  yield takeEvery(types.WORKSPACE_PRODUCT_CARD_DOWNLOAD, download)
}

export default {
  workspaceProductCardSaga,
}
