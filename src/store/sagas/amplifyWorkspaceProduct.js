import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/amplifyWorkspaceProduct'
import * as actions from '../actions/amplifyWorkspaceProduct'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const { page } = action.payload

    const amplifyWorkspaceProductList = yield backend.service('amplify-workspace-product').find({
      query: {
        ...action.payload
      }
    })

    if (_.size(amplifyWorkspaceProductList)) {
      yield put(actions.getSuccess(amplifyWorkspaceProductList, page))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get amplify organizations list error'),
    })

    yield put(actions.getFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const amplifyWorkspaceProduct = yield backend.service('amplify-workspace-product').get(id)

    if (_.size(amplifyWorkspaceProduct)) {
      yield put(actions.getIdSuccess(amplifyWorkspaceProduct))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get amplify organizations info error'),
    })

    yield put(actions.getIdFailure(e))
  }
}

function* create(action) {
  try {
    const newAmplifyWorkspaceProduct = yield backend.service('amplify-workspace-product').create(action.payload.params)

    if (_.size(newAmplifyWorkspaceProduct)) {
      yield put(actions.createSuccess(newAmplifyWorkspaceProduct))
      yield put(push('/amplify-workspace-product'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new amplify product for organization',
    })
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.createFailure(e))
  }
}

function* remove(action) {
  try {
    const { id, size, page } = action.payload
    const renderPage = size === 1 ? page - 1 : page
    const deleteAmplifyWorkspaceProduct = yield backend.service('amplify-workspace-product').remove(id)

    if (_.size(deleteAmplifyWorkspaceProduct)) {
      yield put(actions.removeSuccess(deleteAmplifyWorkspaceProduct))
      yield put(actions.get({ page: renderPage }))

      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Amplify product of organization deleted success',
      })
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete product of organization error'),
    })

    yield put(actions.removeFailure(e))
  }
}

function* update(action) {
  try {
    const { id, ...otherParams} = action.payload
    const updatedAmplifyWorkspaceProduct = yield backend.service('amplify-workspace-product').patch(id, otherParams)

    if (_.size(updatedAmplifyWorkspaceProduct)) {
      yield put(actions.updateSuccess(updatedAmplifyWorkspaceProduct))
      yield put(push('/amplify-workspace-product'))

      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Amplify product of organization updated success',
      })
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update amplify product of organization error'),
    })

    yield put(actions.updateFailure(e))
  }
}

function* amplifyWorkspaceProductSaga() {
  yield takeEvery(types.AMPLIFY_WORKSPACE_PRODUCT_GET, get)
  yield takeEvery(types.AMPLIFY_WORKSPACE_PRODUCT_CREATE, create)
  yield takeEvery(types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE, remove)
  yield takeEvery(types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID, getId)
  yield takeEvery(types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE, update)
}

export default {
  amplifyWorkspaceProductSaga
}