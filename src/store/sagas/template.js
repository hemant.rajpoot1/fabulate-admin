import { 
  put, 
  takeEvery, 
  // select 
} from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/template'
import * as actions from '../actions/template'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

// import { getTranslate } from 'react-localize-redux'

// function* translate() {
//   let store = yield select()
//   const translate = getTranslate(store.locale)
//   return translate
// }

function* get(action) {

  try {
    const templateList = yield backend.service('template').find({
      query: action.payload.queryParams
    })
    if(_.size(templateList)) {
      yield put(actions.getSuccess(templateList))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workflow list error'),
    })

    yield put(actions.getFailure(e))

  }

}


function* create(action) {

  try {
    const createTemplate = yield backend.service('template').create({
      ...action.payload
    })

    if(_.size(createTemplate)) {

      yield put(actions.createSuccess(createTemplate))
      yield put(push('/workflow'))                                                                                                
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Workflow created',
      }) 
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create workflow error'),
    })

    yield put(actions.createFailure(e))

  }

}

function* remove(action) {

  try {

    const id = action.payload

    const deleteTemplate = yield backend.service('template').remove(id)

    if(_.size(deleteTemplate)) {
      yield put(actions.removeSuccess(deleteTemplate))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete workflow error'),
    })

    yield put(actions.removeFailure(e))

  }

}


function* getId(action) {
  try {
    const id = action.payload

    const template = yield backend.service('template').get(id)
    if(_.size(template)) {

      yield put(actions.getIdSuccess(template))

    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workflow error'),
    })

    yield put(actions.getIdFailure(e))

  }
}

function* update(action) {
  try {

    let { id } = action.payload

    const updateWorkspace = yield backend.service('template').patch(id, action.payload)

    if(_.size(updateWorkspace)) {

      yield put(actions.updateSuccess(updateWorkspace))

      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Workflow updated',
      })      
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update workflow error'),
    })

    yield put(actions.updateFailure(e))

  }

}


function* templateSaga() {

  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.GET, get)
  yield takeEvery(types.REMOVE, remove)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.UPDATE, update)

}

export default {
  templateSaga,
}
