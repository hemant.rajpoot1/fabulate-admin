import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/skills'
import * as actions from '../actions/skills'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {

  try {
    const query = action.payload
    const skillsList = yield backend.service('skill').find({
      query
    })
    if(_.size(skillsList)) {
      yield put(actions.getSuccess(skillsList))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get skills list error'),
    })

    yield put(actions.getFailure(e))

  }

}

function* create(action) {
  const newSkillBody = action.payload.params
  const request = {
    'skillName': newSkillBody
  }
  try {
    const newSkill = yield backend.service('skill').create(request)

    if(_.size(newSkill)) {

      yield put(actions.createSuccess(newSkill))
      yield put(push('/skills'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new skill',
    })

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create skill error'),
    })

    yield put(actions.createFailure(e))

  }

}

function* getId(action) {
  try {
    const id = action.payload

    const skill = yield backend.service('skill').get(id)
    if(_.size(skill)) {

      yield put(actions.getIdSuccess(skill))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organization error'),
    })

    yield put(actions.getIdFailure(e))

  }
}

function* remove(action) {

  try {

    const id = action.payload

    const deleteSkill = yield backend.service('skill').remove(id)

    if(_.size(deleteSkill)) {
      yield put(actions.removeSuccess(deleteSkill))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete organization error'),
    })

    yield put(actions.removeFailure(e))

  }

}

function* update(action) {

  try {
    const { id } = action.payload
    const updateSkill = yield backend.service('skill').patch(id, action.payload)

    if(_.size(updateSkill)) {

      yield put(actions.updateSuccess(updateSkill))

      yield put(push('/skills'))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update skill error'),
    })

    yield put(actions.updateFailure(e))

  }

}


function* skillsSaga() {
  yield takeEvery(types.SKILLS_GET, get)
  yield takeEvery(types.SKILLS_GET_ID, getId)
  yield takeEvery(types.SKILLS_REMOVE, remove)
  yield takeEvery(types.SKILLS_UPDATE, update)
  yield takeEvery(types.SKILLS_CREATE, create)
}

export default {
  skillsSaga,
}
