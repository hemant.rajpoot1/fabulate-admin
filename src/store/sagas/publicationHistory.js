import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/publicationHistory'
import * as actions from '../actions/publicationHistory'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  const page = _.get(action, 'payload.page', 1)
  const publicationName = _.get(action, 'payload.publicationName')
  try {
    const publicationHistoryList = yield backend.service('publication-history').find({
      query: {
        page,
        publicationName
      }
    })
    if(_.size(publicationHistoryList)) {
      yield put(actions.getSuccess({
        ...publicationHistoryList,
        page
      }))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get publication history list error'),
    })
    yield put(actions.getFailure(e))
  }
}

function* create(action) {
  try {
    const newPublication = yield backend.service('publication-history').create(action.payload.params)

    if(_.size(newPublication)) {

      yield put(actions.createSuccess(newPublication))
      yield put(push('/publication-history'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new publication',
    })

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create publication error'),
    })

    yield put(actions.createFailure(e))

  }

}

function* getId(action) {
  try {
    const id = action.payload
    const publication = yield backend.service('publication-history').get(id)
    if(_.size(publication)) {
      yield put(actions.getIdSuccess(publication))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get publication error'),
    })
    yield put(actions.getIdFailure(e))
  }
}

function* remove(action) {

  try {
    const id = action.payload
    const removedPublication = yield backend.service('publication-history').remove(id)
    if(_.size(removedPublication)) {
      yield put(actions.removeSuccess(removedPublication))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete publication error'),
    })
    yield put(actions.removeFailure(e))
  }
}

function* update(action) {

  try {
    const { id } = action.payload
    const updatedPublication = yield backend.service('publication-history').patch(id, action.payload)
    if(_.size(updatedPublication)) {
      yield put(actions.updateSuccess(updatedPublication))
      yield put(push('/publication-history'))
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update publication error'),
    })
    yield put(actions.updateFailure(e))
  }

}


function* publicationHistorySaga() {
  yield takeEvery(types.PUBLICATION_HISTORY_GET, get)
  yield takeEvery(types.PUBLICATION_HISTORY_GET_ID, getId)
  yield takeEvery(types.PUBLICATION_HISTORY_REMOVE, remove)
  yield takeEvery(types.PUBLICATION_HISTORY_UPDATE, update)
  yield takeEvery(types.PUBLICATION_HISTORY_CREATE, create)
}

export default {
  publicationHistorySaga,
}
