import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/analyticsProject'
import * as actions from '../actions/analyticsProject'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const { page } = action.payload
    const analyticsProjectList = yield backend.service('analytics-project').find({
      query: {
        ...action.payload
      }
    })

    if (_.size(analyticsProjectList)) {
      yield put(actions.getSuccess(analyticsProjectList, page))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organizations list error'),
    })

    yield put(actions.getFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const analyticsProject = yield backend.service('analytics-project').get(id)

    if (_.size(analyticsProject)) {
      yield put(actions.getIdSuccess(analyticsProject))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get analytics project info error'),
    })

    yield put(actions.getIdFailure(e))
  }
}

function* create(action) {
  try {
    const newAnalyticsProject = yield backend.service('analytics-project').create(action.payload.params)

    if (_.size(newAnalyticsProject)) {
      yield put(actions.createSuccess(newAnalyticsProject))
      yield put(push('/analytics-project'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new analytics project',
    })
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.createFailure(e))
  }
}

function* remove(action) {
  try {
    const { id, size, page } = action.payload
    const renderPage = size === 1 ? page - 1 : page
    const deleteProject = yield backend.service('analytics-project').remove(id)

    if (_.size(deleteProject)) {
      yield put(actions.removeSuccess(deleteProject))
      yield put(actions.get({ page: renderPage }))

      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Analytics project deleted success',
      })
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete analytics project error'),
    })

    yield put(actions.removeFailure(e))
  }
}

function* update(action) {
  try {
    const { id, ...otherParams} = action.payload
    const updatedAnalyticsProject = yield backend.service('analytics-project').patch(id, otherParams)

    if (_.size(updatedAnalyticsProject)) {
      yield put(actions.updateSuccess(updatedAnalyticsProject))
      yield put(push('/analytics-project'))

      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Analytics project updated success',
      })
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update analytics project error'),
    })

    yield put(actions.updateFailure(e))
  }
}

function* updateDatasets(action) {
  try {
    const idAnalyticsProject = action.payload
    const updatedDatasets = yield backend.service('analytics-project/updateDatasets').create({
      query: {
        id: idAnalyticsProject
      }
    })

    yield put(actions.updateDatasetsSuccess(updatedDatasets))
  } catch(e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update datasets error')
    })

    yield put(actions.updateDatasetsFailure(e))
  }
}

function* analyticsProjectSaga() {
  yield takeEvery(types.ANALYTICS_PROJECT_GET, get)
  yield takeEvery(types.ANALYTICS_PROJECT_CREATE, create)
  yield takeEvery(types.ANALYTICS_PROJECT_REMOVE, remove)
  yield takeEvery(types.ANALYTICS_PROJECT_GET_ID, getId)
  yield takeEvery(types.ANALYTICS_PROJECT_UPDATE, update)
  yield takeEvery(types.ANALYTICS_PROJECT_UPDATE_DATASETS, updateDatasets)
}

export default {
  analyticsProjectSaga
}