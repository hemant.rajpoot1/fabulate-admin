import { put, takeEvery } from 'redux-saga/effects'

import _ from 'lodash'
import PNotify from 'pnotify'

import backend from 'store/api/feathers'

import types from 'store/constants/workroom'
import * as actions from 'store/actions/workroom'

import 'pnotify/dist/pnotify.buttons'

function* getId(action) {
  try {
    const query = action.payload

    const workroom = yield backend.service('workroom').find({
      query
    })
    if(_.size(workroom)) {
      yield put(actions.getIdSuccess(workroom))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workroom error'),
      hide: true,
      buttons: {
        closer: true,
        sticker: true,
      },       
    })

    yield put(actions.getIdFailure(e))

  }
}

function* update(action) {
  try {
    const data = action.payload

    const workroom = yield backend.service('workroom').patch(data.id, {...data})
   
    if(_.size(workroom)) {
      yield put(actions.updateSuccess(workroom))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workroom error'),
      hide: true,
      buttons: {
        closer: true,
        sticker: true,
      },       
    })

    yield put(actions.updateFailure(e))

  }
}


function* workroomSaga() {
  yield takeEvery(types.WORKROOM_GET_ID, getId)
  yield takeEvery(types.WORKROOM_UPDATE, update)

}

export default {
  workroomSaga
}
