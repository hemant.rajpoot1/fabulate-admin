import { put, takeEvery } from 'redux-saga/effects'

import types from 'store/constants/pastUserSessions'
import * as actions from 'store/actions/pastUserSessions'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

import moment from 'moment'
import _ from 'lodash'

function* get(action) {

  try {
    const filter = action.payload

    const query = {}

    if (_.get(filter, 'loginDate') || _.get(filter, 'loginTime')) {
      const loginDate = (_.get(filter, 'loginDate') || moment()).format('LL')
      const loginTime = _.get(filter, 'loginTime') !== null ? _.get(filter, 'loginTime', '').split(':') || [0, 0] : [0, 0]
      const loginAt = moment.utc(loginDate).set({ 'hours': loginTime[0], 'minutes': loginTime[1] }).format()
      query.loginAt = loginAt
    }

    if (_.get(filter, 'logoutDate') || _.get(filter, 'logoutTime')) {
      const logoutDate = (_.get(filter, 'logoutDate') || moment()).format('LL')
      const logoutTime = _.get(filter, 'logoutTime') !== null ? _.get(filter, 'logoutTime', '').split(':') || [0, 0] : [0, 0]
      const logoutAt = moment.utc(logoutDate).set({ 'hours': logoutTime[0], 'minutes': logoutTime[1] }).format()
      query.logoutAt = logoutAt
    }

    if (filter.userName) query.userName = filter.userName
    query.isActiveSession = false
    query.page = action.payload.page
    query.orderBy = filter.orderBy

    const pastSessions = yield backend.service('user-session').find({ query })
    
    yield put(actions.getSuccess(pastSessions))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'message', 'Get user sessions error'),
    })

    yield put(actions.getFailure(e))

  }

}

function* pastUserSessionsSaga() {
  yield takeEvery(types.GET, get)
}

export default {
  pastUserSessionsSaga,
}
