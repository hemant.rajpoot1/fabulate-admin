import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/task'
import * as actions from '../actions/task'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {

  try {

    const taskList = yield backend.service('task').find()
    if(_.size(taskList)) {
      yield put(actions.getSuccess(taskList))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get tasks list error'),
    })

    yield put(actions.getFailure(e))

  }

}

function* create(action) {
  try {
    const newWorkspace = yield backend.service('task').create(action.payload.params)

    if(_.size(newWorkspace)) {

      yield put(actions.createSuccess(newWorkspace))
      yield put(push('/task'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new task',
    })

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create task error'),
    })

    yield put(actions.createFailure(e))

  }

}

function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })
    if(_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.searchFailure(e))

  }

}

function* getId(action) {
  try {
    const id = action.payload

    const task = yield backend.service('task').get(id)
    if(_.size(task)) {

      yield put(actions.getIdSuccess(task))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get task error'),
    })

    yield put(actions.getIdFailure(e))

  }
}

function* remove(action) {

  try {

    const id = action.payload

    const deleteWorkspace = yield backend.service('task').remove(id)

    if(_.size(deleteWorkspace)) {
      yield put(actions.removeSuccess(deleteWorkspace))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete task error'),
    })

    yield put(actions.removeFailure(e))

  }

}

function* update(action) {

  try {

    let { id } = action.payload

    const updateWorkspace = yield backend.service('task').patch(id, action.payload)

    if(_.size(updateWorkspace)) {

      yield put(actions.updateSuccess(updateWorkspace))

      yield put(push('/task'))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update task error'),
    })

    yield put(actions.updateFailure(e))

  }

}


function* taskSaga() {
  yield takeEvery(types.TASK_GET, get)
  yield takeEvery(types.TASK_GET_ID, getId)
  yield takeEvery(types.TASK_REMOVE, remove)
  yield takeEvery(types.TASK_UPDATE, update)
  yield takeEvery(types.TASK_CREATE, create)
  yield takeEvery(types.TASK_SEARCH, search)
}

export default {
  taskSaga,
}
