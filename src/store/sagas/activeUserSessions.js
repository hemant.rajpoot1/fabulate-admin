import { put, takeEvery } from 'redux-saga/effects'

import types from 'store/constants/activeUserSessions'
import * as actions from 'store/actions/activeUserSessions'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

import moment from 'moment'
import _ from 'lodash'


function* get(action) {

  try {
    const filter = action.payload

    const query = {}

    if (_.get(filter, 'loginDate') || _.get(filter, 'loginTime')) {
      const loginDate = (_.get(filter, 'loginDate') || moment()).format('LL')
      const loginTime = _.get(filter, 'loginTime') !== null ? _.get(filter, 'loginTime', '').split(':') || [0, 0] : [0, 0]
      const loginAt = moment.utc(loginDate).set({ 'hours': loginTime[0], 'minutes': loginTime[1] }).format()
      query.loginAt = moment.utc(loginAt).format()
    }

    if (filter.userName) query.userName = filter.userName
    query.isActiveSession = true
    query.page = action.payload.page
    query.orderBy = filter.orderBy

    const activeSessions = yield backend.service('user-session').find({ query })

    yield put(actions.getSuccess(activeSessions))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'message', 'Get user sessions error'),
    })

    yield put(actions.getFailure(e))
  }
}

function* activeUserSessionsSaga() {
  yield takeEvery(types.GET, get)
}

export default {
  activeUserSessionsSaga,
}
