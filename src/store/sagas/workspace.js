import { put, takeEvery, call, all } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/workspace'
import * as actions from '../actions/workspace'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const { page } = action.payload
    const workspaceList = yield backend.service('workspace').find({
      query: {
        ...action.payload
      }
    })
     
    if (_.size(workspaceList)) {
      yield put(actions.getSuccess(workspaceList, page))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organizations list error'),
    })

    yield put(actions.getFailure(e))
  }
}

function* create(action) {
  try {
    const newWorkspace = yield backend.service('workspace').create(action.payload.params)

    if (_.size(newWorkspace)) {
      yield put(actions.createSuccess(newWorkspace))
      yield put(push('/workspace'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new organization',
    })

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Create organization error'),
    })

    yield put(actions.createFailure(e))
  }
}

function* getUsersWorkspace(id) {
  try {
    const workspaceUsers = yield backend.service('user').find({query:{ workspaceId: id}})
    yield put(actions.getUsersSuccess(_.keyBy(workspaceUsers.data, 'id')))
  } catch (e) {
    
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get users workspace error'),
    })

    yield put(actions.getUsersFailure(e))
  }
}

function* getId(action) {
  try {
    const {id, withUsers } = action.payload
  
    const workspace = yield backend.service('workspace').get(id)

    if(withUsers){
      yield call(getUsersWorkspace, id)
    }
  
    if (_.size(workspace)) {
      yield put(actions.getIdSuccess(workspace))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get organization error'),
    })

    yield put(actions.getIdFailure(e))
  }
}

function* remove(action) {
  try {
    const { id, workspacesLength, page } = action.payload
    const renderPage = workspacesLength === 1 ? page - 1 : page
    const deleteWorkspace = yield backend.service('workspace').remove(id)

    if (_.size(deleteWorkspace)) {
      yield put(actions.removeSuccess(deleteWorkspace))
      yield put(actions.get({ page: renderPage }))
    }
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete organization error'),
    })

    yield put(actions.removeFailure(e))
  }
}

function* removeUser(action) {
  try {
    const { userId, workspaceId } = action.payload
    const workspaceUser = yield backend.service('workspace-user').find({query:{
      workspaceId,
      userId,
    }})
  
    const { id } = workspaceUser
  
    if(id) {
      const removedWorkspaceUser = yield backend.service('workspace-user').remove(id)
      yield put(actions.removeUserSuccess(removedWorkspaceUser))
    } 
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Remove user to organization error'),
    })

    yield put(actions.removeUserFailure(e))
  }
}

function* addUser(action) {
  try {
    const { userId, workspaceId, userName } = action.payload
    const workspaceUserQueryResult = yield backend.service('workspace-user').create({userId, workspaceId})
    yield put(actions.addUserSuccess({...workspaceUserQueryResult, userName}))
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Add user to organization error'),
    })

    yield put(actions.addUserFailure(e))
  }
}

function* update(action) {
  try {
    const { id, users } = action.payload

    if(!_.isEmpty(users.deleted)) {
      yield all(_.map(users.deleted, userId => put(actions.removeUser({workspaceId: id, userId}))))
    }
    
    const addedUsers = _.values(users.added)
    
    if(!_.isEmpty(addedUsers)) {
      yield all(
        _.map(addedUsers, user => put(actions.addUser({userId: user.id, workspaceId: id, userName: user.userName})))
      )
    }

    const updateWorkspace = yield backend.service('workspace').patch(id, _.omit(action.payload, ['users']))

    if (_.size(updateWorkspace)) {
      yield put(actions.updateSuccess(updateWorkspace))
      yield put(push('/workspace'))
    }

    new PNotify({
      addclass: 'bg-success',
      title: 'Success',
      text: 'Update organization success',
    })
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update organization error'),
    })

    yield put(actions.updateFailure(e))
  }
}

function* search(action) {
  try {
    const amplifyProduct = action.payload.amplifyProduct

    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })
    if (_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }

      amplifyProduct ? yield put(actions.searchAmplifyProductSuccess(data.workspace)) : yield put(actions.searchSuccess(data))
    }

  } catch (e) {
    const amplifyProduct = action.payload.amplifyProduct

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    amplifyProduct ? yield put(actions.searchAmplifyProductFailure(e)) : yield put(actions.searchFailure(e))
  }
}

function* workspaceSaga() {
  yield takeEvery(types.WORKSPACE_GET, get)
  yield takeEvery(types.WORKSPACE_GET_ID, getId)
  yield takeEvery(types.WORKSPACE_REMOVE, remove)
  yield takeEvery(types.WORKSPACE_UPDATE, update)
  yield takeEvery(types.WORKSPACE_CREATE, create)
  yield takeEvery(types.WORKSPACE_SEARCH, search)
  yield takeEvery(types.WORKSPACE_UPDATE_ADD_USER, addUser)
  yield takeEvery(types.WORKSPACE_UPDATE_REMOVE_USER, removeUser)
}

export default {
  workspaceSaga,
}
