import { put, takeEvery } from 'redux-saga/effects'

import _ from 'lodash'
import PNotify from 'pnotify'

import backend from 'store/api/feathers'

import types from 'store/constants/payment'
import * as actions from 'store/actions/payment'

function* get(action) {

  try {
    const queryParams = action.payload.queryParams
    const page = action.payload.page

    const paymentList = yield backend.service('payment').find({
      query: {
        ...queryParams,
        page
      }
    })

    yield put(actions.getSuccess(paymentList))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get payment list error'),
    })

    yield put(actions.getFailure(e))

  }

}

function* getId(action) {
  try {
    const id = action.payload

    const payment = yield backend.service('payment').get(id)
    if(_.size(payment)) {

      yield put(actions.getIdSuccess(payment))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get cause error'),
    })

    yield put(actions.getIdFailure(e))

  }
}

function* update(action) {
  try {

    let { id } = action.payload
    const updateCause = yield backend.service('payment').patch(
      id,
      action.payload, 
    )

    if(_.size(updateCause)) {
      yield put(actions.updateSuccess(updateCause))
      new PNotify({
        addclass: 'bg-info',
        title: 'Information',
        text: 'Payment updated',
      })      
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Update payment error')
    })

    yield put(actions.updateFailure(e))

  }

}

function* paymentSaga() {
  yield takeEvery(types.PAYMENT_GET, get)
  yield takeEvery(types.PAYMENT_GET_ID, getId)
  yield takeEvery(types.PAYMENT_UPDATE, update)
}

export default {
  paymentSaga
}
