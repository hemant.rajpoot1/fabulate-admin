import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/cause'
import * as actions from '../actions/cause'

import PNotify from 'pnotify'
import 'pnotify/dist/pnotify.buttons'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const queryParams = action.payload.queryParams
    const page = action.payload.page
    const causeList = yield backend.service('cause').find({
      query: {
        ...queryParams,
        page
      }
    })
    if(_.size(causeList)) {
      yield put(actions.getSuccess(causeList, page || 0))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get causes list error'),
      buttons: {
        closer: true,
        sticker: true,
      },   
    })

    yield put(actions.getFailure(e))

  }

}

function* getDashboard(action) {

  try {
    const causeList = yield backend.service('cause').find({
      query: action.payload.queryParams
    })

    yield put(actions.getDashboardSuccess(causeList, action.payload.subName, _.size(causeList)))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get causes list error'),
      buttons: {
        closer: true,
        sticker: true,
      },  
    })

    yield put(actions.getDashboardFailure(e))

  }

}

function* getTask(action) {
  try {
    const causeTask = yield backend.service('task').get(action.payload.id, {
      query: action.payload.queryParams
    })
    yield put(actions.getTaskSuccess(causeTask))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get task error'),
      buttons: {
        closer: true,
        sticker: true,
      },      
    })

    yield put(actions.getTaskFailure(e))

  }
}


function* create(action) {
  try {

    const cause = { ...action.payload.cause }
    const history = { ...action.payload.history }
    const newCause = yield backend.service('cause').create({...cause})

    if(_.size(newCause)) {

      yield put(actions.createSuccess(newCause))

      yield put(actions.resetSelect(true))
      
      yield put(push('/dashboard'))  
      // yield put(push('/brief')) 
      const notice = new PNotify({
        addclass: 'bg-teal',
        text: `Your Brief "${newCause.causeName}" was successfully ${newCause.causeStatus === 0 ? 'saved in the brief section.' : 'posted in the marketplace.'}`,
        buttons: {
          closer: true,
          sticker: true,
        },        
      })   
      yield put(notice.get().on('click', () => {
        history.push(`/cause/${newCause.id}`)
      }))    
    }


  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Create brief error'),
      buttons: {
        closer: true,
        sticker: true,
      },     
    })

    yield put(actions.createFailure(e))

  }

}

function* uploadFile(action) {
  const formData = new FormData() 
  formData.append('uri', action.payload.params.uri)
  try {
    const uploadData = yield backend.service('uploads').create(formData, {
      headers: {
        //required param
        'Content-Type': 'multipart/form-data',
        'Content-Disposition': 'form-data',
      }
    })
    if(_.size(uploadData)) {
      yield put(actions.uploadFileSuccess({
        ...uploadData,
        fieldType: action.payload.params.fieldType
      }, action.payload.params.preview, action.payload.isCreate))
      new PNotify({
        addclass: 'bg-info custom',
        text: 'Attached file uploaded',
        buttons: {
          closer: true,
          sticker: true,
        },          
      }) 
    }
  } catch (e) {
    const msg = _.get(e, 'response.data.err', e.message)
    if(msg === 'Field value too long') {
      new PNotify({
        addclass: 'bg-danger',
        text: 'Attached file size is too large',
        buttons: {
          closer: true,
          sticker: true,
        },     
      })
    } else if(msg === 'Cannot read property \'MISSING_COMMA\' of undefined') {
      new PNotify({
        addclass: 'bg-danger',
        text: 'Attached file size can\'t be zero',
        buttons: {
          closer: true,
          sticker: true,
        },     
      })
    } else {
      new PNotify({
        addclass: 'bg-danger',
        title: 'Error',
        text: msg,
        buttons: {
          closer: true,
          sticker: true,
        },     
      })
    }
    yield put(actions.uploadFileFailure(e, action.payload.params.preview, action.payload.fieldType, action.payload.isCreate))
  }

}

function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })
    
    if(_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }


  } catch (e) {
  
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
      buttons: {
        closer: true,
        sticker: true,
      },        
    })

    yield put(actions.searchFailure(e))

  }

}

function* getId(action) {
  try {
    const id = action.payload

    const cause = yield backend.service('cause').get(id)
    if(_.size(cause)) {

      yield put(actions.getIdSuccess(cause))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get cause error'),
      buttons: {
        closer: true,
        sticker: true,
      },        
    })

    yield put(actions.getIdFailure(e))

  }
}

function* remove(action) {

  try {

    const id = action.payload

    const deleteWorkspace = yield backend.service('cause').remove(id)

    if(_.size(deleteWorkspace)) {
      yield put(actions.removeSuccess(deleteWorkspace))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Delete cause error'),
      buttons: {
        closer: true,
        sticker: true,
      },     
    })

    yield put(actions.removeFailure(e))

  }

}

function* update(action) {
  try {

    let { id } = action.payload.data
    const isPost = action.payload.isPost
    const updateCause = yield backend.service('cause').patch(
      id,
      action.payload.data, 
      {
        query: action.payload.data.query
      }
    )

    if(_.size(updateCause)) {
      yield put(actions.updateSuccess(updateCause))

      if(isPost) {
        new PNotify({
          addclass: 'bg-teal',
          text: `Your Brief "${updateCause.causeName}" was successfully posted in the marketplace.`,
          buttons: {
            closer: true,
            sticker: true,
          },        
        }) 
      } else {
        new PNotify({
          addclass: 'bg-info',
          text: 'Brief updated',
          buttons: {
            closer: true,
            sticker: true,
          },          
        })      
      }
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      text: _.get(e, 'response.data.err', e.message || 'Update cause error'),
      buttons: {
        closer: true,
        sticker: true,
      },     
    })

    yield put(actions.updateFailure(e))

  }

}

function* editAttach(action) {

  try {

    const id  = action.payload.params
    const editFileData = yield backend.service('media').remove(id, action.payload)

    if(_.size(editFileData)) {
      yield put(actions.editAttachSuccess())
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Remove file error'),
      buttons: {
        closer: true,
        sticker: true,
      },  
    })

    yield put(actions.editAttachFailure(e))

  }

}

function* downloadFile(action) {
  try {
    const id  = action.payload.id
    const aRef = action.payload.ref
    const downloadFileData = yield backend.service('download').get(id)

    if(downloadFileData.uri) {

      aRef.setAttribute('href', downloadFileData.uri)
      aRef.setAttribute('download', downloadFileData.id)
      aRef.click()

      yield put(actions.downloadFileSuccess())
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Download file error'),
      buttons: {
        closer: true,
        sticker: true,
      },  
    })

    yield put(actions.downloadFileFailure(e))

  }
}

function* generateAnalyticsPageviewsData (action) {
  const briefId = action.payload
  try {
    const result = yield backend.service('analytics-generate-pageviews-data').create({
      briefId
    })
    if (result) {
      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: result,
        buttons: {
          closer: true,
          sticker: true,
        },
      })
    }
  } catch(e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Generate analytics pageviews data error'),
      buttons: {
        closer: true,
        sticker: true,
      },  
    })
  }
}

function* generateAnalyticsFormSubmitData (action) {
  const briefId = action.payload
  try {
    const result = yield backend.service('analytics-generate-form-submit-data').create({
      briefId
    })
    if (result) {
      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: result,
        buttons: {
          closer: true,
          sticker: true,
        },
      })
    }
  } catch(e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Generate analytics data error'),
      buttons: {
        closer: true,
        sticker: true,
      },  
    })
  }
}

function* causeSaga() {
  yield takeEvery(types.CAUSE_GET, get)
  yield takeEvery(types.CAUSE_GET_DASHBOARD, getDashboard)
  yield takeEvery(types.CAUSE_GET_ID, getId)
  yield takeEvery(types.CAUSE_REMOVE, remove)
  yield takeEvery(types.CAUSE_UPDATE, update)
  yield takeEvery(types.CAUSE_CREATE, create)
  yield takeEvery(types.CAUSE_SEARCH, search)    
  yield takeEvery(types.UPLOAD, uploadFile)
  yield takeEvery(types.DOWNLOAD_FILE, downloadFile) 
  yield takeEvery(types.EDIT_FILE, editAttach)
  yield takeEvery(types.CAUSE_GET_TASK, getTask)
  yield takeEvery(types.CAUSE_GENERATE_ANALYTICS_PAGEVIEWS_DATA, generateAnalyticsPageviewsData)
  yield takeEvery(types.CAUSE_GENERATE_ANALYTICS_FORMSUBMIT_DATA, generateAnalyticsFormSubmitData)
}

export default {
  causeSaga
}
