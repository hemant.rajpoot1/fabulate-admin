import { put, takeEvery } from 'redux-saga/effects'

import types from '../constants/xeroContact'

import * as xeroContactActions from '../actions/xeroContact'

import backend from 'store/api/feathers'

function* getXeroContact(action) {

  try {
    const data = yield backend.service('xero-contact').find({
      query: {...action.payload}
    }) 

    if(data) {
      yield put(xeroContactActions.getXeroContactSuccess(data))
    }
  } catch (e) {

    yield put(xeroContactActions.getXeroContactFailure(e))
  }

}

function* xeroContactSaga() {

  yield takeEvery(types.XERO_CONTACTS, getXeroContact)

}

export default {
  xeroContactSaga
}
