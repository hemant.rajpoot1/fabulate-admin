import { put, takeEvery, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/asset'
import * as actions from '../actions/asset'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

import { getTranslate } from 'react-localize-redux'

function* translate() {
  let store = yield select()
  const translate = getTranslate(store.locale)
  return translate
}

function* get(action) {
  try {
    const { queryParams } = action.payload
    const assetsList = yield backend.service('asset').find({
      query: {
        ...queryParams
      }
    })

    if(_.size(assetsList)) {

      yield put(actions.getSuccess(assetsList))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get assets list error'),
    })

    yield put(actions.getFailure(e))

  }

}

function* create(action) {
  try {
    const newAsset = yield backend.service('asset').create(action.payload.params)

    if(_.size(newAsset)) {

      yield put(actions.createSuccess(newAsset))
      yield put(push('/asset'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'Add new asset',
    })
  } catch (e) {
    const translateFunc = yield translate()
    let err = e

    if(e && e.errors && e.errors.length > 0) {
      err = e.errors[0]
    }

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(err, 'response.data.err', translateFunc(`assetError.${err.message}`))
      // text: _.get(e, 'response.data.err', 'Create asset error'),
    })

    yield put(actions.createFailure(err))

  }

}

function* remove(action) {

  try {

    const id = action.payload

    const deleteAsset = yield backend.service('asset').remove(id)

    if(_.size(deleteAsset)) {

      yield put(actions.removeSuccess(deleteAsset))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete asset error'),
    })

    yield put(actions.removeFailure(e))

  }

}

function* getId(action) {

  try {

    const asset = yield backend.service('asset').get(action.payload)

    if(_.size(asset.data)) {

      yield put(actions.getIdSuccess(asset))

    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get asset error'),
    })

    yield put(actions.getIdFailure(e))

  }

}

function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })
    if(_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }


  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.searchFailure(e))

  }

}

function* uploadFile(action) {
  const formData = new FormData()
  formData.append('uri', action.payload.params.uri)
  try {
    const uploadData = yield backend.service('uploads').create(formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Content-Disposition': 'form-data',
      }
    })
    if(_.size(uploadData)) {
      yield put(actions.uploadFileSuccess(uploadData, action.payload.params.preview))
    }


  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.uploadFileFailure(e, action.payload.params.preview))

  }

}

function* update(action) {

  try {

    let { id } = action.payload
    const updateWorkspace = yield backend.service('asset').patch(id, action.payload)

    if(_.size(updateWorkspace)) {

      yield put(actions.updateSuccess(updateWorkspace))

      yield put(push('/asset'))

    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update asset error'),
    })

    yield put(actions.updateFailure(e))

  }

}

function* editAttach(action) {

  try {

    const id  = action.payload.params
    const editFileData = yield backend.service('media').remove(id, action.payload)

    if(_.size(editFileData)) {
      yield put(actions.editAttachSuccess())
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Remove file error'),
    })

    yield put(actions.editAttachFailure(e))

  }

}

function* downloadFile(action) {
  try {
    const id  = action.payload.id
    const aRef = action.payload.ref
    const downloadFileData = yield backend.service('uploads').get(id)

    if(downloadFileData.uri) {
      // document.location.href = downloadFileData.uri
      aRef.setAttribute('href', downloadFileData.uri)
      aRef.setAttribute('download', downloadFileData.id)
      aRef.click()

      yield put(actions.downloadFileSuccess())
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Download file error'),
    })

    yield put(actions.downloadFileFailure(e))

  }
}


function* assetSaga() {
  yield takeEvery(types.GET, get)
  yield takeEvery(types.REMOVE, remove)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.CREATE, create)
  yield takeEvery(types.ASSET_SEARCH, search)
  yield takeEvery(types.UPLOAD, uploadFile)
  yield takeEvery(types.ASSET_UPDATE, update)
  yield takeEvery(types.EDIT_FILE, editAttach)
  yield takeEvery(types.DOWNLOAD_FILE, downloadFile)
}

export default {
  assetSaga,
}
