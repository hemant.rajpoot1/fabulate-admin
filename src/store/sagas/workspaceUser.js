import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'

import types from '../constants/workspaceUser'
import * as actions from '../actions/workspaceUser'
import * as userActions from '../actions/user'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {
  try {
    const page = action.payload.page
    const workspaceUserList = yield backend.service('workspace-user').find({
      query: { page }
    })

    if (_.size(workspaceUserList)) {
      yield put(actions.getSuccess(workspaceUserList, page))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workspace-users list error'),
    })

    yield put(actions.getFailure(e))
  }

}

function* create(action) {
  try {
    const newWorkspace = yield backend.service('workspace-user').create(action.payload.params)

    if (_.size(newWorkspace)) {
      yield put(actions.createSuccess(newWorkspace))
      yield put(push('/workspace-user'))
    }

    new PNotify({
      addclass: 'bg-info',
      title: 'Information',
      text: 'New relation added successfully.',
    })
  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.createFailure(e))
  }
}

function* search(action) {
  try {
    const searchData = yield backend.service(`search/${action.payload.type}`).find({ query: action.payload.query })

    if (_.size(searchData)) {
      const data = {
        [action.payload.type]: searchData.data.rows
      }
      yield put(actions.searchSuccess(data))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.searchFailure(e))
  }
}

function* getId(action) {
  try {
    const id = action.payload
    const workspaceUser = yield backend.service('workspace-user').get(id)

    if (_.size(workspaceUser)) {
      yield put(actions.getIdSuccess(workspaceUser))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workspace-user info error'),
    })

    yield put(actions.getIdFailure(e))
  }
}

function* remove(action) {
  try {
    const { id, filter, isRelation } = action.payload
    const deleteWorkspaceUser = yield backend.service('workspace-user').remove(id)

    if (_.size(deleteWorkspaceUser)) {
      yield put(actions.removeSuccess(deleteWorkspaceUser))
    }

    if (isRelation) {
      yield put(actions.get(filter))
    } else {
      // reload user list
      yield put(userActions.get(filter))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Delete organization-user item error'),
    })

    yield put(actions.removeFailure(e))
  }
}

function* getSearch(action) {
  try {
    const workspaceList  = yield backend.service('workspace-user').find({
      query: {
        ...action.payload
      }
    })
    
    if (_.size(workspaceList)) {
      yield put(actions.getSearchUsersOfWorkspaceSuccess(workspaceList))
    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get workspace-users list error'),
    })

    yield put(actions.getSearchUsersOfWorkspaceFailure(e))
  }
}



function* workspaceUserSaga() {
  yield takeEvery(types.WORKSPACE_USER_GET, get)
  yield takeEvery(types.WORKSPACE_USER_GET_ID, getId)
  yield takeEvery(types.WORKSPACE_USER_REMOVE, remove)
  yield takeEvery(types.WORKSPACE_USER_CREATE, create)
  yield takeEvery(types.WORKSPACE_USER_SEARCH, search)
  yield takeEvery(types.SEARCH_USERS_OF_WORKSPACE, getSearch)
}

export default {
  workspaceUserSaga,
}
