import { put, takeEvery, select } from 'redux-saga/effects'

import _ from 'lodash'

import types from '../constants/causeUser'
import * as actions from '../actions/causeUser'
import * as userActions from '../actions/user'
import * as causeActions from '../actions/cause'

import PNotify from 'pnotify'

import backend from 'store/api/feathers'

function* get(action) {

  try {
    const filter = action.payload.queryParams
    const causeId = filter ? filter.causeId : null

    const pitchList = yield backend.service('causeUser').find({
      query: {
        ...filter
      },
    })

    yield put(actions.getSuccess(pitchList, causeId))

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get pitches list error'),
      hide: true,
      buttons: {
        closer: true,
        sticker: true,
      },        
    })

    yield put(actions.getFailure(e))
  }
}

function* getId(action) {

  try {

    const filter = {
      query: {
        id: action.payload,
      }
    }

    const user = yield backend.service('causeUser').find(filter)

    if (_.size(user.data)) {

      yield put(actions.getIdSuccess(user))

    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Get user error'),
    })

    yield put(actions.getIdFailure(e))

  }

}

function* create(action) {

  try {
    const causeUserCreate = yield backend.service('causeUser').create(
      action.payload.data, 
      {
        query: {
          ...action.payload.query
        }
      }
    )

    if (_.size(causeUserCreate)) {
      const store = yield select()
      const contractor = store.user.contractor[causeUserCreate.causeUser.contractorId]
      const cause = causeUserCreate.cause
      if(_.size(cause)){
        yield put(causeActions.getIdSuccess({...cause }))
      }

      if (contractor) {
        contractor.causeUserContractor = [{
          ...causeUserCreate.causeUser
        }]
        yield put(userActions.update({ ...contractor }, false))
      }

      yield put(actions.createSuccess(causeUserCreate.causeUser))


    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.createFailure(e))

  }

}

function* update(action) {

  try {
    const data = action.payload.data
    const causeUserUpdate = yield backend.service('causeUser').patch(data.id, data)
    const pnotifyText = action.payload.pnotifyText

    if (_.size(causeUserUpdate)) {
      yield put(actions.updateSuccess(causeUserUpdate))

      if (pnotifyText) {
        new PNotify({
          addclass: 'bg-success',
          title: 'Success!',
          text: pnotifyText,
          hide: true,
          buttons: {
            closer: true,
            sticker: true,
          },
        })
      }
    }
  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
      hide: true,
      buttons: {
        closer: true,
        sticker: true,
      },
    })

    yield put(actions.updateFailure(e))
  }
}

function* remove(action) {

  try {
    const causeUserRemove = yield backend.service('causeUser').remove(action.payload)

    if (_.size(causeUserRemove)) {
      const store = yield select()
      const contractor = store.user.contractor[causeUserRemove.contractorId]
      const cause = causeUserRemove.cause 
     
      if(contractor) {
        delete contractor.causeUserContractor
        yield put(userActions.update({ ...contractor }, false))
      }

      if(_.size(cause)) {

        yield put(causeActions.getIdSuccess({...cause }))
      }

      yield put(actions.removeSuccess(causeUserRemove))


    }

  } catch (e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message),
    })

    yield put(actions.removeFailure(e))

  }

}


function* causeUserSaga() {

  yield takeEvery(types.GET, get)

  yield takeEvery(types.GET_ID, getId)

  yield takeEvery(types.CREATE, create)

  yield takeEvery(types.REMOVE, remove)

  yield takeEvery(types.UPDATE, update)
  
}

export default {
  causeUserSaga,
}
