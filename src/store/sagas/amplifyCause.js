import { put, takeEvery } from 'redux-saga/effects'
import { push } from 'react-router-redux'

import _ from 'lodash'
import PNotify from 'pnotify'

import types from 'store/constants/amplifyCause'
import * as actions from 'store/actions/amplifyCause'
import backend from 'store/api/feathers'

import 'pnotify/dist/pnotify.buttons'

function* get(action) {

  try {
  
    const amplifyCauseList = yield backend.service('amplify-cause').find({
      query: {
       ...action.payload
      } 
    })
    if(_.size(amplifyCauseList)) {
      yield put(actions.getSuccess(amplifyCauseList))
    }

  } catch (e) {

    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get amplify causes list error'),
      buttons: {
        closer: true,
        sticker: true,
      },   
    })

    yield put(actions.getFailure(e))
  }
}

function* getId(action) {

  try{

    const amplifyCause = yield backend.service('amplify-cause').find({
      query: {
        ...action.payload
      }
    })

    if(_.size(amplifyCause)){
      yield put(actions.getIdSuccess(amplifyCause))
    }
  } catch(e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', e.message || 'Get amplify cause list error'),
      buttons: {
        closer: true,
        sticker: true,
      },   
    })

    yield put(actions.getIdFailure(e))
  }
}

function* update(action) {

  try {
    const { id, data } = action.payload
    const updateLegalPage = yield backend.service('amplify-cause').patch(id, data)
    if (_.size(updateLegalPage)) {
      new PNotify({
        addclass: 'bg-success',
        title: 'Success',
        text: 'Amplify cause updated successfully',
      })
      
      yield put(actions.updateSuccess(updateLegalPage))
      yield put(push('/amplify-cause'))
    }
  } catch(e) {
    new PNotify({
      addclass: 'bg-danger',
      title: 'Error',
      text: _.get(e, 'response.data.err', 'Update amplify cause error'),
    })

    yield put(actions.updateFailure(e))
  }
}


function* amplifyCauseSaga() {
  yield takeEvery(types.GET, get)
  yield takeEvery(types.GET_ID, getId)
  yield takeEvery(types.UPDATE, update)
}

export default {
  amplifyCauseSaga
}
