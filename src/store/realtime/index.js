// import React, { Component } from 'react'
// import { Link } from 'react-router-dom'

import io from 'socket.io-client'

import { store } from 'store/store'
import * as causeActions from 'store/actions/cause'
import * as userActions from 'store/actions/user'

const apiUrl = `${process.env['REACT_APP_API_SCHEME']}://` +
  `${process.env['REACT_APP_API_HOST']}:${process.env['REACT_APP_API_PORT']}`


export default (user, history) => {
  const socket = io(apiUrl, {})
  let socketId
  // const causeActionsBind = bindActionCreators(causeActions)

  socket.on('soketId', socketData => {
    socketId = socketData.socketId
    socket.emit('user', { ...user, socketId })
    console.log('socket connection start')
  })

  socket.on('CAUSE_CREATE_SUCCESS', data => {
    store.dispatch(causeActions.createSuccess(data))
  })

  socket.on('CAUSE_REMOVE_SUCCESS', data => {
    // console.log('CAUSE_REMOVE', data)
    store.dispatch(causeActions.removeSuccess(data))
  })

  socket.on('CAUSE_UPDATE_SUCCESS', data => {
    // console.log('CAUSE_UPDATE', data)
    store.dispatch(causeActions.updateSuccess(data, true))
    store.dispatch(causeActions.getIdSuccess({data}, true)) 
  })  

  socket.on('UPDATE_USER_BY_SOCKET', data => {
    // console.log('UPDATE_USER_BY_SOCKET data', data)   
    store.dispatch(userActions.updateUserBySocket(data))
  }) 

}