import types from '../constants/analyticsProject'

import _ from 'lodash'

const fetching = {
  isGetFetching: false,
  isGetIdFetching: false,
}

const initialState = {
  origin: {},
  error: null,
  page: 1,
  count: 0,
  validation: {
    name: {
      value: '',
      isValid: undefined,
    },
    projectId: {
      value: '',
      isValid: undefined,
    },
    readKey: {
      value: '',
      isValid: undefined,
    },
    writeKey: {
      value: '',
      isValid: undefined,
    },
    masterKey: {
      value: '',
      isValid: undefined,
    },
  },
  filter: {
    projectName: undefined,
  },
  ...fetching,
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.ANALYTICS_PROJECT_GET:
  {
    return {
      ...state,
      isGetFetching: true,
    }
  }  
  
  case types.ANALYTICS_PROJECT_GET_SUCCESS:
  { 
    return {
      ...state,
      origin:_.keyBy(action.payload.data, 'id'),
      page: action.payload.page, 
      count: action.payload.count,
      isGetFetching: false
    }
  }

  case types.ANALYTICS_PROJECT_SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  } 
  
  case types.ANALYTICS_PROJECT_SEARCH_SELECT:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...state.search.selected,
          ...payload 
        }
      }
    }
  }
  
  case types.ANALYTICS_PROJECT_SEARCH_SELECT_RESET:  
  {
    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...initialState.search.selected,
        }
      }
    }
  }

  case types.ANALYTICS_PROJECT_REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }

  case types.ANALYTICS_PROJECT_GET_ID:
  {
    return {
      ...state,
      isGetIdFetching: true,
    }
  }

  case types.ANALYTICS_PROJECT_GET_ID_SUCCESS:  
  {
    const payload = action.payload

    const edit = {
      name: payload.name,
      projectId: payload.projectId,
      readKey: payload.readKey,
      writeKey: payload.writeKey,
      masterKey: payload.masterKey
    }

    const validation = _.transform(edit, (result, value, key) => {
      result[key] = {
        value,
        isValid: true,
      }
    }, {})

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      validation,
      isGetIdFetching: false,
    }
  }

  case types.ANALYTICS_PROJECT_UPDATE_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
    }
  }

  case types.ANALYTICS_PROJECT_SET_FILTER:
  {
    return {
      ...state,
      filter: {
        ...state.filter,
        ...action.payload
      }
    }
  }

  case types.ANALYTICS_PROJECT_RESET_FILTER:
  {
    return {
      ...state,
      filter: {
        ...state.filter,
        projectName: undefined,
      }
    }
  }

  case types.VALIDATION: 
  {
    const data = action.payload.data
    return {
      ...state,
      validation: {
        ...state.validation,
        ...data
      }
    }
  }

  case types.ANALYTICS_PROJECT_CLEAR_VALIDATION_AND_SEARCH:
  {
    return {
      ...state,
      validation: initialState.validation,
      search: initialState.search,
    }
  }

  case types.ANALYTICS_PROJECT_UPDATE_DATASETS_SUCCESS:
  {
    const { data } = action.payload
    const { id } = data
    
    return {
      ...state,
      origin: {
        ...state.origin,
        [id]: data
      }
    }
  }

  case types.ANALYTICS_PROJECT_GET_FAILURE:
  case types.ANALYTICS_PROJECT_SEARCH_FAILURE:
  case types.ANALYTICS_PROJECT_CREATE_FAILURE:
  case types.ANALYTICS_PROJECT_REMOVE_FAILURE:
  case types.ANALYTICS_PROJECT_GET_ID_FAILURE:
  case types.ANALYTICS_PROJECT_UPDATE_FAILURE:
  case types.ANALYTICS_PROJECT_UPDATE_DATASETS_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }

  default:
    return state
  }
}