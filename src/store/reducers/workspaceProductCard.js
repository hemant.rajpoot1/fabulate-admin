import types from '../constants/workspaceProductCard'
import authTypes from '../constants/auth'
import _ from 'lodash'

const validationParams = {
  workspaceId: {
    value: '',
    isValid: undefined,
  },
  productCardId: {
    value: '',
    isValid: undefined,
  },
  buyerPrice: {
    value: null,
    isValid: undefined,
  },
  contractorPrice: {
    value: null,
    isValid: undefined,
  },
  fabulateCommission: {
    value: 10,
    isValid: undefined,
  },
  unlimitedPrice: {
    value: null,
    isValid: undefined,
  }
}

const initialState = {
  edit: {
    isEdit: false,
  },
  filter: {
    workspaceId: undefined,
  },
  origin: {},
  validationCreate: {
    ...validationParams
  },
  validationEdit: {
    ...validationParams
  },
  sort: {
    value: null,
    name: null
  },
  page: 1,
  count: 0,
  isGetFetching: false
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.WORKSPACE_PRODUCT_CARD_GET: {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_GET_SUCCESS:
  {
    return {
      ...state,
      origin: _.keyBy(action.payload.data, 'id'),
      count: action.payload.count,
      page: action.payload.page,
      isGetFetching: false
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_GET_ID: {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_GET_ID_SUCCESS:
  {
    const payload = action.payload
    const toArr = Object.entries(validationParams)
    const nextValidModel = toArr.reduce((obj, item) => {
      const key = item[0]
      obj[key] = {
        ...validationParams[key],
        value: !_.isNil(payload[key]) ? payload[key] : validationParams[key].value,
        isValid: true
      }
      return obj
    }, {})
    return {
      ...state,
      origin: {
        ...state.origin,
        [payload.id]: payload
      },
      validationEdit: {
        ...validationParams,
        ...nextValidModel
      },
      isGetFetching: false
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_REMOVE: {
    let newWorkspaceProductCard = {
      ...state.origin
    }
    newWorkspaceProductCard = _.omitBy(newWorkspaceProductCard,
      workspaceProductCard => workspaceProductCard.id === action.payload)
    return {
      ...state,
      isGetFetching: true,
      origin: {
        ...newWorkspaceProductCard
      },
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_REMOVE_SUCCESS:
  {
    return {
      ...state,
      isGetFetching: false
    }
  }


  case types.WORKSPACE_PRODUCT_CARD_EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload },
      validationEdit: {
        ...action.payload
      }
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_SET_IS_EDIT: {
    return {
      ...state,
      edit: {
        ...state.edit,
        isEdit: action.payload,
      },
      validationEdit: {
        ...action.payload
      }
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_CREATE: {
    return {
      ...state,
      isGetFetching: true
    }
  }


  case types.WORKSPACE_PRODUCT_CARD_CREATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
      },
      validationCreate: {
        ...validationParams
      },
      isGetFetching: false
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_UPDATE:
  {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_UPDATE_SUCCESS:
  {
    return {
      ...state,
      validationEdit: {
        ...validationParams
      },
      isGetFetching: false
    }
  }
  case types.WORKSPACE_PRODUCT_CARD_DOWNLOAD_SUCCESS:
  {
    return {
      ...state,
      edit: {
        isEdit: false
      }
    }
  }  

  case types.VALIDATION:
  {
    const { productCardId, type, newModel, isCreate, data } = action.payload

    const validationType = isCreate ? 'validationCreate' : 'validationEdit'

    if (isCreate) {
      return {
        ...state,
        [validationType]: {
          ...state[validationType],
          ...data
        }
      }
    }
    else {
      return {
        ...state,
        [validationType]: {
          ...state[validationType],
          [productCardId]: {
            ...state.validationEdit[productCardId],
            [type]: { ...newModel }
          }
        }
      }
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_SEARCH_SUCCESS:
  {
    const payload = action.payload

    return {
      ...state,
      search: {
        ...state.search,
        ...payload
      }
    }
  }

  case types.SET_SORT: {
    const payload = action.payload
    return {
      ...state,
      sort: {
        ...payload
      }
    }
  }

  case types.WORKSPACE_PRODUCT_CARD_GET_FAILURE:
  case types.WORKSPACE_PRODUCT_CARD_REMOVE_FAILURE:
  case types.WORKSPACE_PRODUCT_CARD_GET_ID_FAILURE:
  case types.WORKSPACE_PRODUCT_CARD_UPDATE_FAILURE:
  case types.WORKSPACE_PRODUCT_CARD_SEARCH_FAILURE:
  case types.WORKSPACE_PRODUCT_CARD_GET_USERS_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }

  case types.SET_FILTER:
  {
    return {
      ...state,
      list: {
        ...initialState.list
      },
      filter: {
        ...state.filter,
        ...action.payload
      }
    }
  }

  case types.RESET_FILTER:
  {
    return {
      ...state,
      origin: {},
      filter: {
        ...initialState.filter,
      }
    }
  }

  default:
    return state
  }
}
