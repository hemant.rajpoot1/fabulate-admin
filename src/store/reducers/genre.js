import types from '../constants/genre'
import authTypes from '../constants/auth'
import _ from 'lodash'

const initialState = {
  list: {},
  edit: {},
  origin: {},
  count: 0,
  page: 1,
  validation: {
    genreName: {
      value: '',
      isValid: undefined,
    }      
  },
  filter: {},
  isGetFetching: false  
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.GENRE_GET:
  {
    return {
      ...state, isGetFetching: true
    }
  }  

  case types.GENRE_GET_SUCCESS:
  {
    const { data, count, page } = action.payload
    return {
      ...state,
      list: _.keyBy(data, 'id'),
      page,
      count,
      isGetFetching: false
    }
  }

  case types.GENRE_REMOVE_SUCCESS:
  {
    let newGenreList = {...state.list}
    newGenreList = _.omitBy(newGenreList, genre => genre.id === action.payload.id)
    return {
      ...state,
      list: {...newGenreList}
    }
  }

  case types.GENRE_GET_ID_SUCCESS:  
  {
    const payload = action.payload
    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      edit: { 
        ...payload 
      }
    }
  } 

  case types.GENRE_EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload }
    }
  }

  case types.VALIDATION: 
  {
    const data = action.payload.data
    return {
      ...state,
      validation: {
        ...state.validation,
        ...data
      }
    }
  } 

  case types.GENRE_GET_FAILURE:
  case types.GENRE_REMOVE_FAILURE:
  case types.GENRE_GET_ID_FAILURE:
  case types.GENRE_UPDATE_FAILURE:
  case types.GENRE_GET_USERS_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }  

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }
  
  case types.SET_FILTER:
  {
    return {
      ...state, filter: {
        ...state.filter, ...action.payload
      }
    }
  }

  case types.RESET_FILTER:
  {
    return {
      ...state, filter: {
        ...initialState.filter
      }
    }
  }

  default:
    return state
  }
}
