import types from '../constants/amplifyProduct'
import _ from 'lodash'

const validationParams = {
  name: {
    value: '',
    isValid: undefined,
  },
  icon: {
    value: '',
    isValid: undefined
  },
  shortDescription: {
    value: '',
    isValid: undefined
  },
  longDescription: {
    value: '',
    isValid: undefined
  },
  impressionGoal: {
    value: 0,
    isValid: undefined
  },
  clickGoal: {
    value: 0,
    isValid: undefined
  },
  promotion: {
    value: '',
    isValid: undefined,
  },         
  included: {
    value: '',
    isValid: undefined
  },
  termsAndCondition: {
    value: '',
    isValid: undefined,
  },
  uniqueAudience: {
    value: 0.00,
    isValid: undefined
  },
  pageViews: {
    value: 0,
    isValid: undefined
  },
  sessionsPerUser: {
    value: 0.00,
    isValid: undefined
  },
  perPersonMonthly: {
    value: 0.00,
    isValid: undefined
  },
  impressionsPerDay: {
    value: '',
    isValid: undefined,
  },
  monthlyActiveUsers: {
    value: '',
    isValid: undefined,
  },
  dailyIntentSignal: {
    value: '',
    isValid: undefined,
  },
  examples: {
    value: [],
    isValid: undefined
  },
  templateId: {
    value: null,
    isValid: undefined
  },
  workspaceId: {
    value: null,
    isValid: undefined
  },
  toAllOrganizations: {
    value: 0,
    isValid: undefined
  },
  buyerPrice: {
    value: null,
    isValid: true
  },
  amplifyPrice: {
    value: null,
    isValid: true
  },
  fabulateCommission: {
    value: null,
    isValid: true
  }
}

const initialState = {
  origin: {},
  error: null,
  attachments: [],
  validationEdit: {
    ...validationParams
  },
  validationCreate: {
    ...validationParams
  },
  search: {
    selected: {
      template: {}
    },
    template: []
  },
  filter: {
    productName: ''
  },
  sort: {
    value: null,
    name: null
  },
  isGetFetching: false,
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.GET: 
  {
    return {
      ...state, isGetFetching: true
    }
  }

  case types.GET_SUCCESS:
  {
    return {
      ...state,
      origin: _.keyBy(action.payload.data, 'id'),
      isGetFetching: false
    }

  }

  case types.GET_ID_SUCCESS:
  {
    const attachments = state.attachments.slice()

    const payload = action.payload.data
    attachments[0] = {
      preview: payload.icon  
    }

    return {
      ...state,
      attachments,
      validationEdit: {
        name: {
          value: payload.name,
          isValid: true
        },
        icon: {
          value: payload.icon,
          isValid: true
        },
        shortDescription: {
          value: payload.shortDescription,
          isValid: true
        },
        longDescription: {
          value: payload.longDescription,
          isValid: true
        },
        impressionGoal: {
          value: payload.impressionGoal,
          isValid: true
        },
        clickGoal: {
          value: payload.clickGoal,
          isValid: true
        },
        promotion: {
          value: payload.promotion,
          isValid: true
        },
        included: {
          value: payload.included,
          isValid: true
        },       
        termsAndCondition: {
          value: payload.termsAndCondition,
          isValid: true
        },
        uniqueAudience: {
          value: payload.uniqueAudience,
          isValid: undefined,
        },  
        pageViews: {
          value: payload.pageViews,
          isValid: undefined
        },
        sessionsPerUser: {
          value: payload.sessionsPerUser,
          isValid: undefined
        },  
        perPersonMonthly: {
          value: payload.perPersonMonthly,
          isValid: undefined
        },
        impressionsPerDay: {
          value: payload.impressionsPerDay,
          isValid: undefined,
        },
        monthlyActiveUsers: {
          value: payload.monthlyActiveUsers,
          isValid: undefined,
        },
        dailyIntentSignal: {
          value: payload.dailyIntentSignal,
          isValid: undefined,
        },
        examples: {
          value: payload.examples ? payload.examples : [],
          isValid: undefined
        },
        templateId: {
          value: payload.templateId,
          isValid: undefined
        }, 
        workspaceId: {
          value: payload.workspaceId,
          isValid: undefined
        },
        toAllOrganizations: {
          value: payload.toAllOrganizations,
          isValid: undefined
        },
        buyerPrice: {
          value: payload.buyerPrice,
          isValid: undefined
        },
        amplifyPrice: {
          value: payload.amplifyPrice,
          isValid: undefined
        },
        fabulateCommission: {
          value: payload.fabulateCommission,
          isValid: undefined
        }
      }
    }

  }  

  case types.CREATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        },
      },
      validationCreate: {
        ...initialState.validationCreate
      }
    }

  }

  case types.UPDATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        },
      },
    }

  }

  case types.REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }    
  

  case types.SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  }    
  
  case types.SEARCH_SELECT:  
  {
    const selected = action.payload.selected
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'

    return {
      ...state,
      [subName]: {
        ...state[subName],
        templateId: {
          value: selected,
          isValid: true
        }
      }
    }
  }
  
  case types.SEARCH_SELECT_RESET:  
  {
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'    

    return {
      ...state,
      [subName]: {
        ...initialState[subName],
      },      

    }
  } 
  
  case types.VALIDATION: 
  {
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'
    const data = action.payload.data

    return {
      ...state,
      [subName]: {
        ...state[subName],
        ...data
      }
    }
  }

  case types.SET_FILTER: 
  {
    return {
      ...state,
      filter: action.payload,
      isGetFetching: true
    }
  }

  case types.RESET_FILTER: 
  {
    return {
      ...state, filter: {
        ...initialState.filter
      }
    }
  }

  case types.SET_SORT:
  {
    return {
      ...state,
      sort: action.payload
    }
  }

  case types.ATTACH:
  {
    const validType = action.payload.isCreate ? 'validationCreate' : 'validationEdit'
    const prevAttachments = state[validType].examples.value
    const params = action.payload.params
    const isAttachments = action.payload.isAttachments

    return isAttachments ? {
      ...state, 
      attachments:  [ action.payload.params[0] ]
    } : {
      ...state,
      [validType]: {
        ...state[validType],
        examples: {
          value: prevAttachments.concat(params)
        },
      },
    }

  }  

  case types.REMOVE_FILE:
  {
    const prevAttachmentsIcon = state.attachments.slice()
    const validType = action.payload.isCreate ? 'validationCreate' : 'validationEdit'
    const prevAttachments = state[validType].examples.value
    const params = action.payload.params
    const isAttachments = action.payload.isAttachments

    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === params
    })

    isAttachments ? prevAttachmentsIcon.splice(index, 1) : prevAttachments.splice(index, 1)

    return isAttachments ? {
      ...state,
      attachments: prevAttachmentsIcon,
      [validType]: {
        ...state[validType], icon: {
          value: prevAttachmentsIcon
        }
      }
    } : {
      ...state,
      [validType]: {
        ...state[validType],
        examples: {
          value: prevAttachments
        },
      },
    }

  }   

  case types.UPLOAD:
  {
    const attachmentsIcon = state.attachments.slice()
    const validType = action.payload.isCreate ? 'validationCreate' : 'validationEdit'
    const isAttachments = action.payload.isAttachments
    const prevAttachments = state[validType].examples.value
    const params = action.payload.params
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === params.preview
    })

    prevAttachments[index] = isAttachments ?  {
      name: attachmentsIcon[0].name,
      preview: attachmentsIcon[0].preview,
      isFetching: true,
      isError: false
    } : {
      name: prevAttachments[index].name,
      preview: prevAttachments[index].preview,
      isFetching: true,
      isError: false
    }

    return isAttachments ? {
      ...state, [validType]: {
        ...state[validType], 
        icon: {
          value: prevAttachments
        }
      }
    } : {
      ...state,
      [validType]: {
        ...state[validType],
        examples: {
          value: prevAttachments
        },
      },
      
    }

  } 
  case types.UPLOAD_SUCCESS:
  {
    const validType = action.payload.isCreate ? 'validationCreate' : 'validationEdit'
    const prevAttachments = state[validType].examples.value
    const preview = action.payload.preview
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === preview
    })
    prevAttachments[index].isFetching = false
    prevAttachments[index].isError = false
    prevAttachments[index].storeId = action.payload.data.id

    return {
      ...state,
      [validType]: {
        ...state[validType],
        examples: {
          value: prevAttachments
        },
      },
    }

  }
  case types.UPLOAD_FAILURE:
  {
    const validType = action.payload.isCreate ? 'validationCreate' : 'validationEdit'
    const prevAttachments = state[validType].examples.value
    const preview = action.payload.preview
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === preview
    })

    prevAttachments[index].isFetching = false
    prevAttachments[index].isError = true

    return {
      ...state,
      [validType]: {
        ...state[validType],
        examples: {
          value: prevAttachments
        },
      },
    }

  } 

  case types.EDIT_FILE:
  {
    const validType = action.payload.isCreate ? 'validationCreate' : 'validationEdit'
    const prevAttachments = state[validType].examples.value
    const params = action.payload.params
    const index = prevAttachments.findIndex((element, index) => {
      return element.id === params
    })


    prevAttachments.splice(index, 1)

    return {
      ...state,
      [validType]: {
        ...state[validType],
        examples: {
          value: prevAttachments
        },
      },
    }

  }   

  default:

    return state

  }

}
