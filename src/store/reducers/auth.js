import types from '../constants/auth'
import userTypes from '../constants/user'

const initialState = {
  accessToken: null,
  user:        null,
  company:     null,
  error:       null,
  edit: {}
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.INIT_USER_EDIT:

  {
    return {
      ...state,
      edit: {
        ...state.user
      },
    }

  }

  case types.USER_EDIT:

  {

    return {
      ...state,
      edit: {
        ...state.edit,
        ...action.payload
      },
    }

  }  

  case types.USER_INFO_UPDATE_SUCCESS:

  {

    return {
      ...state,
      user: action.payload,
      edit: {
        ...state.edit,
        image: action.payload.image
      }
    }

  }


  case types.USER_GET_SUCCESS:

  {

    return {
      ...state,
      user: action.payload,
    }

  }

  case types.USER_UPLOAD_PHOTO_SUCCESS:

  {

    return {
      ...state,
      user: action.payload,
      edit: {
        ...state.edit,
        image: action.payload.image
      }      
    }

  }  

  case types.SIGNIN_SUCCESS:

  {
    return {
      ...state,
      accessToken: action.payload,
    }

  }

  case types.SIGNOUT_SUCCESS:

  {
    return {
      ...initialState,
    }

  }

  case types.SIGNIN_FAILURE:
  case types.SIGNOUT_FAILURE:

  {

    return {
      ...state,
      error: action.payload.error,
    }

  }
  

  case types.SIGNUP_FAILURE:
  case types.RESET_PASSWORD_FAILURE:
  case types.VERIFY_USER_BY_EMAIL_FAILURE: 
  case types.USER_INFO_UPDATE_FAILURE: 
  
  {
    return {
      ...state,
      error: action.payload.error,
    }
  }

  case types.UPDATE_PASSWORD_SUCCESS:
  {

    return {
      ...state,
      //accessToken: action.payload.accessToken,
    }

  }

  case userTypes.UPDATE_USER_BY_SOCKET:
  {
    const payload = action.payload
    let nextAuth = {}

    if(state.user.id === payload.id) {
      nextAuth = {
        user: {
          ...state.user,
          ...action.payload
        },
        edit: {
          ...state.edit,
          ...action.payload
        } 
      }
    }

    return {
      ...state,
      ...nextAuth,     
    }

  }  

  default:

    return state

  }

}
