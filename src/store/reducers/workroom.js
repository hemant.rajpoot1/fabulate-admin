import types from '../constants/workroom'
import authTypes from '../constants/auth'

//validation model
const validationParams = {
  id: {
    value: '',
    isValid: undefined,
  },
  contractorId: {
    value: '',
    isValid: undefined,
  },  
  buyerId: {
    value: '',
    isValid: undefined,
  },    
  workItems: {
    value: [],
    isValid: undefined,
  },
  workroomItems: {
    value: null,
    isValid: undefined,
  },  
  workItemsBuyer: {
    value: [],
    isValid: undefined,
  },
  milestone: {
    value: {},
    isValid: undefined,
  },  
  feedback: {
    value: '',
    isValid: undefined,
  },
  causeId: {
    value: null,
    isValid: undefined,
  },
  attachments: []
}

//flags for check loaded data
const fetching = {
  isGetIdFetching: false,
}

const initialState = {
  error: null,
  validationEdit: {   
    ...validationParams                                               
  },
  info: {},
  ...fetching
}

export default (state = initialState, action) => {

  switch (action.type) {
  case types.WORKROOM_GET_ID:
  { 
    return {
      ...state,
      validationEdit: {
        ...initialState.validationEdit
      },  
      info: {},    
      isGetIdFetching: true
    }    
  }

  case types.WORKROOM_GET_ID_SUCCESS:
  {
    const payload = action.payload
    if(!(payload && payload[0])) {
      return {
        ...state,
        validationEdit: {
          ...initialState.validationEdit
        },
        info: { 
          ...initialState.info
        }
      }
    }

    //data coercion to a validation object
    const toArr = Object.entries(validationParams)
    const data = payload[0] || {}
    const nextValidModel = toArr.reduce((obj, item) => {
      
      const key = item[0]
      obj[key] = {
        value: data[key],
        isValid: true
      }

      return obj
    }, {})

    return {
      ...state,
      validationEdit: {
        ...nextValidModel
      },
      info: {
        causeWorkroom: {
          ...data.causeWorkroom
        },
        buyerWorkroom: {
          ...data.buyerWorkroom
        },
        contractorWorkroom: {
          ...data.contractorWorkroom
        },                
      },
      isGetIdFetching: false
    }
  }

  case types.WORKROOM_GET_ID_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetIdFetching: false      
    }
  }  
  
  case types.WORKROOM_UPDATE_SUCCESS:
  {
    const payload = action.payload
    //data coercion to a validation object
    const toArr = Object.entries(validationParams)
    const nextValidModel = toArr.reduce((obj, item) => {
      const key = item[0]
      obj[key] = {
        value: payload[key],
        isValid: true
      }

      return obj
    }, {})

    return {
      ...state,
      validationEdit: {
        ...nextValidModel
      },
      info: {
        ...payload.causeWorkroom
      }       
    }
  }

  case types.VALIDATION:
  {
    return {
      ...state,
      validationEdit: { 
        ...state.validationEdit, 
        ...action.payload.data 
      }     
    }

  } 

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }

  case types.UPLOAD_SUCCESS:
  {
    const { data, productInfo, currentIndex } = action.payload
    const { id } = productInfo
    const prevAttachments = [...state.validationEdit.workroomItems.value[id].workItems[currentIndex].attachments]
    const preview = data.preview
    const index = prevAttachments.findIndex(element => {
      return element.preview === preview
    })
    prevAttachments[index].isFetching = false
    prevAttachments[index].isError = false
    prevAttachments[index].storeId = data.id
    prevAttachments[index].mediaName = data.fileName
    
    return {
      ...state
    }
  }

  case types.UPLOAD_FAILURE:
  {
    const prevAttachments = [...state.attachments]
    const preview = action.payload.preview
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === preview
    })
    prevAttachments[index].isFetching = false
    prevAttachments[index].isError = true

    return {
      ...state,
      attachments: prevAttachments
    }
  }

  default:
    return state
  }
}
