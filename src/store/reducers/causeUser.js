import types from '../constants/causeUser'
import authTypes from '../constants/auth'
import _ from 'lodash'

const initialState = {
  origin: {},
  causePitches: {},
  edit: {}
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.GET:
  { 
    return {
      ...state,
      isGetFetching: true
    }    
  }

  case types.GET_SUCCESS:

  {
    const causeId = action.payload.causeId
    const data = action.payload.data
    
    let nextState = {
      origin: _.keyBy(action.payload.data, 'id'),
      ...state
    }

    if(causeId) {
      nextState = {
        origin: _.keyBy(action.payload.data, 'id'),
        ...state,
        causePitches: {
          ...state.causePitches,
          [causeId]: {
            ...state.causePitches[causeId],
            ..._.keyBy(data, 'id')
          }
        },
        isGetFetching: false
      }     
    }

    return {
      ...nextState,
    }

  }

  case types.GET_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false      
    }
  } 

  case types.GET_ID_SUCCESS:

  {
    const payload = action.payload.data[0]
    return {
      ...state,
      origin: {...state.origin, ...{[payload.id]: payload}},
      edit: { ...payload }
    }

  }

  case types.CREATE_SUCCESS:

  {

    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      }
    }
  }  

  case types.UPDATE_SUCCESS:

  {
    const payload = action.payload.data

    return {
      ...state,
      causePitches: {
        ...state.causePitches,
        [payload.causeId]: {
          ...state.causePitches[payload.causeId],
          [payload.id]: {
            ...payload
          }
        }
      },
      isUpdateFetching: false
    }
  }    

  case types.REMOVE_SUCCESS:

  {

    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }  

  case types.GET_ID_FAILURE:
  case types.CREATE_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }

  }

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }

  default:

    return state

  }

}
