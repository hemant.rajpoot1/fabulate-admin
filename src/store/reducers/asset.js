import types from '../constants/asset'
import authTypes from '../constants/auth'

import _ from 'lodash'

const initialState = {
  origin: {},
  error: null,
  edit: {},
  filter: {
    causeId: undefined,
    assetSearch: undefined,
  },
  validation: {
    causeName: {
      value: '',
      isValid: undefined,
    },
    assetName: {
      value: '',
      isValid: undefined,
    },
  },
  search: {
    selected: {
      cause: {}
    },
    cause: [],
  },
  attachments: []
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.GET_SUCCESS:

  {

    return {
      ...state,
      origin: _.keyBy(action.payload, 'id'),
    }

  }

  case types.CREATE_SUCCESS:

  {

    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        }
      },
    }

  }  

  case types.REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }

  }

  case types.GET_ID_SUCCESS:

  {
  
    const payload = action.payload.data[0]
    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      edit: { 
        id: payload.id, 
        assetName: payload.assetName
      },
      search: {
        selected: {
          cause: {
            id: payload.causeAsset.id,
            name: payload.causeAsset.causeName            
          }
        }
      },   
      validation: {
        causeName: {
          value: payload.causeAsset.causeName,
          isValid: payload.causeAsset.causeName ? undefined : false,
        },   
        assetName: {
          value: payload.assetName || '',
          isValid: payload.assetName ? true : false,
        },            
      },
      attachments: payload.mediaAsset 
    }
  } 

  case types.SET_FILTER:

  {

    return {
      ...state,
      filter: { 
        ...state.filter, 
        ...action.payload 
      }
    }

  } 

  case types.RESET_FILTER:

  {

    return {
      ...state,
      filter: { 
        ...state.filter, 
        assetSearch: undefined,
      }
    }

  } 
  
  case types.UPLOAD:
  {
    const prevAttachments = state.attachments.slice()
    const params = action.payload.params
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === params.preview
    })
    prevAttachments[index] = {
      name: prevAttachments[index].name,
      preview: prevAttachments[index].preview,
      isFetching: true,
      isError: false
    }
    
    return {
      ...state,
      attachments: prevAttachments
    }

  } 
  case types.UPLOAD_SUCCESS:
  {
    const prevAttachments = state.attachments.slice()
    const preview = action.payload.preview
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === preview
    })

    prevAttachments[index].isFetching = false
    prevAttachments[index].isError = false
    prevAttachments[index].storeId = action.payload.data.id

    return {
      ...state,
      attachments: prevAttachments
    }

  }
  case types.UPLOAD_FAILURE:
  {
    const prevAttachments = state.attachments.slice()
    const preview = action.payload.preview
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === preview
    })

    prevAttachments[index].isFetching = false
    prevAttachments[index].isError = true

    return {
      ...state,
      attachments: prevAttachments
    }

  } 


  case types.ATTACH:
  {
    const prevAttachments = state.attachments
    const params = action.payload.params
    return {
      ...state,
      attachments: prevAttachments.concat(params)
    }

  }  
  
  case types.REMOVE_FILE:
  {
    const prevAttachments = state.attachments.slice()
    const params = action.payload.params
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === params
    })


    prevAttachments.splice(index, 1)

    return {
      ...state,
      attachments: prevAttachments
    }

  }    

  case types.EDIT_FILE:
  {
    const prevAttachments = state.attachments.slice()
    const params = action.payload.params
    const index = prevAttachments.findIndex((element, index) => {
      return element.id === params
    })


    prevAttachments.splice(index, 1)

    return {
      ...state,
      attachments: prevAttachments
    }

  }      

  case types.ASSET_SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  }    
  
  case types.ASSET_SEARCH_SELECT:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...state.search.selected,
          ...payload 
        }
      }
    }
  }
  
  case types.ASSET_SEARCH_SELECT_RESET:  
  {

    return {
      ...state,
      validation: {
        ...initialState.validation,
      },
      search: { 
        ...state.search,
        selected: {
          ...initialState.search.selected,
        }
      },
      attachments: []
    }
  }   

  case types.VALIDATION:

  {

    return {
      ...state,
      validation: { 
        ...state.validation, 
        ...action.payload 
      }
    }

  }

  case types.ASSET_EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload }
    }
  }  
  

  case types.GET_FAILURE:
  case types.REMOVE_FAILURE:
  case types.GET_ID_FAILURE:
  case types.CREATE_FAILURE:
  case types.ASSET_UPDATE_FAILURE:

  {

    return {
      ...state,
      error: action.payload,
    }

  }

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }

  default:

    return state

  }

}
