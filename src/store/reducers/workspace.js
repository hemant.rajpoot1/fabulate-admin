import types from '../constants/workspace'
import authTypes from '../constants/auth'
import _ from 'lodash'

const fetching = {
  isGetFetching: false,
  isGetIdFetching: false,
}

const initialState = {
  origin: {},
  error: null,
  edit: {
    isEdit: false,
    users: {
      added: {},
      deleted: [],
    }
  },
  page: 1,
  count: 0,
  users: {},
  attachments: [], 
  search: {
    user: [],
    amplifyProduct: []
  },
  filter: {
    workspaceName: undefined,
  },
  validation: {
    name: {
      value: '',
      isValid: undefined,
    },
    commission: {
      value: 10,
      isValid: undefined,
    },
    xeroId: {
      value: null,
      isValid: undefined,
    },
    accountCode: {
      value: 200,
      isValid: undefined,
    },
    paymentTerms: {
      value: 3,
      isValid: undefined,
    },
    bankNumber: {
      value: '',
      isValid: undefined,
    },
    gst: {
      value: false,
      isValid: undefined,
    },
    abn: {
      value: 0,
      isValid: undefined,
    },
    analyticsProjectId: {
      value: null,
      isValid: undefined
    },
    isUnlimited: {
      value: false,
      isValid: undefined
    },
    copyProductCardOrganization: {
      value: null,
      isValid: undefined
    },
    icon: {
      value: null,
      isValid: undefined
    },
    shortDescription: {
      value: null,
      isValid: undefined
    },
    longDescription: {
      value: null,
      isValid: undefined
    },
    workspaceType: {
      value: 0,
      isValid: undefined
    },
    createAmplifyProductsOrganisation: {
      value: null,
      isValid: undefined
    }
  },
  ...fetching,
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.WORKSPACE_GET_SUCCESS:
  { 
    return {
      ...state,
      origin:_.keyBy(action.payload.data, 'id'),
      page: action.payload.page, 
      count: action.payload.count
    }
  }

  case types.WORKSPACE_REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }

  case types.WORKSPACE_GET_ID:
  {
    return {
      ...state,
      isGetIdFetching: true,
    }
  }

  case types.WORKSPACE_GET_ID_SUCCESS:  
  {
    const payload = action.payload
    const attachments = state.attachments.slice()

    attachments[0] = {
      preview: payload.icon  
    }

    return {
      ...state,
      attachments,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      edit: {
        ...state.edit,
        ...payload,
      },
      isGetIdFetching: false,
    }
  }

  case types.WORKSPACE_GET_USERS_SUCCESS:
  {
    const payload = action.payload
    return {
      ...state,
      users: payload,
    }
  }

  case types.WORKSPACE_EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload }
    }
  }

  case types.VALIDATION:
  {
    return {
      ...state,
      validation: { 
        ...state.validation, 
        ...action.payload 
      }
    }
  }

  case types.WORKSPACE_CREATE: {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.WORKSPACE_CREATE_SUCCESS: {
    const payload = action.payload
    return {
      ...state,
      origin: {
        ...state.origin,
        [payload.id]: payload
      },
      validation: initialState.validation,
      isGetFetching: false,
    }
  }

  case types.WORKSPACE_UPDATE: {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.WORKSPACE_UPDATE_SUCCESS:
  {
    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin,
        [payload.id]: payload
      },
      isGetFetching: false
    }
  }

  case types.WORKSPACE_EDIT_SET_IS_EDIT: {
    return {
      ...state,
      edit: {
        ...state.edit,
        isEdit: action.payload,
      }
    }
  }

  case types.WORKSPACE_SEARCH_SUCCESS:
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  }


  case types.WORKSPACE_UPDATE_ADD_USER_SUCCESS:
  {
    const newUser = action.payload
    const newAdded = _.remove(state.edit.users.added, user => !_.isEqual(newUser.id, user.id))


    return {
      ...state,
      users: {
        ...state.users,
        [newUser.id]: {
          ...newUser,
        }
      },
      edit: {
        ...state.edit,
        users: {
          ...state.edit.users,
          added: _.keyBy(newAdded, 'id'),
        }
      },
    }
  }

  case types.WORKSPACE_UPDATE_REMOVE_USER_SUCCESS: {
    const removedUser = action.payload
    const newUsers = _.filter(state.users, user => !_.isEqual(removedUser.userId, user.id))
    return {
      ...state,
      edit: {
        ...state.edit,
        users: {
          ...state.edit.users,
          deleted: _.difference(state.edit.deleted, [removedUser.id]),
        },
      },
      users: _.keyBy(newUsers, 'id'),
    }
  }

  case types.WORKSPACE_GET_FAILURE:
  case types.WORKSPACE_CREATE_FAILURE:
  case types.WORKSPACE_REMOVE_FAILURE:
  case types.WORKSPACE_GET_ID_FAILURE:
  case types.WORKSPACE_UPDATE_FAILURE:
  case types.WORKSPACE_GET_USERS_FAILURE:
  case types.WORKSPACE_SEARCH_FAILURE:
  case types.WORKSPACE_UPDATE_ADD_USER_FAILURE:
  case types.WORKSPACE_SEARCH_AMPLIFY_PRODUCT_FAILURE:
  case types.WORKSPACE_UPDATE_REMOVE_USER_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false,
      isGetIdFetching: false
    }
  }

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }

  case types.SET_FILTER:
  {
    return {
      ...state,
      list: {
        ...initialState.list
      },
      filter: {
        ...state.filter,
        ...action.payload
      }
    }
  }

  case types.RESET_FILTER:
  {
    return {
      ...state,
      filter: {
        ...initialState.filter,
      }
    }
  }

  case types.ATTACH: 
  {
    return {
      ...state, 
      attachments: [ action.payload.params[0] ]
    }
  }

  case types.REMOVE_FILE:
  {
    const prevAttachments = state.attachments.slice()
    const params = action.payload.params
    const index = prevAttachments.findIndex((element, index) => {
      return element.preview === params
    })

    prevAttachments.splice(index, 1)

    return {
      ...state,
      attachments: prevAttachments
    }
  }
  
  case types.UPLOAD:
  {
    const attachments = state.attachments.slice()
    
    attachments[0] = {
      name: attachments[0].name,
      preview: attachments[0].preview,
      isFetching: true,
      isError: false
    }
    
    return {
      ...state,
      attachments
    }
  } 

  case types.UPLOAD_SUCCESS:
  {
    const attachments = state.attachments.slice()
    
    attachments[0].isFetching = false
    attachments[0].isError = false
    attachments[0].storeId = action.payload.data.id

    return {
      ...state,
      attachments
    }
  }

  case types.UPLOAD_FAILURE:
  {
    const attachments = state.attachments.slice()

    attachments[0].isFetching = false
    attachments[0].isError = true

    return {
      ...state,
      attachments
    }
  }

  case types.RESET_ATTACHMENTS: 
  {
    return {
      ...state, attachments: []
    }
  }

  case types.WORKSPACE_SEARCH_AMPLIFY_PRODUCT_SUCCESS:
  {
    return {
      ...state, search: {
        ...state.search, amplifyProduct: {
          ...state.search.amplifyProduct, ...action.payload
        }
      }
    }
  }

  default:
    return state
  }
}
