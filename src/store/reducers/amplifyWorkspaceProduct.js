import types from '../constants/amplifyWorkspaceProduct.js'

import _ from 'lodash'

const fetching = {
  isGetFetching: false,
  isGetIdFetching: false,
}

const initialState = {
  origin: {},
  error: null,
  page: 1,
  count: 0,
  validation: {
    workspaceId: {
      value: '',
      isValid: undefined,
    },
    amplifyProductId: {
      value: '',
      isValid: undefined,
    },
    buyerPrice: {
      value: 0,
      isValid: undefined
    },
    amplifyPrice: {
      value: 0,
      isValid: undefined
    },
    fabulateCommission: {
      value: 10,
      isValid: undefined
    },
  },
  filter: {
    amplifyProductName: undefined
  },
  ...fetching,
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.AMPLIFY_WORKSPACE_PRODUCT_GET:
  {
    return {
      ...state,
      isGetFetching: true
    }
  }
  
  case types.AMPLIFY_WORKSPACE_PRODUCT_GET_SUCCESS:
  {
    return {
      ...state,
      count: action.payload.count,
      origin:_.keyBy(action.payload.data, 'id'),
      page: action.payload.page,
      isGetFetching: false
    }
  }

  case types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID:
  {
    return {
      ...state,
      isGetIdFetching: true,
    }
  }
  
  case types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID_SUCCESS:  
  {
    const payload = action.payload

    const edit = {
      workspaceId: payload.workspaceId,
      amplifyProductId: payload.amplifyProductId,
      buyerPrice: payload.buyerPrice,
      amplifyPrice: payload.amplifyPrice,
      fabulateCommission: payload.fabulateCommission
    }

    const validation = _.transform(edit, (result, value, key) => {
      result[key] = {
        value,
        isValid: true,
      }
    }, {})

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      validation,
      isGetIdFetching: false,
    }
  }

  case types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE:
  {
    return {
      ...state,
      isGetIdFetching: true
    }
  }

  case types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin},
      isGetIdFetching: false
    }
  }

  case types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE:
  {
    return {
      ...state,
      isGetIdFetching: true
    }
  }

  case types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      isGetIdFetching: true
    }
  }

  case types.VALIDATION: 
  {
    const data = action.payload.data
    return {
      ...state,
      validation: {
        ...state.validation,
        ...data
      }
    }
  }

  case types.AMPLIFY_WORKSPACE_PRODUCT_GET_FAILURE:
  case types.AMPLIFY_WORKSPACE_PRODUCT_CREATE_FAILURE:
  case types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE_FAILURE:
  case types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID_FAILURE:
  case types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false,
      isGetIdFetching: false
    }
  }

  case types.SET_FILTER: 
  {
    return {
      ...state, filter: {
        ...state.filter, ...action.payload
      }, isGetFetching: true
    }
  }

  case types.RESET_FILTER: 
  {
    return {
      ...state, filter: {
      ...initialState.filter
      }, isGetFetching: true
    }
  }

  default:
    return state
  }
}