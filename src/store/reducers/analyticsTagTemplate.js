import types from '../constants/analyticsTagTemplate'

import _ from 'lodash'

const fetching = {
  isGetFetching: false,
  isGetIdFetching: false,
}

const initialState = {
  origin: {},
  error: null,
  page: 1,
  count: 0,
  validation: {
    surveyScript: {
      value: '',
      isValid: undefined,
    },
    workspaces: {
      value: {},
      isValid: true
    }
  },
  search: {
    selected: {
      workspace: {},
    },
    workspace: [],
  },
  ...fetching,
}

export default (state = initialState, action) => {
  switch (action.type) {
  
  case types.ANALYTICS_TAG_TEMPLATE_GET_SUCCESS:
  { 
    return {
      ...state,
      origin:_.keyBy(action.payload.data, 'id'),
      page: action.payload.page, 
      count: action.payload.count
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  } 
  
  case types.ANALYTICS_TAG_TEMPLATE_SEARCH_SELECT:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...state.search.selected,
          ...payload 
        }
      }
    }
  }
  
  case types.ANALYTICS_TAG_TEMPLATE_SEARCH_SELECT_RESET:  
  {
    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...initialState.search.selected,
        }
      }
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_GET_ID:
  {
    return {
      ...state,
      isGetIdFetching: true,
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_GET_ID_SUCCESS:  
  {
    const payload = action.payload

    const edit = {
      surveyScript: payload.surveyScript,
      workspaces: _.keyBy(_.get(payload, 'workspacesAnalyticsTagTemplate', []), 'id')
    }

    const validation = _.transform(edit, (result, value, key) => {
      result[key] = {
        value,
        isValid: true,
      }
    }, {})

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      validation,
      isGetIdFetching: false,
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_SET_VALIDATION_WORKSPACES:
  {
    return {
      ...state,
      validation: {
        ...state.validation,
        workspaces: {
          value: _.keyBy(action.payload, 'id'),
          isValid: true
        }
      }
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_UPDATE_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
    }
  }

  case types.VALIDATION: 
  {
    const data = action.payload.data
    return {
      ...state,
      validation: {
        ...state.validation,
        ...data
      }
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_CLEAR_VALIDATION_AND_SEARCH:
  {
    return {
      ...state,
      validation: initialState.validation,
      search: initialState.search,
    }
  }

  case types.ANALYTICS_TAG_TEMPLATE_GET_FAILURE:
  case types.ANALYTICS_TAG_TEMPLATE_SEARCH_FAILURE:
  case types.ANALYTICS_TAG_TEMPLATE_CREATE_FAILURE:
  case types.ANALYTICS_TAG_TEMPLATE_REMOVE_FAILURE:
  case types.ANALYTICS_TAG_TEMPLATE_GET_ID_FAILURE:
  case types.ANALYTICS_TAG_TEMPLATE_UPDATE_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }

  default:
    return state
  }
}
