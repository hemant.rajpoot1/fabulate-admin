import types from 'store/constants/gettyImage'

const initialState = {
  gettyLimit: '',
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.SAVE_GETTY_LIMIT:
  { 
    return {
      ...state,
      gettyLimit: action.payload,
    }
  }

  default:
    return state
  }
}
