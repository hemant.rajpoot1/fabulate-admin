import types from '../constants/workspaceUser'
import authTypes from '../constants/auth'
import _ from 'lodash'

const initialState = {
  origin: {},
  page: 1,
  count: 0,
  error: null,
  validation: {
    name: {
      value: '',
      isValid: undefined,
    },
    userName: {
      value: '',
      isValid: undefined,
    },    
  },
  search: {
    selected: {
      user: {},
      workspace: {}
    },
    workspace: [],
    user: []
  },
  filter: {
    userName: undefined
  }  
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.WORKSPACE_USER_GET_SUCCESS:
  { 
    return {
      ...state,
      origin:_.keyBy(action.payload.data, 'id'),
      page: action.payload.page, 
      count: action.payload.count
    }
  }

  case types.WORKSPACE_USER_REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }

  case types.WORKSPACE_USER_GET_ID_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      edit: { 
        ...payload 
      }
    }
  }  

  case types.VALIDATION:
  {
    return {
      ...state,
      validation: { 
        ...state.validation, 
        ...action.payload 
      }
    }
  } 

  case types.WORKSPACE_USER_SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  }    
  
  case types.WORKSPACE_USER_SEARCH_SELECT:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...state.search.selected,
          ...payload 
        }
      }
    }
  }
  
  case types.WORKSPACE_USER_SEARCH_SELECT_RESET:  
  {
    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...initialState.search.selected,
        }
      }
    }
  }  

  case types.WORKSPACE_USER_GET_FAILURE:
  case types.WORKSPACE_USER_CREATE_FAILURE:
  case types.WORKSPACE_USER_REMOVE_FAILURE:
  case types.WORKSPACE_USER_GET_ID_FAILURE:
  case types.WORKSPACE_USER_UPDATE_FAILURE:
  case types.WORKSPACE_USER_GET_USERS_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }  

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }
  
  case types.SET_FILTER: {
    return {
      ...state, filter: {
        ...state.filter, ...action.payload
      }
    }
  }
  

  case types.SEARCH_USERS_OF_WORKSPACE_SUCCESS: {
    return {
      ...state,
      origin:_.keyBy(action.payload.data, 'id'),
      page: action.payload.page, 
      count: action.payload.count
    }
  }

  case types.SEARCH_USERS_OF_WORKSPACE_FAILURE:

  // eslint-disable-next-line no-fallthrough
  case types.RESET_FILTER: {
    return {
      ...state,
      filter: {
        ...initialState.filter
      }
    }
  }

  default:
    return state
  }
}
