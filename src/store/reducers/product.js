import types from '../constants/product'
import _ from 'lodash'

const validationParams = {
  productDisplayName: {
    value: '',
    isValid: undefined,
  },
  productCardName: {
    value: '',
    isValid: undefined,
  },
  sizeLabel: {
    value: '',
    isValid: undefined
  },
  sizeOrder: {
    value: 0,
    isValid: undefined
  },
  descriptionText: {
    value: '',
    isValid: undefined
  },
  productSequence: {
    value: null,
    isValid: undefined
  },
  defaultPrice: {
    value: 0,
    isValid: undefined,
  },         
  templateId: {
    value: '',
    isValid: undefined
  },
  contentCategory: {
    value: [],
    isValid: undefined,
  },
  genre: {
    value: [],
    isValid: undefined
  }
}

const initialState = {
  origin: {},
  filter: {},
  sort: {},
  error: null,
  validationEdit: {
    ...validationParams
  },
  validationCreate: {
    ...validationParams
  },
  search: {
    selected: {
      template: {}
    },
    template: []
  },
  isGetFetching: false, 
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.GET:
  {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.GET_SUCCESS:

  {
    return {
      ...state,
      origin: _.keyBy(action.payload.data, 'id'),
      isGetFetching: false
    }

  }

  case types.GET_ID_SUCCESS:

  {
    const payload = action.payload.data
    return {
      ...state,
      validationEdit: {
        productDisplayName: {
          value: payload.productDisplayName,
          isValid: true
        },
        productCardName: {
          value: payload.productCardName,
          isValid: true
        },
        sizeLabel: {
          value: payload.sizeLabel,
          isValid: true
        },
        sizeOrder: {
          value: payload.sizeOrder,
          isValid: true
        },
        descriptionText: {
          value: payload.descriptionText,
          isValid: true
        },
        productSequence: {
          value: payload.productSequence,
          isValid: true
        },
        templateId: {
          value: payload.templateId,
          isValid: true
        },       
        defaultPrice: {
          value: payload.defaultPrice,
          isValid: true
        },
        contentCategory: {
          value: payload.contentCategory 
            ? [
              ...payload.contentCategory
            ]
            : [],
          isValid: undefined,
        },  
        genre: {
          value: payload.genre
            ? [
              ...payload.genre
            ]
            : [],
          isValid: undefined
        }                  
      }
    }

  }  

  case types.CREATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        },
      },
      validationCreate: {
        ...initialState.validationCreate
      }
    }

  }

  case types.UPDATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        },
      },
    }

  }

  case types.REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }    
  

  case types.SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  }    
  
  case types.SEARCH_SELECT:  
  {
    const selected = action.payload.selected
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'

    return {
      ...state,
      [subName]: {
        ...state[subName],
        templateId: {
          value: selected,
          isValid: true
        }
      }
    }
  }
  
  case types.SEARCH_SELECT_RESET:  
  {
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'    

    return {
      ...state,
      [subName]: {
        ...initialState[subName],
      },      

    }
  } 
  
  case types.VALIDATION: 
  {
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'
    const data = action.payload.data

    return {
      ...state,
      [subName]: {
        ...state[subName],
        ...data
      }
    }
  }

  case types.SET_FILTER: 
  {
    return {
      ...state, 
      filter: action.payload
    }
  }

  case types.RESET_FILTER: 
  {
    return {
      ...state, 
      filter: {
        ...initialState.filter
      }
    }
  }

  case types.SET_SORT:
  {
    return {
      ...state,
      sort: action.payload
    }
  }

  default:

    return state

  }

}