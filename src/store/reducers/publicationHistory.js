import types from '../constants/publicationHistory'
import authTypes from '../constants/auth'
import _ from 'lodash'

const initialState = {
  list: {},
  edit: {},
  count: 0,
  page: 1,
  validation: {
    publicationName: {
      value: '',
      isValid: undefined,
    },
    image: {
      value: '',
      isValid: undefined
    }
  },
  filter: {
    publicationName: '',
  },
  isGetFetching: false,
  isGetIdFetching: false
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.PUBLICATION_HISTORY_GET:
  {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.PUBLICATION_HISTORY_GET_SUCCESS:
  {
    const { data, count, page } = action.payload
    return {
      ...state,
      list: _.keyBy(data, 'id'),
      page,
      count,
      isGetFetching: false
    }
  }

  case types.PUBLICATION_HISTORY_GET_ID: {
    return {
      ...state,
      isGetIdFetching: true
    }
  }

  case types.PUBLICATION_HISTORY_GET_ID_SUCCESS:
  {
    const payload = action.payload
    return {
      ...state,
      edit: {
        ...payload
      },
      isGetIdFetching: false
    }
  }

  case types.PUBLICATION_HISTORY_CREATE:
  {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.PUBLICATION_HISTORY_CREATE_SUCCESS:
  {
    return {
      ...state,
      isGetFetching: false
    }
  }

  case types.PUBLICATION_HISTORY_REMOVE_SUCCESS:
  {
    const newPublicationHisroyList = _.omitBy(state.list, publication => publication.id === action.payload.id)
    return {
      ...state,
      list: newPublicationHisroyList
    }
  }

  case types.PUBLICATION_HISTORY_EDIT:
  {
    return {
      ...state,
      edit: {
        ...state.edit,
        ...action.payload
      }
    }
  }

  case types.PUBLICATION_HISTORY_SET_FILTER:
  {
    return {
      ...state,
      filter: {
        ...state.filter,
        ...action.payload
      }
    }
  }

  case types.PUBLICATION_HISTORY_RESET_FILTER:
  {
    return {
      ...state,
      filter: {
        ...state.filter,
        publicationName: ''
      }
    }
  }

  case types.VALIDATION:
  {
    const data = action.payload.data

    return {
      ...state,
      validation: {
        ...state.validation,
        ...data
      }
    }
  }

  case types.PUBLICATION_HISTORY_GET_FAILURE:
  case types.PUBLICATION_HISTORY_REMOVE_FAILURE:
  case types.PUBLICATION_HISTORY_GET_ID_FAILURE:
  case types.PUBLICATION_HISTORY_UPDATE_FAILURE:
  case types.PUBLICATION_HISTORY_GET_USERS_FAILURE:
  case types.PUBLICATION_HISTORY_CREATE_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false,
      isGetIdFetching: false
    }
  }

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }

  default:
    return state
  }
}
