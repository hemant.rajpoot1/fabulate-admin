import types from '../constants/xeroContact'

const initialState = {
  contacts: [],
  filter: {}
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.XERO_CONTACTS_FILTER:
    return {
      ...state,
      filter: action.payload,
    }

  case types.XERO_CONTACTS:
    return {
      ...state,
      filter: action.payload
    }

  case types.XERO_CONTACTS_SUCCESS:
    return {
      ...state,
      contacts: action.payload.data,
    }

  case types.XERO_CONTACTS_FAILURE:
    return {
      ...state
    }

  default:
    return state
  }

}
