import types from '../constants/skills'
import authTypes from '../constants/auth'
import _ from 'lodash'

const initialState = {
  edit: {},  
  validation: {
    skillName: {
      value: '',
      isValid: undefined,
    }      
  },
  filter: {
    skillName: '',
  } 
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.SKILLS_GET_SUCCESS:
  { 
    return {
      ...state,
      skills: action.payload
    }
  }

  case types.SKILLS_REMOVE_SUCCESS:
  {
    let newSkills = {...state.skills}
    newSkills = _.omitBy(newSkills, skill => skill.id === action.payload.id)

    return {
      ...state,
      skills: {...newSkills}
    }
  }

  case types.SKILLS_GET_ID_SUCCESS:  
  {
    const payload = action.payload
    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      edit: { 
        ...payload 
      }
    }
  } 

  case types.SKILLS_EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload }
    }
  }

  case types.SKILLS_SET_FILTER:
  {
    return {
      ...state,
      filter: action.payload,
    }
  }

  case types.SKILLS_RESET_FILTER:
  {
    return {
      ...state,
      filter: {
        skillName: undefined,
      }
    }
  }

  case types.VALIDATION: 
  {
    const data = action.payload.data

    return {
      ...state,
      validation: {
        ...state.validation,
        ...data
      }
    }
  } 

  case types.SKILLS_GET_FAILURE:
  case types.SKILLS_REMOVE_FAILURE:
  case types.SKILLS_GET_ID_FAILURE:
  case types.SKILLS_UPDATE_FAILURE:
  case types.SKILLS_GET_USERS_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }  

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }  

  default:
    return state
  }
}
