import types from '../constants/contentCategory'
import _ from 'lodash'

const validationParams = {
  contentCategoryName: {
    value: '',
    isValid: undefined,
  },
  workroomType: {
    value: 0,
    isValid: true
  },
  contentCategorySequence: {
    value: null,
    isValid: undefined
  }
}

const initialState = {
  origin: {},
  filter: {},
  error: null,
  validationEdit: {
    ...validationParams
  },
  validationCreate: {
    ...validationParams
  },  
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.GET_SUCCESS:
  {
    return {
      ...state,
      origin: _.keyBy(action.payload.data, 'id'),
    }
  }
  case types.GET_ID_SUCCESS:
  {
    const payload = action.payload
    return {
      ...state,
      validationEdit: {
        contentCategoryName: {
          value: payload.contentCategoryName,
          isValid: true
        },
        contentCategorySequence: {
          value: payload.contentCategorySequence,
          isValid: true
        },
        workroomType: {
          value: payload.workroomType,
          isValid: true,
        }            
      }
    }
  }  

  case types.CREATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        },
      },
      validationCreate: {
        ...initialState.validationCreate
      }
    }
  }

  case types.UPDATE_SUCCESS:
  {
    return {
      ...state,
      origin: {
        ...state.origin,
        [action.payload.id]: {
          ...action.payload
        },
      },
    }
  }

  case types.REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }    
  
  case types.VALIDATION: 
  {
    const isCreate = action.payload.isCreate
    const subName = isCreate ? 'validationCreate' : 'validationEdit'
    const data = action.payload.data

    return {
      ...state,
      [subName]: {
        ...state[subName],
        ...data
      }
    }
  }

  default:
    return state
  }
}