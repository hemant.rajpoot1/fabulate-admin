import types from '../constants/settings'

const validationParams = {
  consumerKey: {
    value: null,
    isValid: undefined
  },
  consumerSecret: {
    value: null,
    isValid: undefined
  }
}

const initialState = {
  origin: {},
  error: null,
  validationEdit: {
    ...validationParams
  },
  isGetFetching: false
}


export default (state = initialState, action) => {

  switch (action.type) {
  case types.SETTINGS_GET: 
  {
    return {
      ...state,
      isGetFetching: true
    }
  }
  case types.SETTINGS_GET_SUCCESS:
  {
    const { payload } = action
    return {
      ...state,
      origin: {
        ...payload,
      },
      validationEdit: {
        consumerKey: {
          value: payload.consumerKey,
          isValid: true
        },
        consumerSecret: {
          value: payload.consumerSecret,
          isValid: true
        }
      },
      isGetFetching: false
    }
  }
  case types.SETTINGS_GET_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false
    }
  }
  case types.SETTINGS_UPDATE: 
  {
    return {
      ...state,
      isGetFetching: true
    }
  }
  case types.SETTINGS_UPDATE_SUCCESS:
  {
    const { payload } = action
    return {
      ...state,
      origin: {
        ...payload,
      },
      validationEdit: {
        consumerKey: {
          value: payload.consumerKey,
          isValid: true
        },
        consumerSecret: {
          value: payload.consumerSecret,
          isValid: true
        }
      },
      isGetFetching: false
    }
  }
  case types.SETTINGS_UPDATE_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false
    }
  }
  case types.VALIDATION:
  {
    return {
      ...state,
      validationEdit: {
        ...state.validationEdit,
        ...action.payload.data
      }
    }
  }
  default: {
    return {
      ...state
    }
  }
  }
}
