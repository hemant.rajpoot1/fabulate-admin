import types from '../constants/task'
import _ from 'lodash'

const initialState = {
  origin: {},
  error: null,
  edit: {},
  validation: {
    taskName: {
      value: '',
      isValid: undefined,
    },
    causeName: {
      value: '',
      isValid: undefined,
    },
    userName: {
      value: '',
      isValid: undefined,
    },        
  },
  search: {
    selected: {
      user: {},
      cause: {}
    },
    cause: [],
    user: []
  }     
}

export default (state = initialState, action) => {

  switch (action.type) {

  case types.TASK_GET_SUCCESS:
  { 
    return {
      ...state,
      origin:_.keyBy(action.payload, 'id'), 
    }
  }

  case types.TASK_REMOVE_SUCCESS:
  {
    const newOrigin = {...state.origin}
    delete newOrigin[action.payload.id]

    return {
      ...state,
      origin: {...newOrigin}
    }
  }

  case types.TASK_GET_ID_SUCCESS:  
  {
    const payload = action.payload
    return {
      ...state,
      origin: {
        ...state.origin, 
        [payload.id]: payload
      },
      edit: { 
        id: payload.data.id, 
        taskName: payload.data.taskName,
      },
      search: {
        selected: {
          cause: {
            id: payload.data.causeTask.id,
            name: payload.data.causeTask.causeName                                 
          },
          user: {
            id: payload.data.userTask.id,
            name: payload.data.userTask.userName            
          }
        }
      },
      validation: {
        taskName: {
          value: payload.data.taskName || '',
          isValid: payload.data.taskName ? undefined : false,
        },
        causeName: {
          value: payload.data.userTask.userName || '',
          isValid: payload.data.userTask.userName ? undefined : false,
        },
        userName: {
          value: payload.data.causeTask.causeName  || '',
          isValid: payload.data.causeTask.causeName  ? undefined : false,
        },        
      },        
    }
  } 

  case types.TASK_EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload }
    }
  }

  case types.VALIDATION:

  {

    return {
      ...state,
      validation: { 
        ...state.validation, 
        ...action.payload 
      }
    }

  }  

  case types.TASK_SEARCH_SUCCESS:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        ...payload 
      }
    }
  }    
  
  case types.TASK_SEARCH_SELECT:  
  {
    const payload = action.payload

    return {
      ...state,
      search: { 
        ...state.search,
        selected: {
          ...state.search.selected,
          ...payload 
        }
      }
    }
  }
  
  case types.TASK_SEARCH_SELECT_RESET:  
  {

    return {
      ...state,
      validation: {
        ...initialState.validation,
      },      
      search: { 
        ...state.search,
        selected: {
          ...initialState.search.selected,
        }
      }
    }
  }   

  case types.TASK_GET_FAILURE:
  case types.TASK_REMOVE_FAILURE:
  case types.TASK_GET_ID_FAILURE:
  case types.TASK_UPDATE_FAILURE:
  case types.TASK_GET_USERS_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
    }
  }  

  default:
    return state
  }
}
