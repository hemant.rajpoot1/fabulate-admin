import types from 'store/constants/amplifyCause'
import _ from 'lodash'

const validationParams = {
  status: {
    value: '',
    isValid: undefined
  },
  campaignDeadline: {
    value: '',
    isValid: undefined
  },
  campaignStart: {
    value: '',
    isValid: undefined
  },
  createdAt: {
    value: '',
    isValid: undefined
  },
  updatedAt: {
    value: '',
    isValid: undefined
  },
  products: {
    value: null,
    isValid: undefined
  },
  owner: {
    valueId: '',
    value: '',
    isValid: undefined
  }
}

const initialState = {
  origin: {},
  error: null,
  voriginCreate: {},
  filter: {
    amplifyCauseName: null
  },
  isGetFetching: false,
  validationEdit: {
    ...validationParams
  }
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.GET_SUCCESS:
  {
    const { payload } = action

    return {
      ...state, isGetFetching: false, origin: payload 
    }
  }

  case types.GET_ID_SUCCESS:
  {
    const { payload } = action
    return {
      ...state, validationEdit: {
        status: {
          value: payload.status,
          isValid: true
        },
        campaignDeadline: {
          value: payload.campaignDeadline,
          isValid: true
        },
        campaignStart: {
          value: payload.campaignStart,
          isValid: true
        },
        createdAt: {
          value: payload.createdAt,
          isValid: true
        },
        updatedAt: {
          value: payload.updatedAt,
          isValid: true
        },
        products: {
          value: payload.amplifyCauseInfo.products,
          isValid: true
        },
        owner: {
          valueId: payload.amplifyBuyerCause.id,
          value: payload.amplifyBuyerCause.userName,
          isValid: true 
        }
      }
    }
  }

  case types.CHANGE_FORM_AMPLIFY_CAUSE:
  {
    const { payload: { isValid, type, value } } = action
    return{
      ...state, validationEdit: { ...state.validationEdit, [type]: { ...state.validationEdit[type], value, isValid} }
    }
  }

  case types.SET_FILTER: 
  {
    return {
      ...state, filter: {
        ...state.filter, ...action.payload
      }, isGetFetching: true
    }
  }

  case types.RESET_FILTER: 
  {
    return {
      ...state, filter: {
      ...initialState.filter
      }
    }
  }

  case types.UPDATE_SUCCESS:
  
  case types.GET_FAILURE:
  case types.GET_ID_FAILURE:
  case types.UPDATE_FAILURE:

  default:
    return state
  }

}
