import types from 'store/constants/activeUserSessions'

const initialState = {
  origin: [],
  filter: {
    orderBy: 'createdAt'
  },
  sort: {},
  page: 1,
  count: 0,
  isGetFetching: false
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.GET:
  {
    return {
      ...state,
      isGetFetching: true,
      page: action.payload.page
    }
  }

  case types.GET_SUCCESS:
  {
    return {
      ...state,
      origin: action.payload.rows,
      count: action.payload.count,
      isGetFetching: false
    }
  }

  case types.SET_FILTER:
  {
    return {
      ...state,
      filter: action.payload
    }
  }

  case types.RESET_FILTER:
  {
    return {
      ...state,
      filter: {
        orderBy: 'createdAt'
      },
      page: 1
    }
  }

  case types.SET_PAGE:
  {
    return {
      ...state,
      page: action.payload
    }
  }

  default:
    return {
      ...state
    }
  }
}