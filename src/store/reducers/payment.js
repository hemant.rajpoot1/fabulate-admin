import types from '../constants/payment'
import authTypes from '../constants/auth'
import _ from 'lodash'

//validation model
const validationParams = {
  id: {
    value: '',
    isValid: undefined,
  },
  causeId: {
    value: '',
    isValid: undefined,
  },  
  contractorId: {
    value: '',
    isValid: undefined,
  },
  buyerId: {
    value: '',
    isValid: undefined,
  },  
  summary: {
    value: '',
    isValid: undefined,
  },
  commission: {
    value: '',
    isValid: undefined,
  },
  taskInfo: {
    value: {},
    isValid: undefined,
  }
}

//flags for check loaded data
const fetching = {
  isGetFetching: false,  
  isGetIdFetching: false,
}

const initialState = {
  list: {
    origin: {},
    page: 0,
    isAll: false
  },  
  error: null,
  validationEdit: {   
    ...validationParams                                               
  },
  info: {},
  ...fetching
}

export default (state = initialState, action) => {

  switch (action.type) {
  case types.PAYMENT_GET:
  { 
    return {
      ...state,
      isGetFetching: true
    }    
  }

  case types.PAYMENT_GET_SUCCESS:
  {
    const isAll = action.payload.data.length < 20
    return {
      ...state,
      list: {
        origin: {
          ...state.list.origin,
          ..._.keyBy(action.payload.data, 'id')
        },
        page: state.list.page + 1,
        isAll
      },
      isGetFetching: false
    }
  }

  case types.PAYMENT_GET_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false      
    }
  } 
  
  case types.PAYMENT_GET_ID: {
    return {
      ...state,
      validationEdit: {
        ...initialState.validationEdit
      },
      info: {},
      isGetIdFetching: true       
    }
  }

  case types.PAYMENT_GET_ID_SUCCESS:  
  {
  
    const payload = action.payload

    //data coercion to a validation object
    const toArr = Object.entries(validationParams)
    const nextValidModel = toArr.reduce((obj, item) => {
      const key = item[0]
      obj[key] = {
        value: payload[key],
        isValid: true
      }

      return obj
    }, {})
    return {
      ...state,
      validationEdit: {
        id: {
          value: payload.id
        },
        ...nextValidModel
      },
      info: {
        causePayment: {
          ...payload.causePayment
        },
        buyerPayment: {
          ...payload.buyerPayment
        },
        contractorPayment: {
          ...payload.contractorPayment
        }
      },
      isGetIdFetching: false
    }
  } 

  case types.PAYMENT_GET_ID_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetIdFetching: false
    }
  } 
  
  case types.PAYMENT_UPDATE_SUCCESS:  
  {
  
    const payload = action.payload

    return {
      ...state,
      list: {
        ...state.list,
        origin: {
          ...state.list.origin,
          [payload.id]: {
            ...state.list.origin[payload.id],
            ...payload
          }
        }
      }
    }
  }   
  
  case types.VALIDATION:

  {
    return {
      ...state,
      validationEdit: { 
        ...state.validationEdit, 
        ...action.payload.data 
      }
    }

  }   

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }  

  default:
    return state
  }
}
