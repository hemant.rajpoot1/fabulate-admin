import types from '../constants/user'
import authTypes from '../constants/auth'
import _ from 'lodash'

//flags for check loaded data
const fetching = {
  isGetFetching: false,  
  isGetIdFetching: false,
  isCreateFetching: false
}

const initialState = {
  origin: {},
  list: {
    origin: {},
    page: 1,
    isAll: false
  },   
  error: null,
  edit: {
    isEdit: false,
  },
  selected: {
    roles: {},
    amplifyRoles: {},
  },    
  filter: {
    userName: undefined,
    // isDeleted: undefined,
    // isInvited: undefined,
    workspaceId: undefined,
    search: undefined,
  },
  validation: {
    userName: {
      value: '',
      isValid: undefined,
    },
    phoneNumber: {
      value: '',
      isValid: undefined,
    },
    email: {
      value: '',
      isValid: undefined,
    },
    password: {
      value: '',
      isValid: undefined,
    }, 
    commission: {
      value: 10,
      isValid: undefined,
    }, 
    xeroId: {
      value: null,
      isValid: undefined,
    },                
  },
  ...fetching
}

const roles = {
  0: {
    id: 0,
    name: 'Super admin'
  },
  1: {
    id: 1,
    name: 'Admin'
  },
  2: {
    id: 2,
    name: 'Buyer'
  },
  3: {
    id: 3,
    name: 'Contractor'
  },
  4: {
    id: 4,
    name: 'Editor'
  },
  5: {
    id: 5,
    name: 'Guest'
  },
  6: {
    id: 6,
    name: 'Amplify',
  },
}

export default (state = initialState, action) => {
  switch (action.type) {
  case types.GET:
  {
    return {
      ...state,
      isGetFetching: true
    } 
  }

  case types.GET_SUCCESS:
  {
    return {
      ...state,
      origin: _.keyBy(action.payload.data, 'id'),
      list: {
        origin: _.keyBy(action.payload.data, 'id'),
        page: action.payload.page,
        count: action.payload.count
      },  
      isGetFetching: false
    }
  }

  case types.GET_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isGetFetching: false      
    }
  }
  
  case types.CREATE:
  {
    return {
      ...state,
      isCreateFetching: true
    }
  } 

  case types.CREATE_SUCCESS:
  {
    const payload = action.payload

    return {
      ...state,
      origin: {
        ...state.origin,
        [payload.id]: {
          ...payload
        }
      },
      list: {
        ...state.list,
        origin: {
          ...state.origin, 
          [payload.id]: payload
        },
      },
      isCreateFetching: false     
    }
  }  

  case types.REMOVE_SUCCESS:
  {
    const payload = action.payload
    const newOrigin = {...state.list.origin}
    delete newOrigin[payload.id]
    return {
      ...state,
      list: {
        ...state.list,
        origin: {...newOrigin}
      }
    }    
  }

  case types.UPDATE_SUCCESS:
  {
    const payload = action.payload
    return {
      ...state,
      origin: {
        ...state.origin,
        [payload.id]: {
          ...payload
        }
      },
      list: {
        ...state.list,
        origin: {
          ...state.list.origin,
          [payload.id]: {
            ...state.list.origin[payload.id],
            ...payload
          }
        },
      },      
    }
  }  

  case types.GET_ID:
  {
    return {
      ...state,
      isGetFetching: true
    }
  }

  case types.GET_ID_SUCCESS:
  {
    const payload = action.payload
    return {
      ...state,
      isGetFetching: false,
      origin: {...state.origin, ...{[payload.id]: payload}},
      list: {
        ...state.list,
        origin: {
          ...state.list.origin,
          [payload.id]: {
            ...state.list.origin[payload.id],
            ...payload
          }
        },
      },      
      edit: { 
        ...state.edit,
        ...payload },
      selected: {
        roles: {
          ...roles[payload.permissions]
        }
      }
    }
  }

  case types.EDIT:
  {
    return {
      ...state,
      edit: { ...state.edit, ...action.payload }
    }
  }

  case types.USER_SET_IS_EDIT: {
    return {
      ...state,
      edit: {
        ...state.edit,
        isEdit: action.payload,
      }
    }
  }


  case types.SET_FILTER:
  {
    return {
      ...state,
      list: {
        ...initialState.list
      },
      filter: {
        ...state.filter,
        ...action.payload
      }
    }
  }

  case types.RESET_FILTER:
  {
    return {
      ...state,
      filter: { ...initialState.filter }
    }
  }

  case types.VALIDATION:
  {
    return {
      ...state,
      validation: { 
        ...state.validation, 
        ...action.payload 
      }
    }
  }

  case types.USER_SEARCH_SELECT:  
  {
    const payload = action.payload
    return {
      ...state,
      selected: {
        ...state.selected,
        ...payload 
      }      
    }
  }
  
  case types.USER_SEARCH_SELECT_RESET:  
  {
    return {
      ...state,
      validation: {
        ...initialState.validation,
      },
      selected: {
        ...initialState.selected,
      }
    }
  }    

  case types.UPDATE_USER_BY_SOCKET: 
  {
    const payload = action.payload
    let nextUsers = {
      origin: {
        ...state.origin
      },            
    }
    let nextUsersList = {
      origin: {
        ...state.list.origin
      },            
    }    

    if(state.origin[payload.id]) {
      nextUsers = {
        ...nextUsers,
        origin: {
          ...state.origin,
          [payload.id]: {
            ...state.origin[payload.id],
            ...payload
          }
        },
      }
    }

    if(state.list.origin[payload.id]) {
      nextUsersList = {
        origin: {
          ...state.list.origin,
          [payload.id]: {
            ...state.list.origin[payload.id],
            ...payload
          }
        },
      }
    }    
    
    return {
      ...state,
      ...nextUsers,
      ...nextUsersList          
    }
  }  

  case types.REMOVE_FAILURE:
  case types.GET_ID_FAILURE:
  case types.UPDATE_FAILURE:
  case types.CREATE_FAILURE:
  {
    return {
      ...state,
      error: action.payload,
      isCreateFetching: false
    }
  }

  case authTypes.SIGNOUT_SUCCESS:
  {
    return {
      ...initialState,
    }
  }  

  default:
    return state
  }
}
