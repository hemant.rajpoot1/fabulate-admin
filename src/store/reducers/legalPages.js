/* eslint-disable no-fallthrough */
import types from 'store/constants/legalPages'

const initialStateValidation = {
  validation: {
    version: {
      value: '',
      isValid: undefined
    },
    forceUpdate: {
      value: false,
      isValid: undefined
    },
    termLink: {
      value: '',
      isValid: undefined,
    },
    privacyLink: {
      value: '',
      isValid: undefined
    },
    learnMoreLink: {
      value: '',
      isValid: undefined
    },
    activeDate: {
      value: '',
      isValid: undefined
    }
  },
}

const initialState = {
  origin: [],
  editOrigin: {},
  ...initialStateValidation, 
  isGetFetching: false
}

export default (state = initialState, action) => {
  switch (action.type) {

  case types.GET: 
  {
    return { ...state, isGetFetching: true }
  }

  case types.GET_SUCCESS: 
  {
    const { data } = action.payload
    return { ...state, isGetFetching: false, origin: data}
  }

  case types.CREATE_SUCCESS:
  {
    return { ...state, validation:{ ...initialStateValidation.validation}}
  }

  case types.CREATE_FAILURE:

  case types.CHANGE_FORM_LEGAL_PAGES:
  {
    const { payload: { isValid, type, value } } = action
    return{
      ...state, validation: { ...state.validation, [type]: { ...state.validation[type], value, isValid} }
    }
  }
  
  case types.GET_ID_SUCCESS:
  {
    const { version, forceUpdate, termsLink, privacyLink, learnMoreLink, activeDate } = action.payload
    
    return { ...state,
      editOrigin: action.payload,
      validation: {
        ...state.validation, 
        version: {
          ...state.validation.version, value: version, isValid: true
        },
        forceUpdate: {
          ...state.validation.forceUpdate, value: forceUpdate, isValid: true
        },
        termLink: {
          ...state.validation.termLink, value: termsLink, isValid: true 
        },
        privacyLink: {
          ...state.validation.privacyLink, value: privacyLink, isValid: true
        },
        learnMoreLink: {
          ...state.validation.learnMoreLink, value: learnMoreLink, isValid: true
        },
        activeDate: {
          ...state.validation.activeDate, value: activeDate, isValid: true
        }
      }}
  }

  case types.CLEAR_VALIDATION: 
  {
    return {
      ...state, validation: {
        ...state.validation, 
        version: {
          ...state.validation.version, value: '', isValid: true
        },
        forceUpdate: {
          ...state.validation.forceUpdate, value: false, isValid: true
        },
        termLink: {
          ...state.validation.termLink, value: '', isValid: true 
        },
        privacyLink: {
          ...state.validation.privacyLink, value: '', isValid: true
        },
        learnMoreLink: {
          ...state.validation.learnMoreLink, value: '', isValid: true
        },
        activeDate: {
          ...state.validation.activeDate, value: '', isValid: true
        }
      }
    }
  }

  case types.REMOVE_FAILURE:
  case types.GET_ID_FAILURE:
  case types.UPDATE_FAILURE:
    
  default:

    return state

  }

}