import { createSelector } from 'reselect'
import _ from 'lodash'

export const getCauseValidation = (state) => {
  const isExistCause = _.get(state, 'router.location.pathname', null) !== '/create-cause'
  const subStore = isExistCause ? 'validationEdit' : 'validationCreate'
  return _.get(state, `cause.${subStore}`, {})
}
export const getProducts = (state) => _.get(state, 'product.origin', {})

export const getFilteredProducts = createSelector(
  [getProducts, getCauseValidation],
  (products, validation) => {
    const filteredProducts = _.reduce(products, (obj, productItem) => {
      if (_.includes(productItem.genre, validation.genre.value)
        && _.includes(productItem.contentCategory, validation.workType.value)
        && productItem.productDisplayName === validation.product.value) {
        return [...obj, {...productItem}]
      } else {
        return obj
      }
    }, [])
    return filteredProducts || {}
  }
)