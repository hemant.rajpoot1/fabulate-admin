import { createSelector } from 'reselect'
import _ from 'lodash'

export const getGenreValidation = (state) => {
  const isExistCause = _.get(state, 'router.location.pathname', null) !== '/create-cause'
  const subStore = isExistCause ? 'validationEdit' : 'validationCreate'
  return _.get(state, `cause.${subStore}.genre.value`, null)
}
export const getContentCategories = (state) => _.get(state, 'contentCategory.origin', {})
export const getProducts = (state) => _.get(state, 'product.origin', {})

export const getFilteredContentCategories = createSelector(
  [getProducts, getContentCategories, getGenreValidation],
  (products, contentCategories, genre) => {
    const filteredProductsByGenre = _.filter(products, productItem => _.includes(productItem.genre, genre))
    const filteredContentCategories = _.filter(contentCategories, contentCategory => {
      if (_.size(filteredProductsByGenre)) {
        const hasSelectedGenre =  _.some(filteredProductsByGenre, productItem => 
          _.includes(productItem.contentCategory, contentCategory.id)
        )
        return hasSelectedGenre
      } else return false
    })
    return filteredContentCategories
  }
) 
