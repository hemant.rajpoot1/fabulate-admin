import { createSelector } from 'reselect'
import _ from 'lodash'

export const getPitchList = (state) => _.get(state, 'causeUser.list.origin', {})
export const getPitchFilter = (state) => _.get(state, 'causeUser.filter', {})

export const getPitchListbyFilter = createSelector(
  [getPitchList, getPitchFilter],
  (list, filter) => {

    const nextPitchList = _.reduce(list, (obj, listItem) => {
      const isStar = !_.isNil(filter.isStar) 
        ? filter.isStar === listItem.isStar 
        : true 
      const isHide = !_.isNil(filter.isHide) 
        ? filter.isHide === listItem.isHide 
        : true

      if(isStar && isHide) {
        return {
          ...obj,
          [listItem.id]: {
            ...listItem
          }
        }
      }

      return obj
    }, {})

    return nextPitchList
  }
)