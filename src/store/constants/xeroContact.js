export default {

  XERO_CONTACTS:         'SERVER:XERO_CONTACTS',
  XERO_CONTACTS_SUCCESS: 'XERO_CONTACTS_SUCCESS',
  XERO_CONTACTS_FAILURE: 'XERO_CONTACTS_FAILURE',

  XERO_CONTACTS_FILTER:  'XERO_CONTACTS_FILTER'

}
