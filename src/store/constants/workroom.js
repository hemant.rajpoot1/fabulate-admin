export default {

  WORKROOM_GET_ID:                 'SERVER:WORKROOM_GET_ID',
  WORKROOM_GET_ID_SUCCESS:         'WORKROOM_GET_ID_SUCCESS',
  WORKROOM_GET_ID_FAILURE:         'WORKROOM_GET_ID_FAILURE',

  WORKROOM_UPDATE:                 'SERVER:WORKROOM_UPDATE',
  WORKROOM_UPDATE_SUCCESS:         'WORKROOM_UPDATE_SUCCESS',
  WORKROOM_UPDATE_FAILURE:         'WORKROOM_UPDATE_FAILURE',

  WORKROOM_EDIT:                   'WORKROOM_EDIT',
  VALIDATION:                      'WORKROOM_VALIDATION',
}
