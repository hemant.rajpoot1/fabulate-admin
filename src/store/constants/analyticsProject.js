export default {
  ANALYTICS_PROJECT_GET: 'SERVER:ANALYTICS_PROJECT_GET',
  ANALYTICS_PROJECT_GET_SUCCESS: 'ANALYTICS_PROJECT_GET_SUCCESS',
  ANALYTICS_PROJECT_GET_FAILURE: 'ANALYTICS_PROJECT_GET_FAILURE',

  ANALYTICS_PROJECT_SEARCH: 'SERVER:ANALYTICS_PROJECT_SEARCH',
  ANALYTICS_PROJECT_SEARCH_SUCCESS: 'ANALYTICS_PROJECT_SEARCH_SUCCESS',
  ANALYTICS_PROJECT_SEARCH_FAILURE: 'ANALYTICS_PROJECT_SEARCH_FAILURE',

  ANALYTICS_PROJECT_SEARCH_SELECT: 'ANALYTICS_PROJECT_SEARCH_SELECT',
  ANALYTICS_PROJECT_SEARCH_SELECT_RESET: 'ANALYTICS_PROJECT_SEARCH_SELECT_RESET',

  VALIDATION: 'ANALYTICS_PROJECT_VALIDATION',

  ANALYTICS_PROJECT_CREATE: 'SERVER:ANALYTICS_PROJECT_CREATE',
  ANALYTICS_PROJECT_CREATE_SUCCESS: 'ANALYTICS_PROJECT_CREATE_SUCCESS',
  ANALYTICS_PROJECT_CREATE_FAILURE: 'ANALYTICS_PROJECT_CREATE_FAILURE',

  ANALYTICS_PROJECT_REMOVE: 'SERVER:ANALYTICS_PROJECT_REMOVE',
  ANALYTICS_PROJECT_REMOVE_SUCCESS: 'ANALYTICS_PROJECT_REMOVE_SUCCESS',
  ANALYTICS_PROJECT_REMOVE_FAILURE: 'ANALYTICS_PROJECT_REMOVE_FAILURE',

  ANALYTICS_PROJECT_GET_ID: 'SERVER:ANALYTICS_PROJECT_GET_ID',
  ANALYTICS_PROJECT_GET_ID_SUCCESS: 'ANALYTICS_PROJECT_GET_ID_SUCCESS',
  ANALYTICS_PROJECT_GET_ID_FAILURE: 'ANALYTICS_PROJECT_GET_ID_FAILURE',

  ANALYTICS_PROJECT_UPDATE: 'SERVER:ANALYTICS_PROJECT_UPDATE',
  ANALYTICS_PROJECT_UPDATE_SUCCESS: 'ANALYTICS_PROJECT_UPDATE_SUCCESS',
  ANALYTICS_PROJECT_UPDATE_FAILURE: 'ANALYTICS_PROJECT_UPDATE_FAILURE',

  ANALYTICS_PROJECT_CLEAR_VALIDATION_AND_SEARCH: 'ANALYTICS_PROJECT_CLEAR_VALIDATION_AND_SEARCH',

  ANALYTICS_PROJECT_SET_FILTER:     'ANALYTICS_PROJECT_SET_FILTER',
  ANALYTICS_PROJECT_RESET_FILTER:   'ANALYTICS_PROJECT_RESET_FILTER',

  ANALYTICS_PROJECT_UPDATE_DATASETS: 'SERVER:ANALYTICS_PROJECT_UPDATE_DATASETS',
  ANALYTICS_PROJECT_UPDATE_DATASETS_SUCCESS: 'ANALYTICS_PROJECT_UPDATE_DATASETS_SUCCESS',
  ANALYTICS_PROJECT_UPDATE_DATASETS_FAILURE: 'ANALYTICS_PROJECT_UPDATE_DATASETS_FAILURE'
}