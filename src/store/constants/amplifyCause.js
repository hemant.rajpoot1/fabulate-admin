export default {
  GET:                                      'SERVER:AMPLIFY_CAUSE_GET',
  GET_SUCCESS:                              'AMPLIFY_CAUSE_GET_SUCCESS',
  GET_FAILURE:                              'AMPLIFY_CAUSE_GET_FAILURE',
    
  GET_ID:                                   'SERVER:AMPLIFY_CAUSE_GET_ID',
  GET_ID_SUCCESS:                           'AMPLIFY_CAUSE_GET_ID_SUCCESS',
  GET_ID_FAILURE:                           'AMPLIFY_CAUSE_GET_ID_FAILURE',
  
  UPDATE:                                   'SERVER:AMPLIFY_CAUSE_UPDATE',
  UPDATE_SUCCESS:                           'AMPLIFY_CAUSE_UPDATE_SUCCESS',
  UPDATE_FAILURE:                           'AMPLIFY_CAUSE_UPDATE_FAILURE',
  
  CHANGE_FORM_AMPLIFY_CAUSE:                'CHANGE_FORM_AMPLIFY_CAUSE',

  SET_FILTER:                               'AMPLIFY_CAUSE_SET_FILTER',
  RESET_FILTER:                             'AMPLIFY_CAUSE_RESET_FILTER'
}
  