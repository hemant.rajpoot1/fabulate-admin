export default {

  TASK_GET:         'SERVER:TASK_GET',
  TASK_GET_SUCCESS: 'TASK_GET_SUCCESS',
  TASK_GET_FAILURE: 'TASK_GET_FAILURE',

  TASK_REMOVE:         'SERVER:TASK_REMOVE',
  TASK_REMOVE_SUCCESS: 'TASK_REMOVE_SUCCESS',
  TASK_REMOVE_FAILURE: 'TASK_REMOVE_FAILURE',

  TASK_GET_ID:         'SERVER:TASK_GET_ID',
  TASK_GET_ID_SUCCESS: 'TASK_GET_ID_SUCCESS',
  TASK_GET_ID_FAILURE: 'TASK_GET_ID_FAILURE',

  TASK_EDIT: 'TASK_EDIT',
  VALIDATION: 'TASK_VALIDATION',

  TASK_UPDATE:         'SERVER:TASK_UPDATE',
  TASK_UPDATE_SUCCESS: 'TASK_UPDATE_SUCCESS',
  TASK_UPDATE_FAILURE: 'TASK_UPDATE_FAILURE',
  
  TASK_CREATE:         'SERVER:TASK_CREATE',
  TASK_CREATE_SUCCESS: 'TASK_CREATE_SUCCESS',
  TASK_CREATE_FAILURE: 'TASK_CREATE_FAILURE',

  TASK_SEARCH:         'SERVER:TASK_SEARCH',
  TASK_SEARCH_SUCCESS: 'TASK_SEARCH_SUCCESS',
  TASK_SEARCH_FAILURE: 'TASK_SEARCH_FAILURE', 

  TASK_SEARCH_SELECT: 'TASK_SEARCH_SELECT',
  TASK_SEARCH_SELECT_RESET: 'TASK_SEARCH_SELECT_RESET',
}
