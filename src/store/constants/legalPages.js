export default {
  GET:                                'SERVER:GET_LEGAL_PAGES',
  GET_SUCCESS:                        'GET_LEGAL_PAGES_SUCCESS',
  GET_FAILURE:                        'GET_LEGAL_PAGES_FAILURE',

  GET_ID:                             'SERVER:GET_ID_LEGAL_PAGE',
  GET_ID_SUCCESS:                     'GET_ID_LEGAL_PAGE_SUCCESS',
  GET_ID_FAILURE:                     'GET_ID_LEGAL_PAGE_FAILURE',

  UPDATE:                             'SERVER:UPDATE_ID_LEGAL_PAGE',
  UPDATE_SUCCESS:                     'UPDATE_ID_LEGAL_PAGE_SUCCESS',
  UPDATE_FAILURE:                     'UPDATE_ID_LEGAL_PAGE_FAILURE',
  
  CREATE:                             'SERVER:CREATE_LEGAL_PAGES',
  CREATE_SUCCESS:                     'CREATE_LEGAL_PAGES_SUCCESS',
  CREATE_FAILURE:                     'CREATE_LEGAL_PAGES_FAILURE',

  REMOVE:                             'SERVER:REMOVE_LEGAL_PAGES_ID',
  REMOVE_SUCCESS:                     'REMOVE_LEGAL_PAGES_ID_SUCCESS',
  REMOVE_FAILURE:                     'REMOVE_LEGAL_PAGES_ID_FAILURE',

  CHANGE_FORM_LEGAL_PAGES:            'CHANGE_FORM_LEGAL_PAGES',
  
  CLEAR_VALIDATION:                   'CLEAR_VALIDATION' 
}