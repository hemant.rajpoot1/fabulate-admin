import { createStore, combineReducers, applyMiddleware } from 'redux'
import createHistory from 'history/createBrowserHistory'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'

import {
  pendingTasksReducer, // The redux reducer
} from 'react-redux-spinner'

import { localeReducer } from 'react-localize-redux'

import initSaga from  './sagas/init'
import authSaga from  './sagas/auth'
import userSaga from './sagas/user'
import workspaceSaga from './sagas/workspace'
import workspaceUserSaga from './sagas/workspaceUser'
import modelSaga from './sagas/model'
import causeSaga from './sagas/cause'
import taskSaga from './sagas/task'
import assetSaga from './sagas/asset'
import templateSaga from './sagas/template'
import productSaga from './sagas/product'
import contentCategory from './sagas/contentCategory'
import causeUserSaga from './sagas/causeUser'
import paymentSaga from './sagas/payment'
import workroomSaga from './sagas/workroom'
import skillsSaga from './sagas/skills'
import xeroContactSaga from './sagas/xeroContact'
import gettySaga from './sagas/gettyLimit'
import settingsSaga from './sagas/settings'
import genreSaga from './sagas/genre'
import workspaceProductCardSaga from './sagas/workspaceProductCard'
import analyticsTagTemplateSaga from './sagas/analyticsTagTemplate'
import publicationHistorySaga from './sagas/publicationHistory'
import analyticsProjectSaga from './sagas/analyticsProject'
import amplifyWorkspaceProductSaga from './sagas/amplifyWorkspaceProduct'
import amplifyProductSaga from './sagas/amplifyProduct'
import legalPagesSaga from './sagas/legalPages'
import activeUserSessionsSaga from './sagas/activeUserSessions'
import pastUserSessionsSaga from './sagas/pastUserSessions'
import amplifyCauseSaga from './sagas/amplifyCause'

import initReducers   from './reducers/init'
import authReducers   from './reducers/auth'
import userReducers   from './reducers/user'
import workspaceReducers   from './reducers/workspace'
import causeReducers   from './reducers/cause'
import workspaceUserReducers   from './reducers/workspaceUser'
import signInReducers   from './reducers/signIn'
import signUpReducers   from './reducers/signUp'
import modelReducers   from './reducers/model'
import taskReducers   from './reducers/task'
import assetReducers   from './reducers/asset'
import templateReducers   from './reducers/template'
import productReducers   from './reducers/product'
import contentCategoryReducers   from './reducers/contentCategory'
import causeUserReducers   from './reducers/causeUser'
import paymentReducers   from './reducers/payment'
import workroomReducers   from './reducers/workroom'
import skillsReducers from './reducers/skills'
import xeroContactReducers from './reducers/xeroContact'
import gettyReduser from './reducers/gettyLimit'
import settingsReducers from './reducers/settings'
import genreReducers from './reducers/genre'
import workspaceProductCardReducers from './reducers/workspaceProductCard'
import analyticsTagTemplateReducers from './reducers/analyticsTagTemplate'
import publicationHistoryReducers from './reducers/publicationHistory'
import analyticsProjectReducers from './reducers/analyticsProject'
import amplifyWorkspaceProductReducers from './reducers/amplifyWorkspaceProduct'
import amplifyProductReducers from './reducers/amplifyProduct'
import legalPages from './reducers/legalPages'
import activeUserActionsReducers from './reducers/activeUserSessions'
import pastUserActionsReducers from './reducers/pastUserSessions'
import amplifyCause from './reducers/amplifyCause'

const sagaMiddleware = createSagaMiddleware()

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory()

// Build the middleware for intercepting and dispatching navigation actions
let middleware = [
  sagaMiddleware,
  routerMiddleware(history)
]

let appReducer = combineReducers({
  pendingTasks:  pendingTasksReducer,
  init:          initReducers,
  auth:          authReducers,
  user:          userReducers,
  workspace:     workspaceReducers,
  workspaceUser: workspaceUserReducers,
  router:        routerReducer,
  locale:        localeReducer,
  signIn:        signInReducers,
  signUp:        signUpReducers,
  model:         modelReducers,
  cause:         causeReducers,
  task:          taskReducers,
  asset:         assetReducers,
  template:      templateReducers,
  product:       productReducers,
  causeUser:     causeUserReducers,
  payment:       paymentReducers,
  workroom:      workroomReducers,
  contentCategory:contentCategoryReducers,
  skill:          skillsReducers,
  xeroContact:    xeroContactReducers,
  settings:       settingsReducers,
  getty:          gettyReduser,
  genre:          genreReducers,
  workspaceProductCard: workspaceProductCardReducers,
  analyticsTagTemplate: analyticsTagTemplateReducers,
  publicationHistory: publicationHistoryReducers,
  analyticsProject: analyticsProjectReducers,
  amplifyWorkspaceProduct: amplifyWorkspaceProductReducers,
  amplifyProduct: amplifyProductReducers,
  legalPages: legalPages,
  activeUserSessions: activeUserActionsReducers,
  pastUserSessions: pastUserActionsReducers,
  amplifyCause: amplifyCause
})

let store = applyMiddleware(...middleware)(createStore)(
  appReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)


sagaMiddleware.run(initSaga)
sagaMiddleware.run(authSaga.signInSaga)
sagaMiddleware.run(authSaga.signUpSaga)
sagaMiddleware.run(authSaga.resetPasswordSaga)
sagaMiddleware.run(authSaga.updatePasswordSaga)
sagaMiddleware.run(authSaga.verifyUserMailSaga)
sagaMiddleware.run(authSaga.updateUserInfoSaga)
sagaMiddleware.run(authSaga.uploadUserPhotoSaga)
sagaMiddleware.run(authSaga.updateUserPasswordSaga)

sagaMiddleware.run(userSaga.usersListSaga)
sagaMiddleware.run(workspaceSaga.workspaceSaga)
sagaMiddleware.run(causeSaga.causeSaga)
sagaMiddleware.run(workspaceUserSaga.workspaceUserSaga)
sagaMiddleware.run(modelSaga.modelSaga)
sagaMiddleware.run(taskSaga.taskSaga)
sagaMiddleware.run(assetSaga.assetSaga)
sagaMiddleware.run(templateSaga.templateSaga)
sagaMiddleware.run(productSaga.productSaga)
sagaMiddleware.run(causeUserSaga.causeUserSaga)
sagaMiddleware.run(paymentSaga.paymentSaga)
sagaMiddleware.run(workroomSaga.workroomSaga)
sagaMiddleware.run(contentCategory.contentCategorySaga)
sagaMiddleware.run(skillsSaga.skillsSaga)
sagaMiddleware.run(xeroContactSaga.xeroContactSaga)
sagaMiddleware.run(gettySaga.gettySaga)
sagaMiddleware.run(settingsSaga.settingsSaga)
sagaMiddleware.run(genreSaga.genreSaga)
sagaMiddleware.run(workspaceProductCardSaga.workspaceProductCardSaga)
sagaMiddleware.run(analyticsTagTemplateSaga.analyticsTagTemplateSaga)
sagaMiddleware.run(publicationHistorySaga.publicationHistorySaga)
sagaMiddleware.run(analyticsProjectSaga.analyticsProjectSaga)
sagaMiddleware.run(amplifyWorkspaceProductSaga.amplifyWorkspaceProductSaga)
sagaMiddleware.run(amplifyProductSaga.amplifyProductSaga)
sagaMiddleware.run(legalPagesSaga.legalPagesSaga)
sagaMiddleware.run(activeUserSessionsSaga.activeUserSessionsSaga)
sagaMiddleware.run(pastUserSessionsSaga.pastUserSessionsSaga)
sagaMiddleware.run(amplifyCauseSaga.amplifyCauseSaga)

export {
  store,
  history,
}
