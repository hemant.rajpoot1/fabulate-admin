import types from '../constants/amplifyWorkspaceProduct'
import { validation } from 'helpers/validation'

export const get = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_GET,
  payload: data
})

export const getSuccess = ({ data, count }, page) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_GET_SUCCESS,
  payload: {
    data,
    count,
    page
  }
})

export const getFailure = (error) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_GET_SUCCESS,
  payload: error
})

export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const create = (params) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_CREATE_FAILURE,
  payload: error
})

export const remove = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE,
  payload: data
})

export const removeSuccess = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_GET_ID_FAILURE,
  payload: error
})

export const update = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.AMPLIFY_WORKSPACE_PRODUCT_UPDATE_FAILURE,
  payload: error
})

export const setFilter = (data) => ({
  type: types.SET_FILTER,
  payload: data
})

export const resetFilter = () => ({
  type: types.RESET_FILTER
})
