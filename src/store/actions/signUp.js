import types from '../constants/signUp'
import { validation } from 'helpers/validation'

export const setParams = (data) => ({
  type: types.SET_PARAMS,
  payload: data,
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)

  return {
    type: types.VALIDATION,
    payload: {[type]: {...newModel}},
  }
}

