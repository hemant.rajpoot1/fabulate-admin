import types from '../constants/payment'
import { validation } from 'helpers/validation'

export const get = (queryParams, page) => ({
  type: types.PAYMENT_GET,
  payload: {
    queryParams,
    page
  }
})

export const getSuccess = ({ data }, isNull) => ({
  type: types.PAYMENT_GET_SUCCESS,
  payload: {
    data,
    isNull
  }
})

export const getFailure = (error) => ({
  type: types.PAYMENT_GET_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.PAYMENT_GET_ID,
  payload: id
})

export const getIdSuccess = ({data}) => ({
  type: types.PAYMENT_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.PAYMENT_GET_ID_FAILURE,
  payload: error
})

export const update = (data) => ({
  type: types.PAYMENT_UPDATE,
  payload: {
    ...data,
  }
})

export const updateSuccess = (data) => ({
  type: types.PAYMENT_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.PAYMENT_UPDATE_FAILURE,
  payload: error
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      }
    }
  }
}