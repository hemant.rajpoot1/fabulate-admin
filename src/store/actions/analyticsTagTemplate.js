import types from '../constants/analyticsTagTemplate'
import { validation } from 'helpers/validation'

export const get = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_GET,
  payload: { ...data }
})

export const getSuccess = ({ data, count }, page) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_GET_SUCCESS,
  payload: {
    data,
    count,
    page
  }
})

export const getFailure = (error) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_GET_SUCCESS,
  payload: error
})

export const search = (type, query) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_SEARCH,
  payload: {
    type, query
  }
})

export const searchSuccess = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_SEARCH_FAILURE,
  payload: error
})

export const select = data => ({
  type: types.ANALYTICS_TAG_TEMPLATE_SEARCH_SELECT,
  payload: { ...data }
})

export const resetSelect = () => ({
  type: types.ANALYTICS_TAG_TEMPLATE_SEARCH_SELECT_RESET,
})

export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const create = (params) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_CREATE_FAILURE,
  payload: error
})

export const remove = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_REMOVE,
  payload: data
})

export const removeSuccess = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_GET_ID_FAILURE,
  payload: error
})

export const update = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_UPDATE_FAILURE,
  payload: error
})


export const clearValidationSearch = () => ({
  type: types.ANALYTICS_TAG_TEMPLATE_CLEAR_VALIDATION_AND_SEARCH,
})

export const setValidationWorkspaces = (data) => ({
  type: types.ANALYTICS_TAG_TEMPLATE_SET_VALIDATION_WORKSPACES,
  payload: data
})