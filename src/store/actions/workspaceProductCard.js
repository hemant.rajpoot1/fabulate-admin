import types from '../constants/workspaceProductCard'
import { validation } from 'helpers/validation'

export const get = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_GET,
  payload: data
})

export const getSuccess = ({ data, count }, page) => ({
  type: types.WORKSPACE_PRODUCT_CARD_GET_SUCCESS,
  payload: {
    data,
    count,
    page
  }
})

export const getFailure = (error) => ({
  type: types.WORKSPACE_PRODUCT_CARD_GET_FAILURE,
  payload: error
})

export const create = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_CREATE,
  payload: data
})

export const createSuccess = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.WORKSPACE_PRODUCT_CARD_CREATE_FAILURE,
  payload: error
})


export const remove = (id) => ({
  type: types.WORKSPACE_PRODUCT_CARD_REMOVE,
  payload: id
})

export const removeSuccess = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.WORKSPACE_PRODUCT_CARD_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.WORKSPACE_PRODUCT_CARD_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.WORKSPACE_PRODUCT_CARD_GET_ID_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_EDIT,
  payload: data,
})

export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const setValidProduct = (model, rules, type, productCardId) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      newModel: newModel,
      productCardId: productCardId,
      type: type,
    }
  }
}

export const setIsEdit = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_SET_IS_EDIT,
  payload: data,
})

export const update = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.WORKSPACE_PRODUCT_CARD_UPDATE_FAILURE,
  payload: error
})

export const setPage = (pageNumber) => ({
  type: types.SET_PAGE,
  payload: pageNumber
})

export const search = (type, query) => ({
  type: types.WORKSPACE_PRODUCT_CARD_SEARCH,
  payload: {
    type, query
  }
})

export const searchSuccess = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.WORKSPACE_PRODUCT_CARD_SEARCH_FAILURE,
  payload: error
})

export const setFilter = (data) => {
  return {
    type: types.SET_FILTER,
    payload: data,
  }
}

export const resetFilter = () => {
  return {
    type: types.RESET_FILTER,
    payload: null,
  }
}

export const download = (data) => ({
  type: types.WORKSPACE_PRODUCT_CARD_DOWNLOAD,
  payload: { ...data }
})

export const downloadSuccess = () => ({
  type: types.WORKSPACE_PRODUCT_CARD_DOWNLOAD_SUCCESS,
})

export const downloadFailure = () => ({
  type: types.WORKSPACE_PRODUCT_CARD_DOWNLOAD_FAILURE,
})

export const setSort = data => ({
  type: types.SET_SORT,
  payload: data
})
