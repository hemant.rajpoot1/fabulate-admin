import types from '../constants/genre'
import { validation } from 'helpers/validation'

export const get = (query) => ({
  type: types.GENRE_GET,
  payload: query
})

export const getSuccess = ({ data, count, page }) => ({
  type: types.GENRE_GET_SUCCESS,
  payload: {
    data,
    count,
    page
  }
})

export const getFailure = (error) => ({
  type: types.GENRE_GET_FAILURE,
  payload: error
})

export const create = (params) => ({
  type: types.GENRE_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.GENRE_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.GENRE_CREATE_FAILURE,
  payload: error
})


export const remove = (id) => ({
  type: types.GENRE_REMOVE,
  payload: id
})

export const removeSuccess = (data) => ({
  type: types.GENRE_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.GENRE_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.GENRE_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.GENRE_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.GENRE_GET_ID_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.GENRE_EDIT,
  payload: data,
})

export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const update = (data) => ({
  type: types.GENRE_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.GENRE_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.GENRE_UPDATE_FAILURE,
  payload: error
})

export const setFilter = (data) => ({
  type: types.SET_FILTER,
  payload: data
})

export const resetFilter = () => ({
  type: types.RESET_FILTER
})