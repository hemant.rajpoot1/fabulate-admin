import types from '../constants/xeroContact'

export const getXeroContact = (filter) => ({
  type: types.XERO_CONTACTS,
  payload: filter
})

export const getXeroContactSuccess = data => ({
  type: types.XERO_CONTACTS_SUCCESS,
  payload: data
})

export const getXeroContactFailure = (err) => ({
  type: types.XERO_CONTACTS_FAILURE,
  payload: err
})

export const setXeroFilter = (filter) => ({
  type: types.XERO_CONTACTS_FILTER,
  payload: filter
})

