import types from 'store/constants/amplifyCause'

export const get = (data) => ({
  type: types.GET,
  payload: data  
})

export const getSuccess = (data) => ({
  type: types.GET_SUCCESS,
  payload: data
})

export const getFailure = (data) => ({
  type: types.GET_FAILURE
})

export const getId = (data) => ({
  type: types.GET_ID,
  payload: data
})

export const getIdSuccess = (data) => ({
  type: types.GET_ID_SUCCESS,
  payload: data
}) 

export const getIdFailure = () => ({
  type: types.GET_ID_FAILURE
})

export const update = (id,data) => ({
  type: types.UPDATE,
  payload: {
    id,
    data
  }
})

export const updateSuccess = (data) => ({
  type: types.UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = () => ({
  type: types.UPDATE_FAILURE
})

export const change = (data) => ({
  type: types.CHANGE_FORM_AMPLIFY_CAUSE,
  payload: data
})

export const setFilter = (data) => ({
  type: types.SET_FILTER,
  payload: data
})

export const resetFilter = () => ({
  type: types.RESET_FILTER
})
