import types from '../constants/signIn'
import { validation } from 'helpers/validation'


export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)

  return {
    type: types.VALIDATION,
    payload: {[type]: {...newModel}},
  }
}

