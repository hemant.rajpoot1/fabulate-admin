import types from '../constants/task'
import { validation } from 'helpers/validation'

export const get = () => ({
  type: types.TASK_GET,
  payload: null
})

export const getSuccess = ({ data }) => ({
  type: types.TASK_GET_SUCCESS,
  payload: data
})

export const getFailure = (error) => ({
  type: types.TASK_GET_FAILURE,
  payload: error
})

export const create = (params) => ({
  type: types.TASK_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.TASK_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.TASK_CREATE_FAILURE,
  payload: error
})

export const select = select => ({
  type: types.TASK_SEARCH_SELECT,
  payload: {...select}
})

export const resetSelect = () => ({
  type: types.TASK_SEARCH_SELECT_RESET,
})

export const search = (type, query) => ({
  type: types.TASK_SEARCH,
  payload: {
    type, query
  }
})

export const searchSuccess = (data) => ({
  type: types.TASK_SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.TASK_SEARCH_FAILURE,
  payload: error
})

export const remove = (id) => ({
  type: types.TASK_REMOVE,
  payload: id
})

export const removeSuccess = (data) => ({
  type: types.TASK_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.TASK_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.TASK_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.TASK_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.TASK_GET_ID_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.TASK_EDIT,
  payload: data,
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)

  return {
    type: types.VALIDATION,
    payload: {[type]: {...newModel}},
  }
}

export const update = (data) => ({
  type: types.TASK_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.TASK_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.TASK_UPDATE_FAILURE,
  payload: error
})
