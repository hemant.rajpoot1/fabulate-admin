import types from '../constants/settings'
import { validation } from 'helpers/validation'

export const get = () => ({
  type: types.SETTINGS_GET
})

export const getSuccess = data => ({
  type: types.SETTINGS_GET_SUCCESS,
  payload: {
    ...data
  }
})

export const getFailure = error => ({
  type: types.SETTINGS_GET_FAILURE,
  payload: error
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      }
    },
  }
}

export const update = (data, type) => ({
  type: types.SETTINGS_UPDATE,
  payload: { 
    data,
    type
  }
})
  
export const updateSuccess = data => ({
  type: types.SETTINGS_UPDATE_SUCCESS,
  payload: { ...data }
})
  
export const updateFailure = (error) => ({
  type: types.SETTINGS_UPDATE_FAILURE,
  payload: error
})