import types from '../constants/user'
import { validation } from 'helpers/validation'

export const get = (queryParams, page) => {
  return {
    type: types.GET,
    payload: {
      queryParams,
      page
    }
  }
}

export const getSuccess = ({data, count}, page) => {
  return {
    type: types.GET_SUCCESS,
    payload: {
      data,
      count,
      page
    }
  }
}

export const getFailure = (error) => {
  return {
    type: types.GET_FAILURE,
    payload: error
  }
}

export const create = (params) => {
  return {
    type: types.CREATE,
    payload: {
      params
    }
  }
}

export const createSuccess = (data) => {
  return {
    type: types.CREATE_SUCCESS,
    payload: data
  }
}

export const createFailure = (error) => {
  return {
    type: types.CREATE_FAILURE,
    payload: error
  }
}

export const remove = (id) => {
  return {
    type: types.REMOVE,
    payload: id
  }
}

export const removeSuccess = (data) => {
  return {
    type: types.REMOVE_SUCCESS,
    payload: data
  }
}

export const removeFailure = (error) => {
  return {
    type: types.REMOVE_FAILURE,
    payload: error
  }
}

export const getId = (id) => {
  return {
    type: types.GET_ID,
    payload: id
  }
}

export const getIdSuccess = (data) => {
  return {
    type: types.GET_ID_SUCCESS,
    payload: data
  }
}

export const getIdFailure = (error) => {
  return {
    type: types.GET_ID_FAILURE,
    payload: error
  }
}

export const edit = (data) => {
  return {
    type: types.EDIT,
    payload: data,
  }
}

export const setFilter = (data) => {
  return {
    type: types.SET_FILTER,
    payload: data,
  }
}

export const resetFilter = (data) => {
  return {
    type: types.RESET_FILTER,
    payload: data,
  }
}

export const select = select => ({
  type: types.USER_SEARCH_SELECT,
  payload: { ...select }
})

export const resetSelect = () => ({
  type: types.USER_SEARCH_SELECT_RESET,
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: { [type]: { ...newModel } },
  }
}

export const update = (data) => {
  return {
    type: types.UPDATE,
    payload: data
  }
}

export const updateSuccess = (data) => {
  return {
    type: types.UPDATE_SUCCESS,
    payload: data
  }
}

export const updateFailure = (error) => {
  return {
    type: types.UPDATE_FAILURE,
    payload: error
  }
}

export const updateUserBySocket = data => ({
  type: types.UPDATE_USER_BY_SOCKET,
  payload: data
})

export const loginAsAnotherUser = (userLogin) => ({
  type: types.USER_LOGIN_AS_ANOTHER_USER,
  payload: userLogin,
})

export const setIsEdit = (data) => ({
  type: types.USER_SET_IS_EDIT,
  payload: data,
})
