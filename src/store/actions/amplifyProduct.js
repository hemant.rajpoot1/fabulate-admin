import types from '../constants/amplifyProduct'
import { validation } from 'helpers/validation'

export const get = (queryParams) => ({
  type: types.GET,
  payload: {
    ...queryParams
  }
})

export const getSuccess = ({ data }) => ({
  type: types.GET_SUCCESS,
  payload: {
    data
  }
})

export const getFailure = (error) => ({
  type: types.GET_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.GET_ID_SUCCESS,
  payload: {
    data
  }
})

export const getIdFailure = (error) => ({
  type: types.GET_ID_FAILURE,
  payload: error
})

export const remove = (id) => ({
  type: types.REMOVE,
  payload: id
})

export const removeSuccess = (data) => ({
  type: types.REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.REMOVE_FAILURE,
  payload: error
})

export const create = (data) => ({
  type: types.CREATE,
  payload: { ...data }
})

export const createSuccess = (data) => ({
  type: types.CREATE_SUCCESS,
  payload: { ...data }
})

export const createFailure = (error) => ({
  type: types.CREATE_FAILURE,
  payload: error
})


export const update = (data) => ({
  type: types.UPDATE,
  payload: {
    ...data
  }
})

export const updateSuccess = (data) => ({
  type: types.UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.UPDATE_FAILURE,
  payload: error
})

export const search = (type, query) => ({
  type: types.SEARCH,
  payload: {
    type, query
  }
})

export const searchSuccess = (data) => ({
  type: types.SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.SEARCH_FAILURE,
  payload: error
})

export const select = (selected, isCreate) => ({
  type: types.SEARCH_SELECT,
  payload: {
    selected,
    isCreate
  }
})

export const resetSelect = (isCreate) => ({
  type: types.SEARCH_SELECT_RESET,
  payload: {
    isCreate
  }
})


export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const setFilter = (data) => ({
  type: types.SET_FILTER,
  payload: data
})

export const resetFilter = () => ({
  type: types.RESET_FILTER
})

export const setSort = (data) => ({
  type: types.SET_SORT,
  payload: data
})

export const attachFile = (params, isCreate, isAttachments) => {
  return {
    type: types.ATTACH,
    payload: {
      params,
      isCreate,
      isAttachments
    }
  }
}

export const removeAttach = (params, isCreate, isAttachments) => {
  return {
    type: types.REMOVE_FILE,
    payload: {
      params,
      isCreate,
      isAttachments
    }
  }
}

export const uploadFile = (params, isCreate, isAttachments) => {
  return {
    type: types.UPLOAD,
    payload: {
      params,
      isCreate,
      isAttachments
    }
  }
}

export const uploadFileSuccess = (data, preview, isCreate) => {

  return {
    type: types.UPLOAD_SUCCESS,
    payload: {
      data,
      preview,
      isCreate
    }
  }

}


export const uploadFileFailure = (error, preview, isCreate) => {

  return {
    type: types.UPLOAD_FAILURE,
    payload: {
      error,
      preview,
      isCreate
    }
  }

}

export const editAttach = (params, isCreate) => {
  return {
    type: types.EDIT_FILE,
    payload: {
      params,
      isCreate
    }
  }
}

export const editAttachSuccess = (params, isCreate) => {
  return {
    type: types.EDIT_FILE_SUCCESS,
    payload: {
      params, 
      isCreate
    }
  }
}

export const editAttachFailure = (params, isCreate) => {
  return {
    type: types.EDIT_FILE_FAILURE,
    payload: {
      params,
      isCreate
    }
  }
}

export const downloadFile = (id, ref) => ({
  type: types.DOWNLOAD_FILE,
  payload: {
    id,
    ref
  }
})

export const downloadFileSuccess = (data) => ({
  type: types.DOWNLOAD_FILE_SUCCESS,
  payload: data
})

export const downloadFileFailure = (error) => ({
  type: types.DOWNLOAD_FILE_FAILURE,
  payload: error
})

