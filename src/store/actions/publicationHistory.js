import types from '../constants/publicationHistory'
import { validation } from 'helpers/validation'

export const get = (query) => ({
  type: types.PUBLICATION_HISTORY_GET,
  payload: query
})

export const getSuccess = (data) => ({
  type: types.PUBLICATION_HISTORY_GET_SUCCESS,
  payload: data
})

export const getFailure = (error) => ({
  type: types.PUBLICATION_HISTORY_GET_FAILURE,
  payload: error
})

export const create = (params) => ({
  type: types.PUBLICATION_HISTORY_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.PUBLICATION_HISTORY_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.PUBLICATION_HISTORY_CREATE_FAILURE,
  payload: error
})


export const remove = (id) => ({
  type: types.PUBLICATION_HISTORY_REMOVE,
  payload: id
})

export const removeSuccess = (data) => ({
  type: types.PUBLICATION_HISTORY_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.PUBLICATION_HISTORY_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.PUBLICATION_HISTORY_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.PUBLICATION_HISTORY_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.PUBLICATION_HISTORY_GET_ID_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.PUBLICATION_HISTORY_EDIT,
  payload: data,
})

export const setFilter = (data) => ({
  type: types.PUBLICATION_HISTORY_SET_FILTER,
  payload: data,
})

export const resetFilter = () => ({
  type: types.PUBLICATION_HISTORY_RESET_FILTER,
})

export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const update = (data) => ({
  type: types.PUBLICATION_HISTORY_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.PUBLICATION_HISTORY_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.PUBLICATION_HISTORY_UPDATE_FAILURE,
  payload: error
})
