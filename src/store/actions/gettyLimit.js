import types from '../constants/gettyImage'

export const setGettyLimit = (limit) => ({
  type: types.SET_GETTY_LIMIT,
  payload: limit,
})

export const getGettyLimit = () => ({
  type: types.GET_GETTY_LIMIT,
})

export const saveGettyLimit = (limit) => ({
  type: types.SAVE_GETTY_LIMIT,
  payload: limit,
})