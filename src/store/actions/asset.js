import types from '../constants/asset'
import { validation } from 'helpers/validation'

export const get = (queryParams) => {

  return {
    type: types.GET,
    payload: {
      queryParams
    }
  }

}

export const getSuccess = ({ data }) => {

  return {
    type: types.GET_SUCCESS,
    payload: data
  }

}

export const getFailure = (error) => {

  return {
    type: types.GET_FAILURE,
    payload: error
  }

}

export const getId = (id) => ({
  type: types.GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.GET_ID_FAILURE,
  payload: error
})

export const downloadFile = (id, ref) => ({
  type: types.DOWNLOAD_FILE,
  payload: {
    id,
    ref
  }
})

export const downloadFileSuccess = (data) => ({
  type: types.DOWNLOAD_FILE_SUCCESS,
  payload: data
})

export const downloadFileFailure = (error) => ({
  type: types.DOWNLOAD_FILE_FAILURE,
  payload: error
})

export const create = (params) => {
  return {
    type: types.CREATE,
    payload: {
      params
    }
  }

}

export const createSuccess = (data) => {

  return {
    type: types.CREATE_SUCCESS,
    payload: data
  }

}


export const createFailure = (error) => {

  return {
    type: types.CREATE_FAILURE,
    payload: error
  }

}

export const attachFile = (params) => {
  return {
    type: types.ATTACH,
    payload: {
      params
    }
  }
}

export const removeAttach = (params) => {
  return {
    type: types.REMOVE_FILE,
    payload: {
      params
    }
  }
}

export const editAttach = (params) => {
  return {
    type: types.EDIT_FILE,
    payload: {
      params
    }
  }
}

export const editAttachSuccess = (params) => {
  return {
    type: types.EDIT_FILE_SUCCESS,
    payload: {
      params
    }
  }
}

export const editAttachFailure = (params) => {
  return {
    type: types.EDIT_FILE_FAILURE,
    payload: {
      params
    }
  }
}

export const uploadFile = (params) => {
  return {
    type: types.UPLOAD,
    payload: {
      params
    }
  }
}

export const uploadFileSuccess = (data, preview) => {

  return {
    type: types.UPLOAD_SUCCESS,
    payload: {
      data,
      preview
    }
  }

}


export const uploadFileFailure = (error, preview) => {

  return {
    type: types.UPLOAD_FAILURE,
    payload: {
      error,
      preview
    }
  }

}

export const remove = (id) => {

  return {
    type: types.REMOVE,
    payload: id
  }

}

export const removeSuccess = (data) => {

  return {
    type: types.REMOVE_SUCCESS,
    payload: data
  }

}

export const removeFailure = (error) => {

  return {
    type: types.REMOVE_FAILURE,
    payload: error
  }

}

export const setFilter = (data) => {

  return {
    type: types.SET_FILTER,
    payload: data,
  }

}

export const resetFilter = (data) => {
  
  return {
    type: types.RESET_FILTER,
    payload: data,
  }

}

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)

  return {
    type: types.VALIDATION,
    payload: {[type]: { ...newModel }},
  }
}

export const select = select => ({
  type: types.ASSET_SEARCH_SELECT,
  payload: {...select}
})

export const resetSelect = () => ({
  type: types.ASSET_SEARCH_SELECT_RESET,
})

export const search = (type, query) => ({
  type: types.ASSET_SEARCH,
  payload: {
    type, query
  }
})

export const searchSuccess = (data) => ({
  type: types.ASSET_SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.ASSET_SEARCH_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.ASSET_EDIT,
  payload: data,
})

export const update = (data) => ({
  type: types.ASSET_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.ASSET_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.ASSET_UPDATE_FAILURE,
  payload: error
})
