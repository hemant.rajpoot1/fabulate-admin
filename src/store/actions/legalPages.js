import types from 'store/constants/legalPages'

export const get = () => ({
  type: types.GET,
  payload: {}
})

export const getSuccess = (data) => ({
  type: types.GET_SUCCESS,
  payload: {
    data
  }
})

export const getFailure = (err) => ({
  type: types.GET_FAILURE,
  payload: err
})

export const getId = (id) => ({
  type: types.GET_ID,
  payload: {
    id
  }
})

export const getIdSuccess = (data) => ({
  type: types.GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = () => ({
  type: types.GET_ID_FAILURE
})

export const create = (data) => ({
  type: types.CREATE,
  payload:{ 
    data
  }
})

export const createSuccess = (data) => ({
  type: types.CREATE_SUCCESS,
  payload: data
})

export const createFailure = (data) => ({
  type: types.CREATE_FAILURE,
  payload: {
    data
  }
})

export const update = (id,data) => ({
  type: types.UPDATE,
  payload: {
    id,
    data
  }
})

export const updateSuccess = (data) => ({
  type: types.UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = () => ({
  type: types.UPDATE_FAILURE
})

export const remove = (id) => ({
  type: types.REMOVE,
  payload: {
    id
  }
})

export const removeSuccess = () => ({
  type: types.REMOVE_SUCCESS
})

export const removeFailure = () => ({
  type: types.REMOVE_FAILURE
})

export const change = (data) => ({
  type: types.CHANGE_FORM_LEGAL_PAGES,
  payload: data
})

export const clearValidation = () => ({
  type: types.CLEAR_VALIDATION
})


