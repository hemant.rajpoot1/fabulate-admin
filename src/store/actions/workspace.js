import types from '../constants/workspace'
import { validation } from 'helpers/validation'

export const get = (data) => ({
  type: types.WORKSPACE_GET,
  payload: { ...data }
})

export const getSuccess = ({ data, count }, page) => ({
  type: types.WORKSPACE_GET_SUCCESS,
  payload: {
    data,
    count,
    page
  }
})

export const getFailure = (error) => ({
  type: types.WORKSPACE_GET_FAILURE,
  payload: error
})

export const create = (params) => ({
  type: types.WORKSPACE_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.WORKSPACE_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.WORKSPACE_CREATE_FAILURE,
  payload: error
})


export const remove = (data) => ({
  type: types.WORKSPACE_REMOVE,
  payload: data
})

export const removeSuccess = (data) => ({
  type: types.WORKSPACE_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.WORKSPACE_REMOVE_FAILURE,
  payload: error
})

export const getId = (id, withUsers) => ({
  type: types.WORKSPACE_GET_ID,
  payload: {id, withUsers}
})

export const getIdSuccess = (data) => ({
  type: types.WORKSPACE_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.WORKSPACE_GET_ID_FAILURE,
  payload: error
})

export const getUsersSuccess = (data) => ({
  type: types.WORKSPACE_GET_USERS_SUCCESS,
  payload: data,
})

export const getUsersFailure = (error) => ({
  type: types.WORKSPACE_GET_USERS_FAILURE,
  payload: error,
})

export const search = (type, query, amplifyProduct) => ({
  type: types.WORKSPACE_SEARCH,
  payload: {
    type, query, amplifyProduct
  }
})

export const searchSuccess = (data) => ({
  type: types.WORKSPACE_SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.WORKSPACE_SEARCH_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.WORKSPACE_EDIT,
  payload: data,
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)

  return {
    type: types.VALIDATION,
    payload: { [type]: { ...newModel } },
  }
}

export const setFilter = (data) => {
  return {
    type: types.SET_FILTER,
    payload: data,
  }
}

export const resetFilter = (data) => {
  return {
    type: types.RESET_FILTER,
    payload: data,
  }
}

export const update = (data) => ({
  type: types.WORKSPACE_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.WORKSPACE_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.WORKSPACE_UPDATE_FAILURE,
  payload: error
})

export const setPage = (pageNumber) => ({
  type: types.SET_PAGE,
  payload: pageNumber
})

export const setIsEdit = (data) => ({
  type: types.WORKSPACE_EDIT_SET_IS_EDIT,
  payload: data,
})

export const addUser = (data) => ({
  type: types.WORKSPACE_UPDATE_ADD_USER,
  payload: data,
})

export const addUserSuccess = (data) => ({
  type: types.WORKSPACE_UPDATE_ADD_USER_SUCCESS,
  payload: data,
})

export const addUserFailure = (error) => ({
  type: types.WORKSPACE_UPDATE_ADD_USER_FAILURE,
  payload: error,
})

export const removeUser = (data) => ({
  type: types.WORKSPACE_UPDATE_REMOVE_USER,
  payload: data,
})

export const removeUserSuccess = (data) => ({
  type: types.WORKSPACE_UPDATE_REMOVE_USER_SUCCESS,
  payload: data,
})

export const removeUserFailure = (error) => ({
  type: types.WORKSPACE_UPDATE_REMOVE_USER_FAILURE,
  payload: error,
})

export const attachFile = (params) => ({
  type: types.ATTACH,
  payload: {
    params
  }
})

export const removeAttach = (params) => ({
  type: types.REMOVE_FILE,
  payload: {
    params,
  },
})

export const uploadFile = (params) => ({
  type: types.UPLOAD,
  payload: {
    params,
  },
})

export const uploadFileSuccess = (data, preview) => ({
  type: types.UPLOAD_SUCCESS,
  payload: {
    data,
    preview,
  },
})

export const resetAttachments = () => ({
  type: types.RESET_ATTACHMENTS
})

export const searchAmplifyProductSuccess = data => ({
  type: types.WORKSPACE_SEARCH_AMPLIFY_PRODUCT_SUCCESS,
  payload: data
})

export const searchAmplifyProductFailure = () => ({
  type: types.WORKSPACE_SEARCH_AMPLIFY_PRODUCT_FAILURE
})