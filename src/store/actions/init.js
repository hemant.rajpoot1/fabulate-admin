import types from '../constants/init'

import {
  pendingTask, // The action key for modifying loading state
  begin, // The action value if a "long" running task begun
  end, // The action value if a "long" running task ended
} from 'react-redux-spinner'

export const init = () => {

  return {
    type: types.INIT,
    payload: null,
    [ pendingTask ]: begin,
  }

}

export const initAuth = () => {

  return {
    type: types.INIT_AUTH,
    payload: null,
  }

}

export const initSuccess = (appStatus) => {

  return {
    type: types.INIT_SUCCESS,
    payload: appStatus,
    [ pendingTask ]: end,
  }

}

export const initFailure = (err) => {

  return {
    type: types.INIT_FAILURE,
    payload: err,
    [ pendingTask ]: end,
  }

}
