import types from '../constants/workspaceUser'
import { validation } from 'helpers/validation'

export const get = (page) => ({
  type: types.WORKSPACE_USER_GET,
  payload: { ...page }
})

export const getSuccess = ({ data, count }, page) => ({
  type: types.WORKSPACE_USER_GET_SUCCESS,
  payload: {
    data,
    count,
    page
  }
})

export const getFailure = (error) => ({
  type: types.WORKSPACE_USER_GET_FAILURE,
  payload: error
})

export const create = (params) => ({
  type: types.WORKSPACE_USER_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.WORKSPACE_USER_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.WORKSPACE_USER_CREATE_FAILURE,
  payload: error
})

export const select = select => ({
  type: types.WORKSPACE_USER_SEARCH_SELECT,
  payload: { ...select }
})

export const resetSelect = () => ({
  type: types.WORKSPACE_USER_SEARCH_SELECT_RESET,
})

export const search = (type, query) => ({
  type: types.WORKSPACE_USER_SEARCH,
  payload: {
    type, query
  }
})

export const searchSuccess = (data) => ({
  type: types.WORKSPACE_USER_SEARCH_SUCCESS,
  payload: data
})

export const searchFailure = (error) => ({
  type: types.WORKSPACE_USER_SEARCH_FAILURE,
  payload: error
})

export const remove = (data) => ({
  type: types.WORKSPACE_USER_REMOVE,
  payload: data
})

export const removeSuccess = (data) => ({
  type: types.WORKSPACE_USER_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.WORKSPACE_USER_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.WORKSPACE_USER_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.WORKSPACE_USER_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.WORKSPACE_USER_GET_ID_FAILURE,
  payload: error
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)

  return {
    type: types.VALIDATION,
    payload: { [type]: { ...newModel } },
  }
}

export const setFilter = (data) => ({
  type: types.SET_FILTER,
  payload: data,
})

export const getSearchUsersOfWorkspace = (data) => ({
  type: types.SEARCH_USERS_OF_WORKSPACE,
  payload: data
})

export const getSearchUsersOfWorkspaceSuccess = (data) => ({
  type: types.SEARCH_USERS_OF_WORKSPACE_SUCCESS,
  payload: data
})

export const getSearchUsersOfWorkspaceFailure = (error) => ({
  type: types.SEARCH_USERS_OF_WORKSPACE_FAILURE,
  payload: error
})

export const resetFilter = (data) => ({
  type: types.RESET_FILTER,
  payload: data,
})


