import types from '../constants/workroom'
import { validation } from 'helpers/validation'

export const getId = (query) => ({
  type: types.WORKROOM_GET_ID,
  payload: query
})

export const getIdSuccess = ({data}) => ({
  type: types.WORKROOM_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.WORKROOM_GET_ID_FAILURE,
  payload: error
})

export const update = (data) => ({
  type: types.WORKROOM_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.WORKROOM_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.WORKROOM_UPDATE_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.WORKROOM_EDIT,
  payload: data,
})

export const setValid = (model, rules, type) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      }
    },
  }
}
