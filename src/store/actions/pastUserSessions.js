import types from 'store/constants/pastUserSessions'

export const get = (filter, page) => ({
  type: types.GET,
  payload: {
    ...filter,
    page
  }
})

export const getSuccess = (data) => ({
  type: types.GET_SUCCESS,
  payload: data,
})

export const getFailure = (err) => ({
  type: types.GET_FAILURE,
  payload: err
})

export const setFilter = (data) => ({
  type: types.SET_FILTER,
  payload: data
})

export const resetFilter = () => ({
  type: types.RESET_FILTER
})

export const setPage = (data) => ({
  type: types.SET_PAGE,
  payload: data
})