import types from '../constants/skills'
import { validation } from 'helpers/validation'

export const get = (data) => ({
  type: types.SKILLS_GET,
  payload: { ...data }
})

export const getSuccess = ({ data }) => ({
  type: types.SKILLS_GET_SUCCESS,
  payload: data
})

export const getFailure = (error) => ({
  type: types.SKILLS_GET_FAILURE,
  payload: error
})

export const create = (params) => ({
  type: types.SKILLS_CREATE,
  payload: {
    params
  }
})

export const createSuccess = (data) => ({
  type: types.SKILLS_CREATE_SUCCESS,
  payload: data
})

export const createFailure = (error) => ({
  type: types.SKILLS_CREATE_FAILURE,
  payload: error
})


export const remove = (id) => ({
  type: types.SKILLS_REMOVE,
  payload: id
})

export const removeSuccess = (data) => ({
  type: types.SKILLS_REMOVE_SUCCESS,
  payload: data
})

export const removeFailure = (error) => ({
  type: types.SKILLS_REMOVE_FAILURE,
  payload: error
})

export const getId = (id) => ({
  type: types.SKILLS_GET_ID,
  payload: id
})

export const getIdSuccess = (data) => ({
  type: types.SKILLS_GET_ID_SUCCESS,
  payload: data
})

export const getIdFailure = (error) => ({
  type: types.SKILLS_GET_ID_FAILURE,
  payload: error
})

export const edit = (data) => ({
  type: types.SKILLS_EDIT,
  payload: data,
})

export const setFilter = (data) => ({
  type: types.SKILLS_SET_FILTER,
  payload: data,
})

export const resetFilter = () => ({
  type: types.SKILLS_RESET_FILTER,
})

export const setValid = (model, rules, type, isCreate) => {
  const newModel = validation(model, rules)
  return {
    type: types.VALIDATION,
    payload: {
      data: {
        [type]: { ...newModel }
      },
      isCreate
    },
  }
}

export const update = (data) => ({
  type: types.SKILLS_UPDATE,
  payload: data
})

export const updateSuccess = (data) => ({
  type: types.SKILLS_UPDATE_SUCCESS,
  payload: data
})

export const updateFailure = (error) => ({
  type: types.SKILLS_UPDATE_FAILURE,
  payload: error
})
