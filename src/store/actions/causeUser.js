import types from '../constants/causeUser'

export const get = (queryParams) => {

  return {
    type: types.GET,
    payload: {
      queryParams
    }
  }

}

export const getSuccess = ({ data }, causeId) => {

  return {
    type: types.GET_SUCCESS,
    payload: {
      data,
      causeId
    }
  }

}

export const getFailure = (error) => {

  return {
    type: types.GET_FAILURE,
    payload: error
  }

}

export const getId = (id) => {

  return {
    type: types.GET_ID,
    payload: id
  }

}

export const getIdSuccess = (data) => {

  return {
    type: types.GET_ID_SUCCESS,
    payload: data
  }

}

export const getIdFailure = (error) => {

  return {
    type: types.GET_ID_FAILURE,
    payload: error
  }

}

export const create = (data, user, query) => {

  return {
    type: types.CREATE,
    payload: { 
      data,
      user,
      query
    }
  }

}

export const createSuccess = (data) => {

  return {
    type: types.CREATE_SUCCESS,
    payload: data
  }

}

export const createFailure = (error) => {

  return {
    type: types.CREATE_FAILURE,
    payload: error
  }

}

export const remove = (id) => {

  return {
    type: types.REMOVE,
    payload: id
  }

}

export const removeSuccess = (data) => {

  return {
    type: types.REMOVE_SUCCESS,
    payload: data
  }

}

export const removeFailure = (error) => {

  return {
    type: types.REMOVE_FAILURE,
    payload: error
  }

}

export const update = (data, user, index) => {

  return {
    type: types.UPDATE,
    payload: { 
      data,
      user,
      index
    }
  }

}

export const updateSuccess = (data) => {
  return {
    type: types.UPDATE_SUCCESS,
    payload: data
  }

}

export const updateFailure = (error) => {

  return {
    type: types.UPDATE_FAILURE,
    payload: error
  }

}

