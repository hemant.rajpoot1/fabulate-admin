export const getPrice = number => `$${number.toFixed(2)}`
